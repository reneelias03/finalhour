AdColony Android SDK
==================================
Modified: December 19, 2014  
SDK Version: 2.2.0

Important Changes:
----------------------------------
1. You must now place the armeabi folder in your project's libs folder.
2. You should include the VIBRATE permission in your AndroidManifest.xml to allow haptic-enabled videos.
3. Added support for haptic-enhanced videos.
4. Added support for IAP Promo Ads.
5. Minor bug fixes and stability improvements.  


To Download:
----------------------------------
The simplest way to obtain the AdColony Android SDK is to click the "Download ZIP" button located in the right-hand navigation pane of this page.



Contains:
----------------------------------
* Demo Apps
  * MultiZoneDemo
  * V4VCDemo
  * VideoInterstitialDemo
  * AdColonyInstantFeedAdsDemo
* Library
  * adcolony.jar
  * armeabi
* W-9 Form.pdf


Getting Started with AdColony:
----------------------------------
First time and returning users should review the [documentation](https://github.com/AdColony/AdColony-Android-SDK/wiki).


Sample Applications:
----------------------------------
Included are three sample apps to use as examples and for help on AdColony integration.


Legal Requirements:
----------------------------------
By downloading the AdColony SDK, you are granted a limited, non-commercial license to use and review the SDK solely for evaluation purposes.  If you wish to integrate the SDK into any commercial applications, you must register an account with AdColony and accept the terms and conditions on the AdColony website.

Note that U.S. based companies will need to complete the W-9 form and send it to us before publisher payments can be issued.


Contact Us:
----------------------------------
For more information, please visit AdColony.com. For questions or assistance, please email us at support@adcolony.com.

