package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import android.provider.CalendarContract;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;


/**
 * Created by rene__000 on 9/23/2014.
 */
public class ColorCustomization implements Screen {

    MyGdxGame game;
    GameButtons[] colorBoxes, selectButtons;
    GameButtons rightOuterButton, rightInnerButton, leftOuterButton, leftInnerButton, menuButton, basicButton, copyButton, pasteButton, nextMenuButton, defaultColorsButton;
    Sprite leftOuterCircle, leftHover, rightOuterCircle, rightHover, earthOceans, earthContinents, moonMinor, moonMajor, grayOverlay, grayOverlayMoon;
    Sprite[] backgroundSprite, joystickSprites, planetSprites;
    VolumeRocker redSlider, greenSlider, blueSlider;
    boolean[] activeButton;
    boolean basic, rockerActive, previousRockerActive, joysticksOnScreen, spritesMoving;
    int currentSelectedIndex;
    Color copiedColor;
    BitmapFont goodTimeFont;
    Texture emptyButtonTemplate;

    public ColorCustomization(MyGdxGame game) {
        this.game = game;
        this.goodTimeFont = game.goodTimeFont;
        goodTimeFont.setColor(Color.WHITE);
        emptyButtonTemplate = game.manager.get("data/EmptyButtonTemplate.png", Texture.class);
        Gdx.input.setCatchBackKey(true);

        colorBoxes = new GameButtons[game.colors.length];
        for (int i = 0; i < colorBoxes.length; i++) {
            colorBoxes[i] = new GameButtons(game.manager.get("data/ColorBox.png", Texture.class), true, new Vector2().setZero());
            colorBoxes[i].setColor(game.colors[i]);
            colorBoxes[i].setSize(colorBoxes[i].getWidth() * game.screenScale, colorBoxes[i].getHeight() * game.screenScale);
            colorBoxes[i].setAlpha(.95f);
            if (i == 0) {
                colorBoxes[i].setPosition(519 * game.screenScale, 200 * game.screenScale);
            } else if (i == colorBoxes.length / 2) {
                colorBoxes[i].setPosition(colorBoxes[0].getX(), colorBoxes[0].getY() - colorBoxes[i].getHeight());
            } else {
                colorBoxes[i].setPosition(colorBoxes[i - 1].getX() + colorBoxes[i].getWidth(), colorBoxes[i - 1].getY());
            }
        }

        menuButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        menuButton.setSize(menuButton.getWidth() * game.screenScale, menuButton.getHeight() * game.screenScale);
        menuButton.setOriginCenter();
        menuButton.setPosition(Gdx.graphics.getWidth() - menuButton.getWidth() * 1.15f, menuButton.getHeight() * 1.75f);

        basicButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        basicButton.setSize(basicButton.getWidth() * game.screenScale, basicButton.getHeight() * game.screenScale);
        basicButton.setOriginCenter();
        basicButton.setPosition(basicButton.getWidth() * .15f, basicButton.getHeight() * 1.75f);
        basicButton.setColor(.28f, .43f, .68f, 1);

        copyButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        copyButton.setSize(copyButton.getWidth() * game.screenScale, copyButton.getHeight() * game.screenScale);
        copyButton.setOriginCenter();
        copyButton.setPosition(basicButton.getX(), basicButton.getY() - basicButton.getHeight() * 1.25f);
        copyButton.setColor(.33f, .78f, .92f, 1);

        pasteButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        pasteButton.setSize(pasteButton.getWidth() * game.screenScale, pasteButton.getHeight() * game.screenScale);
        pasteButton.setOriginCenter();
        pasteButton.setPosition(menuButton.getX(), menuButton.getY() - menuButton.getHeight() * 1.25f);
        pasteButton.setColor(.33f, .78f, .92f, 1);

        basic = true;

        leftOuterCircle = new Sprite(game.manager.get("data/JoystickOuterWhite.png", Texture.class));
        leftOuterCircle.setSize(leftOuterCircle.getWidth() * 1.65f * game.screenScale, leftOuterCircle.getHeight() * 1.65f * game.screenScale);
        leftOuterCircle.setOriginCenter();
        leftOuterCircle.setPosition(Gdx.graphics.getWidth() / 4 - leftOuterCircle.getOriginX() / 4, Gdx.graphics.getHeight() / 2 + 50 * game.screenScale);
        leftOuterCircle.setColor(game.prefs.getFloat("LeftOuterColorR"), game.prefs.getFloat("LeftOuterColorG"), game.prefs.getFloat("LeftOuterColorB"), 1);

        leftHover = new Sprite(game.manager.get("data/JoystickHoverWhite.png", Texture.class));
        leftHover.setSize(leftHover.getWidth() * 1.55f * game.screenScale, leftHover.getHeight() * 1.55f * game.screenScale);
        leftHover.setOriginCenter();
        leftHover.setPosition(leftOuterCircle.getX() + leftOuterCircle.getOriginX() - leftHover.getOriginX(), leftOuterCircle.getY() + leftOuterCircle.getOriginY() - leftHover.getOriginY());
        leftHover.setColor(game.prefs.getFloat("LeftInnerColorR"), game.prefs.getFloat("LeftInnerColorG"), game.prefs.getFloat("LeftInnerColorB"), 1);

        rightOuterCircle = new Sprite(game.manager.get("data/JoystickOuterWhite.png", Texture.class));
        rightOuterCircle.setSize(rightOuterCircle.getWidth() * 1.65f * game.screenScale, rightOuterCircle.getHeight() * 1.65f * game.screenScale);
        rightOuterCircle.setOriginCenter();
        rightOuterCircle.setPosition(Gdx.graphics.getWidth() * 3 / 4 - rightOuterCircle.getOriginX() * 1.5f, Gdx.graphics.getHeight() / 2 + 50 * game.screenScale);
        rightOuterCircle.setColor(game.prefs.getFloat("RightOuterColorR"), game.prefs.getFloat("RightOuterColorG"), game.prefs.getFloat("RightOuterColorB"), 1);

        rightHover = new Sprite(game.manager.get("data/JoystickHoverWhite.png", Texture.class));
        rightHover.setSize(rightHover.getWidth() * 1.55f * game.screenScale, rightHover.getHeight() * 1.55f * game.screenScale);
        rightHover.setOriginCenter();
        rightHover.setPosition(rightOuterCircle.getX() + rightOuterCircle.getOriginX() - rightHover.getOriginX(), rightOuterCircle.getY() + rightOuterCircle.getOriginY() - rightHover.getOriginY());
        rightHover.setColor(game.prefs.getFloat("RightInnerColorR"), game.prefs.getFloat("RightInnerColorG"), game.prefs.getFloat("RightInnerColorB"), 1);

        joystickSprites = new Sprite[]{leftOuterCircle, leftHover, rightOuterCircle, rightHover};

        leftOuterButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        leftOuterButton.setSize(leftOuterButton.getWidth() * game.screenScale, leftOuterButton.getHeight() * game.screenScale);
        leftOuterButton.setOriginCenter();
        leftOuterButton.setPosition(Gdx.graphics.getWidth() / 5 - leftOuterButton.getOriginX(), colorBoxes[0].getY() + leftOuterButton.getHeight() * 1.5f);
        leftOuterButton.setColor(.33f, .78f, .92f, 1);

        leftInnerButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        leftInnerButton.setSize(leftInnerButton.getWidth() * game.screenScale, leftInnerButton.getHeight() * game.screenScale);
        leftInnerButton.setOriginCenter();
        leftInnerButton.setPosition(Gdx.graphics.getWidth() * 2 / 5 - leftInnerButton.getOriginX(), colorBoxes[0].getY() + leftInnerButton.getHeight() * 1.5f);
        leftInnerButton.setColor(.33f, .78f, .92f, 1);

        rightOuterButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        rightOuterButton.setSize(rightOuterButton.getWidth() * game.screenScale, rightOuterButton.getHeight() * game.screenScale);
        rightOuterButton.setOriginCenter();
        rightOuterButton.setPosition(Gdx.graphics.getWidth() * 3 / 5 - rightOuterButton.getOriginX(), colorBoxes[0].getY() + rightOuterButton.getHeight() * 1.5f);
        rightOuterButton.setColor(.33f, .78f, .92f, 1);

        rightInnerButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        rightInnerButton.setSize(rightInnerButton.getWidth() * game.screenScale, rightInnerButton.getHeight() * game.screenScale);
        rightInnerButton.setOriginCenter();
        rightInnerButton.setPosition(Gdx.graphics.getWidth() * 4 / 5 - rightInnerButton.getOriginX(), colorBoxes[0].getY() + rightInnerButton.getHeight() * 1.5f);
        rightInnerButton.setColor(.33f, .78f, .92f, 1);

        nextMenuButton = new GameButtons(game.manager.get("data/SideMenuButton.png", Texture.class), true, new Vector2().setZero());
        nextMenuButton.setSize(nextMenuButton.getWidth() * game.screenScale, nextMenuButton.getHeight() * game.screenScale);
        nextMenuButton.setOriginCenter();
        nextMenuButton.setPosition(Gdx.graphics.getWidth() - nextMenuButton.getWidth() * 1.5f, rightHover.getY());

        defaultColorsButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        defaultColorsButton.setSize(defaultColorsButton.getWidth() * game.screenScale, defaultColorsButton.getHeight() * game.screenScale);
        defaultColorsButton.setOriginCenter();
        defaultColorsButton.setPosition(copyButton.getX(), copyButton.getY());
        defaultColorsButton.setColor(Color.YELLOW);

        selectButtons = new GameButtons[]{leftOuterButton, leftInnerButton, rightOuterButton, rightInnerButton};

        activeButton = new boolean[selectButtons.length];
        for (int i = 0; i < selectButtons.length; i++) {
            if (i == 0) {
                activeButton[i] = true;
                selectButtons[i].setAlpha(1);
            } else {
                activeButton[i] = false;
                selectButtons[i].setAlpha(.5f);

            }
        }

        backgroundSprite = new Sprite[2];

        for (int i = 0; i < backgroundSprite.length; i++) {
            backgroundSprite[i] = new Sprite(this.game.manager.get("data/SpaceSquare.png", Texture.class));
            backgroundSprite[i].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
            if (i == 0) {
                backgroundSprite[i].setPosition(0, 0);
            } else {
                backgroundSprite[i].setPosition(0, backgroundSprite[i - 1].getY() - backgroundSprite[i].getHeight());
            }
        }
        redSlider = new VolumeRocker(game.manager.get("data/VolumeSliderBack.png", Texture.class), game.manager.get("data/VolumeSliderFill.png", Texture.class), game.manager.get("data/VolumeRockerControl.png", Texture.class),
                new Vector2(Gdx.graphics.getWidth() / 2 - 100 * game.screenScale / 2, leftOuterButton.getY() - 75 * game.screenScale * 1.5f), new Vector2(450 * game.screenScale, 75 * game.screenScale), game.prefs.getFloat("SfxVolumeLevel"));
        greenSlider = new VolumeRocker(game.manager.get("data/VolumeSliderBack.png", Texture.class), game.manager.get("data/VolumeSliderFill.png", Texture.class), game.manager.get("data/VolumeRockerControl.png", Texture.class),
                new Vector2(Gdx.graphics.getWidth() / 2 - 100 * game.screenScale / 2, redSlider.GetRockerPosition().y - 75 * game.screenScale * 1.5f), new Vector2(450 * game.screenScale, 75 * game.screenScale), game.prefs.getFloat("SfxVolumeLevel"));
        blueSlider = new VolumeRocker(game.manager.get("data/VolumeSliderBack.png", Texture.class), game.manager.get("data/VolumeSliderFill.png", Texture.class), game.manager.get("data/VolumeRockerControl.png", Texture.class),
                new Vector2(Gdx.graphics.getWidth() / 2 - 100 * game.screenScale / 2, greenSlider.GetRockerPosition().y - 75 * game.screenScale * 1.5f), new Vector2(450 * game.screenScale, 75 * game.screenScale), game.prefs.getFloat("SfxVolumeLevel"));

        currentSelectedIndex = 0;

        rockerActive = false;
        previousRockerActive = rockerActive;
        joysticksOnScreen = false;
        spritesMoving = false;

        earthOceans = new Sprite(game.manager.get("data/EmptyCircle.png", Texture.class));
        earthOceans.setSize(earthOceans.getWidth() * game.screenScale, earthOceans.getHeight() * game.screenScale);
        earthOceans.setColor(game.prefs.getFloat("EarthCircleR"), game.prefs.getFloat("EarthCircleG"), game.prefs.getFloat("EarthCircleB"), 1f);

        earthContinents = new Sprite(game.manager.get("data/Continents.png", Texture.class));
        earthContinents.setSize(earthContinents.getWidth() * game.screenScale, earthContinents.getHeight() * game.screenScale);
        earthContinents.setColor(game.prefs.getFloat("EarthContinentsR"), game.prefs.getFloat("EarthContinentsG"), game.prefs.getFloat("EarthContinentsB"), 1f);

        moonMajor = new Sprite(game.manager.get("data/EmptyCircle.png", Texture.class));
        moonMajor.setSize(earthOceans.getWidth() * .27f, earthOceans.getHeight() * .27f);
        moonMajor.setColor(game.prefs.getFloat("MoonMajorR"), game.prefs.getFloat("MoonMajorG"), game.prefs.getFloat("MoonMajorB"), 1);

        moonMinor = new Sprite(game.manager.get("data/MoonBlotsWhite.png", Texture.class));
        moonMinor.setSize(moonMajor.getWidth(), moonMajor.getHeight());
        moonMinor.setColor(game.prefs.getFloat("MoonMinorR"), game.prefs.getFloat("MoonMinorG"), game.prefs.getFloat("MoonMinorB"), 1);

        if (joysticksOnScreen) {
            earthContinents.setPosition(Gdx.graphics.getWidth() / 3 + Gdx.graphics.getWidth(), joystickSprites[0].getY());
            earthOceans.setPosition(earthContinents.getX(), earthContinents.getY());
        } else {
            earthContinents.setPosition(Gdx.graphics.getWidth() / 3, joystickSprites[0].getY());
            earthOceans.setPosition(earthContinents.getX(), earthContinents.getY());
            leftOuterCircle.setPosition(Gdx.graphics.getWidth() / 4 - leftOuterCircle.getOriginX() / 4 + Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 2 + 50 * game.screenScale);
            leftHover.setPosition(leftOuterCircle.getX() + leftOuterCircle.getOriginX() - leftHover.getOriginX(), leftOuterCircle.getY() + leftOuterCircle.getOriginY() - leftHover.getOriginY());
            rightOuterCircle.setPosition(Gdx.graphics.getWidth() * 3 / 4 - rightOuterCircle.getOriginX() * 1.5f + Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 2 + 50 * game.screenScale);
            rightHover.setPosition(rightOuterCircle.getX() + rightOuterCircle.getOriginX() - rightHover.getOriginX(), rightOuterCircle.getY() + rightOuterCircle.getOriginY() - rightHover.getOriginY());
        }

        moonMajor.setPosition(earthContinents.getX() + earthContinents.getWidth() * 1.75f, earthContinents.getY() + earthContinents.getHeight() / 2 - moonMajor.getHeight() / 2);
        moonMinor.setPosition(moonMajor.getX(), moonMajor.getY());

        grayOverlay = new Sprite(game.manager.get("data/EmptyCircleGray.png", Texture.class));
        grayOverlay.setSize(earthOceans.getWidth(), earthOceans.getHeight());
        grayOverlay.setOriginCenter();
        grayOverlay.setPosition(earthOceans.getX(), earthOceans.getY());
        grayOverlay.setAlpha(.35f);

        grayOverlayMoon = new Sprite(game.manager.get("data/EmptyCircleGray.png", Texture.class));
        grayOverlayMoon.setSize(moonMajor.getWidth(), moonMajor.getHeight());
        grayOverlayMoon.setOriginCenter();
        grayOverlayMoon.setPosition(moonMajor.getX(), moonMajor.getY());
        grayOverlayMoon.setAlpha(.35f);

        planetSprites = new Sprite[]{earthOceans, earthContinents, moonMajor, moonMinor};
    }

    @Override
    public void render(float delta) {
        basicButton.Update();
        nextMenuButton.Update();
        if (basic) {
            defaultColorsButton.Update();
        }
        if (redSlider.Active() || greenSlider.Active() || blueSlider.Active()) {
            rockerActive = true;
        } else {
            rockerActive = false;
        }
        if (!rockerActive && !previousRockerActive) {
            if (basicButton.Tapped()) {
                basic = !basic;
                if (!basic) {
                    if (joysticksOnScreen) {
                        redSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().r);
                        greenSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().g);
                        blueSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().b);
                    } else {
                        redSlider.SetValue(planetSprites[currentSelectedIndex].getColor().r);
                        greenSlider.SetValue(planetSprites[currentSelectedIndex].getColor().g);
                        blueSlider.SetValue(planetSprites[currentSelectedIndex].getColor().b);
                    }
                }
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                }
            }

            if (nextMenuButton.Tapped()) {
                if (!spritesMoving) {
                    spritesMoving = true;
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            }

            if (defaultColorsButton.Tapped()) {
                if(!joysticksOnScreen) {
                    earthContinents.setColor(.5f, .73f, .5f, 1);
                    earthOceans.setColor(.28f, .43f, .68f, 1);
                    moonMajor.setColor(Color.LIGHT_GRAY);
                    moonMinor.setColor(Color.DARK_GRAY);
                }
                else {
                    leftOuterCircle.setColor(.28f, .43f, .68f, 1);
                    leftHover.setColor(.28f, .43f, .68f, 1);
                    rightOuterCircle.setColor(.83f, 0, .02f, 1);
                    rightHover.setColor(.83f, 0, .02f, 1);
                }
            }
        }

        if (spritesMoving && joysticksOnScreen) {
            for (int i = 0; i < joystickSprites.length; i++) {
                joystickSprites[i].setPosition(joystickSprites[i].getX() + 130 * game.screenScale, joystickSprites[i].getY());
            }
            earthContinents.setPosition(earthContinents.getX() + 130 * game.screenScale, earthContinents.getY());
            earthOceans.setPosition(earthOceans.getX() + 130 * game.screenScale, earthOceans.getY());
            moonMajor.setPosition(earthContinents.getX() + earthContinents.getWidth() * 1.75f, moonMajor.getY());
            moonMinor.setPosition(moonMajor.getX(), moonMajor.getY());
            grayOverlay.setX(earthOceans.getX());
            grayOverlayMoon.setX(moonMajor.getX());
            if (earthOceans.getX() >= Gdx.graphics.getWidth() / 3) {
                spritesMoving = false;
                joysticksOnScreen = false;
                float earthOldX = earthOceans.getX();
                earthOceans.setX(Gdx.graphics.getWidth() / 3);
                earthContinents.setX(earthOceans.getX());
                float locationDifference = earthOceans.getX() - earthOldX;
                moonMajor.setPosition(earthContinents.getX() + earthContinents.getWidth() * 1.75f, moonMajor.getY());
                moonMinor.setPosition(moonMajor.getX(), moonMajor.getY());
                grayOverlay.setX(earthOceans.getX());
                grayOverlayMoon.setX(moonMajor.getX());
                for (int i = 0; i < joystickSprites.length; i++) {
                    joystickSprites[i].setPosition(joystickSprites[i].getX() + locationDifference, joystickSprites[i].getY());
                }
                if (!basic) {
                    if (joysticksOnScreen) {
                        redSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().r);
                        greenSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().g);
                        blueSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().b);
                    } else {
                        redSlider.SetValue(planetSprites[currentSelectedIndex].getColor().r);
                        greenSlider.SetValue(planetSprites[currentSelectedIndex].getColor().g);
                        blueSlider.SetValue(planetSprites[currentSelectedIndex].getColor().b);
                    }
                }
            }
            if (!spritesMoving && !joysticksOnScreen && nextMenuButton.getRotation() != 0) {
                nextMenuButton.setRotation(0);
                nextMenuButton.setPosition(Gdx.graphics.getWidth() - nextMenuButton.getWidth() * 1.5f, rightHover.getY());
            }
        }

        if (spritesMoving && !joysticksOnScreen) {
            for (int i = 0; i < joystickSprites.length; i++) {
                joystickSprites[i].setPosition(joystickSprites[i].getX() - 130 * game.screenScale, joystickSprites[i].getY());
            }
            earthContinents.setPosition(earthContinents.getX() - 130 * game.screenScale, earthContinents.getY());
            earthOceans.setPosition(earthOceans.getX() - 130 * game.screenScale, earthOceans.getY());
            moonMajor.setPosition(earthContinents.getX() + earthContinents.getWidth() * 1.75f, moonMajor.getY());
            moonMinor.setPosition(moonMajor.getX(), moonMajor.getY());
            grayOverlay.setX(earthOceans.getX());
            grayOverlayMoon.setX(moonMajor.getX());
            if (earthOceans.getX() <= -Gdx.graphics.getWidth() + Gdx.graphics.getWidth() / 3) {
                spritesMoving = false;
                joysticksOnScreen = true;
                if (!basic) {
                    if (joysticksOnScreen) {
                        redSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().r);
                        greenSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().g);
                        blueSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().b);
                    } else {
                        redSlider.SetValue(planetSprites[currentSelectedIndex].getColor().r);
                        greenSlider.SetValue(planetSprites[currentSelectedIndex].getColor().g);
                        blueSlider.SetValue(planetSprites[currentSelectedIndex].getColor().b);
                    }
                }
            }
            if (!spritesMoving && joysticksOnScreen && nextMenuButton.getRotation() != 180) {
                nextMenuButton.setRotation(180);
                nextMenuButton.setPosition(nextMenuButton.getWidth() * .5f, nextMenuButton.getY());
            }
        }

        if (basic) {
            for (int i = 0; i < colorBoxes.length; i++) {
                colorBoxes[i].Update();
                if (colorBoxes[i].Tapped()) {
                    if (joysticksOnScreen) {
                        joystickSprites[currentSelectedIndex].setColor(colorBoxes[i].getColor());
                        joystickSprites[currentSelectedIndex].setAlpha(1f);
                    } else {
                        planetSprites[currentSelectedIndex].setColor(colorBoxes[i].getColor());
                        planetSprites[currentSelectedIndex].setAlpha(1f);
                    }

                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }

                }
            }
        } else {
            redSlider.Update(greenSlider.Active(), blueSlider.Active());
            greenSlider.Update(redSlider.Active(), blueSlider.Active());
            blueSlider.Update(redSlider.Active(), greenSlider.Active());

            if (joysticksOnScreen) {
                joystickSprites[currentSelectedIndex].setColor(redSlider.CurrentValue(), greenSlider.CurrentValue(), blueSlider.CurrentValue(), 1);
            } else {
                planetSprites[currentSelectedIndex].setColor(redSlider.CurrentValue(), greenSlider.CurrentValue(), blueSlider.CurrentValue(), 1);
            }

            copyButton.Update();
            pasteButton.Update();
            if (!rockerActive && !previousRockerActive) {
                if (copyButton.Tapped()) {
                    if (joysticksOnScreen) {
                        copiedColor = joystickSprites[currentSelectedIndex].getColor();
                    } else {
                        copiedColor = planetSprites[currentSelectedIndex].getColor();
                    }
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
                if (copiedColor == null) {
                    pasteButton.setAlpha(.5f);
                } else {
                    pasteButton.setAlpha(1f);
                    if (pasteButton.Tapped()) {
                        if (joysticksOnScreen) {
                            joystickSprites[currentSelectedIndex].setColor(copiedColor);
                            redSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().r);
                            greenSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().g);
                            blueSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().b);
                        } else {
                            planetSprites[currentSelectedIndex].setColor(copiedColor);
                            redSlider.SetValue(planetSprites[currentSelectedIndex].getColor().r);
                            greenSlider.SetValue(planetSprites[currentSelectedIndex].getColor().g);
                            blueSlider.SetValue(planetSprites[currentSelectedIndex].getColor().b);
                        }
                        if (game.prefs.getBoolean("Vibrate")) {
                            Gdx.input.vibrate(150);
                        }
                        if (game.prefs.getBoolean("Sound")) {
                            game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                        }
                    }
                }
            }
        }

        for (int i = 0; i < selectButtons.length; i++) {
            selectButtons[i].Update();
            if (!rockerActive && !previousRockerActive) {
                if (selectButtons[i].Tapped()) {
                    for (int j = 0; j < activeButton.length; j++) {
                        if (j == i) {
                            activeButton[j] = true;
                            selectButtons[j].setAlpha(1);
                        } else {
                            activeButton[j] = false;
                            selectButtons[j].setAlpha(.5f);

                        }
                        currentSelectedIndex = i;
                        if (!basic) {
                            if (joysticksOnScreen) {
                                redSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().r);
                                greenSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().g);
                                blueSlider.SetValue(joystickSprites[currentSelectedIndex].getColor().b);
                            } else {
                                redSlider.SetValue(planetSprites[currentSelectedIndex].getColor().r);
                                greenSlider.SetValue(planetSprites[currentSelectedIndex].getColor().g);
                                blueSlider.SetValue(planetSprites[currentSelectedIndex].getColor().b);
                            }
                        }
                    }
                }
            }
        }


        menuButton.Update();
        if (!rockerActive && !previousRockerActive) {
            if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE) || menuButton.Tapped() || game.BackPressed()) {
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                }

                game.prefs.putFloat("LeftOuterColorR", leftOuterCircle.getColor().r);
                game.prefs.putFloat("LeftOuterColorG", leftOuterCircle.getColor().g);
                game.prefs.putFloat("LeftOuterColorB", leftOuterCircle.getColor().b);

                game.prefs.putFloat("LeftInnerColorR", leftHover.getColor().r);
                game.prefs.putFloat("LeftInnerColorG", leftHover.getColor().g);
                game.prefs.putFloat("LeftInnerColorB", leftHover.getColor().b);

                game.prefs.putFloat("RightOuterColorR", rightOuterCircle.getColor().r);
                game.prefs.putFloat("RightOuterColorG", rightOuterCircle.getColor().g);
                game.prefs.putFloat("RightOuterColorB", rightOuterCircle.getColor().b);

                game.prefs.putFloat("RightInnerColorR", rightHover.getColor().r);
                game.prefs.putFloat("RightInnerColorG", rightHover.getColor().g);
                game.prefs.putFloat("RightInnerColorB", rightHover.getColor().b);

                game.prefs.putFloat("EarthCircleR", earthOceans.getColor().r);
                game.prefs.putFloat("EarthCircleG", earthOceans.getColor().g);
                game.prefs.putFloat("EarthCircleB", earthOceans.getColor().b);

                game.prefs.putFloat("EarthContinentsR", earthContinents.getColor().r);
                game.prefs.putFloat("EarthContinentsG", earthContinents.getColor().g);
                game.prefs.putFloat("EarthContinentsB", earthContinents.getColor().b);

                game.prefs.putFloat("MoonMajorR", moonMajor.getColor().r);
                game.prefs.putFloat("MoonMajorG", moonMajor.getColor().g);
                game.prefs.putFloat("MoonMajorB", moonMajor.getColor().b);

                game.prefs.putFloat("MoonMinorR", moonMinor.getColor().r);
                game.prefs.putFloat("MoonMinorG", moonMinor.getColor().g);
                game.prefs.putFloat("MoonMinorB", moonMinor.getColor().b);

                game.prefs.flush();

                game.manager.unload("data/ColorBox.png");
                game.manager.unload("data/VolumeSliderBack.png");
                game.manager.unload("data/VolumeSliderFill.png");
                game.manager.unload("data/VolumeRockerControl.png");
                game.setScreen(new CustomizePortal(game));
                this.dispose();
            }
        }
        previousRockerActive = rockerActive;
        //Drawing
        game.batch.begin();
        for (Sprite background : backgroundSprite) {
            background.draw(game.batch);
        }
        if (basic) {
            for (GameButtons box : colorBoxes) {
                box.draw(game.batch);
            }
        } else {
            redSlider.Draw(game.batch);
            greenSlider.Draw(game.batch);
            blueSlider.Draw(game.batch);
            copyButton.draw(game.batch);
            pasteButton.draw(game.batch);
        }
        for (Sprite object : joystickSprites) {
            object.draw(game.batch);
        }
        basicButton.draw(game.batch);
        for (GameButtons button : selectButtons) {
            button.draw(game.batch);
        }
        earthOceans.draw(game.batch);
        earthContinents.draw(game.batch);
        grayOverlay.draw(game.batch);
        moonMajor.draw(game.batch);
        moonMinor.draw(game.batch);
        grayOverlayMoon.draw(game.batch);
        nextMenuButton.draw(game.batch);
        menuButton.draw(game.batch);
        if (basic) {
            defaultColorsButton.draw(game.batch);
        }
        TextDrawing();
        game.batch.end();
    }

    private void TextDrawing() {
        goodTimeFont.setScale(game.screenScale * .55f);
        goodTimeFont.setColor(Color.WHITE);
        goodTimeFont.draw(game.batch, "Back", menuButton.getX() + 25 * game.screenScale, menuButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);

        if (basic) {
            goodTimeFont.draw(game.batch, "Basic", basicButton.getX() + 16 * game.screenScale, basicButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
            goodTimeFont.setScale(game.screenScale * .52f);
            goodTimeFont.draw(game.batch, "Reset", defaultColorsButton.getX() + 17.5f * game.screenScale, defaultColorsButton.getY() + goodTimeFont.getLineHeight() + 1f * game.screenScale);
        } else {
            goodTimeFont.setColor(Color.RED);
            goodTimeFont.draw(game.batch, "Red:", redSlider.GetRockerPosition().x - 292 * game.screenScale, redSlider.GetRockerPosition().y + goodTimeFont.getLineHeight() - 30 * game.screenScale);
            goodTimeFont.setColor(Color.GREEN);
            goodTimeFont.draw(game.batch, "Green:", greenSlider.GetRockerPosition().x - 428 * game.screenScale, greenSlider.GetRockerPosition().y + goodTimeFont.getLineHeight() - 30 * game.screenScale);
            goodTimeFont.setColor(Color.BLUE);
            goodTimeFont.draw(game.batch, "Blue:", blueSlider.GetRockerPosition().x - 350 * game.screenScale, blueSlider.GetRockerPosition().y + goodTimeFont.getLineHeight() - 30 * game.screenScale);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, "Copy", copyButton.getX() + 25 * game.screenScale, copyButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
            goodTimeFont.setScale(game.screenScale * .5f);
            goodTimeFont.draw(game.batch, "Paste", pasteButton.getX() + 18 * game.screenScale, pasteButton.getY() + goodTimeFont.getLineHeight() + 2 * game.screenScale);
            goodTimeFont.setScale(game.screenScale * .3f);
            goodTimeFont.draw(game.batch, "Advanced", basicButton.getX() + 15 * game.screenScale, basicButton.getY() + goodTimeFont.getLineHeight() + 25 * game.screenScale);
        }

        if (joysticksOnScreen) {
            goodTimeFont.setScale(game.screenScale * .26f);
            goodTimeFont.draw(game.batch, "Left Outer", leftOuterButton.getX() + 30f * game.screenScale, leftOuterButton.getY() + goodTimeFont.getLineHeight() + 30 * game.screenScale);
            goodTimeFont.draw(game.batch, "Left Inner", leftInnerButton.getX() + 40f * game.screenScale, leftInnerButton.getY() + goodTimeFont.getLineHeight() + 30 * game.screenScale);
            goodTimeFont.draw(game.batch, "Right Outer", rightOuterButton.getX() + 14.5f * game.screenScale, rightOuterButton.getY() + goodTimeFont.getLineHeight() + 30 * game.screenScale);
            goodTimeFont.draw(game.batch, "Right Inner", rightInnerButton.getX() + 26f * game.screenScale, rightInnerButton.getY() + goodTimeFont.getLineHeight() + 30 * game.screenScale);
        } else {
            goodTimeFont.setScale(game.screenScale * .23f);
            goodTimeFont.draw(game.batch, "Earth Major", leftOuterButton.getX() + 15f * game.screenScale, leftOuterButton.getY() + goodTimeFont.getLineHeight() + 35 * game.screenScale);
            goodTimeFont.draw(game.batch, "Earth Minor", leftInnerButton.getX() + 25f * game.screenScale, leftInnerButton.getY() + goodTimeFont.getLineHeight() + 35 * game.screenScale);
            goodTimeFont.draw(game.batch, "Moon Major", rightOuterButton.getX() + 19.5f * game.screenScale, rightOuterButton.getY() + goodTimeFont.getLineHeight() + 35 * game.screenScale);
            goodTimeFont.draw(game.batch, "Moon Minor", rightInnerButton.getX() + 28f * game.screenScale, rightInnerButton.getY() + goodTimeFont.getLineHeight() + 35 * game.screenScale);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
