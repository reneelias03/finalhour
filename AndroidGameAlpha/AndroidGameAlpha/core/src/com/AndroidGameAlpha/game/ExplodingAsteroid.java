package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/**
 * Created by rene__000 on 11/18/2014.
 */
public class ExplodingAsteroid extends Asteroid {
    protected Vector2 _explodeRadius;
    public Vector2 ExplodeRadius()
    {
        return _explodeRadius;
    }
    private boolean _earthCollision;
    public boolean GetEarthCollision() {
        return _earthCollision;
    }
    public void SetEarthCollision (boolean earthCollision) {
        _earthCollision = earthCollision;
    }
    protected Sprite _circleOverlay;
    protected Texture _cannonBallShine;
//    Vector2 _originalSize;

    public Sprite get_circleOverlay() {
        return _circleOverlay;
    }

    protected Sprite shineSprite;

    public ExplodingAsteroid(Texture texture, Vector2 PlanetLocation, float scale, float speed, float cameraZoom, Vector2 deadZone, TextureAtlas explosionAtlas, Texture circleOverlay, Texture cannonBallShine)
    {
        super(texture, PlanetLocation, scale, speed, cameraZoom, deadZone, explosionAtlas);
        this.setColor(Color.RED);
//        _originalSize = new Vector2(getWidth(), getHeight());
       _explodeRadius = new Vector2().setZero();
        _normalAsteroid = false;
        _earthCollision = false;
        _circleOverlay = new Sprite(circleOverlay);
        _circleOverlay.setSize(this.getWidth() * 2, this.getHeight() * 2);
        _circleOverlay.setColor(Color.RED);
        _circleOverlay.setAlpha(.25f);
        _circleOverlay.setOriginCenter();

        if(cannonBallShine != null) {
            _cannonBallShine = cannonBallShine;
            shineSprite = new Sprite(cannonBallShine);
            shineSprite.setSize(this.getWidth(), this.getHeight());
            shineSprite.setOriginCenter();
        }
//        this._explosionAnimation.setFrameDuration(1f);
//        _active = true;
    }

    @Override
    public void Update(float cameraZoom, LaserBullets[] bullets, Planet Earth, Powerup powerup, boolean vibrate, Sound explosion, boolean playSound, float deltaTime, float volume) {
        super.Update(cameraZoom, bullets, Earth, powerup, vibrate, explosion, playSound, deltaTime, volume);

        if(_active) {
            if (_toScore || (!_earthCollision && _exploding && !_toScore)) {
                this.setSize(this.getWidth() * 8, this.getHeight() * 8);
                _explodeRadius = new Vector2(0, (float) (this.getHeight() / 2) * .9f);
                this.setPosition(this.getX() - (this.getWidth() / 2) + this.getOriginX(), this.getY() - (this.getHeight() / 2) + this.getOriginY());
                this.setRotation(0);
                this.setOriginCenter();
                _earthCollision = true;
            }

            _circleOverlay.setPosition(this.getX() - _circleOverlay.getOriginX() / 2, this.getY() - _circleOverlay.getOriginY() / 2);
            if (shineSprite != null) {
                shineSprite.setPosition(this.getX(), this.getY());
                shineSprite.setRotation(this.getRotation());
            }

            if (_exploding) {
                if (!_toScore) {
                    _earthCollision = true;
                }
                if (_explosionAnimation.getKeyFrameIndex(_frameTime) > 5 && _explosionAnimation.getKeyFrameIndex(_frameTime) < 16) {
                    if (CheckCollision(new Vector2(Earth.getX(), Earth.getY()), Earth.GetRadius(), new Vector2(Earth.getOriginX(), Earth.getOriginY())) && !Earth.IsHit() && !(powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.Explosion) && !powerup.explosionSimultaneous) {
                        Earth.SetHealth(Earth.GetHealth() - 1);
                        if (playSound) {
                            explosion.play(.2f * volume);
                        }
                        if (Earth.GetHealth() > 0) {
                            Earth.SetIsHit(true);
                            Earth.PrepareHitSprite();
                        }
                        if (vibrate) {
                            Gdx.input.vibrate(300);
                        }
                    }
                }
            }
        }
    }

    public boolean CheckCollision(Vector2 Location, Vector2 radius, Vector2 Origin)
    {
        Vector2 distanceBetweenCircles = new Vector2( Location.x + Origin.x - this.getX() - this.getOriginX(),  Location.y + Origin.y - this.getY() - this.getOriginY());
        if(_explodeRadius.len() + radius.len() >= distanceBetweenCircles.len())
        {
            return true;
        }
        return false;
    }

    public void Draw(SpriteBatch batch)
    {
        if(!_exploding) {
            _circleOverlay.draw(batch);
        }
//        else {
//            _circleOverlay.setSize(_explodeRadius.len() * 2, _explodeRadius.len() * 2);
//            _circleOverlay.draw(batch);
//        }
        this.draw(batch);
        if(shineSprite != null) {
            shineSprite.draw(batch);
        }
    }

    public void Reset(Vector2 PlanetLocation)
    {
        ResetAsteroid(_originalTexture, PlanetLocation);
        this.setColor(Color.RED);
//        _originalSize = new Vector2(getWidth(), getHeight());
        _explodeRadius = new Vector2().setZero();
        _normalAsteroid = false;
        _earthCollision = false;
//        _circleOverlay = new Sprite(circleOverlay);
        _circleOverlay.setSize(this.getWidth() * 2, this.getHeight() * 2);
//        _circleOverlay.setColor(Color.RED);
//        _circleOverlay.setAlpha(.25f);
        _circleOverlay.setOriginCenter();

        if(_cannonBallShine != null) {
//            shineSprite = new Sprite(_cannonBallShine);
            shineSprite.setSize(this.getWidth(), this.getHeight());
            shineSprite.setOriginCenter();
        }
    }


}
