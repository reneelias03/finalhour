package com.AndroidGameAlpha.game;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import android.widget.FrameLayout.*;
import android.view.*;

/**
 * Created by rene__000 on 9/18/2014.
 */

public interface ActionResolver {
    public boolean getSignedInGPGS();
    public void loginGPGS();
    public void submitScoreGPGS(String leaderboard, int score);
    public void unlockAchievementGPGS(String achievementId);
    public void getLeaderboardGPGS(String leaderboard);
    public void getAchievementsGPGS();
//    public AdView createAdView();
//    public View createGameView(AndroidApplicationConfiguration cfg);
//    public void startAdvertising(AdView adView);
    public void showOrLoadInterstital();
    public void adColonyShowInterstitial();
    public boolean adColonyShowReadyInterstitial();
    public void adColonyShowReady();
    public void purchaseCoinItem(int index);
    public boolean returnPurchaseSuccess();
    public void queryInventory();
    public boolean adWatchedSuccessfully();
    public void restoreAds();
    public boolean adsRestored();
    public boolean mhelperSetupFinished();
}
