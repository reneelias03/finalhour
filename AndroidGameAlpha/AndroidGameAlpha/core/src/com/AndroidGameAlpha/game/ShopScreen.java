package com.AndroidGameAlpha.game;

import android.widget.Button;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

import java.security.Key;

/**
 * Created by rene__000 on 10/23/2014.
 */
public class ShopScreen implements Screen {

    MyGdxGame game;
    Sprite[][] themes;
    Sprite earthOceans, earthContinents, moonMajor, moonMinor, whitePixel, coinTotalSprite, whitePixelBottom, grayOverlay, grayOverlayMoon;
    float planetScale;
    GameButtons purchaseButton, leftScrollButton, rightScrollButton, backButton;
    BitmapFont goodTimeFont;
    boolean[] activeTheme;
    boolean screenScrolling;
    int activeThemeIndex, targetThemeIndex;
    int[] themeCosts;

    public ShopScreen(MyGdxGame game) {
        this.game = game;
        goodTimeFont = game.goodTimeFont;
//        game.prefs.putInteger("Coins", 1000);
//        game.prefs.flush();
//
//        game.prefs.putBoolean("PixelSkinUnlocked", false);
//        game.prefs.putBoolean("CatSkinUnlocked", false);
//        game.prefs.putBoolean("NinjaSkinUnlocked", false);
//        game.prefs.flush();

        planetScale = .45f * game.screenScale;

        earthOceans = new Sprite(this.game.manager.get("data/EmptyCircle.png", Texture.class));
        earthOceans.setSize(earthOceans.getWidth() * planetScale, earthOceans.getHeight() * planetScale);
        earthOceans.setOriginCenter();
        earthOceans.setPosition(Gdx.graphics.getWidth() / 2 - earthOceans.getWidth(), Gdx.graphics.getHeight() / 2 - earthOceans.getOriginY());
        earthOceans.setColor(game.prefs.getFloat("EarthCircleR"), game.prefs.getFloat("EarthCircleG"), game.prefs.getFloat("EarthCircleB"), 1f);

        earthContinents = new Sprite(game.manager.get("data/Continents.png", Texture.class));
        earthContinents.setSize(earthOceans.getWidth(), earthOceans.getHeight());
        earthContinents.setOriginCenter();
        earthContinents.setPosition(earthOceans.getX(), earthOceans.getY());
        earthContinents.setColor(game.prefs.getFloat("EarthContinentsR"), game.prefs.getFloat("EarthContinentsG"), game.prefs.getFloat("EarthContinentsB"), 1f);

        moonMajor = new Sprite(this.game.manager.get("data/EmptyCircle.png", Texture.class));
        moonMajor.setSize(earthOceans.getWidth() * .27f, earthOceans.getHeight() * .27f);
        moonMajor.setOriginCenter();
        moonMajor.setPosition(earthContinents.getX() + earthContinents.getWidth() * 1.75f, earthOceans.getY() + earthContinents.getOriginY() - moonMajor.getOriginY());
        moonMajor.setColor(game.prefs.getFloat("MoonMajorR"), game.prefs.getFloat("MoonMajorG"), game.prefs.getFloat("MoonMajorB"), 1);

        moonMinor = new Sprite(this.game.manager.get("data/MoonBlotsWhite.png", Texture.class));
        moonMinor.setSize(moonMajor.getWidth(), moonMajor.getHeight());
        moonMinor.setOriginCenter();
        moonMinor.setPosition(moonMajor.getX(), moonMajor.getY());
        moonMinor.setColor(game.prefs.getFloat("MoonMinorR"), game.prefs.getFloat("MoonMinorG"), game.prefs.getFloat("MoonMinorB"), 1);

        purchaseButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        purchaseButton.setSize(purchaseButton.getWidth() * game.screenScale, purchaseButton.getHeight() * game.screenScale);
        purchaseButton.setOriginCenter();
        purchaseButton.setPosition(Gdx.graphics.getWidth() / 2 - purchaseButton.getOriginX(), 100 * game.screenScale);

        themes = new Sprite[7][];
        for (int i = 0; i < themes.length; i++) {
            themes[i] = new Sprite[9];
        }

        //region DefaultThemeLoad
        themes[0][0] = new Sprite(this.game.manager.get("data/SpaceSquare.png", Texture.class));
        themes[0][0].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
        themes[0][0].setPosition(0, 0);

        themes[0][1] = new Sprite(this.game.manager.get("data/AsteroidRedone.png", Texture.class));
        themes[0][1].setSize(themes[0][1].getWidth() * planetScale, themes[0][1].getHeight() * planetScale);
        themes[0][1].setPosition(Gdx.graphics.getWidth() - 200 * game.screenScale, Gdx.graphics.getHeight() - 230 * game.screenScale);

        themes[0][2] = new Sprite(this.game.manager.get("data/AsteroidRedone.png", Texture.class));
        themes[0][2].setSize((themes[0][2].getWidth() + 20) * planetScale, (themes[0][2].getHeight() + 20) * planetScale);
        themes[0][2].setPosition(150 * game.screenScale, Gdx.graphics.getHeight() - 400 * game.screenScale);

        themes[0][3] = new Sprite(this.game.manager.get("data/AsteroidRedone.png", Texture.class));
        themes[0][3].setSize((themes[0][3].getWidth() + 10) * planetScale, (themes[0][3].getHeight() + 10) * planetScale);
        themes[0][3].setPosition(600 * game.screenScale, Gdx.graphics.getHeight() - 800 * game.screenScale);

        themes[0][4] = new Sprite(this.game.manager.get("data/AsteroidRedone.png", Texture.class));
        themes[0][4].setSize((themes[0][4].getWidth() + 15) * planetScale, (themes[0][4].getHeight() + 15) * planetScale);
        themes[0][4].setPosition(Gdx.graphics.getWidth() / 2 + 350 * game.screenScale, Gdx.graphics.getHeight() / 2 - 300 * game.screenScale);

        themes[0][7] = new Sprite(this.game.manager.get("data/AsteroidRedone.png", Texture.class));
        themes[0][7].setSize((themes[0][4].getWidth() + 5), (themes[0][4].getHeight() + 5));
        themes[0][7].setPosition(Gdx.graphics.getWidth() / 2 , Gdx.graphics.getHeight() - 220 * game.screenScale);
        //endregion

        //region ThrowbackThemeLoad
        themes[1][0] = new Sprite(this.game.manager.get("data/PixelSpace.png", Texture.class));
        themes[1][0].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
        themes[1][0].setPosition(Gdx.graphics.getWidth(), 0);

        themes[1][1] = new Sprite(this.game.manager.get("data/PixelAsteroidRedone14.png", Texture.class));
        themes[1][1].setSize(themes[0][1].getWidth(), themes[0][1].getHeight());
        themes[1][1].setPosition(Gdx.graphics.getWidth() - 200 * game.screenScale + Gdx.graphics.getWidth(), Gdx.graphics.getHeight() - 230 * game.screenScale);

        themes[1][2] = new Sprite(this.game.manager.get("data/PixelAsteroidRedone14.png", Texture.class));
        themes[1][2].setSize(themes[0][2].getWidth(), themes[0][2].getHeight());
        themes[1][2].setPosition(150 * game.screenScale + Gdx.graphics.getWidth(), Gdx.graphics.getHeight() - 400 * game.screenScale);

        themes[1][3] = new Sprite(this.game.manager.get("data/PixelAsteroidRedone14.png", Texture.class));
        themes[1][3].setSize(themes[0][3].getWidth(), themes[0][3].getHeight());
        themes[1][3].setPosition(600 * game.screenScale + Gdx.graphics.getWidth(), Gdx.graphics.getHeight() - 800 * game.screenScale);

        themes[1][4] = new Sprite(this.game.manager.get("data/PixelAsteroidRedone14.png", Texture.class));
        themes[1][4].setSize(themes[0][4].getWidth(), themes[0][4].getHeight());
        themes[1][4].setPosition(Gdx.graphics.getWidth() / 2 + 350 * game.screenScale + Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 2 - 300 * game.screenScale);

        themes[1][5] = new Sprite(this.game.manager.get("data/EmptyCirclePixel.png", Texture.class));
        themes[1][5].setSize(earthOceans.getWidth(), earthOceans.getHeight());
        themes[1][5].setPosition(earthOceans.getX() + Gdx.graphics.getWidth(), earthOceans.getY());
        themes[1][5].setColor(game.prefs.getFloat("EarthCircleR"), game.prefs.getFloat("EarthCircleG"), game.prefs.getFloat("EarthCircleB"), 1f);

        themes[1][6] = new Sprite(this.game.manager.get("data/MoonPixelNew11.png", Texture.class));
        themes[1][6].setSize(moonMajor.getWidth(), moonMajor.getHeight());
        themes[1][6].setPosition(moonMajor.getX() + Gdx.graphics.getWidth(), moonMajor.getY());
        themes[1][6].setColor(game.prefs.getFloat("MoonMajorR"), game.prefs.getFloat("MoonMajorG"), game.prefs.getFloat("MoonMajorB"), 1);

        themes[1][7] = new Sprite(this.game.manager.get("data/PixelAsteroidRedone14.png", Texture.class));
        themes[1][7].setSize((themes[0][4].getWidth() + 5), (themes[0][4].getHeight() + 5));
        themes[1][7].setPosition(Gdx.graphics.getWidth() / 2 + Gdx.graphics.getWidth(), Gdx.graphics.getHeight() - 220 * game.screenScale);

        themes[1][8] = new Sprite(this.game.manager.get("data/ContinentsPixel.png", Texture.class));
        themes[1][8].setSize(earthOceans.getWidth(), earthOceans.getHeight());
        themes[1][8].setPosition(earthOceans.getX() + Gdx.graphics.getWidth(), earthOceans.getY());
        themes[1][8].setColor(game.prefs.getFloat("EarthContinentsR"), game.prefs.getFloat("EarthContinentsG"), game.prefs.getFloat("EarthContinentsB"), 1f);
        //endregion

        //region CatSkinTheme
        themes[2][0] = new Sprite(this.game.manager.get("data/CatBackground.png", Texture.class));
        themes[2][0].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
        themes[2][0].setPosition(Gdx.graphics.getWidth() * 2, 0);

        themes[2][1] = new Sprite(this.game.manager.get("data/MouseMeanSmall.png", Texture.class));
        themes[2][1].setSize(themes[0][1].getWidth(), themes[0][1].getHeight());
        themes[2][1].setPosition(Gdx.graphics.getWidth() - 200 * game.screenScale + Gdx.graphics.getWidth() * 2, Gdx.graphics.getHeight() - 230 * game.screenScale);

        themes[2][2] = new Sprite(this.game.manager.get("data/MouseMeanSmall.png", Texture.class));
        themes[2][2].setSize(themes[0][2].getWidth(), themes[0][2].getHeight());
        themes[2][2].setPosition(150 * game.screenScale + Gdx.graphics.getWidth() * 2, Gdx.graphics.getHeight() - 400 * game.screenScale);

        themes[2][3] = new Sprite(this.game.manager.get("data/MouseMeanSmall.png", Texture.class));
        themes[2][3].setSize(themes[0][3].getWidth(), themes[0][3].getHeight());
        themes[2][3].setPosition(600 * game.screenScale + Gdx.graphics.getWidth() * 2, Gdx.graphics.getHeight() - 800 * game.screenScale);

        themes[2][4] = new Sprite(this.game.manager.get("data/MouseMeanSmall.png", Texture.class));
        themes[2][4].setSize(themes[0][4].getWidth(), themes[0][4].getHeight());
        themes[2][4].setPosition(Gdx.graphics.getWidth() / 2 + 350 * game.screenScale + Gdx.graphics.getWidth() * 2, Gdx.graphics.getHeight() / 2 - 300 * game.screenScale);

        themes[2][5] = new Sprite(this.game.manager.get("data/CatSkinProto.png", Texture.class));
        themes[2][5].setSize(themes[2][5].getWidth() * planetScale, themes[2][5].getHeight() * planetScale);
        themes[2][5].setPosition(earthOceans.getX() - 23f * game.screenScale + Gdx.graphics.getWidth() * 2, earthOceans.getY());

        themes[2][7] = new Sprite(this.game.manager.get("data/MouseMeanSmall.png", Texture.class));
        themes[2][7].setSize((themes[0][4].getWidth() + 5), (themes[0][4].getHeight() + 5));
        themes[2][7].setPosition(Gdx.graphics.getWidth() / 2 + Gdx.graphics.getWidth() * 2, Gdx.graphics.getHeight() - 220 * game.screenScale);
        //endregion

        //region NinjaSkinTheme
        themes[3][0] = new Sprite(this.game.manager.get("data/NinjaBackground1920.png", Texture.class));
        themes[3][0].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
        themes[3][0].setPosition(Gdx.graphics.getWidth() * 3, 0);

        themes[3][1] = new Sprite(this.game.manager.get("data/ShurikenRedone.png", Texture.class));
        themes[3][1].setSize(themes[0][1].getWidth(), themes[0][1].getHeight());
        themes[3][1].setPosition(Gdx.graphics.getWidth() - 200 * game.screenScale + Gdx.graphics.getWidth() * 3, Gdx.graphics.getHeight() - 230 * game.screenScale);

        themes[3][2] = new Sprite(this.game.manager.get("data/ShurikenRedone.png", Texture.class));
        themes[3][2].setSize(themes[0][2].getWidth(), themes[0][2].getHeight());
        themes[3][2].setPosition(150 * game.screenScale + Gdx.graphics.getWidth() * 3, Gdx.graphics.getHeight() - 400 * game.screenScale);

        themes[3][3] = new Sprite(this.game.manager.get("data/ShurikenRedone.png", Texture.class));
        themes[3][3].setSize(themes[0][3].getWidth(), themes[0][3].getHeight());
        themes[3][3].setPosition(600 * game.screenScale + Gdx.graphics.getWidth() * 3, Gdx.graphics.getHeight() - 800 * game.screenScale);

        themes[3][4] = new Sprite(this.game.manager.get("data/ShurikenRedone.png", Texture.class));
        themes[3][4].setSize(themes[0][4].getWidth(), themes[0][4].getHeight());
        themes[3][4].setPosition(Gdx.graphics.getWidth() / 2 + 350 * game.screenScale + Gdx.graphics.getWidth() * 3, Gdx.graphics.getHeight() / 2 - 300 * game.screenScale);

        themes[3][5] = new Sprite(this.game.manager.get("data/NinjaSkinProto.png", Texture.class));
        themes[3][5].setSize(earthOceans.getWidth(), earthOceans.getHeight());
        themes[3][5].setPosition(earthOceans.getX() + Gdx.graphics.getWidth() * 3, earthOceans.getY());

        themes[3][6] = new Sprite(this.game.manager.get("data/NinjaHatSmall.png", Texture.class));
        themes[3][6].setSize(themes[3][6].getWidth() * planetScale * 1.25f, themes[3][6].getHeight() * planetScale * 1.25f);
        themes[3][6].setPosition(moonMajor.getX() - ((themes[3][6].getWidth() - moonMajor.getWidth()) / 2) + Gdx.graphics.getWidth() * 3,  moonMajor.getY() + moonMajor.getHeight() * 5f / 10f);

        themes[3][7] = new Sprite(this.game.manager.get("data/ShurikenRedone.png", Texture.class));
        themes[3][7].setSize((themes[0][4].getWidth() + 5), (themes[0][4].getHeight() + 5));
        themes[3][7].setPosition(Gdx.graphics.getWidth() / 2 + Gdx.graphics.getWidth() * 3, Gdx.graphics.getHeight() - 220 * game.screenScale);
        //endregion

        //region PirateSkinTheme
        themes[4][0] = new Sprite(this.game.manager.get("data/JackyPirateFog.png", Texture.class));
        themes[4][0].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
        themes[4][0].setPosition(Gdx.graphics.getWidth() * 4, 0);

        themes[4][1] = new Sprite(this.game.manager.get("data/CanonBallAsteroid100.png", Texture.class));
        themes[4][1].setSize(themes[0][1].getWidth(), themes[0][1].getHeight());
        themes[4][1].setPosition(Gdx.graphics.getWidth() - 200 * game.screenScale + Gdx.graphics.getWidth() * 4, Gdx.graphics.getHeight() - 230 * game.screenScale);

        themes[4][2] = new Sprite(this.game.manager.get("data/CanonBallAsteroid100.png", Texture.class));
        themes[4][2].setSize(themes[0][2].getWidth(), themes[0][2].getHeight());
        themes[4][2].setPosition(150 * game.screenScale + Gdx.graphics.getWidth() * 4, Gdx.graphics.getHeight() - 400 * game.screenScale);

        themes[4][3] = new Sprite(this.game.manager.get("data/CanonBallAsteroid100.png", Texture.class));
        themes[4][3].setSize(themes[0][3].getWidth(), themes[0][3].getHeight());
        themes[4][3].setPosition(600 * game.screenScale + Gdx.graphics.getWidth() * 4, Gdx.graphics.getHeight() - 800 * game.screenScale);

        themes[4][4] = new Sprite(this.game.manager.get("data/CanonBallAsteroid100.png", Texture.class));
        themes[4][4].setSize(themes[0][4].getWidth(), themes[0][4].getHeight());
        themes[4][4].setPosition(Gdx.graphics.getWidth() / 2 + 350 * game.screenScale + Gdx.graphics.getWidth() * 4, Gdx.graphics.getHeight() / 2 - 300 * game.screenScale);

        themes[4][5] = new Sprite(this.game.manager.get("data/PirateEarth.png", Texture.class));
        themes[4][5].setSize(themes[4][5].getWidth() * planetScale, themes[4][5].getHeight() * planetScale);
        themes[4][5].setPosition(earthOceans.getX() - 30.5f * game.screenScale + Gdx.graphics.getWidth() * 4, earthOceans.getY());

        themes[4][6] = new Sprite(this.game.manager.get("data/MoonPirateSkin.png", Texture.class));
        themes[4][6].setSize(moonMajor.getWidth(), moonMajor.getHeight());
        themes[4][6].setPosition(moonMajor.getX() + Gdx.graphics.getWidth() * 4,  moonMajor.getY());

        themes[4][7] = new Sprite(this.game.manager.get("data/CanonBallAsteroid100.png", Texture.class));
        themes[4][7].setSize((themes[0][4].getWidth() + 5), (themes[0][4].getHeight() + 5));
        themes[4][7].setPosition(Gdx.graphics.getWidth() / 2 + Gdx.graphics.getWidth() * 4, Gdx.graphics.getHeight() - 220 * game.screenScale);
        //endregion

        //region SoccerTheme
        themes[5][0] = new Sprite(this.game.manager.get("data/SoccerField1920.png", Texture.class));
        themes[5][0].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
//        themes[5][0].setPosition(Gdx.graphics.getWidth() * 5, -themes[5][0].getHeight() / 4);

        if(game.prefs.getBoolean("16x9")) {
            themes[5][0].setPosition(Gdx.graphics.getWidth() * 5, -themes[5][0].getHeight() / 4 + 50 * game.screenScale);
        } else if(game.prefs.getBoolean("16x10")) {
            themes[5][0].setPosition(Gdx.graphics.getWidth() * 5, -themes[5][0].getHeight() / 4 + 120 * game.screenScale);
        } else if(game.prefs.getBoolean("4x3")) {
            themes[5][0].setPosition(Gdx.graphics.getWidth() * 5, -themes[5][0].getHeight() / 4 + 240 * game.screenScale);
        } else if(game.prefs.getBoolean("5x3")) {
            themes[5][0].setPosition(Gdx.graphics.getWidth() * 5, -themes[5][0].getHeight() / 4 + 100 * game.screenScale);
        } else if(game.prefs.getBoolean("3x2")) {
            themes[5][0].setPosition(Gdx.graphics.getWidth() * 5, -themes[5][0].getHeight() / 4 + 160 * game.screenScale);
        }

        themes[5][1] = new Sprite(this.game.manager.get("data/SoccerballAsteroid100.png", Texture.class));
        themes[5][1].setSize(themes[0][1].getWidth(), themes[0][1].getHeight());
        themes[5][1].setPosition(Gdx.graphics.getWidth() - 200 * game.screenScale + Gdx.graphics.getWidth() * 5, Gdx.graphics.getHeight() - 230 * game.screenScale);

        themes[5][2] = new Sprite(this.game.manager.get("data/SoccerballAsteroid100.png", Texture.class));
        themes[5][2].setSize(themes[0][2].getWidth(), themes[0][2].getHeight());
        themes[5][2].setPosition(150 * game.screenScale + Gdx.graphics.getWidth() * 5, Gdx.graphics.getHeight() - 400 * game.screenScale);

        themes[5][3] = new Sprite(this.game.manager.get("data/SoccerballAsteroid100.png", Texture.class));
        themes[5][3].setSize(themes[0][3].getWidth(), themes[0][3].getHeight());
        themes[5][3].setPosition(600 * game.screenScale + Gdx.graphics.getWidth() * 5, Gdx.graphics.getHeight() - 800 * game.screenScale);

        themes[5][4] = new Sprite(this.game.manager.get("data/SoccerballAsteroid100.png", Texture.class));
        themes[5][4].setSize(themes[0][4].getWidth(), themes[0][4].getHeight());
        themes[5][4].setPosition(Gdx.graphics.getWidth() / 2 + 350 * game.screenScale + Gdx.graphics.getWidth() * 5, Gdx.graphics.getHeight() / 2 - 300 * game.screenScale);

        themes[5][5] = new Sprite(this.game.manager.get("data/EarthSoccer.png", Texture.class));
        themes[5][5].setSize(themes[5][5].getWidth() * planetScale, themes[5][5].getHeight() * planetScale);
        themes[5][5].setPosition(earthOceans.getX() - 26f * game.screenScale + Gdx.graphics.getWidth() * 5, earthOceans.getY());

        themes[5][6] = new Sprite(this.game.manager.get("data/MoonReferee.png", Texture.class));
        themes[5][6].setSize((moonMajor.getHeight() / themes[5][6].getHeight()) * themes[5][6].getWidth(), moonMajor.getHeight());
        themes[5][6].setPosition(moonMajor.getX()  - 7f * game.screenScale + Gdx.graphics.getWidth() * 5,  moonMajor.getY());

        themes[5][7] = new Sprite(this.game.manager.get("data/SoccerballAsteroid100.png", Texture.class));
        themes[5][7].setSize((themes[0][4].getWidth() + 5), (themes[0][4].getHeight() + 5));
        themes[5][7].setPosition(Gdx.graphics.getWidth() / 2 + Gdx.graphics.getWidth() * 5, Gdx.graphics.getHeight() - 220 * game.screenScale);





        themes[6][0] = new Sprite(this.game.manager.get("data/SumoBackground.png", Texture.class));
        themes[6][0].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
//        themes[6][0].setPosition(Gdx.graphics.getWidth() * 6, -themes[6][0].getHeight() / 4);

        if(game.prefs.getBoolean("16x9")) {
            themes[6][0].setPosition(Gdx.graphics.getWidth() * 6, -themes[6][0].getHeight() / 4 + 50 * game.screenScale);
        } else if(game.prefs.getBoolean("16x10")) {
            themes[6][0].setPosition(Gdx.graphics.getWidth() * 6, -themes[6][0].getHeight() / 4 + 120 * game.screenScale);
        } else if(game.prefs.getBoolean("4x3")) {
            themes[6][0].setPosition(Gdx.graphics.getWidth() * 6, -themes[6][0].getHeight() / 4 + 240 * game.screenScale);
        } else if(game.prefs.getBoolean("5x3")) {
            themes[6][0].setPosition(Gdx.graphics.getWidth() * 6, -themes[6][0].getHeight() / 4 + 100 * game.screenScale);
        } else if(game.prefs.getBoolean("3x2")) {
            themes[6][0].setPosition(Gdx.graphics.getWidth() * 6, -themes[6][0].getHeight() / 4 + 160 * game.screenScale);
        }

        themes[6][1] = new Sprite(this.game.manager.get("data/SumoAsteroid.png", Texture.class));
        themes[6][1].setSize(themes[0][1].getWidth(), themes[0][1].getHeight());
        themes[6][1].setPosition(Gdx.graphics.getWidth() - 200 * game.screenScale + Gdx.graphics.getWidth() * 6, Gdx.graphics.getHeight() - 230 * game.screenScale);

        themes[6][2] = new Sprite(this.game.manager.get("data/SumoAsteroid.png", Texture.class));
        themes[6][2].setSize(themes[0][2].getWidth(), themes[0][2].getHeight());
        themes[6][2].setPosition(150 * game.screenScale + Gdx.graphics.getWidth() * 6, Gdx.graphics.getHeight() - 400 * game.screenScale);

        themes[6][3] = new Sprite(this.game.manager.get("data/SumoAsteroid.png", Texture.class));
        themes[6][3].setSize(themes[0][3].getWidth(), themes[0][3].getHeight());
        themes[6][3].setPosition(600 * game.screenScale + Gdx.graphics.getWidth() * 6, Gdx.graphics.getHeight() - 800 * game.screenScale);

        themes[6][4] = new Sprite(this.game.manager.get("data/SumoAsteroid.png", Texture.class));
        themes[6][4].setSize(themes[0][4].getWidth(), themes[0][4].getHeight());
        themes[6][4].setPosition(Gdx.graphics.getWidth() / 2 + 350 * game.screenScale + Gdx.graphics.getWidth() * 6, Gdx.graphics.getHeight() / 2 - 300 * game.screenScale);

        themes[6][5] = new Sprite(this.game.manager.get("data/SumoEarth.png", Texture.class));
        themes[6][5].setSize(themes[6][5].getWidth() * planetScale, themes[6][5].getHeight() * planetScale);
        themes[6][5].setPosition(earthOceans.getX() - 1.75f * game.screenScale + Gdx.graphics.getWidth() * 6, earthOceans.getY());

        themes[6][6] = new Sprite(this.game.manager.get("data/SumoMoon.png", Texture.class));
        themes[6][6].setSize((moonMajor.getHeight() / themes[6][6].getHeight()) * themes[6][6].getWidth(), moonMajor.getHeight());
        themes[6][6].setPosition(moonMajor.getX() + Gdx.graphics.getWidth() * 6,  moonMajor.getY());

        themes[6][7] = new Sprite(this.game.manager.get("data/SumoAsteroid.png", Texture.class));
        themes[6][7].setSize((themes[0][4].getWidth() + 5), (themes[0][4].getHeight() + 5));
        themes[6][7].setPosition(Gdx.graphics.getWidth() / 2 + Gdx.graphics.getWidth() * 6, Gdx.graphics.getHeight() - 220 * game.screenScale);


        //endregion

        whitePixel = new Sprite(game.manager.get("data/WhitePixel.png", Texture.class));
        whitePixel.setSize(Gdx.graphics.getWidth(), 80 * game.screenScale);
        whitePixel.setPosition(0, Gdx.graphics.getHeight() - 150 * game.screenScale);
        whitePixel.setColor(Color.MAROON);
        whitePixel.setAlpha(.75f);

        activeThemeIndex = 0;

        activeTheme = new boolean[themes.length];
        for (int i = 0; i < activeTheme.length; i++) {
            if (i == activeThemeIndex) {
                activeTheme[i] = true;
            } else {
                activeTheme[i] = false;
            }
        }

        rightScrollButton = new GameButtons(game.manager.get("data/SideMenuButton.png", Texture.class), true, new Vector2().setZero());
        rightScrollButton.setSize(rightScrollButton.getWidth() * game.screenScale, rightScrollButton.getHeight() * game.screenScale);
        rightScrollButton.setOriginCenter();
        rightScrollButton.setPosition(Gdx.graphics.getWidth() - rightScrollButton.getWidth() * 1.5f, Gdx.graphics.getHeight() / 2 - rightScrollButton.getOriginY());
//        rightScrollButton.setColor(.28f, .43f, .68f, 1);

        leftScrollButton = new GameButtons(game.manager.get("data/SideMenuButton.png", Texture.class), true, new Vector2().setZero());
        leftScrollButton.setSize(leftScrollButton.getWidth() * game.screenScale, leftScrollButton.getHeight() * game.screenScale);
        leftScrollButton.setOriginCenter();
        leftScrollButton.setRotation(180);
        leftScrollButton.setPosition(leftScrollButton.getWidth() * .5f, rightScrollButton.getY());
//        leftScrollButton.setColor(.28f, .43f, .68f, 1);

        coinTotalSprite = new Sprite(game.manager.get("data/CoinLarge.png", Texture.class));
        coinTotalSprite.setSize(coinTotalSprite.getWidth() * .15f * game.screenScale, coinTotalSprite.getHeight() * .15f * game.screenScale);
        coinTotalSprite.setPosition(Gdx.graphics.getWidth() / 2  + 120 * game.screenScale, earthOceans.getY() - 128.5f * game.screenScale);

        screenScrolling = false;
        targetThemeIndex = activeThemeIndex;

        backButton = new GameButtons(purchaseButton.getTexture(), true, new Vector2().setZero());
        backButton.setSize(backButton.getWidth() * game.screenScale, backButton.getHeight() * game.screenScale);
        backButton.setOriginCenter();
        backButton.setPosition(Gdx.graphics.getWidth() - backButton.getWidth() - backButton.getOriginX() / 2, backButton.getHeight() / 2);
        backButton.setColor(Color.WHITE);

        purchaseButton.setPosition(purchaseButton.getX(), backButton.getY());

        themeCosts = new int[]{500, 1000, 1000, 1000, 1000, 1000};

        whitePixelBottom = new Sprite(game.manager.get("data/WhitePixel.png", Texture.class));
        whitePixelBottom.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 5f);
        whitePixelBottom.setPosition(0, 0);
        whitePixelBottom.setColor(Color.MAROON);
        whitePixelBottom.setAlpha(.75f);

        grayOverlay = new Sprite(game.manager.get("data/EmptyCircleGray.png", Texture.class));
        grayOverlay.setSize(earthOceans.getWidth(), earthOceans.getHeight());
        grayOverlay.setOriginCenter();
        grayOverlay.setPosition(earthOceans.getX(), earthOceans.getY());
        grayOverlay.setAlpha(.35f);

        grayOverlayMoon = new Sprite(game.manager.get("data/EmptyCircleGray.png", Texture.class));
        grayOverlayMoon.setSize(moonMajor.getWidth(), moonMajor.getHeight());
        grayOverlayMoon.setOriginCenter();
        grayOverlayMoon.setPosition(moonMajor.getX(), moonMajor.getY());
        grayOverlayMoon.setAlpha(.35f);

        game.handler.showAds(false);
    }

    @Override
    public void render(float delta) {

        purchaseButton.Update();
        if(!screenScrolling) {
            backButton.Update();
            if(backButton.Tapped() || game.BackPressed()) {
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                }
//                if(!game.prefs.getBoolean("CatSkin")) {
//                    game.manager.unload("data/MouseMeanSmall.png");
//                    game.manager.unload("data/CatSkinProto.png");
//                    game.manager.unload("data/CatBackground.png");
////                    game.manager.unload("data/YarnBallExperimentGreenless1920.png");
//                }
                if(!game.prefs.getBoolean("PirateSkin")) {
                    game.manager.unload("data/CanonBallAsteroid100.png");
                    game.manager.unload("data/CanonBallShine.png");
                    game.manager.unload("data/MoonPirateSkin.png");
                    game.manager.unload("data/PirateEarth.png");
                    game.manager.unload("data/JackyPirateFog.png");
                }
                if(!game.prefs.getBoolean("SoccerSkin")) {
                    game.manager.unload("data/EarthSoccer.png");
                    game.manager.unload("data/MoonReferee.png");
                    game.manager.unload("data/SoccerballAsteroid100.png");
                    game.manager.unload("data/SoccerField1920.png");
                }
                if(!game.prefs.getBoolean("SumoSkin")) {
                    game.manager.unload("data/SumoBackground.png");
                    game.manager.unload("data/SumoEarth.png");
                    game.manager.unload("data/SumoMoon.png");
                    game.manager.unload("data/SumoAsteroid.png");
                }
                game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
                game.setScreen(new CustomizePortal(game));
                this.dispose();
            }
        }
        if(!activeTheme[0] || (!screenScrolling && !activeTheme[0])) {
            leftScrollButton.Update();
        }
        if(!activeTheme[activeTheme.length - 1] || (!screenScrolling && !activeTheme[activeTheme.length - 1])) {
            rightScrollButton.Update();
        }

        if(activeThemeIndex == 3 || activeThemeIndex == 4 && leftScrollButton.getColor() != Color.BLACK)
        {
            leftScrollButton.setColor(Color.BLACK);
            rightScrollButton.setColor(Color.BLACK);
        } else if (leftScrollButton.getColor() != Color.WHITE){
            leftScrollButton.setColor(Color.WHITE);
            rightScrollButton.setColor(Color.WHITE);
        }

        if(rightScrollButton.Tapped() && targetThemeIndex == activeThemeIndex && targetThemeIndex != themes.length - 1) {
            targetThemeIndex++;
            screenScrolling = true;
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
        } else if(leftScrollButton.Tapped() && targetThemeIndex == activeThemeIndex && targetThemeIndex != 0) {
            targetThemeIndex--;
            screenScrolling = true;
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
        }

        //region ScrollingLogic
        if(screenScrolling && targetThemeIndex > activeThemeIndex) {
            for(int i = 0; i < themes.length; i++)
            {
                for(int j = 0; j < themes[i].length; j++)
                {
                    if(themes[i][j] != null) {
                        themes[i][j].setPosition(themes[i][j].getX() - 130 * game.screenScale, themes[i][j].getY());
                    }
                }
            }
            if(themes[0][0].getX() <= -Gdx.graphics.getWidth() * targetThemeIndex) {
                float locationDifference = themes[0][0].getX() - (-Gdx.graphics.getWidth() * targetThemeIndex);
                for(int i = 0; i < themes.length; i++)
                {
                    for(int j = 0; j < themes[i].length; j++)
                    {
                        if(themes[i][j] != null) {
                            themes[i][j].setPosition(themes[i][j].getX() + Math.abs(locationDifference), themes[i][j].getY());
                        }
                    }
                }
                screenScrolling = false;
                activeThemeIndex = targetThemeIndex;
                for (int i = 0; i < activeTheme.length; i++) {
                    if (i == activeThemeIndex) {
                        activeTheme[i] = true;
                    } else {
                        activeTheme[i] = false;
                    }
                }
            }
        } else if(screenScrolling && targetThemeIndex < activeThemeIndex) {
            for(int i = 0; i < themes.length; i++)
            {
                for(int j = 0; j < themes[i].length; j++)
                {
                    if(themes[i][j] != null) {
                        themes[i][j].setPosition(themes[i][j].getX() + 130 * game.screenScale, themes[i][j].getY());
                    }
                }
            }
            if(themes[0][0].getX() >= -Gdx.graphics.getWidth() * targetThemeIndex) {
                float locationDifference = themes[0][0].getX() - (-Gdx.graphics.getWidth() * targetThemeIndex);
                for(int i = 0; i < themes.length; i++)
                {
                    for(int j = 0; j < themes[i].length; j++)
                    {
                        if(themes[i][j] != null) {
                            themes[i][j].setPosition(themes[i][j].getX() - Math.abs(locationDifference), themes[i][j].getY());
                        }
                    }
                }
                screenScrolling = false;
                activeThemeIndex = targetThemeIndex;
                for (int i = 0; i < activeTheme.length; i++) {
                    if (i == activeThemeIndex) {
                        activeTheme[i] = true;
                    } else {
                        activeTheme[i] = false;
                    }
                }
            }
        }
        //endregion

        //region PurchaseButtonColorChange
        if(activeTheme[0]) {
            if(game.prefs.getBoolean("DefaultSkin")) {
                purchaseButton.setColor(.25f, .43f, .25f, 1);
            } else {
                purchaseButton.setColor(.28f, .43f, .68f, 1);
            }
        } else if(activeTheme[1]) {
            if(!game.prefs.getBoolean("PixelSkinUnlocked") && (game.prefs.getInteger("Coins") < themeCosts[activeThemeIndex - 1])) {
                purchaseButton.setColor(Color.YELLOW);
            } else if (!game.prefs.getBoolean("PixelSkinUnlocked")){
                purchaseButton.setColor(Color.GREEN);
            } else if(game.prefs.getBoolean("PixelSkin")) {
                purchaseButton.setColor(.25f, .43f, .25f, 1);
            } else {
                purchaseButton.setColor(.28f, .43f, .68f, 1);
            }
        } else if(activeTheme[2]) {
            if(!game.prefs.getBoolean("CatSkinUnlocked") && (game.prefs.getInteger("Coins") < themeCosts[activeThemeIndex - 1])) {
                purchaseButton.setColor(Color.YELLOW);
            } else if(!game.prefs.getBoolean("CatSkinUnlocked")) {
                purchaseButton.setColor(Color.GREEN);
            } else if(game.prefs.getBoolean("CatSkin")) {
                purchaseButton.setColor(.25f, .43f, .25f, 1);
            } else {
                purchaseButton.setColor(.28f, .43f, .68f, 1);
            }
        } else if(activeTheme[3]) {
            if(!game.prefs.getBoolean("NinjaSkinUnlocked") && (game.prefs.getInteger("Coins") < themeCosts[activeThemeIndex - 1])) {
                purchaseButton.setColor(Color.YELLOW);
            } else if(!game.prefs.getBoolean("NinjaSkinUnlocked")) {
                purchaseButton.setColor(Color.GREEN);
            } else if(game.prefs.getBoolean("NinjaSkin")) {
                purchaseButton.setColor(.25f, .43f, .25f, 1);
            } else {
                purchaseButton.setColor(.28f, .43f, .68f, 1);
            }
        } else if(activeTheme[4]) {
            if(!game.prefs.getBoolean("PirateSkinUnlocked") && (game.prefs.getInteger("Coins") < themeCosts[activeThemeIndex - 1])) {
                purchaseButton.setColor(Color.YELLOW);
            } else if(!game.prefs.getBoolean("PirateSkinUnlocked")) {
                purchaseButton.setColor(Color.GREEN);
            } else if(game.prefs.getBoolean("PirateSkin")) {
                purchaseButton.setColor(.25f, .43f, .25f, 1);
            } else {
                purchaseButton.setColor(.28f, .43f, .68f, 1);
            }
        } else if(activeTheme[5]) {
            if(!game.prefs.getBoolean("SoccerSkinUnlocked") && (game.prefs.getInteger("Coins") < themeCosts[activeThemeIndex - 1])) {
                purchaseButton.setColor(Color.YELLOW);
            } else if(!game.prefs.getBoolean("SoccerSkinUnlocked")) {
                purchaseButton.setColor(Color.GREEN);
            } else if(game.prefs.getBoolean("SoccerSkin")) {
                purchaseButton.setColor(.25f, .43f, .25f, 1);
            } else {
                purchaseButton.setColor(.28f, .43f, .68f, 1);
            }
        } else if(activeTheme[6]) {
            if(!game.prefs.getBoolean("SumoSkinUnlocked") && (game.prefs.getInteger("Coins") < themeCosts[activeThemeIndex - 1])) {
                purchaseButton.setColor(Color.YELLOW);
            } else if(!game.prefs.getBoolean("SumoSkinUnlocked")) {
                purchaseButton.setColor(Color.GREEN);
            } else if(game.prefs.getBoolean("SumoSkin")) {
                purchaseButton.setColor(.25f, .43f, .25f, 1);
            } else {
                purchaseButton.setColor(.28f, .43f, .68f, 1);
            }
        }
        //endregion

        //region PurchaseButtonTap
        if(purchaseButton.Tapped() && !screenScrolling) {
            if(activeTheme[0]) {
                if(!game.prefs.getBoolean("DefaultSkin")) {
                    game.prefs.putBoolean("DefaultSkin", true);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            } else if(activeTheme[1]) {
                if(!game.prefs.getBoolean("PixelSkinUnlocked") && game.prefs.getInteger("Coins") >= themeCosts[activeThemeIndex - 1]) {
                    game.prefs.putBoolean("PixelSkinUnlocked", true);
                    game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") - themeCosts[activeThemeIndex - 1]);
                    game.prefs.flush();
                    if(game.prefs.getBoolean("Sound")) {
                        game.cashRegisterSound.play(.45f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", true);
                    game.prefs.putBoolean("PixelSkin", true);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                } else if (!game.prefs.getBoolean("PixelSkinUnlocked")) {
                    if(game.prefs.getBoolean("Sound")) {
                        game.errorSound.play(.75f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                } else if(!game.prefs.getBoolean("PixelSkin")) {
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", true);
                    game.prefs.putBoolean("PixelSkin", true);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            } else if(activeTheme[2]) {
                if(!game.prefs.getBoolean("CatSkinUnlocked") && game.prefs.getInteger("Coins") >= themeCosts[activeThemeIndex - 1]) {
                    game.prefs.putBoolean("CatSkinUnlocked", true);
                    game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") - themeCosts[activeThemeIndex - 1]);
                    game.prefs.flush();
                    if(game.prefs.getBoolean("Sound")) {
                        game.cashRegisterSound.play(.45f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", true);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                } else if (!game.prefs.getBoolean("CatSkinUnlocked")) {
                    if(game.prefs.getBoolean("Sound")) {
                        game.errorSound.play(.75f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                } else if(!game.prefs.getBoolean("CatSkin")) {
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", true);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            } else if(activeTheme[3]) {
                if(!game.prefs.getBoolean("NinjaSkinUnlocked") && game.prefs.getInteger("Coins") >= themeCosts[activeThemeIndex - 1]) {
                    game.prefs.putBoolean("NinjaSkinUnlocked", true);
                    game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") - themeCosts[activeThemeIndex - 1]);
                    game.prefs.flush();
                    if(game.prefs.getBoolean("Sound")) {
                        game.cashRegisterSound.play(.45f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", true);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                } else if (!game.prefs.getBoolean("NinjaSkinUnlocked")) {
                    if(game.prefs.getBoolean("Sound")) {
                        game.errorSound.play(.75f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                } else if(!game.prefs.getBoolean("NinjaSkin")) {
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", true);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            } else if(activeTheme[4]) {
                if(!game.prefs.getBoolean("PirateSkinUnlocked") && game.prefs.getInteger("Coins") >= themeCosts[activeThemeIndex - 1]) {
                    game.prefs.putBoolean("PirateSkinUnlocked", true);
                    game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") - themeCosts[activeThemeIndex - 1]);
                    game.prefs.flush();
                    if(game.prefs.getBoolean("Sound")) {
                        game.cashRegisterSound.play(.45f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", true);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                } else if (!game.prefs.getBoolean("PirateSkinUnlocked")) {
                    if(game.prefs.getBoolean("Sound")) {
                        game.errorSound.play(.75f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                } else if(!game.prefs.getBoolean("PirateSkin")) {
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", true);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            } else if(activeTheme[5]) {
                if(!game.prefs.getBoolean("SoccerSkinUnlocked") && game.prefs.getInteger("Coins") >= themeCosts[activeThemeIndex - 1]) {
                    game.prefs.putBoolean("SoccerSkinUnlocked", true);
                    game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") - themeCosts[activeThemeIndex - 1]);
                    game.prefs.flush();
                    if(game.prefs.getBoolean("Sound")) {
                        game.cashRegisterSound.play(.45f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", true);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                } else if (!game.prefs.getBoolean("SoccerSkinUnlocked")) {
                    if(game.prefs.getBoolean("Sound")) {
                        game.errorSound.play(.75f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                } else if(!game.prefs.getBoolean("SoccerSkin")) {
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", true);
                    game.prefs.putBoolean("SumoSkin", false);
                    game.prefs.flush();
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            } else if(activeTheme[6]) {
                if(!game.prefs.getBoolean("SumoSkinUnlocked") && game.prefs.getInteger("Coins") >= themeCosts[activeThemeIndex - 1]) {
                    game.prefs.putBoolean("SumoSkinUnlocked", true);
                    game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") - themeCosts[activeThemeIndex - 1]);
                    game.prefs.flush();
                    if(game.prefs.getBoolean("Sound")) {
                        game.cashRegisterSound.play(.45f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", true);
                    game.prefs.flush();
                } else if (!game.prefs.getBoolean("SumoSkinUnlocked")) {
                    if(game.prefs.getBoolean("Sound")) {
                        game.errorSound.play(.75f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                } else if(!game.prefs.getBoolean("SumoSkin")) {
                    game.prefs.putBoolean("DefaultSkin", false);
                    game.prefs.putBoolean("CatSkin", false);
                    game.prefs.putBoolean("NinjaSkin", false);
                    game.prefs.putBoolean("PixelSkin", false);
                    game.prefs.putBoolean("PirateSkin", false);
                    game.prefs.putBoolean("SoccerSkin", false);
                    game.prefs.putBoolean("SumoSkin", true);
                    game.prefs.flush();
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            }
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
        }
        //endregion

        //region Draw
        game.batch.begin();
        for (int i = 0; i < themes.length; i++) {
            for (int j = 0; j < themes[i].length; j++) {
                if (themes[i][j] != null) {
                    themes[i][j].draw(game.batch);
                }
            }
        }
        earthOceans.draw(game.batch);
        earthContinents.draw(game.batch);
        grayOverlay.draw(game.batch);
        moonMajor.draw(game.batch);
        moonMinor.draw(game.batch);
        grayOverlayMoon.draw(game.batch);
        whitePixel.draw(game.batch);

        if(activeThemeIndex != 0 || (activeThemeIndex == 0 && targetThemeIndex != 0)) {
            if(activeThemeIndex != targetThemeIndex && targetThemeIndex != 0) {
                themes[targetThemeIndex][5].draw(game.batch);
                if(themes[targetThemeIndex][6] != null) {
                    themes[targetThemeIndex][6].draw(game.batch);
                }
                if(themes[targetThemeIndex][8] != null) {
                    themes[targetThemeIndex][8].draw(game.batch);
                }
            } else {
                themes[activeThemeIndex][5].draw(game.batch);
                if(themes[activeThemeIndex][6] != null) {
                    themes[activeThemeIndex][6].draw(game.batch);
                }
                if(themes[targetThemeIndex][8] != null) {
                    themes[targetThemeIndex][8].draw(game.batch);
                }
            }
        }
        if(!activeTheme[0]) {
            leftScrollButton.draw(game.batch);
        }
        if(!activeTheme[activeTheme.length - 1]) {
            rightScrollButton.draw(game.batch);
        }
        whitePixelBottom.draw(game.batch);
        backButton.draw(game.batch);
        purchaseButton.draw(game.batch);
        TextDrawing();
        game.batch.end();
        //endregion
    }

    private void TextDrawing() {
        //region ThemeDrawing
        String themeText = "";
        String buttonText = "";
        if (activeThemeIndex == 0) {
            themeText = "Default Theme";
            goodTimeFont.setScale(game.screenScale * .55f);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, themeText, Gdx.graphics.getWidth() / 2 - themeText.length() * 30f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
            if(game.prefs.getBoolean("DefaultSkin")) {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Active", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            } else {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Select", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            }
        } else if (activeThemeIndex == 1) {
            themeText = "ThrowBack Theme";
            goodTimeFont.setScale(game.screenScale * .55f);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, themeText, Gdx.graphics.getWidth() / 2 - themeText.length() * 35f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
            if(game.prefs.getBoolean("PixelSkin")) {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Active", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            } else if(!game.prefs.getBoolean("PixelSkinUnlocked")){
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Buy", purchaseButton.getX() + 100f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
                goodTimeFont.setScale(game.screenScale * .25f);
                goodTimeFont.draw(game.batch, "Cost: 500", purchaseButton.getOriginX(), purchaseButton.getY() + 100 * game.screenScale);
                String currentUserTotal = String.format("Bank: %d", game.prefs.getInteger("Coins"));
//                String currentUserTotal = "Bank: 1050";
                goodTimeFont.draw(game.batch, currentUserTotal, purchaseButton.getOriginX(), purchaseButton.getY() + 50 * game.screenScale);
                coinTotalSprite.setPosition(purchaseButton.getOriginX() - coinTotalSprite.getWidth() - 10 * game.screenScale, purchaseButton.getY() + 22 * game.screenScale);
                coinTotalSprite.draw(game.batch);
            } else {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Select", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            }
        } else if (activeThemeIndex == 2) {
            themeText = "Cat Theme";
            goodTimeFont.setScale(game.screenScale * .55f);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, themeText, Gdx.graphics.getWidth() / 2 - themeText.length() * 34f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
            if(game.prefs.getBoolean("CatSkin")) {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Active", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            } else if(!game.prefs.getBoolean("CatSkinUnlocked")){
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Buy", purchaseButton.getX() + 100f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
                goodTimeFont.setScale(game.screenScale * .25f);
                goodTimeFont.draw(game.batch, "Cost: 1000", purchaseButton.getOriginX(), purchaseButton.getY() + 100 * game.screenScale);
                String currentUserTotal = String.format("Bank: %d", game.prefs.getInteger("Coins"));
//                String currentUserTotal = "Bank: 1050";
                goodTimeFont.draw(game.batch, currentUserTotal, purchaseButton.getOriginX(), purchaseButton.getY() + 50 * game.screenScale);
                coinTotalSprite.setPosition(purchaseButton.getOriginX() - coinTotalSprite.getWidth() - 10 * game.screenScale, purchaseButton.getY() + 22 * game.screenScale);
                coinTotalSprite.draw(game.batch);
            } else {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Select", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            }
        } else if (activeThemeIndex == 3) {
            themeText = "Ninja Theme";
            goodTimeFont.setScale(game.screenScale * .55f);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, themeText, Gdx.graphics.getWidth() / 2 - themeText.length() * 32f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
            if(game.prefs.getBoolean("NinjaSkin")) {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Active", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            } else if(!game.prefs.getBoolean("NinjaSkinUnlocked")){
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Buy", purchaseButton.getX() + 100f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
                goodTimeFont.setScale(game.screenScale * .25f);
                goodTimeFont.draw(game.batch, "Cost: 1000", purchaseButton.getOriginX(), purchaseButton.getY() + 100 * game.screenScale);
                String currentUserTotal = String.format("Bank: %d", game.prefs.getInteger("Coins"));
//                String currentUserTotal = "Bank: 1050";
                goodTimeFont.draw(game.batch, currentUserTotal, purchaseButton.getOriginX(), purchaseButton.getY() + 50 * game.screenScale);
                coinTotalSprite.setPosition(purchaseButton.getOriginX() - coinTotalSprite.getWidth() - 10 * game.screenScale, purchaseButton.getY() + 22 * game.screenScale);
                coinTotalSprite.draw(game.batch);
            } else {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Select", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            }
        } else if (activeThemeIndex == 4) {
            themeText = "Pirate Theme";
            goodTimeFont.setScale(game.screenScale * .55f);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, themeText, Gdx.graphics.getWidth() / 2 - themeText.length() * 30f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
            if(game.prefs.getBoolean("PirateSkin")) {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Active", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            } else if(!game.prefs.getBoolean("PirateSkinUnlocked")){
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Buy", purchaseButton.getX() + 100f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
                goodTimeFont.setScale(game.screenScale * .25f);
                goodTimeFont.draw(game.batch, "Cost: 1000", purchaseButton.getOriginX(), purchaseButton.getY() + 100 * game.screenScale);
                String currentUserTotal = String.format("Bank: %d", game.prefs.getInteger("Coins"));
//                String currentUserTotal = "Bank: 1050";
                goodTimeFont.draw(game.batch, currentUserTotal, purchaseButton.getOriginX(), purchaseButton.getY() + 50 * game.screenScale);
                coinTotalSprite.setPosition(purchaseButton.getOriginX() - coinTotalSprite.getWidth() - 10 * game.screenScale, purchaseButton.getY() + 22 * game.screenScale);
                coinTotalSprite.draw(game.batch);
            } else {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Select", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            }
        } else if (activeThemeIndex == 5) {
            themeText = "Soccer Theme";
            goodTimeFont.setScale(game.screenScale * .55f);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, themeText, Gdx.graphics.getWidth() / 2 - themeText.length() * 30f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
            if(game.prefs.getBoolean("SoccerSkin")) {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Active", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            } else if(!game.prefs.getBoolean("SoccerSkinUnlocked")){
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Buy", purchaseButton.getX() + 100f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
                goodTimeFont.setScale(game.screenScale * .25f);
                goodTimeFont.draw(game.batch, "Cost: 1000", purchaseButton.getOriginX(), purchaseButton.getY() + 100 * game.screenScale);
                String currentUserTotal = String.format("Bank: %d", game.prefs.getInteger("Coins"));
//                String currentUserTotal = "Bank: 1050";
                goodTimeFont.draw(game.batch, currentUserTotal, purchaseButton.getOriginX(), purchaseButton.getY() + 50 * game.screenScale);
                coinTotalSprite.setPosition(purchaseButton.getOriginX() - coinTotalSprite.getWidth() - 10 * game.screenScale, purchaseButton.getY() + 22 * game.screenScale);
                coinTotalSprite.draw(game.batch);
            } else {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Select", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            }
        } else if (activeThemeIndex == 6) {
            themeText = "Sumo Theme";
            goodTimeFont.setScale(game.screenScale * .55f);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, themeText, Gdx.graphics.getWidth() / 2 - themeText.length() * 34f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
            if(game.prefs.getBoolean("SumoSkin")) {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Active", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            } else if(!game.prefs.getBoolean("SumoSkinUnlocked")){
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Buy", purchaseButton.getX() + 100f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
                goodTimeFont.setScale(game.screenScale * .25f);
                goodTimeFont.draw(game.batch, "Cost: 1000", purchaseButton.getOriginX(), purchaseButton.getY() + 100 * game.screenScale);
                String currentUserTotal = String.format("Bank: %d", game.prefs.getInteger("Coins"));
//                String currentUserTotal = "Bank: 1050";
                goodTimeFont.draw(game.batch, currentUserTotal, purchaseButton.getOriginX(), purchaseButton.getY() + 50 * game.screenScale);
                coinTotalSprite.setPosition(purchaseButton.getOriginX() - coinTotalSprite.getWidth() - 10 * game.screenScale, purchaseButton.getY() + 22 * game.screenScale);
                coinTotalSprite.draw(game.batch);
            } else {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Select", purchaseButton.getX() + 40f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
            }
        }
        //endregion
        goodTimeFont.setScale(game.screenScale * .55f);
        goodTimeFont.setColor(Color.WHITE);
        goodTimeFont.draw(game.batch, "Back", backButton.getX() + 29 * game.screenScale, backButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

}
