package com.AndroidGameAlpha.game;

import android.gesture.Gesture;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rene__000 on 8/21/2014.
 */
public class Planet extends Sprite {

    //region Variables
    private int _health;
    public int GetHealth()
    {
        return _health;
    }
    public void SetHealth(int health)
    {
        _health = health;
    }
    private float _rotationContinuance;
    private float _frameDuration;
    private float _movementSpeed;
    private float _currentAngle;
    private float _previousAngle;
    private float _calibrationX;
    private float _previousAccelerometerX;
    private float _currentAccelerometerX;
    private float _hitTimer;
    private float _elapsedHitTimer;
    private float _additionalAcceleromterXNumber;
    private float _hitSpriteAlpha;
    private float _alphaSpeed;
    private float _rotationSpeed;
    private Vector2 _currentDistanceBetweenFingers;
    private Vector2 _previousDistanceBetweenFingers;

    private Vector2 _distanceBetweenPlanets;
    public Vector2 DistanceBetweenPlanets(){
        return _distanceBetweenPlanets;
    }
    private Vector2 _radius;
    public void SetRadius()
    {
        _radius = new Vector2(0, this.getOriginY());
    }
    public Vector2 GetRadius()
    {
        return _radius;
    }
    private Animation _planetAnimation;
    private Animation.PlayMode _playMode;
    private TextureRegion _currentFrame;
    private TextureAtlas _planetAtlas;
    private Sprite _hitSprite;
    public Sprite HitSprite(){return _hitSprite;}
    private boolean _enableSwipeRotation;
    private boolean _draggable;
    private boolean _parentPlanet;
    private boolean _firstUpdate;
    private boolean _beingRotated;
    private boolean _beingDragged;
    private boolean _additionalAccelerometerX;
    private boolean _accelerometerMovement;
    private boolean _isHit;
    public boolean IsHit()
    {
        return _isHit;
    }
    public void SetIsHit(boolean isHit)
    {
        _isHit = isHit;
    }
    private Gestures _currentTouchState;
    private Gestures _previousTouchState;
    //endregion

    public Planet(TextureRegion texture, boolean enableSwipeRotation, float rotationSpeed, boolean draggable, boolean parentPlanet, boolean accelerometerMovement, float distanceBetweenPlanets, float movementSpeed, float calibrationX, Texture hitTexture) {
        super(texture);
        _enableSwipeRotation = enableSwipeRotation;
        _rotationSpeed = rotationSpeed;
        _draggable = draggable;
        _parentPlanet = parentPlanet;
        _distanceBetweenPlanets = new Vector2(0, distanceBetweenPlanets);
        _firstUpdate = true;
        _movementSpeed = movementSpeed;
        _beingRotated = false;
        _beingDragged = false;
        this.setOriginCenter();
        _currentDistanceBetweenFingers = new Vector2().setZero();
        _previousDistanceBetweenFingers = new Vector2().setZero();
        _radius = new Vector2(0, this.getHeight() - this.getOriginY());
        _calibrationX = calibrationX;
        _additionalAccelerometerX = false;
        _accelerometerMovement = accelerometerMovement;
        _health = 3;
        _hitSprite = new Sprite(hitTexture);
        _hitSprite.setSize(this.getWidth(), this.getHeight());
        _hitSprite.setPosition(this.getX(), this.getY());
        _hitTimer = 1f;
        _elapsedHitTimer = 0;
        _hitSpriteAlpha = 1.0f;
        _alphaSpeed = .05f;
    }

    public Planet(Texture texture, boolean enableSwipeRotation, float rotationSpeed, boolean draggable, boolean parentPlanet, boolean accelerometerMovement, float distanceBetweenPlanets, float movementSpeed, float calibrationX, Texture hitTexture) {
        super(texture);
        _enableSwipeRotation = enableSwipeRotation;
        _rotationSpeed = rotationSpeed;
        _draggable = draggable;
        _parentPlanet = parentPlanet;
        _distanceBetweenPlanets = new Vector2(0, distanceBetweenPlanets);
        _firstUpdate = true;
        _movementSpeed = movementSpeed;
        _beingRotated = false;
        _beingDragged = false;
        this.setOriginCenter();
        _currentDistanceBetweenFingers = new Vector2().setZero();
        _previousDistanceBetweenFingers = new Vector2().setZero();
        _radius = new Vector2(0, this.getHeight() - this.getOriginY());
        _calibrationX = calibrationX;
        _additionalAccelerometerX = false;
        _accelerometerMovement = accelerometerMovement;
        _health = 3;
        _hitSprite = new Sprite(hitTexture);
        _hitSprite.setSize(this.getWidth(), this.getHeight());
        _hitSprite.setPosition(this.getX(), this.getY());
        _hitSprite.setColor(Color.RED);
        _hitTimer = 1f;
        _elapsedHitTimer = 0;
        _hitSpriteAlpha = 1.0f;
        _alphaSpeed = .05f;
    }

    public Planet(Animation.PlayMode playMode, float frameDuration, TextureAtlas planetAtlas, boolean enableSwipeRotation, float rotationSpeed, boolean draggable, boolean parentPlanet, boolean accelerometerMovement, float distanceBetweenPlanets, float movementSpeed, float calibrationX, Texture hitTexture) {

        _playMode = playMode;
        _frameDuration = frameDuration;
        _planetAtlas = planetAtlas;
        _planetAnimation = new Animation(_frameDuration, _planetAtlas.getRegions(), _playMode);
        _enableSwipeRotation = enableSwipeRotation;
        _rotationSpeed = rotationSpeed;
        _draggable = draggable;
        _parentPlanet = parentPlanet;
        _distanceBetweenPlanets = new Vector2(0, distanceBetweenPlanets);
        _firstUpdate = true;
        _movementSpeed = movementSpeed;
        _beingRotated = false;
        _beingDragged = false;
        _currentFrame = _planetAnimation.getKeyFrame(0, true);
        this.setRegion(_currentFrame);
        this.setColor(1, 1, 1, 1);
        this.setSize(_currentFrame.getRegionWidth(), _currentFrame.getRegionHeight());
        this.setOrigin(this.getWidth() / 2, this.getHeight() / 2);
        _currentDistanceBetweenFingers = new Vector2().setZero();
        _previousDistanceBetweenFingers = new Vector2().setZero();
        _radius = new Vector2(0, this.getHeight() - this.getOriginY());
        _calibrationX = calibrationX;
        _additionalAccelerometerX = false;
        _accelerometerMovement = accelerometerMovement;
        _health = 3;
        _hitSprite = new Sprite(hitTexture);
        _hitSprite.setSize(this.getWidth(), this.getHeight());
        _hitSprite.setPosition(this.getX(), this.getY());
        _hitTimer = 1f;
        _elapsedHitTimer = 0;
        _hitSpriteAlpha = 1.0f;
        _alphaSpeed = .05f;
    }

    public void Update(float deltaTime, float cameraZoom, Planet planet, Gestures currentTouchState, Gestures previousTouchState, Vector2 currentDistanceBetweenFingers, Vector2 previousDistanceBetweenFingers, boolean accelControls) {

        _currentTouchState = currentTouchState;
        _previousTouchState = previousTouchState;
        _currentAccelerometerX = Gdx.input.getAccelerometerX();
        if (_currentTouchState == Gestures.ThreeFingers || _currentTouchState == Gestures.TwoFingers) {
            _currentDistanceBetweenFingers.x = currentDistanceBetweenFingers.x;
            _currentDistanceBetweenFingers.y = currentDistanceBetweenFingers.y;
            _previousDistanceBetweenFingers.x = previousDistanceBetweenFingers.x;
            _previousDistanceBetweenFingers.y = previousDistanceBetweenFingers.y;
        }

        if (_planetAnimation != null) {
            if(_parentPlanet) {
                _currentFrame = _planetAnimation.getKeyFrame(deltaTime, true);
            }
            else{
                _currentFrame = _planetAnimation.getKeyFrame(0);
            }
            this.setRegion(_currentFrame);
        }

        //_currentAngle = this.getRotation();
        //Setting a sibling planet's orbit around parent planet
        if (!_parentPlanet) {
            if (planet != null) {
                if (_firstUpdate) {
                    this.setPosition(planet.getX() + planet.getWidth() / 2 - (this.getWidth() / 2), planet.getY() + _distanceBetweenPlanets.y);
                    _distanceBetweenPlanets = new Vector2(this.getX() - planet.getX(), this.getY() - planet.getY() + planet.getHeight() / 2 - this.getHeight() / 2);
                    this.setRotation(planet.getRotation());
                    _firstUpdate = false;
                } else {
                    _distanceBetweenPlanets.setAngle(planet.getRotation());
                    this.setRotation(planet.getRotation() - 90);
                    this.setPosition(planet.getX() + planet.getWidth() / 2 - (this.getWidth() / 2) + _distanceBetweenPlanets.x, planet.getY() + planet.getHeight() / 2 - this.getHeight() / 2 + _distanceBetweenPlanets.y);
                }
                _isHit = planet.IsHit();
            }
        }

        if (_parentPlanet && _currentTouchState == Gestures.OneFinger && GestureManager.IsCurrentlyBeingTouched(this.getBoundingRectangle(), cameraZoom) && _draggable) {
            DragLocation(cameraZoom);
            _beingDragged = true;
        } else {
            _beingDragged = false;
        }

        if (_parentPlanet) {
            if (this.getRotation() > 360) {
                this.setRotation(this.getRotation() - 360);
            } else if (this.getRotation() < 0) {
                this.setRotation(this.getRotation() + 360);
            }
        }

        if (_enableSwipeRotation) {
            if (currentTouchState == Gestures.TwoFingers && GestureManager.IsCurrentlyBeingTouched(this.getBoundingRectangle(), cameraZoom)) {
                SwipeRotate();
                _beingRotated = true;
            } else {
                _beingRotated = false;
            }
            if ((_currentTouchState == Gestures.OneFinger || _currentTouchState == Gestures.None) && _rotationContinuance != 0) {
                RotationContinuance();
            }
        }
        else if(accelControls)
        {
            if(Math.abs(Gdx.input.getAccelerometerX()) > 3f) {
                this.setRotation(this.getRotation() - Gdx.input.getAccelerometerY() * 2f);
                _radius.setAngle(this.getRotation());
            }
        }

        if (_parentPlanet && !_beingRotated) {
            if (!_beingDragged) {
                if(_accelerometerMovement) {
                    AccelerometerMovement(cameraZoom);
                }
            }
            KeepPlanetInbounds(cameraZoom);
        }

        if(_isHit)
        {
            if(_hitSpriteAlpha >= 1 || _hitSpriteAlpha <= 0)
            {
                _alphaSpeed *= -1;
            }
            if(_hitSprite.getWidth() != this.getWidth() || _hitSprite.getHeight() != this.getHeight())
            {
                _hitSprite.setSize(this.getWidth(), this.getHeight());
            }
            _hitSprite.setPosition(this.getX(), this.getY());
            _hitSprite.setAlpha(_hitSpriteAlpha + _alphaSpeed);
            _elapsedHitTimer += Gdx.graphics.getDeltaTime();
            if(_elapsedHitTimer >= _hitTimer)
            {
                _isHit = false;
                _elapsedHitTimer = 0;
            }
        }
        _previousAccelerometerX = _currentAccelerometerX;
        _previousAngle = _currentAngle;

    }

    //region Functions
    public void SwipeRotate() {
        float angleDifference = _currentDistanceBetweenFingers.angle() - _previousDistanceBetweenFingers.angle();
        if (_previousTouchState != _currentTouchState) {
            _currentAngle = this.getRotation();
            _previousAngle = _currentAngle;
        } else {
            _currentAngle += angleDifference;
        }
        if (_currentAngle != _previousAngle) {
            this.rotate(-(_currentAngle - _previousAngle) * _rotationSpeed);
        }
        _rotationContinuance = (_currentAngle - _previousAngle) * _rotationSpeed;
    }

    public void RotationContinuance() {
        this.rotate(-_rotationContinuance);
        _rotationContinuance *= .95f;
        if (Math.abs(_rotationContinuance) < .1f) {
            _rotationContinuance = 0;
        }
    }

    public void DragLocation(float cameraZoom) {
        Vector2 newPosition = GestureManager.DragLocation(this.getWidth(), this.getHeight(), cameraZoom);
        this.setX(newPosition.x);
        this.setY(newPosition.y);
    }

    public void AccelerometerMovement(float cameraZoom) {

        if (Gdx.input.getRoll() <= -90 && Gdx.input.getRoll() > -180 && !_additionalAccelerometerX) {
            _additionalAccelerometerX = true;
        }

        if (Gdx.input.getRoll() > -90 && _additionalAccelerometerX) {
            _additionalAccelerometerX = false;
        }

        if (_calibrationX > 0) {
            if(_additionalAccelerometerX)
            {
                if ((10 - _calibrationX) + (10 - Gdx.input.getAccelerometerX()) <= 10) {
                    this.setY(this.getY() - ((10 - _calibrationX) + (10 - Gdx.input.getAccelerometerX())) * _movementSpeed);
                }
                else if((10 - _calibrationX) + (10 - Gdx.input.getAccelerometerX()) > 10)
                {
                    this.setY(this.getY() - (10 * _movementSpeed));
                }
            }
            else {
                if (Gdx.input.getAccelerometerX() > _calibrationX && InYBounds(cameraZoom)) {
                    if (Gdx.input.getAccelerometerX() - _calibrationX > 1.1f ) {

                        this.setY(this.getY() - (Gdx.input.getAccelerometerX() - _calibrationX) * _movementSpeed);
                    }
                }
                //AccelerometerX eventually becomes less than
                if (Gdx.input.getAccelerometerX() < _calibrationX) {
                    if (_calibrationX - Gdx.input.getAccelerometerX() > 1.1f && Gdx.input.getAccelerometerX() > -10 + _calibrationX && InYBounds(cameraZoom)) {
                        this.setY(this.getY() + (_calibrationX - Gdx.input.getAccelerometerX()) * _movementSpeed);
                    } else if (_calibrationX - Gdx.input.getAccelerometerX() > 1.1f && Gdx.input.getAccelerometerX() < -10 + _calibrationX && InYBounds(cameraZoom)) {
                        this.setY(this.getY() + 10 * _movementSpeed);
                    }
                }
            }


        } else if (_calibrationX < 0) {
            if(Gdx.input.getAccelerometerX() < _calibrationX && InYBounds(cameraZoom))
            {
                if(Gdx.input.getAccelerometerX() - _calibrationX < -1.1f && !_additionalAccelerometerX)
                {
                    this.setY(this.getY() - (Gdx.input.getAccelerometerX() - _calibrationX) * _movementSpeed);
                }
                else if(_additionalAccelerometerX && -10 + Gdx.input.getAccelerometerX() >= -10 + _calibrationX)
                {
                    this.setY(this.getY() - (-10 + Gdx.input.getAccelerometerX() - _calibrationX) * _movementSpeed);
                }
            }
            if(Gdx.input.getAccelerometerX() - _calibrationX > 1.1f  && Gdx.input.getAccelerometerX() < 10 + _calibrationX && InYBounds(cameraZoom))
            {
                this.setY(this.getY() + (Gdx.input.getAccelerometerX() - _calibrationX) * _movementSpeed);
            }
        } else if (Math.abs(Gdx.input.getAccelerometerX()) > 1.1f && InYBounds(cameraZoom)){
                this.setY(this.getY() - (Gdx.input.getAccelerometerX()) * _movementSpeed);
        }

        if(Math.abs(Gdx.input.getAccelerometerY()) > 1.1f && InXBounds(cameraZoom))
        {
            this.setX(this.getX() + Gdx.input.getAccelerometerY() * _movementSpeed);
        }

    }

    public boolean InYBounds(float cameraZoom)
    {
        if(this.getY() >= 0 + ((Gdx.graphics.getHeight() / 2) - (Gdx.graphics.getHeight() / 2) * cameraZoom) && this.getY() + this.getHeight() <= Gdx.graphics.getHeight() - ((Gdx.graphics.getHeight() / 2) - (Gdx.graphics.getHeight() / 2) * cameraZoom)) {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean InXBounds(float cameraZoom)
    {
        if(this.getX() >= 0 + ((Gdx.graphics.getWidth() / 2) - (Gdx.graphics.getWidth() / 2) * cameraZoom) && this.getX() + this.getWidth() <= Gdx.graphics.getWidth() - ((Gdx.graphics.getWidth() / 2) - (Gdx.graphics.getWidth() / 2) * cameraZoom)) {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void KeepPlanetInbounds(float cameraZoom) {
        if (this.getX() < 0 + ((Gdx.graphics.getWidth() / 2) - (Gdx.graphics.getWidth() / 2) * cameraZoom)) {
            this.setX(0 + ((Gdx.graphics.getWidth() / 2) - (Gdx.graphics.getWidth() / 2) * cameraZoom));
        }
        if (this.getX() + this.getWidth() > Gdx.graphics.getWidth() - ((Gdx.graphics.getWidth() / 2) - (Gdx.graphics.getWidth() / 2) * cameraZoom)) {
            this.setX(Gdx.graphics.getWidth() - ((Gdx.graphics.getWidth() / 2) - (Gdx.graphics.getWidth() / 2) * cameraZoom) - this.getWidth());
        }
        if (this.getY() < 0 + ((Gdx.graphics.getHeight() / 2) - (Gdx.graphics.getHeight() / 2) * cameraZoom)) {
            this.setY(0 + ((Gdx.graphics.getHeight() / 2) - (Gdx.graphics.getHeight() / 2) * cameraZoom));
        }
        if (this.getY() + this.getHeight() > Gdx.graphics.getHeight() - ((Gdx.graphics.getHeight() / 2) - (Gdx.graphics.getHeight() / 2) * cameraZoom)) {
            this.setY(Gdx.graphics.getHeight() - ((Gdx.graphics.getHeight() / 2) - (Gdx.graphics.getHeight() / 2) * cameraZoom) - this.getHeight());
        }
    }

    public void PrepareHitSprite()
    {
        _hitSprite.setPosition(this.getX(), this.getY());
        _hitSprite.setSize(this.getWidth(), this.getHeight());
    }

    public void EarthRotationJoystick(float targetAngle, boolean PC)
    {
        if(!PC) {
            float rotDifference = 360 - this.getRotation();
            float tempTargetAngle = targetAngle + rotDifference;

            if (tempTargetAngle > 360) {
                tempTargetAngle -= 360;
            }

            if (tempTargetAngle > 180) {
                _rotationSpeed = -15;
            } else {
                _rotationSpeed = 15;
            }

            if ((this.getRotation() > 355f || this.getRotation() < 5f) && (targetAngle > 355f || targetAngle < 5f)) {
                this.setRotation(targetAngle);
            } else if (Math.abs(targetAngle - this.getRotation()) < 10) {
                this.setRotation(targetAngle);
            } else {
                this.setRotation(this.getRotation() + _rotationSpeed);
            }
        } else {
            this.setRotation(targetAngle);
        }
    }


    public Vector2 OppositeMoonLocation(Vector2 moonLocation, float moonOriginX, float moonOriginY)
    {
        return new Vector2((this.getX() + this.getOriginX()) + ((this.getX() + this.getOriginX()) - (moonLocation.x + moonOriginX)), (this.getY() + this.getOriginY()) + ((this.getY() + this.getOriginY()) - (moonLocation.y + moonOriginY)) );
    }
    //endregion
}
