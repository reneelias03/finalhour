package com.AndroidGameAlpha.game;

import android.graphics.Bitmap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rene__000 on 10/24/2014.
 */
public class CustomizePortal implements Screen {

    MyGdxGame game;
    BitmapFont goodTimeFont;
    GameButtons themeButton, colorCustomizerButton, backButton, powerupShopButton, coinsButton;
    Sprite background, whitePixel;
    boolean switchToThemes, switchToThemesBegin, backPressed, previousBackPressed;

    public CustomizePortal(MyGdxGame game)
    {
        this.game = game;
        this.goodTimeFont = game.goodTimeFont;

        Gdx.input.setCatchBackKey(true);

        themeButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        themeButton.setSize(themeButton.getWidth() * game.screenScale, themeButton.getHeight() * game.screenScale);
        themeButton.setOriginCenter();
        themeButton.setPosition(Gdx.graphics.getWidth() / 2 - themeButton.getWidth() * 1.25f, Gdx.graphics.getHeight() / 2 + themeButton.getOriginY());

        colorCustomizerButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        colorCustomizerButton.setSize(colorCustomizerButton.getWidth() * game.screenScale, colorCustomizerButton.getHeight() * game.screenScale);
        colorCustomizerButton.setOriginCenter();
        colorCustomizerButton.setPosition(Gdx.graphics.getWidth() / 2 - colorCustomizerButton.getWidth() * 1.25f, themeButton.getY() - themeButton.getHeight() * 1.5f);

        powerupShopButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        powerupShopButton.setSize(powerupShopButton.getWidth() * game.screenScale, powerupShopButton.getHeight() * game.screenScale);
        powerupShopButton.setOriginCenter();
        powerupShopButton.setPosition(Gdx.graphics.getWidth() / 2 + powerupShopButton.getOriginX() / 2, themeButton.getY());

        coinsButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        coinsButton.setSize(coinsButton.getWidth() * game.screenScale, coinsButton.getHeight() * game.screenScale);
        coinsButton.setOriginCenter();
        coinsButton.setPosition(Gdx.graphics.getWidth() / 2 + coinsButton.getOriginX() / 2, colorCustomizerButton.getY());

        backButton = new GameButtons(themeButton.getTexture(), true, new Vector2().setZero());
        backButton.setSize(backButton.getWidth() * game.screenScale, backButton.getHeight() * game.screenScale);
        backButton.setOriginCenter();
        backButton.setPosition(Gdx.graphics.getWidth() - backButton.getWidth() - backButton.getOriginX() / 2, backButton.getHeight() / 2);

        background = new Sprite(this.game.manager.get("data/SpaceSquare.png", Texture.class));
        background.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
        background.setPosition(0, 0);

        whitePixel = new Sprite(game.manager.get("data/WhitePixel.png", Texture.class));
        whitePixel.setSize(Gdx.graphics.getWidth(), 80 * game.screenScale);
        whitePixel.setPosition(0, Gdx.graphics.getHeight() - 222 * game.screenScale);
        whitePixel.setColor(1, .8f, 0, 1);
//        whitePixel.setColor(Color.MAROON);
        whitePixel.setAlpha(.35f);

        switchToThemes = false;
        switchToThemesBegin = false;

        backPressed = false;
        previousBackPressed = false;
    }

    @Override
    public void render(float delta) {
        themeButton.Update();
        colorCustomizerButton.Update();
        backButton.Update();
        powerupShopButton.Update();
        coinsButton.Update();

        backPressed = Gdx.input.isKeyPressed(Input.Keys.BACK);

        if(themeButton.Tapped() && !switchToThemesBegin) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
//            if(!game.prefs.getBoolean("PixelSkin")) {
//                game.manager.load("data/PixelEarthNew20.png", Texture.class);
//                game.manager.load("data/EmptyCirclePixel.png", Texture.class);
//                game.manager.load("data/ContinentsPixel.png", Texture.class);
//                game.manager.load("data/MoonPixelNew11.png", Texture.class);
//                game.manager.load("data/PixelAsteroidRedone14.png", Texture.class);
//                game.manager.load("data/PixelSpace.png", Texture.class);
//            }
//            if(!game.prefs.getBoolean("NinjaSkin")) {
//                game.manager.load("data/NinjaSkinProto.png", Texture.class);
//                game.manager.load("data/ShurikenRedone.png", Texture.class);
//                game.manager.load("data/NinjaHatSmall.png", Texture.class);
//                game.manager.load("data/NinjaBackground1920.png", Texture.class);
//            }
//            if(!game.prefs.getBoolean("CatSkin")) {
//                game.manager.load("data/MouseMeanSmall.png", Texture.class);
//                game.manager.load("data/CatSkinProto.png", Texture.class);
//                game.manager.load("data/CatBackground.png", Texture.class);
////                game.manager.load("data/YarnBallExperimentGreenless1920.png", Texture.class);
//
//            }
            if(!game.prefs.getBoolean("PirateSkin")) {
                game.manager.load("data/CanonBallAsteroid100.png", Texture.class);
                game.manager.load("data/CanonBallShine.png", Texture.class);
                game.manager.load("data/MoonPirateSkin.png", Texture.class);
                game.manager.load("data/PirateEarth.png", Texture.class);
                game.manager.load("data/JackyPirateFog.png", Texture.class);
            }
            if(!game.prefs.getBoolean("SoccerSkin")) {
                game.manager.load("data/EarthSoccer.png", Texture.class);
                game.manager.load("data/MoonReferee.png", Texture.class);
                game.manager.load("data/SoccerballAsteroid100.png", Texture.class);
                game.manager.load("data/SoccerField1920.png", Texture.class);
            }
            if(!game.prefs.getBoolean("SumoSkin")) {
                game.manager.load("data/SumoBackground.png", Texture.class);
                game.manager.load("data/SumoEarth.png", Texture.class);
                game.manager.load("data/SumoMoon.png", Texture.class);
                game.manager.load("data/SumoAsteroid.png", Texture.class);
            }
            game.manager.finishLoading();
            game.setScreen(new ShopScreen(game));
            this.dispose();
        }

        if(colorCustomizerButton.Tapped() && !switchToThemesBegin) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            game.manager.load("data/ColorBox.png", Texture.class);
            game.manager.load("data/VolumeSliderBack.png", Texture.class);
            game.manager.load("data/VolumeSliderFill.png", Texture.class);
            game.manager.load("data/VolumeRockerControl.png", Texture.class);
            game.manager.finishLoading();
            game.setScreen(new ColorCustomization(game));
            this.dispose();
        }
        if(powerupShopButton.Tapped() && !switchToThemesBegin) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
//            game.setScreen(new DescriptionPage(game));
            game.setScreen(new PowerupShop(game, null));
            this.dispose();
        }
        if(coinsButton.Tapped() && !switchToThemesBegin) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            game.setScreen(new CoinsScreen(game));
            this.dispose();
        }
        if((backButton.Tapped() || (!backPressed && previousBackPressed)) && !switchToThemesBegin) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
                if(!game.prefs.getBoolean("PixelSkin")) {
                game.manager.unload("data/EmptyCirclePixel.png");
                game.manager.unload("data/ContinentsPixel.png");
                game.manager.unload("data/MoonPixelNew11.png");
                game.manager.unload("data/PixelAsteroidRedone14.png");
                game.manager.unload("data/PixelSpace.png");
            }
            if(!game.prefs.getBoolean("NinjaSkin")) {
                game.manager.unload("data/NinjaSkinProto.png");
                game.manager.unload("data/ShurikenRedone.png");
                game.manager.unload("data/NinjaHatSmall.png");
                game.manager.unload("data/NinjaBackground1920.png");
            }
            game.setScreen(new MenuScreen(game));
            this.dispose();
        }

        previousBackPressed = backPressed;

        //Draws.w.a.g
        game.batch.begin();
        background.draw(game.batch);
        themeButton.draw(game.batch);
        colorCustomizerButton.draw(game.batch);
        powerupShopButton.draw(game.batch);
        whitePixel.draw(game.batch);
        backButton.draw(game.batch);
        coinsButton.draw(game.batch);
        TextDrawing();
        game.batch.end();
    }

    private void TextDrawing() {
        String titleText = "Customize";
        goodTimeFont.setScale(game.screenScale * .55f);
        goodTimeFont.setColor(Color.WHITE);
        goodTimeFont.draw(game.batch, titleText, Gdx.graphics.getWidth() / 2 - titleText.length() * 33.5f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
        goodTimeFont.draw(game.batch, "Back", backButton.getX() + 29 * game.screenScale, backButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
        goodTimeFont.draw(game.batch, "Shop", powerupShopButton.getX() + 30f * game.screenScale, powerupShopButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
        goodTimeFont.draw(game.batch, "Coins", coinsButton.getX() + 17f * game.screenScale, coinsButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
        goodTimeFont.setScale(game.screenScale * .39f);
        goodTimeFont.draw(game.batch, "Themes", themeButton.getX() + 30f * game.screenScale, themeButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
        goodTimeFont.draw(game.batch, "Colors", colorCustomizerButton.getX() + 22f * game.screenScale, colorCustomizerButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

}
