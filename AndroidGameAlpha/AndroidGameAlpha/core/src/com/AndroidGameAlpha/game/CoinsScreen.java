package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by rene__000 on 1/22/2015.
 */
public class CoinsScreen  implements Screen {

    MyGdxGame game;
    GameButtons backButton, oneDollarButton, twoDollarButton, threeDollarButton, watchAdButton;
    Sprite background, whitePixel, coinTotalSprite;
    BitmapFont goodTimeFont;
    int activeButtonIndex, viewsLeft;
    boolean texturesUnloaded, videoShown;
    double time;
    int hours, minutes;

    public CoinsScreen(MyGdxGame game) {
        this.game = game;
        goodTimeFont = game.goodTimeFont;

        backButton = new GameButtons(game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        backButton.setSize(backButton.getWidth() * game.screenScale, backButton.getHeight() * game.screenScale);
        backButton.setOriginCenter();
        backButton.setPosition(Gdx.graphics.getWidth() - backButton.getWidth() - backButton.getOriginX() / 2, backButton.getHeight() / 2);

        oneDollarButton = new GameButtons(game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        oneDollarButton.setSize(oneDollarButton.getWidth() * game.screenScale, oneDollarButton.getHeight() * game.screenScale);
        oneDollarButton.setOriginCenter();
        oneDollarButton.setPosition(Gdx.graphics.getWidth() / 4 - oneDollarButton.getOriginX(), Gdx.graphics.getHeight() / 2);

        twoDollarButton = new GameButtons(game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        twoDollarButton.setSize(twoDollarButton.getWidth() * game.screenScale, twoDollarButton.getHeight() * game.screenScale);
        twoDollarButton.setOriginCenter();
        twoDollarButton.setPosition(Gdx.graphics.getWidth() / 2 - twoDollarButton.getOriginX(), Gdx.graphics.getHeight() / 2);

        threeDollarButton = new GameButtons(game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        threeDollarButton.setSize(threeDollarButton.getWidth() * game.screenScale, threeDollarButton.getHeight() * game.screenScale);
        threeDollarButton.setOriginCenter();
        threeDollarButton.setPosition(Gdx.graphics.getWidth() * .75f - threeDollarButton.getOriginX(), Gdx.graphics.getHeight() / 2);

        watchAdButton = new GameButtons(game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        watchAdButton.setSize(watchAdButton.getWidth() * game.screenScale, watchAdButton.getHeight() * game.screenScale);
        watchAdButton.setOriginCenter();
        watchAdButton.setPosition(twoDollarButton.getX() + 10f * game.screenScale, Gdx.graphics.getHeight() * .22f);

        background = new Sprite(this.game.manager.get("data/SpaceSquare.png", Texture.class));
        background.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
        background.setPosition(0, 0);

        whitePixel = new Sprite(game.manager.get("data/WhitePixel.png", Texture.class));
        whitePixel.setSize(Gdx.graphics.getWidth(), 80 * game.screenScale);
        whitePixel.setPosition(0, Gdx.graphics.getHeight() - 222 * game.screenScale);
        whitePixel.setColor(1, .8f, 0, 1);
//        whitePixel.setColor(Color.MAROON);
        whitePixel.setAlpha(.35f);

        coinTotalSprite = new Sprite(game.manager.get("data/CoinLarge.png", Texture.class));
        coinTotalSprite.setSize(coinTotalSprite.getWidth() * .16f * game.screenScale, coinTotalSprite.getHeight() * .16f * game.screenScale);
        coinTotalSprite.setOriginCenter();

        game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));

//        game.actionResolver.queryInventory();

        activeButtonIndex = 0;
        texturesUnloaded = false;
        hours = 0;
        minutes = 0;

        time = System.currentTimeMillis();

        viewsLeft = game.prefs.getInteger("AdViewCount");
        videoShown = false;
//        viewsLeft = 5;
//        dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
//        date = new Date();
    }

    @Override
    public void render(float delta) {
        backButton.Update();
        oneDollarButton.Update();
        twoDollarButton.Update();
        threeDollarButton.Update();
        watchAdButton.Update();


//        String hello = dateFormat.format(date);
        if(viewsLeft == 0) {
            time = System.currentTimeMillis();
//            hours = (int)(((double)game.prefs.getLong("AdTime") + 21600000d - (double)time) / 3600000d);
//            minutes = (int)(((((double)game.prefs.getLong("AdTime") + 21600000d - (double)time) / 3600000d) - hours) * 60);

            //Every 3 hours instead of every 6
            hours = (int)(((double)game.prefs.getLong("AdTime") + 10800000d - (double)time) / 3600000d);
            minutes = (int)(((((double)game.prefs.getLong("AdTime") + 10800000d - (double)time) / 3600000d) - hours) * 60);
        }
        if(time > game.prefs.getLong("AdTime") + 10800000l) {
            viewsLeft = 5;
        }
        if(game.actionResolver.returnPurchaseSuccess()) {
            if(activeButtonIndex == 0) {
                game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") + 4000);
            } else if(activeButtonIndex == 1) {
                game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") + 10000);
            } else if(activeButtonIndex == 2) {
                game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") + 20000);
            }
            game.prefs.flush();
            if(game.prefs.getBoolean("Sound")) {
                game.cashRegisterSound.play(.45f * game.prefs.getFloat("SfxVolumeLevel"));
            }
        }

        if(game.actionResolver.adWatchedSuccessfully()) {
            if (viewsLeft == 5) {
                game.prefs.putLong("AdTime", System.currentTimeMillis());
                game.prefs.flush();
            }
            viewsLeft--;
            game.prefs.putInteger("AdViewCount", viewsLeft);
            game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") + 25);
            game.prefs.flush();
            if(game.prefs.getBoolean("Sound")) {
                game.cashRegisterSound.play(.45f * game.prefs.getFloat("SfxVolumeLevel"));
            }
        }

        if(oneDollarButton.Tapped()) {
            game.actionResolver.purchaseCoinItem(0);
//            if(game.actionResolver.returnPurchaseSuccess()) {
//                whitePixel.setColor(Color.GREEN);
//            }
            activeButtonIndex = 0;
//            game.actionResolver.queryInventory();
        }
        if(twoDollarButton.Tapped()) {
            game.actionResolver.purchaseCoinItem(1);
//            if(game.actionResolver.returnPurchaseSuccess()) {
//                whitePixel.setColor(Color.RED);
//            }
            activeButtonIndex = 1;
//            game.actionResolver.queryInventory();
        }
        if(threeDollarButton.Tapped()) {
            game.actionResolver.purchaseCoinItem(2);
//            if(game.actionResolver.returnPurchaseSuccess()) {
//                whitePixel.setColor(Color.WHITE);
//            }
            activeButtonIndex = 2;
//            game.actionResolver.queryInventory();
        }

        if(backButton.Tapped() || game.BackPressed()) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
//            game.manager.load("data/MouseMeanSmall.png", Texture.class);
//            game.manager.load("data/CatSkinProto.png", Texture.class);
//            game.manager.load("data/YarnBallExperimentGreenless1920.png", Texture.class);
//            game.manager.load("data/EarthSoccer.png", Texture.class);
//            game.manager.load("data/MoonReferee.png", Texture.class);
//            game.manager.load("data/SoccerballAsteroid100.png", Texture.class);
//            game.manager.load("data/SoccerField1920.png", Texture.class);
//            game.manager.finishLoading();
            game.setScreen(new CustomizePortal(game));
            this.dispose();
        }

        if(watchAdButton.Tapped()) {
            if(viewsLeft > 0) {
//                game.actionResolver.showOrLoadInterstital();
                game.actionResolver.adColonyShowInterstitial();
                boolean show;
                do {
                    show = game.actionResolver.adColonyShowReadyInterstitial();
                } while (!show);
            }
//            if (viewsLeft == 5) {
//                game.prefs.putLong("AdTime", System.currentTimeMillis());
//                game.prefs.flush();
//            }
//            viewsLeft--;
//            if(!texturesUnloaded) {
//                game.manager.unload("data/MouseMeanSmall.png");
//                game.manager.unload("data/CatSkinProto.png");
//                game.manager.unload("data/YarnBallExperimentGreenless1920.png");
//                game.manager.unload("data/EarthSoccer.png");
//                game.manager.unload("data/MoonReferee.png");
//                game.manager.unload("data/SoccerballAsteroid100.png");
//                game.manager.unload("data/SoccerField1920.png");
//                texturesUnloaded = true;
//            }
//              game.actionResolver.adColonyShowReady();
        }


        //Draw
        game.batch.begin();
        background.draw(game.batch);
        whitePixel.draw(game.batch);
        backButton.draw(game.batch);
        oneDollarButton.draw(game.batch);
        coinTotalSprite.setPosition(oneDollarButton.getX() + 45f * game.screenScale, oneDollarButton.getY() + coinTotalSprite.getOriginY() / 2);
        coinTotalSprite.draw(game.batch);

        twoDollarButton.draw(game.batch);
        coinTotalSprite.setPosition(twoDollarButton.getX() + 27.5f * game.screenScale, oneDollarButton.getY() + coinTotalSprite.getOriginY() / 2);
        coinTotalSprite.draw(game.batch);

        threeDollarButton.draw(game.batch);
        coinTotalSprite.setPosition(threeDollarButton.getX() + 15f * game.screenScale, oneDollarButton.getY() + coinTotalSprite.getOriginY() / 2);
        coinTotalSprite.draw(game.batch);

        watchAdButton.draw(game.batch);
        coinTotalSprite.setPosition(watchAdButton.getX() + watchAdButton.getOriginX() - coinTotalSprite.getWidth() - 25f * game.screenScale, watchAdButton.getY() + coinTotalSprite.getOriginY() / 2);
        coinTotalSprite.draw(game.batch);

        coinTotalSprite.setPosition(oneDollarButton.getOriginX() / 2, backButton.getY());
        coinTotalSprite.draw(game.batch);

        TextDrawing();
        game.batch.end();
    }

    private void TextDrawing() {
        String titleText = "Coins";
        goodTimeFont.setScale(game.screenScale * .55f);
        goodTimeFont.setColor(Color.WHITE);
        goodTimeFont.draw(game.batch, titleText, Gdx.graphics.getWidth() / 2 - titleText.length() * 32f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
        goodTimeFont.draw(game.batch, "Back", backButton.getX() + 29 * game.screenScale, backButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);

        goodTimeFont.setScale(game.screenScale * .45f);
        goodTimeFont.draw(game.batch, "Purchase", Gdx.graphics.getWidth() / 2 - 240f * game.screenScale, twoDollarButton.getY() + twoDollarButton.getHeight() + twoDollarButton.getOriginY() * .75f + goodTimeFont.getLineHeight());
        goodTimeFont.draw(game.batch, "Watch An Ad", oneDollarButton.getX() - 125f * game.screenScale, Gdx.graphics.getHeight() * .3f);
        if(viewsLeft > 0) {
            goodTimeFont.draw(game.batch, String.format("%d views left!", viewsLeft), watchAdButton.getX() + watchAdButton.getWidth() + 10f * game.screenScale, Gdx.graphics.getHeight() * .3f);
        } else {
            if(minutes < 10) {
                goodTimeFont.draw(game.batch, String.format("Return: %d:0%d", hours, minutes), watchAdButton.getX() + watchAdButton.getWidth() + 10f * game.screenScale, Gdx.graphics.getHeight() * .3f);
            } else {
                goodTimeFont.draw(game.batch, String.format("Return: %d:%d", hours, minutes), watchAdButton.getX() + watchAdButton.getWidth() + 10f * game.screenScale, Gdx.graphics.getHeight() * .3f);
            }
        }
        goodTimeFont.draw(game.batch, String.format("Bank: %d", game.prefs.getInteger("Coins")), coinTotalSprite.getX() + coinTotalSprite.getWidth() * 1.25f, coinTotalSprite.getY() + goodTimeFont.getLineHeight() * .85f);
        goodTimeFont.setScale(game.screenScale * .4f);
        goodTimeFont.draw(game.batch, "4000", oneDollarButton.getX() + coinTotalSprite.getWidth() + 50f * game.screenScale, oneDollarButton.getY() + goodTimeFont.getLineHeight() + 10f * game.screenScale);
        goodTimeFont.draw(game.batch, "10000", twoDollarButton.getX() + coinTotalSprite.getWidth() + 37.5f * game.screenScale, twoDollarButton.getY() + goodTimeFont.getLineHeight() + 10f * game.screenScale);
        goodTimeFont.draw(game.batch, "25", watchAdButton.getX() + coinTotalSprite.getWidth() + 95f * game.screenScale, watchAdButton.getY() + goodTimeFont.getLineHeight() + 10f * game.screenScale);
        goodTimeFont.setScale(game.screenScale * .39f);
        goodTimeFont.draw(game.batch, "20000", threeDollarButton.getX() + coinTotalSprite.getWidth() + 25f * game.screenScale, threeDollarButton.getY() + goodTimeFont.getLineHeight() + 10f * game.screenScale);

        goodTimeFont.setScale(game.screenScale * .35f);
        goodTimeFont.setColor(Color.GREEN);
        goodTimeFont.draw(game.batch, "$0.99", oneDollarButton.getX() + coinTotalSprite.getWidth() + 25f * game.screenScale, oneDollarButton.getY() - 20f * game.screenScale);
        goodTimeFont.draw(game.batch, "$1.99", twoDollarButton.getX() + coinTotalSprite.getWidth() + 25f * game.screenScale, twoDollarButton.getY() - 20f * game.screenScale);
        goodTimeFont.draw(game.batch, "$2.99", threeDollarButton.getX() + coinTotalSprite.getWidth() + 25f * game.screenScale, threeDollarButton.getY() - 20f * game.screenScale);


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
