package com.AndroidGameAlpha.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by rene__000 on 9/4/2014.
 */
public class Asteroid extends Sprite {

    protected Vector2 _speed;
    public Vector2 Speed()
    {
        return _speed;
    }
    public void SetSpeed(Vector2 speed)
    {
        _speed = speed;
    }
    protected float _speedMultiplier;
    protected Vector2 _radius;
    public Vector2 Radius()
    {
        return _radius;
    }
    public boolean _toScore;
    public boolean _doubleScore;
    private boolean _firstAppearance;
    private boolean _outOfBounds;
    public boolean OutOfBounds()
    {
        return _outOfBounds;
    }
    public void setOutOfBounds(boolean outOfBounds) {
        _outOfBounds = outOfBounds;
    }
    protected boolean _exploding;
    public boolean Exploding()
    {
        return _exploding;
    }
    public boolean over;
    private Rectangle _largerRectangle;
    private int _fingerCurrentlyTouching;
    protected Animation _explosionAnimation;
    public Animation GetExplosionAnimation()
    {
        return _explosionAnimation;
    }
    private TextureRegion _currentFrame;
    protected Texture _originalTexture;
    public Texture OriginalTexture()
    {
        return _originalTexture;
    }
    protected float _frameTime, _timeSinceExplosion;
    public Vector2 _originalSize;
    protected float rotationSpeed;
    public void RotationSpeed(float rotationSpeed)
    {
        this.rotationSpeed = rotationSpeed;
    }
    protected boolean _normalAsteroid;
    protected boolean _hugeAsteroid;
    protected Vector2 _deadZone;
    protected float _scale;
    protected TextureAtlas _explosionAtlas;
    public boolean _active;
    
    public Asteroid(Texture texture, Vector2 PlanetLocation, float scale, float speed, float cameraZoom, Vector2 deadZone, TextureAtlas explosionAtlas)
    {
        super(texture);
        _originalTexture = texture;
        Random random = new Random();
        _scale = scale;
        int screenLocation = random.nextInt(4);
        int randomSizeAddition = random.nextInt(26);
        this.setSize((100 + randomSizeAddition) * scale, (100 + randomSizeAddition) * scale);
        this.setOriginCenter();
        if(screenLocation == 0) {
            //Spawn to left of screen
            this.setPosition(((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2) - this.getWidth() * 2,
                    deadZone.y + random.nextInt((int)(Gdx.graphics.getHeight() * cameraZoom - deadZone.y - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2)));
        }
        else if(screenLocation == 1)
        {
            //Spawn to top of screen
            this.setPosition(random.nextInt((int)(Gdx.graphics.getWidth() * cameraZoom - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2)),
                    (Gdx.graphics.getHeight() - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2) + this.getHeight() * 2);
        }
        else if(screenLocation == 2)
        {
            //Spawn to the right of the screen
            this.setPosition(Gdx.graphics.getWidth() - ((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2) + this.getWidth(),
                    deadZone.y + random.nextInt((int)(Gdx.graphics.getHeight()* cameraZoom - deadZone.y - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2)));
        }
        else
        {
            //Spawn to the bottom of the screen
            this.setPosition(deadZone.x + random.nextInt((int)(Gdx.graphics.getWidth() * cameraZoom  - deadZone.x * 2 - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2)),
                    ((Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2) - this.getHeight());
        }
        _deadZone = deadZone;
        _speedMultiplier = speed;
        _speed = new Vector2(PlanetLocation.x - this.getX(), PlanetLocation.y - this.getY());
        _speed.nor();
        _speed.x *= speed;
        _speed.y *= speed;
        _radius = new Vector2(0, this.getOriginY());
        _firstAppearance = false;
        _outOfBounds = false;
        _toScore = false;
        _largerRectangle = new Rectangle(this.getX() - this.getOriginX() / 2, this.getY() - this.getOriginY() / 2, this.getWidth() * 2, this.getHeight() * 2);
        _fingerCurrentlyTouching = 0;
        if(explosionAtlas.getRegions() != null) {
            _explosionAtlas = explosionAtlas;
            _explosionAnimation = new Animation(.018f, explosionAtlas.getRegions(), Animation.PlayMode.NORMAL);
//        _explosionAnimation = new Animation(1f, explosionAtlas.getRegions(), Animation.PlayMode.NORMAL);
            _exploding = false;
            _frameTime = 0;
            try {
                _currentFrame = _explosionAnimation.getKeyFrame(_frameTime);
            } catch (Exception e){

            }

        }
        _originalSize = new Vector2(this.getWidth(), this.getHeight());
        rotationSpeed = random.nextInt(10) + 1;
        _timeSinceExplosion = 0;
//        this.setColor(Color.BLUE);
        _normalAsteroid = true;
        _hugeAsteroid = false;
        _doubleScore = false;
        _active = false;
    }

    public void Update(float cameraZoom, LaserBullets[] bullets, Planet Earth, Powerup powerup, boolean vibrate, Sound explosion, boolean playSound, float deltaTime, float volume)
    {
        if(_active) {
            this.setPosition(this.getX() + _speed.x, this.getY() + _speed.y);

            if (WithinScreenBounds(cameraZoom) && !_firstAppearance) {
                _firstAppearance = true;
            }
            if (!WithinScreenBounds(cameraZoom) && _firstAppearance && !_outOfBounds) {
                _outOfBounds = true;
                _active = false;
            }
            if (!_hugeAsteroid && bullets != null) {
                for (int i = 0; i < bullets.length; i++) {
                    if (bullets[i]._active && this.getBoundingRectangle().overlaps(bullets[i].getBoundingRectangle()) && !_exploding) {
                        powerup.Generate(new Vector2(this.getX(), this.getY()), Earth.GetHealth());
                        if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.PiercingLaser && bullets[i]._asteroidCount < 1) {
                            bullets[i]._asteroidCount++;
                            bullets[i].setColor(Color.YELLOW);
                        } else {
//                            bullets.remove(bullets.get(i));
                            bullets[i]._active = false;
                        }
                        _exploding = true;
                        _toScore = true;
                        if (playSound) {
                            explosion.play(.2f * volume);
                        }
                        break;
                    }

                }
            }
            if (Earth != null && CheckEarthCollision(Earth) && !Earth.IsHit() && !_exploding) {
                Earth.SetHealth(Earth.GetHealth() - 1);
                if (playSound) {
                    explosion.play(.2f * volume);
                }
                if (Earth.GetHealth() > 0) {
                    Earth.SetIsHit(true);
                    Earth.PrepareHitSprite();
                }
                if (vibrate) {
                    Gdx.input.vibrate(300);
                }
                _exploding = true;
            }
//            GetFingerCurrentlyTouching(cameraZoom);
//            _largerRectangle = new Rectangle(this.getBoundingRectangle().getX() - (this.getBoundingRectangle().getWidth() * 3 / 4), this.getBoundingRectangle().getY() - (this.getBoundingRectangle().getHeight() * 3 / 4), this.getBoundingRectangle().getWidth() * 4, this.getBoundingRectangle().getHeight() * 4);
//            if (powerup != null && powerup.PowerUpType() == PowerupType.DivineIntervention && powerup.PowerupActive() && !_exploding) {
//                if (GestureManager.IsCurrentlyBeingTouchedIndex(_largerRectangle, cameraZoom, _fingerCurrentlyTouching) && Gdx.input.isTouched(_fingerCurrentlyTouching)) {
//                    _exploding = true;
//                    _toScore = true;
//                    if (playSound) {
//                        explosion.play(.2f * volume);
//                    }
//                    if (vibrate) {
//                        Gdx.input.vibrate(150);
//                    }
//                }
//            }
            if (powerup != null && ((powerup.PowerUpType() == PowerupType.Explosion && powerup.PowerupActive()) || powerup.explosionSimultaneous) && !powerup.ExplsionDying() && !_exploding) {
                Vector2 distanceBetweenCircles = new Vector2(powerup.ExplosionSprite().getX() + powerup.ExplosionSprite().getOriginX() - this.getX() - this.getOriginX(), powerup.ExplosionSprite().getY() + powerup.ExplosionSprite().getOriginY() - this.getY() - this.getOriginY());
                if (_radius.len() + powerup.ExplosionRadius() >= distanceBetweenCircles.len()) {
                    _exploding = true;
                    _toScore = true;
                    if (playSound) {
                        explosion.play(.2f * volume);
                    }
                    if (vibrate) {
                        Gdx.input.vibrate(150);
                    }
                }
            }
            if (_exploding) {
                _frameTime += deltaTime;
                _timeSinceExplosion += deltaTime;
                this.setRegion(_explosionAnimation.getKeyFrame(_frameTime));
                if (_normalAsteroid) {
                    this.setSize(_originalSize.x * 1.5f, _originalSize.y * 1.5f);
                }
            } else {
                this.rotate(rotationSpeed);
            }
            if (_explosionAnimation.isAnimationFinished(_frameTime) && _exploding || _timeSinceExplosion > 1) {
                _outOfBounds = true;
                _active = false;
                setRegion(_originalTexture);
                _exploding = false;
            }
        }

    }

    private boolean WithinScreenBounds(float cameraZoom)
    {
        if(this.getX() + this.getWidth() > (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2
                && this.getX() < Gdx.graphics.getWidth() - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) /2
                && this.getY() + this.getHeight() > (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2
                && this.getY() < Gdx.graphics.getHeight() - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected boolean CheckEarthCollision(Planet Earth)
    {
        Vector2 distanceBetweenCircles = new Vector2( Earth.getX() + Earth.getOriginX() - this.getX() - this.getOriginX(),  Earth.getY() + Earth.getOriginY() - this.getY() - this.getOriginY());
        float len = _radius.len();
        if(_radius.len() + Earth.GetRadius().len() >= distanceBetweenCircles.len())
        {
            return true;
        }
        return false;
    }


    public void GetFingerCurrentlyTouching(float cameraZoom) {
        int[] currentFingersDown = GestureManager.CurrentFingersDown();
        for (int i = 0; i < currentFingersDown.length; i++) {
            if (GestureManager.IsCurrentlyBeingTouchedIndex(this.getBoundingRectangle(), cameraZoom, currentFingersDown[i])) {
                _fingerCurrentlyTouching = currentFingersDown[i];
                break;
            }
        }
    }

    public void ResetAsteroid(Texture asteroidTexture, Vector2 PlanetLocation) {

        Random random = new Random();
        int screenLocation = random.nextInt(4);
        int randomSizeAddition = random.nextInt(26);
        this.setSize((100 + randomSizeAddition) * _scale, (100 + randomSizeAddition) * _scale);
        setRegion(_originalTexture);
        this.setOriginCenter();
        if(screenLocation == 0) {
            //Spawn to left of screen
            this.setPosition(((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2) - this.getWidth() * 2,
                    _deadZone.y + random.nextInt((int)(Gdx.graphics.getHeight() * 1 - _deadZone.y - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2)));
        }
        else if(screenLocation == 1)
        {
            //Spawn to top of screen
            this.setPosition(random.nextInt((int)(Gdx.graphics.getWidth() * 1 - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2)),
                    (Gdx.graphics.getHeight() - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2) + this.getHeight() * 2);
        }
        else if(screenLocation == 2)
        {
            //Spawn to the right of the screen
            this.setPosition(Gdx.graphics.getWidth() - ((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2) + this.getWidth(),
                    _deadZone.y + random.nextInt((int)(Gdx.graphics.getHeight()* 1 - _deadZone.y - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2)));
        }
        else
        {
            //Spawn to the bottom of the screen
            this.setPosition(_deadZone.x + random.nextInt((int)(Gdx.graphics.getWidth() * 1  - _deadZone.x * 2 - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2)),
                    ((Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2) - this.getHeight());
        }
        _speed = new Vector2(PlanetLocation.x - this.getX(), PlanetLocation.y - this.getY());
        _speed.nor();
        _speed.x *= _speedMultiplier;
        _speed.y *= _speedMultiplier;
        _radius = new Vector2(0, this.getOriginY());
        _firstAppearance = false;
        _outOfBounds = false;
        _toScore = false;
        _largerRectangle = new Rectangle(this.getX() - this.getOriginX() / 2, this.getY() - this.getOriginY() / 2, this.getWidth() * 2, this.getHeight() * 2);
        _fingerCurrentlyTouching = 0;
        if(_explosionAtlas.getRegions() != null) {
            _explosionAnimation = new Animation(.018f, _explosionAtlas.getRegions(), Animation.PlayMode.NORMAL);
//        _explosionAnimation = new Animation(1f, explosionAtlas.getRegions(), Animation.PlayMode.NORMAL);
            _exploding = false;
            _frameTime = 0;
            try {
                _currentFrame = _explosionAnimation.getKeyFrame(_frameTime);
            } catch (Exception e){

            }

        }
        _originalSize = new Vector2(this.getWidth(), this.getHeight());
        rotationSpeed = random.nextInt(10) + 1;
        _timeSinceExplosion = 0;
//        this.setColor(Color.BLUE);
        _normalAsteroid = true;
        _hugeAsteroid = false;
        _doubleScore = false;
    }
}


