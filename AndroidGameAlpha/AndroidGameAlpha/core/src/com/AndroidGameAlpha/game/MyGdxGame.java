package com.AndroidGameAlpha.game;

import android.app.Service;
import android.content.ServiceConnection;
import android.view.ContextThemeWrapper;
import android.widget.Button;
import com.badlogic.gdx.*;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.assets.AssetManager;
import com.google.android.gms.R;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.games.*;
import com.google.android.gms.common.api.*;
import com.google.example.games.basegameutils.GameHelper;
import com.jirbo.adcolony.AdColonyAdAvailabilityListener;
import org.w3c.dom.Text;
import com.google.android.gms.games.Games;


import java.text.DecimalFormat;

public class MyGdxGame extends Game {
    public SpriteBatch batch;
    public static com.badlogic.gdx.assets.AssetManager manager;
    private boolean toSwitch, endOfDialogue, screenSwitched, currentTouch, previousTouch, blackSpriteDarkening, HDAssets;
    public boolean  backPressed, previousBackPressed;
    BitmapFont.BitmapFontData portFontData, goodTimeFontData;
    public BitmapFont font, portFont, goodTimeFont;
    private TextureRegion[] fontRegions, goodFontRegions;
    private float managerProgress, timeBetweenCharacters, elapsedTime, lineEndWaitTime, backgroundAlpha;
    public float screenWidth, screenHeight;
    public Sound LaserSound, SmallExplosionSound, PowerupSound, MenuButtonSound, TypeWriterSound, catMeow, coinClick, errorSound, cashRegisterSound;
    public Music gameSong, menuSong, openingSong;
    public static Preferences prefs;
    public float screenScale, screenScaleHeight;
    public ActionResolver actionResolver;
    public Color[] colors;
    String[] dialogueLines;
    String currentDialogue;
    int currentDialogueIndex, currentCharacterIndex;
    public Sprite[] backgroundSprite;
    public Sprite blackSprite;
    DecimalFormat df;
    protected IActivityRequestHandler handler;
//    protected InterstitialAd interstitialAd;
//    protected ServiceConnection serviceConnection;
//    AdColonyAdAvailabilityListener adColonyAdAvailabilityListener;

//    public MyGdxGame(ActionResolver actionResolver, AdView adView) {
    public MyGdxGame(ActionResolver actionResolver, IActivityRequestHandler handler) {
        this.actionResolver = actionResolver;
        this.handler = handler;
//        this.serviceConnection = serviceConnection;
//        this.adColonyAdAvailabilityListener = adColonyAdAvailabilityListener;
    }

    @Override
    public void create() {

        Gdx.graphics.setDisplayMode(1600, 900, false);
        prefs = Gdx.app.getPreferences("AlphaPreferences");
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();
        screenScale = screenWidth / 1920;
        Gdx.input.setCatchBackKey(true);

//        if(screenHeight * screenWidth <= 921600) {
//            HDAssets = false;
//        } else {
//            HDAssets = true;
//        }

        if (screenHeight % 10 == 0 && screenWidth / (screenHeight / 10) == 16) {
            screenScaleHeight = 60 * (screenHeight / 1200);
            prefs.putBoolean("16x9", false);
            prefs.putBoolean("16x10", true);
            prefs.putBoolean("4x3", false);
            prefs.putBoolean("3x2", false);
            prefs.putBoolean("5x3", false);
        } else if (screenHeight % 3 == 0 && screenWidth / (screenHeight / 3) == 4) {
            screenScaleHeight = 190 * (screenHeight / 1536);
            prefs.putBoolean("16x9", false);
            prefs.putBoolean("16x10", false);
            prefs.putBoolean("4x3", true);
            prefs.putBoolean("3x2", false);
            prefs.putBoolean("5x3", false);
        } else if (screenHeight % 2 == 0 && screenWidth / (screenHeight / 2) == 3) {
            screenScaleHeight = 113 * (screenHeight / 1440);
            prefs.putBoolean("16x9", false);
            prefs.putBoolean("16x10", false);
            prefs.putBoolean("4x3", false);
            prefs.putBoolean("3x2", true);
            prefs.putBoolean("5x3", false);
        } else if (screenHeight % 3 == 0 && screenWidth / (screenHeight / 3) == 5) {
            screenScaleHeight = 36 * (screenHeight / 1152);
            prefs.putBoolean("16x9", false);
            prefs.putBoolean("16x10", false);
            prefs.putBoolean("4x3", false);
            prefs.putBoolean("3x2", false);
            prefs.putBoolean("5x3", true);
        } else {
            screenScaleHeight = 0;
            prefs.putBoolean("16x9", true);
            prefs.putBoolean("16x10", false);
            prefs.putBoolean("4x3", false);
            prefs.putBoolean("3x2", false);
            prefs.putBoolean("5x3", false);
        }



        batch = new SpriteBatch();

        font = new BitmapFont(Gdx.files.internal("data/poop.fnt"), Gdx.files.internal("data/poop.png"), false);
        portFontData = new BitmapFont.BitmapFontData(Gdx.files.internal("data/PORT118/PORT118.fnt"), false);
        fontRegions = new TextureRegion[]{new TextureRegion(new Texture(Gdx.files.internal("data/PORT118/PORT1181.png"))), new TextureRegion(new Texture(Gdx.files.internal("data/PORT118/PORT1182.png"))),
                new TextureRegion(new Texture(Gdx.files.internal("data/PORT118/PORT1183.png"))), new TextureRegion(new Texture(Gdx.files.internal("data/PORT118/PORT1184.png"))),
                new TextureRegion(new Texture(Gdx.files.internal("data/PORT118/PORT1185.png")))};
        portFont = new BitmapFont(portFontData, fontRegions, false);
        portFont.setColor(Color.WHITE);

        goodTimeFontData = new BitmapFont.BitmapFontData(Gdx.files.internal("data/GoodTimeFont/GoodTimeFont.fnt"), false);
        goodFontRegions = new TextureRegion[]{new TextureRegion(new Texture(Gdx.files.internal("data/GoodTimeFont/GoodTimeFont1.png"))), new TextureRegion(new Texture(Gdx.files.internal("data/GoodTimeFont/GoodTimeFont2.png"))),
                new TextureRegion(new Texture(Gdx.files.internal("data/GoodTimeFont/GoodTimeFont3.png"))), new TextureRegion(new Texture(Gdx.files.internal("data/GoodTimeFont/GoodTimeFont4.png"))),
                new TextureRegion(new Texture(Gdx.files.internal("data/GoodTimeFont/GoodTimeFont5.png")))};
        goodTimeFont = new BitmapFont(goodTimeFontData, goodFontRegions, false);
        goodTimeFont.setColor(Color.WHITE);

        df = new DecimalFormat("#.00");
        font.setColor(Color.WHITE);

        colors = new Color[]{Color.WHITE, Color.BLACK, Color.RED, Color.GREEN, Color.BLUE, Color.LIGHT_GRAY, Color.GRAY,
                new Color(.28f, .43f, .68f, 1), Color.PINK, Color.ORANGE, Color.YELLOW, Color.MAGENTA, Color.CYAN, Color.OLIVE, Color.PURPLE,
                new Color(.83f, 0, .02f, 1), Color.TEAL, new Color(.5f, .73f, .5f, 1)};

        if (!prefs.contains("FirstPlay") || !prefs.contains("PauseOnInventory")) {
            prefs.putBoolean("FirstPlay", true);
            prefs.flush();
        } else {
            prefs.putBoolean("FirstPlay", false);
            prefs.flush();
        }

        if (prefs.getBoolean("FirstPlay")) {
            if(!prefs.contains("LeaderBoardChange")) {
                if (!prefs.contains("SumoSkin")) {
                    if (!prefs.contains("Sound")) {
                        prefs.putBoolean("Sound", true);
                        prefs.putBoolean("Vibrate", false);

                        prefs.putBoolean("AccelControls", false);
                        prefs.putBoolean("Normal", false);
                        prefs.putBoolean("Easy", true);
                        prefs.putBoolean("Hard", false);
                        prefs.putBoolean("PC", false);
                        prefs.putFloat("SfxVolumeLevel", 1f);
                        prefs.putFloat("MusicVolumeLevel", 1f);
                        prefs.putBoolean("SmallStick", true);
                        prefs.putFloat("MovementSpeed", 10f);
                        prefs.putString("AreYouAGod", "CgkImf-Mg5YTEAIQBA");
                        prefs.putBoolean("AYGUnlocked", false);
                        prefs.putString("StationaryPlanet", "CgkImf-Mg5YTEAIQBQ");
                        prefs.putBoolean("SPUnlocked", false);
                        prefs.putString("OnYourWay", "CgkImf-Mg5YTEAIQBg");
                        prefs.putBoolean("OYWUnlocked", false);
                        prefs.putString("StepUpToTheBigLeagues", "CgkImf-Mg5YTEAIQBw");
                        prefs.putBoolean("SUTTBLUnlocked", false);
                        prefs.putString("ThereMayBeHopeYet", "CgkImf-Mg5YTEAIQCA");
                        prefs.putBoolean("TMBHYUnlocked", false);
                        prefs.putString("AnAbleHero", "CgkImf-Mg5YTEAIQCw");
                        prefs.putBoolean("AAHUnlocked", false);
                        prefs.putString("Masterful", "CgkImf-Mg5YTEAIQDA");
                        prefs.putBoolean("MUnlocked", false);
                        prefs.putString("EasyWeave", "CgkImf-Mg5YTEAIQDQ");
                        prefs.putBoolean("EWUnlocked", false);
                        prefs.putString("NormalWeave", "CgkImf-Mg5YTEAIQDg");
                        prefs.putBoolean("NWUnlocked", false);
                        prefs.putString("HardWeave", "CgkImf-Mg5YTEAIQDw");
                        prefs.putBoolean("HWUnlocked", false);

                        prefs.putBoolean("DefaultSkin", true);
                        prefs.putBoolean("CatSkin", false);
                        prefs.putBoolean("NinjaSkin", false);
                        prefs.putBoolean("PixelSkin", false);
                        prefs.putBoolean("PirateSkin", false);
                        prefs.putBoolean("SoccerSkin", false);
                    }
                    prefs.putBoolean("SumoSkin", false);
                }
                prefs.putString("EasyLeaderboard", "CgkImf-Mg5YTEAIQEA");
                prefs.putString("NormalLeaderboard", "CgkImf-Mg5YTEAIQEQ");
                prefs.putString("HardLeaderboard", "CgkImf-Mg5YTEAIQEg");
                prefs.putBoolean("LeaderBoardChange", true);
            }

            if(!prefs.contains("TutorialPlay")) {
                prefs.putBoolean("TutorialPlay", true);
            }

            if(!prefs.contains("PauseOnInventory")) {
                prefs.putBoolean("PauseOnInventory", true);
            }

            if(!prefs.contains("SumoSkinUnlocked")) {
                if (!prefs.contains("SoccerSkinUnlocked")) {
                    if (!prefs.contains("PirateSkinUnlocked")) {
                        if (!prefs.contains("PixelSkinUnlocked")) {
                            prefs.putBoolean("PixelSkinUnlocked", false);
                            prefs.putBoolean("CatSkinUnlocked", false);
                            prefs.putBoolean("NinjaSkinUnlocked", false);
                        }
                        prefs.putBoolean("PirateSkinUnlocked", false);
                    }
                    prefs.putBoolean("SoccerSkinUnlocked", false);
                }
                prefs.putBoolean("SumoSkinUnlocked", false);
            }

            if(!prefs.contains("FastLaserOnly"))
            {
                prefs.putInteger("FastLaserOnly", 0);
                prefs.putInteger("DualShotOnly", 0);
                prefs.putInteger("PiercingOnly", 0);
                prefs.putInteger("X2InHand", 0);
                prefs.putInteger("HealthInHand", 0);
                prefs.putInteger("BombInHand", 0);
                if(!prefs.contains("PowerupShopVisit")) {
                    prefs.putBoolean("PowerupShopVisit", false);
                }
            }


            if(!prefs.contains("EasyHighScore")) {
                prefs.putInteger("EasyHighScore", 0);
                prefs.putInteger("NormalHighScore", 0);
                prefs.putInteger("HardHighScore", 0);
            }

            if(!prefs.contains("Coins")) {
                prefs.putInteger("Coins", 0);
            }


            if(!prefs.contains("LeftOuterColorR")) {
                prefs.putFloat("LeftOuterColorR", .28f);
                prefs.putFloat("LeftOuterColorG", .43f);
                prefs.putFloat("LeftOuterColorB", .68f);

                prefs.putFloat("LeftInnerColorR", .28f);
                prefs.putFloat("LeftInnerColorG", .43f);
                prefs.putFloat("LeftInnerColorB", .68f);

                prefs.putFloat("RightOuterColorR", .83f);
                prefs.putFloat("RightOuterColorG", 0);
                prefs.putFloat("RightOuterColorB", .02f);

                prefs.putFloat("RightInnerColorR", .83f);
                prefs.putFloat("RightInnerColorG", 0);
                prefs.putFloat("RightInnerColorB", .02f);

                prefs.putFloat("EarthCircleR", .28f);
                prefs.putFloat("EarthCircleG", .43f);
                prefs.putFloat("EarthCircleB", .68f);

                prefs.putFloat("EarthContinentsR", .5f);
                prefs.putFloat("EarthContinentsG", .73f);
                prefs.putFloat("EarthContinentsB", .5f);

                prefs.putFloat("MoonMajorR", Color.LIGHT_GRAY.r);
                prefs.putFloat("MoonMajorG", Color.LIGHT_GRAY.g);
                prefs.putFloat("MoonMajorB", Color.LIGHT_GRAY.b);

                prefs.putFloat("MoonMinorR", Color.DARK_GRAY.r);
                prefs.putFloat("MoonMinorG", Color.DARK_GRAY.g);
                prefs.putFloat("MoonMinorB", Color.DARK_GRAY.b);
            }

            if(!prefs.contains("PremiumVersion")) {
                prefs.putBoolean("PremiumVersion", false);
            }

            if(!prefs.contains("AdViewCount")) {
                prefs.putInteger("AdViewCount", 5);
                prefs.putLong("AdTime", System.currentTimeMillis());
            }
            prefs.flush();
        }


        dialogueLines = new String[]{"General: How far off course are we now?", "Major: We're about to hit the asteroid belt, sir.",
                "General: Is there any way to correct our orbit at this point?", "Major: I'm afraid not, sir.", "General: What are our chances of survival?",
                "Major: ...", "Major: I'd rather not say.", "General: ...", "General: I understand.", "General: Well, we have no choice now. We must use the weapon.", "General: Is the Moon ready?",
                "Major: Yes, sir.", "General: So commences humanity's greatest test.", "General: Our courage must remain unhindered, even in our final hour."};

        currentDialogue = "";

        timeBetweenCharacters = .05f;
        elapsedTime = 0;
        lineEndWaitTime = 2;
        currentDialogueIndex = 0;
        currentCharacterIndex = 0;
        endOfDialogue = false;
        toSwitch = false;
        screenSwitched = false;

        LaserSound = Gdx.audio.newSound(Gdx.files.internal("data/LaserSound.mp3"));
        SmallExplosionSound = Gdx.audio.newSound(Gdx.files.internal("data/SmallExplosion.mp3"));
        PowerupSound = Gdx.audio.newSound(Gdx.files.internal("data/PowerupSound.mp3"));
        MenuButtonSound = Gdx.audio.newSound(Gdx.files.internal("data/MenuClick.mp3"));
        TypeWriterSound = Gdx.audio.newSound(Gdx.files.internal("data/TypeWriterSound.mp3"));
        gameSong = Gdx.audio.newMusic(Gdx.files.internal("data/DST-2ndBallad.mp3"));
        menuSong = Gdx.audio.newMusic(Gdx.files.internal("data/DST-MapOfDoom.mp3"));
        openingSong = Gdx.audio.newMusic(Gdx.files.internal("data/DST-Leres.mp3"));
        catMeow = Gdx.audio.newSound(Gdx.files.internal("data/CatMeow.mp3"));
        coinClick = Gdx.audio.newSound(Gdx.files.internal("data/Click.mp3"));
        errorSound = Gdx.audio.newSound(Gdx.files.internal("data/ErrorSound.mp3"));
        cashRegisterSound = Gdx.audio.newSound(Gdx.files.internal("data/CashRegisterSound.mp3"));

        if (prefs.getBoolean("Sound")) {
            openingSong.play();
            openingSong.setVolume(.6f * prefs.getFloat("MusicVolumeLevel"));
            openingSong.setLooping(false);
        }

        manager = new com.badlogic.gdx.assets.AssetManager();

        //region LoadingAssets
//        if(HDAssets) {
            manager.load("data/SpaceSquare.png", Texture.class);

//        } else {
//            manager.load("data/SpaceSquare1280.png", Texture.class);
//            manager.load("data/SoccerField1280.png", Texture.class);
//            manager.load("data/PirateBackgroundProto1280.png", Texture.class);
//            manager.load("data/YarnBallExperimentGreenless1280.png", Texture.class);
//            manager.load("data/NinjaBackground1280.png", Texture.class);
//            manager.load("data/PixelSpace1280.png", Texture.class);
//        }

        manager.load("data/number.png", Texture.class);
        manager.load("data/poop.png", Texture.class);
        manager.load("data/JoystickOuterWhite.png", Texture.class);
        manager.load("data/JoystickHoverWhite.png", Texture.class);
        manager.load("data/black.png", Texture.class);
        manager.load("data/HitEarthRed.png", Texture.class);
        manager.load("data/PauseButton.png", Texture.class);
        manager.load("data/X2Powerup.png", Texture.class);
        manager.load("data/LifePowerup.png", Texture.class);
        manager.load("data/ShootSpeedPowerup.png", Texture.class);
        manager.load("data/ExplosionPowerup.png", Texture.class);
        manager.load("data/DualSided.png", Texture.class);
        manager.load("data/PierceBulletPowerup.png", Texture.class);
        manager.load("data/LaserBulletWhite.png", Texture.class);



        manager.load("data/EmptyButtonTemplate.png", Texture.class);
        manager.load("data/SquareButton.png", Texture.class);
//        manager.load("data/LargeSquareButton.png", Texture.class);
        manager.load("data/LargeSquareButtonClear.png", Texture.class);
        manager.load("data/WhitePixel.png", Texture.class);








        if(prefs.getBoolean("PixelSkin")) {
            manager.load("data/PixelSpace.png", Texture.class);
            manager.load("data/EmptyCirclePixel.png", Texture.class);
            manager.load("data/ContinentsPixel.png", Texture.class);
            manager.load("data/MoonPixelNew11.png", Texture.class);
            manager.load("data/PixelAsteroidRedone14.png", Texture.class);
        }

        if(prefs.getBoolean("NinjaSkin")) {
            manager.load("data/NinjaBackground1920.png", Texture.class);
            manager.load("data/NinjaSkinProto.png", Texture.class);
            manager.load("data/ShurikenRedone.png", Texture.class);
            manager.load("data/NinjaHatSmall.png", Texture.class);
        }

//        if(prefs.getBoolean("CatSkin")) {
            manager.load("data/CatBackground.png", Texture.class);
            manager.load("data/MouseMeanSmall.png", Texture.class);
            manager.load("data/CatSkinProto.png", Texture.class);
//        }

        if(prefs.getBoolean("PirateSkin")) {
            manager.load("data/JackyPirateFog.png", Texture.class);
            manager.load("data/CanonBallAsteroid100.png", Texture.class);
            manager.load("data/CanonBallShine.png", Texture.class);
            manager.load("data/MoonPirateSkin.png", Texture.class);
            manager.load("data/PirateEarth.png", Texture.class);

        }
        if(prefs.getBoolean("SoccerSkin")) {
            manager.load("data/SoccerField1920.png", Texture.class);
            manager.load("data/EarthSoccer.png", Texture.class);
            manager.load("data/MoonReferee.png", Texture.class);
            manager.load("data/SoccerballAsteroid100.png", Texture.class);
        }
        if(prefs.getBoolean("SumoSkin")) {
            manager.load("data/SumoBackground.png", Texture.class);
            manager.load("data/SumoEarth.png", Texture.class);
            manager.load("data/SumoMoon.png", Texture.class);
            manager.load("data/SumoAsteroid.png", Texture.class);
        }

        manager.load("data/EmptyCircle.png", Texture.class);
        manager.load("data/Continents.png", Texture.class);
        manager.load("data/EmptyCircleGray.png", Texture.class);
//        manager.load("data/EmptyCircleBlack.png", Texture.class);
        manager.load("data/MoonBlotsWhite.png", Texture.class);
        manager.load("data/AsteroidRedone.png", Texture.class);
        manager.load("data/SideMenuButton.png", Texture.class);
        manager.load("data/CoinLarge.png", Texture.class);



//        manager.load("data/GameInventory.png", Texture.class);
//        if(prefs.getBoolean("16x10")) {
//            manager.load("data/Credits16x10.png", Texture.class);
//        } else if(prefs.getBoolean("3x2")) {
//            manager.load("data/Credits3x2.png", Texture.class);
//        } else if(prefs.getBoolean("4x3")) {
//            manager.load("data/Credits4x3.png", Texture.class);
//        } else {
//            manager.load("data/Credits16x9.png", Texture.class);
//        }
//        manager.load("data/RealEarthLaserAnimationThick-packed/pack.atlas", TextureAtlas.class);
//        manager.load("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class);
//
//        manager.load("data/SpeedPowerup-packed/pack.atlas", TextureAtlas.class);
        manager.load("data/number.fnt", BitmapFont.class);
        manager.load("data/poop.fnt", BitmapFont.class);
        //endregion


        backgroundSprite = new Sprite[2];
        for (int i = 0; i < backgroundSprite.length; i++) {
            backgroundSprite[i] = new Sprite(new Texture(Gdx.files.internal("data/SpaceSquare.png")));
            backgroundSprite[i].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
            if (i == 0) {
                backgroundSprite[i].setPosition(0, 0);
            } else {
                backgroundSprite[i].setPosition(0, backgroundSprite[i - 1].getY() - backgroundSprite[i].getHeight());
            }
        }

        blackSprite = new Sprite(new Texture(Gdx.files.internal("data/black.png")));
        blackSprite.setSize(Gdx.graphics.getWidth(), font.getLineHeight() * 1.25f * screenScale);
        blackSprite.setPosition(0, Gdx.graphics.getHeight() / 2 - blackSprite.getHeight());
        blackSprite.setAlpha(.25f);
        backgroundAlpha = 0;
        currentTouch = false;
        previousTouch = currentTouch;
        blackSpriteDarkening = false;
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        backPressed = Gdx.input.isKeyPressed(Input.Keys.BACK);
        if(!screenSwitched) {
            currentTouch = Gdx.input.isTouched();

            if (!endOfDialogue && elapsedTime >= timeBetweenCharacters && currentCharacterIndex != dialogueLines[currentDialogueIndex].length() && currentDialogueIndex != dialogueLines.length && !screenSwitched) {
                currentDialogue += dialogueLines[currentDialogueIndex].charAt(currentCharacterIndex);
                if (prefs.getBoolean("Sound")) {
                    TypeWriterSound.play(.5f * prefs.getFloat("SfxVolumeLevel"));
                }
                currentCharacterIndex++;
                elapsedTime = 0;
            }

            if (!endOfDialogue && currentCharacterIndex == dialogueLines[currentDialogueIndex].length() && elapsedTime > lineEndWaitTime && !screenSwitched) {
                currentDialogueIndex++;
                if (currentDialogueIndex == dialogueLines.length) {
                    endOfDialogue = true;
                } else {
                    currentCharacterIndex = 0;
                    currentDialogue = "";
                }
                elapsedTime = 0;
            }

            if (endOfDialogue && elapsedTime > lineEndWaitTime && manager.update() && !screenSwitched && !toSwitch) {
                if (backgroundAlpha == 0) {
                    blackSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
                    blackSprite.setPosition(0, 0);
                    blackSpriteDarkening = true;
                }
                backgroundAlpha += .01f;
                blackSprite.setAlpha(backgroundAlpha);
            }

            if ((manager.update() && !currentTouch && previousTouch && !screenSwitched) || backgroundAlpha >= 1 && !screenSwitched) {
                toSwitch = true;
            }


            for (Sprite background : backgroundSprite) {
                background.setPosition(background.getX(), background.getY() - 1 * screenScale);
                if (background.getY() + background.getHeight() < 0) {
                    background.setPosition(background.getX(), Gdx.graphics.getHeight());
                }
            }

            managerProgress = manager.getProgress();
            manager.update();
            elapsedTime += Gdx.graphics.getDeltaTime();
            previousTouch = currentTouch;


            if (!screenSwitched) {
                batch.begin();
                for (Sprite background : backgroundSprite) {
                    background.draw(batch);
                }
                if (!blackSpriteDarkening) {
                    blackSprite.draw(batch);
                }
                font.setScale(.85f * screenScale);
                font.setColor(Color.ORANGE);
                if (managerProgress < 1) {
                    font.draw(batch, String.format("Loading: " + df.format(managerProgress * 100) + "%c", '%'), Gdx.graphics.getWidth() - 265 * screenScale, 50 * screenScale);
                } else {
                    font.draw(batch, String.format("Tap anywhere to skip"), Gdx.graphics.getWidth() - 350 * screenScale, 50 * screenScale);
                }
                font.setScale(screenScale * 1.25f);
                font.setColor(Color.WHITE);
                if (!endOfDialogue) {
                    font.draw(batch, String.format(currentDialogue), Gdx.graphics.getWidth() / 2 - (dialogueLines[currentDialogueIndex].length() * 12f) * screenScale, Gdx.graphics.getHeight() / 2);
                } else {
                    font.draw(batch, String.format(currentDialogue), Gdx.graphics.getWidth() / 2 - (dialogueLines[currentDialogueIndex - 1].length() * 12f) * screenScale, Gdx.graphics.getHeight() / 2);
                }
                if (blackSpriteDarkening) {
                    blackSprite.draw(batch);
                }
                batch.end();
            }
        }

        if (toSwitch) {
            openingSong.stop();
            setScreen(new MenuScreen(this));
            toSwitch = false;
            screenSwitched = true;

        }

        previousBackPressed = backPressed;

        super.render();
    }

    public boolean BackPressed() {
        if(!backPressed && previousBackPressed) {
            return true;
        }
        return false;
    }
}

