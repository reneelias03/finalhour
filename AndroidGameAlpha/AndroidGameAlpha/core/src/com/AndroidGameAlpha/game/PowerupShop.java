package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rene__000 on 12/23/2014.
 */
public class PowerupShop implements Screen {

    MyGdxGame game;
    GameButtons[] buttons;
    SpriteBatch batch;
    Sprite background, whitePixel, purchaseDialog, activePowerup, coinTotalSprite, coinTotalSpriteBank, blackSprite;
    Sprite[] powerups;
    BitmapFont goodTimeFont;
    GameButtons backButton, purchaseButton, leftScrollButton, rightScrollButton, inventoryButton;
    boolean purchaseDialogActive, inventoryActive;
    int activeButton, purchaseQuantity;
    boolean screenTouched, previousScreenTouched;
    GameScreen gameScreen;

    public PowerupShop(MyGdxGame game, GameScreen gameScreen) {
        this.game = game;
        this.batch = game.batch;
        this.goodTimeFont = game.goodTimeFont;
        this.gameScreen = gameScreen;

        buttons = new GameButtons[6];
        Texture buttonTexture = game.manager.get("data/EmptyButtonTemplate.png", Texture.class);
        powerups = new Sprite[6];
        powerups[0] = new Sprite(game.manager.get("data/ShootSpeedPowerup.png", Texture.class));
        powerups[1] = new Sprite(game.manager.get("data/DualSided.png", Texture.class));
        powerups[2] = new Sprite(game.manager.get("data/PierceBulletPowerup.png", Texture.class));
        powerups[3] = new Sprite(game.manager.get("data/X2Powerup.png", Texture.class));
        powerups[4] = new Sprite(game.manager.get("data/LifePowerup.png", Texture.class));
        powerups[5] = new Sprite(game.manager.get("data/ExplosionPowerup.png", Texture.class));

        float y = Gdx.graphics.getHeight() * .645f;
        float[] xLocations = new float[3];
        xLocations[0] = Gdx.graphics.getWidth() / 4 - (buttonTexture.getWidth() * game.screenScale) / 2;
        xLocations[1] = Gdx.graphics.getWidth() / 2 - (buttonTexture.getWidth() * game.screenScale) / 2;
        xLocations[2] = Gdx.graphics.getWidth() * .75f - (buttonTexture.getWidth() * game.screenScale) / 2;
        int xCount = 0;
        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new GameButtons(buttonTexture, true, new Vector2(xLocations[xCount], y));
            buttons[i].setSize(buttons[i].getWidth() * game.screenScale, buttons[i].getHeight() * game.screenScale);
            buttons[i].setOriginCenter();
            buttons[i].setColor(.28f, .43f, .68f, 1);
            powerups[i].setSize(buttons[i].getHeight() * .75f, buttons[i].getHeight() * .75f);
            powerups[i].setOriginCenter();
            powerups[i].setPosition(buttons[i].getX() + buttons[i].getOriginX() - powerups[i].getOriginX(), buttons[i].getY() + buttons[i].getOriginY() - powerups[i].getOriginY());

            xCount++;
            if (i == buttons.length / 2 - 1) {
                y -= buttons[i].getHeight() * 2.95f;
                xCount = 0;
            }
        }

        background = new Sprite(this.game.manager.get("data/SpaceSquare.png", Texture.class));
        background.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
        background.setPosition(0, 0);


        whitePixel = new Sprite(game.manager.get("data/WhitePixel.png", Texture.class));
        whitePixel.setSize(Gdx.graphics.getWidth(), 80 * game.screenScale);
        whitePixel.setPosition(0, Gdx.graphics.getHeight() - 150 * game.screenScale);
        whitePixel.setColor(.25f, .43f, .25f, 1);
        whitePixel.setAlpha(.35f);

        backButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        backButton.setSize(backButton.getWidth() * game.screenScale, backButton.getHeight() * game.screenScale);
        backButton.setOriginCenter();
        backButton.setPosition(Gdx.graphics.getWidth() - backButton.getWidth() - backButton.getOriginX() / 2, backButton.getHeight() / 2);

        inventoryButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        inventoryButton.setSize(inventoryButton.getWidth() * game.screenScale, inventoryButton.getHeight() * game.screenScale);
        inventoryButton.setOriginCenter();
        inventoryButton.setPosition(backButton.getX() - inventoryButton.getWidth() * 1.25f, backButton.getHeight() / 2);
        inventoryButton.setColor(Color.PURPLE);

        purchaseDialogActive = false;
        activeButton = 0;

        blackSprite = new Sprite(game.blackSprite.getTexture());
        blackSprite.setAlpha(.95f);
        blackSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        blackSprite.setPosition(0, 0);

        purchaseDialog = new Sprite(game.manager.get("data/LargeSquareButtonClear.png", Texture.class));
        purchaseDialog.setSize(Gdx.graphics.getHeight() * .85f, Gdx.graphics.getHeight() * .85f);
        purchaseDialog.setOriginCenter();
        purchaseDialog.setPosition(Gdx.graphics.getWidth() / 2 - purchaseDialog.getOriginX(), Gdx.graphics.getHeight() / 2 - purchaseDialog.getOriginY());
        purchaseDialog.setColor(.28f, .43f, .68f, 1);
//        purchaseDialog.setAlpha(.95f);

        purchaseButton = new GameButtons(backButton.getTexture(), true, new Vector2().setZero());
        purchaseButton.setSize(backButton.getWidth(), backButton.getHeight());
        purchaseButton.setOriginCenter();
        purchaseButton.setPosition(Gdx.graphics.getWidth() / 2 - purchaseButton.getOriginX(), purchaseDialog.getY() + purchaseButton.getHeight());

        rightScrollButton = new GameButtons(game.manager.get("data/SideMenuButton.png", Texture.class), true, new Vector2().setZero());
        rightScrollButton.setSize(rightScrollButton.getWidth() * .6f * game.screenScale, rightScrollButton.getHeight() * .6f * game.screenScale);
        rightScrollButton.setOriginCenter();
        rightScrollButton.setPosition(Gdx.graphics.getWidth() / 2 + rightScrollButton.getWidth(), Gdx.graphics.getHeight() / 2 - rightScrollButton.getHeight() * .5f);
//        rightScrollButton.setColor(.28f, .43f, .68f, 1);

        leftScrollButton = new GameButtons(game.manager.get("data/SideMenuButton.png", Texture.class), true, new Vector2().setZero());
        leftScrollButton.setSize(rightScrollButton.getWidth(), rightScrollButton.getHeight());
        leftScrollButton.setOriginCenter();
        leftScrollButton.setRotation(180);
        leftScrollButton.setPosition(Gdx.graphics.getWidth() / 2 - rightScrollButton.getWidth() * 2f, rightScrollButton.getY());
//        leftScrollButton.setColor(.28f, .43f, .68f, 1);

        coinTotalSprite = new Sprite(game.manager.get("data/CoinLarge.png", Texture.class));
        coinTotalSprite.setSize(coinTotalSprite.getWidth() * .16f * game.screenScale, coinTotalSprite.getHeight() * .16f * game.screenScale);
        coinTotalSprite.setOriginCenter();
        coinTotalSprite.setPosition(purchaseButton.getX(), purchaseButton.getY() + purchaseButton.getHeight() * 1.5f - 25f * game.screenScale);

        coinTotalSpriteBank = new Sprite(game.manager.get("data/CoinLarge.png", Texture.class));
        coinTotalSpriteBank.setSize(coinTotalSpriteBank.getWidth() * .16f * game.screenScale, coinTotalSpriteBank.getHeight() * .16f * game.screenScale);
        coinTotalSpriteBank.setOriginCenter();
        coinTotalSpriteBank.setPosition(backButton.getOriginX() / 2, backButton.getY());

        purchaseQuantity = 1;
        inventoryActive = game.prefs.getBoolean("PowerupShopVisit");
        if(!game.prefs.getBoolean("PowerupShopVisit")) {
            game.prefs.putBoolean("PowerupShopVisit", true);
            game.prefs.flush();
        }

        game.handler.showAds(false);
    }

    @Override
    public void render(float delta) {
        screenTouched = Gdx.input.isTouched();

        if(purchaseDialogActive) {
            if(game.prefs.getInteger("Coins") >= 25 * purchaseQuantity) {
                purchaseButton.setColor(.45f, .83f, .45f, 1);
            } else {
                purchaseButton.setColor(Color.RED);
            }

            purchaseButton.Update();

            if(purchaseButton.Tapped()) {
                if(game.prefs.getInteger("Coins") >= 25 * purchaseQuantity) {
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.cashRegisterSound.play(.45f * game.prefs.getFloat("SfxVolumeLevel"));

                    }
                    game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") - 25 * purchaseQuantity);
                    if(activeButton == 0) {
                        game.prefs.putInteger("FastLaserOnly", game.prefs.getInteger("FastLaserOnly") + purchaseQuantity);
                    } else if(activeButton == 1) {
                        game.prefs.putInteger("DualShotOnly", game.prefs.getInteger("DualShotOnly") + purchaseQuantity);
                    } else if(activeButton == 2) {
                        game.prefs.putInteger("PiercingOnly", game.prefs.getInteger("PiercingOnly") + purchaseQuantity);
                    } else if(activeButton == 3) {
                        game.prefs.putInteger("X2InHand", game.prefs.getInteger("X2InHand") + purchaseQuantity);
                    } else if(activeButton == 4) {
                        game.prefs.putInteger("HealthInHand", game.prefs.getInteger("HealthInHand") + purchaseQuantity);
                    } else if (activeButton == 5) {
                        game.prefs.putInteger("BombInHand", game.prefs.getInteger("BombInHand") + purchaseQuantity);
                    }
                    purchaseDialogActive = false;
                } else {
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.errorSound.play(.75f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
                game.prefs.flush();
            }


            if(purchaseQuantity > 1) {
                leftScrollButton.Update();
            } else {
                leftScrollButton.setTapped(false);
            }
            rightScrollButton.Update();
            if(leftScrollButton.Tapped()) {
                purchaseQuantity--;
            }
            if(rightScrollButton.Tapped()) {
                purchaseQuantity++;
            }

            if(!screenTouched && previousScreenTouched && !GestureManager.IsCurrentlyBeingTouched(purchaseDialog.getBoundingRectangle())) {
                purchaseDialogActive = !purchaseDialogActive;
            }
        }
        else {
            backButton.Update();
            inventoryButton.Update();

            if(inventoryButton.Tapped()) {
                inventoryActive = !inventoryActive;
            }
        }

        if(backButton.Tapped() || game.BackPressed()) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            if(gameScreen == null) {
                game.setScreen(new CustomizePortal(game));
            } else {
//                gameScreen.previousCoinAddition = game.prefs.getInteger("Coins");
                game.setScreen(gameScreen);
            }
            game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
            this.dispose();
        }

        for(int i = 0; i < buttons.length; i++)
        {
            if(!purchaseDialogActive) {
                buttons[i].Update();
                if(buttons[i].Tapped()){
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                    purchaseDialogActive = true;
                    purchaseQuantity = 1;
                    activeButton = i;
                    activePowerup =  new Sprite(powerups[i].getTexture());
                    activePowerup.setSize(powerups[i].getWidth() * 2, powerups[i].getHeight() * 2);
                    activePowerup.setOriginCenter();
                    activePowerup.setPosition(Gdx.graphics.getWidth() / 2 - activePowerup.getOriginX(), Gdx.graphics.getHeight() * .65f);
                    break;
                }
            }
        }



        previousScreenTouched = screenTouched;

        //draw

        game.batch.begin();
        background.draw(batch);
//        whitePixel.setSize(Gdx.graphics.getWidth(), 80 * game.screenScale);
//        whitePixel.setPosition(0, Gdx.graphics.getHeight() - 150 * game.screenScale);
//        whitePixel.setColor(Color.GREEN);
//        whitePixel.setAlpha(.35f);
        whitePixel.draw(batch);
        whitePixel.setSize(buttons[1].getWidth() * .8f, 10 * game.screenScale);
        whitePixel.setPosition(buttons[1].getX() + 35f * game.screenScale, whitePixel.getY() - 95f * game.screenScale);
        whitePixel.setColor(.25f, .43f, .25f, 1);
        whitePixel.setAlpha(1);
        whitePixel.draw(batch);
        whitePixel.setSize(buttons[4].getWidth() * 1.55f, 10 * game.screenScale);
        whitePixel.setPosition(buttons[4].getX() - 95f * game.screenScale, buttons[4].getY() + buttons[4].getHeight() * 1.15f);
        whitePixel.draw(batch);
        for (GameButtons button : buttons) {
            button.draw(batch);
        }
        for (Sprite powerup : powerups) {
            powerup.draw(batch);
        }
        whitePixel.setSize(Gdx.graphics.getWidth(), 80 * game.screenScale);
        whitePixel.setPosition(0, Gdx.graphics.getHeight() - 150 * game.screenScale);
        whitePixel.setColor(.25f, .43f, .25f, 1);
        whitePixel.setAlpha(.35f);
        backButton.draw(batch);
        inventoryButton.draw(batch);
        coinTotalSpriteBank.draw(batch);
        TextDrawing();
        if(purchaseDialogActive)
        {
            blackSprite.draw(batch);
            purchaseDialog.draw(batch);
            purchaseButton.draw(batch);
            activePowerup.draw(batch);
            coinTotalSprite.draw(batch);
            if(purchaseQuantity > 1) {
                leftScrollButton.draw(batch);
            }
            rightScrollButton.draw(batch);
            PurchaseDialogTextDraw();
        }
        game.batch.end();
    }


    private void PurchaseDialogTextDraw() {
        goodTimeFont.setScale(game.screenScale * .39f);
        goodTimeFont.draw(game.batch, "Buy", purchaseButton.getX() + 100f * game.screenScale, purchaseButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
        goodTimeFont.setColor(Color.BLACK);
        if(activeButton == 0) {
            goodTimeFont.draw(game.batch, "Laser Speed Boost", activePowerup.getX() - 295f * game.screenScale, activePowerup.getY() - 25f * game.screenScale);
        } else if(activeButton == 1) {
            goodTimeFont.draw(game.batch, "Dual Shot", activePowerup.getX() - 115f * game.screenScale, activePowerup.getY() - 25f * game.screenScale);
        } else if(activeButton == 2) {
            goodTimeFont.draw(game.batch, "Piercing Laser", activePowerup.getX() - 195f * game.screenScale, activePowerup.getY() - 25f * game.screenScale);
        } else if(activeButton == 3) {
            goodTimeFont.draw(game.batch, "X2 Multiplier", activePowerup.getX() - 185f * game.screenScale, activePowerup.getY() - 25f * game.screenScale);
        } else if(activeButton == 4) {
            goodTimeFont.draw(game.batch, "Health", activePowerup.getX() - 47.5f * game.screenScale, activePowerup.getY() - 25f * game.screenScale);
        } else if(activeButton == 5) {
            goodTimeFont.draw(game.batch, "Bomb", activePowerup.getX() - 17.5f * game.screenScale, activePowerup.getY() - 25f * game.screenScale);
        }
        goodTimeFont.setColor(Color.WHITE);
        float minusAmount;
        if(purchaseQuantity == 1) {
            minusAmount = 10f;
        } else if(purchaseQuantity >= 2 && purchaseQuantity <= 9) {
            minusAmount = 20f;
        } else if(purchaseQuantity >= 10 && purchaseQuantity <= 19) {
            minusAmount = 30f;
        } else {
            minusAmount = 45f;
        }
        goodTimeFont.draw(game.batch, String.format("%d", purchaseQuantity), Gdx.graphics.getWidth() / 2 - minusAmount * game.screenScale, rightScrollButton.getY() + rightScrollButton.getHeight() * .75f);
        goodTimeFont.setScale(game.screenScale * .25f);
        String cost = String.format("%d", 25 * purchaseQuantity);
        goodTimeFont.draw(game.batch, "Cost: " + cost, coinTotalSprite.getX() + coinTotalSprite.getWidth() + coinTotalSprite.getOriginX(), purchaseButton.getY() + purchaseButton.getHeight() * 1.5f + 55 * game.screenScale);
        String currentUserTotal = String.format("Bank: %d", game.prefs.getInteger("Coins"));
//                String currentUserTotal = "Bank: 1050";
        goodTimeFont.draw(game.batch, currentUserTotal, coinTotalSprite.getX() + coinTotalSprite.getWidth() + coinTotalSprite.getOriginX(), purchaseButton.getY() + purchaseButton.getHeight() * 1.5f + 5 * game.screenScale);

        if(activeButton > 2 && activeButton < 6) {
            goodTimeFont.setScale(.15f * game.screenScale);
            goodTimeFont.setColor(Color.RED);
            goodTimeFont.draw(batch, "** Only one assistance powerup of each", purchaseDialog.getX() + purchaseDialog.getWidth() * .15f, purchaseDialog.getY() + goodTimeFont.getLineHeight() + 75f * game.screenScale);
            goodTimeFont.draw(batch, "type can be used each match **", purchaseDialog.getX() + purchaseDialog.getWidth() * .25f, purchaseDialog.getY() + goodTimeFont.getLineHeight() + 45f * game.screenScale);

            goodTimeFont.setColor(Color.WHITE);
        }
    }

    private void TextDrawing() {
        goodTimeFont.setScale(.55f * game.screenScale);
        goodTimeFont.setColor(Color.WHITE);
//        goodTimeFont.setColor(Color.BLACK);
        goodTimeFont.draw(game.batch, "Back", backButton.getX() + 29 * game.screenScale, backButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
        String pageTitle = "Powerup Shop";
        goodTimeFont.draw(batch, pageTitle, Gdx.graphics.getWidth() / 2 - pageTitle.length() * 35f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
        goodTimeFont.setScale(.45f * game.screenScale);
        goodTimeFont.draw(batch, "Laser", buttons[1].getX() + 30f * game.screenScale, whitePixel.getY() - 25f * game.screenScale);
        goodTimeFont.draw(batch, "Assistance", buttons[4].getX() - 95f * game.screenScale, buttons[4].getY() + buttons[4].getHeight() * 1.72f);
        goodTimeFont.draw(game.batch, String.format("Bank: %d", game.prefs.getInteger("Coins")), coinTotalSpriteBank.getX() + coinTotalSpriteBank.getWidth() * 1.25f, coinTotalSpriteBank.getY() + goodTimeFont.getLineHeight() * .85f);

        if(!inventoryActive) {
            goodTimeFont.setScale(.29f * game.screenScale);
            goodTimeFont.draw(batch, "Inventory", inventoryButton.getX() + 15f * game.screenScale, inventoryButton.getY() + goodTimeFont.getLineHeight() + 25f * game.screenScale);
            goodTimeFont.setScale(.145f * game.screenScale);
            goodTimeFont.draw(batch, "The Laser Speed Boost", buttons[0].getX(), buttons[0].getY() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "will be the only laser", buttons[0].getX(), buttons[0].getY() - goodTimeFont.getLineHeight() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "modifying powerup", buttons[0].getX() + 25f * game.screenScale, buttons[0].getY() - goodTimeFont.getLineHeight() * 2 - 20f * game.screenScale);
            goodTimeFont.draw(batch, "that spawns.", buttons[0].getX() + 75f * game.screenScale, buttons[0].getY() - goodTimeFont.getLineHeight() * 3 - 20f * game.screenScale);

            goodTimeFont.draw(batch, "The Dual Shot", buttons[1].getX() + 65f * game.screenScale, buttons[1].getY() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "will be the only laser", buttons[1].getX(), buttons[1].getY() - goodTimeFont.getLineHeight() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "modifying powerup", buttons[1].getX() + 25f * game.screenScale, buttons[1].getY() - goodTimeFont.getLineHeight() * 2 - 20f * game.screenScale);
            goodTimeFont.draw(batch, "that spawns.", buttons[1].getX() + 75f * game.screenScale, buttons[1].getY() - goodTimeFont.getLineHeight() * 3 - 20f * game.screenScale);

            goodTimeFont.draw(batch, "The Piercing Laser", buttons[2].getX() + 35f * game.screenScale, buttons[2].getY() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "will be the only laser", buttons[2].getX(), buttons[2].getY() - goodTimeFont.getLineHeight() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "modifying powerup", buttons[2].getX() + 25f * game.screenScale, buttons[2].getY() - goodTimeFont.getLineHeight() * 2 - 20f * game.screenScale);
            goodTimeFont.draw(batch, "that spawns.", buttons[2].getX() + 75f * game.screenScale, buttons[2].getY() - goodTimeFont.getLineHeight() * 3 - 20f * game.screenScale);

            goodTimeFont.draw(batch, "A X2 Multiplier", buttons[3].getX() + 60f * game.screenScale, buttons[3].getY() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "in your inventory", buttons[3].getX() + 40f * game.screenScale, buttons[3].getY() - goodTimeFont.getLineHeight() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "to be used at any", buttons[3].getX() + 43f * game.screenScale, buttons[3].getY() - goodTimeFont.getLineHeight() * 2 - 20f * game.screenScale);
            goodTimeFont.draw(batch, "time during game.", buttons[3].getX() + 46f * game.screenScale, buttons[3].getY() - goodTimeFont.getLineHeight() * 3 - 20f * game.screenScale);

            goodTimeFont.draw(batch, "A Health Up", buttons[4].getX() + 85f * game.screenScale, buttons[4].getY() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "in your inventory", buttons[4].getX() + 40f * game.screenScale, buttons[4].getY() - goodTimeFont.getLineHeight() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "to be used at any", buttons[4].getX() + 43f * game.screenScale, buttons[4].getY() - goodTimeFont.getLineHeight() * 2 - 20f * game.screenScale);
            goodTimeFont.draw(batch, "time during game.", buttons[4].getX() + 46f * game.screenScale, buttons[4].getY() - goodTimeFont.getLineHeight() * 3 - 20f * game.screenScale);

            goodTimeFont.draw(batch, "A Bomb", buttons[5].getX() + 120f * game.screenScale, buttons[5].getY() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "in your inventory", buttons[5].getX() + 40f * game.screenScale, buttons[5].getY() - goodTimeFont.getLineHeight() - 20f * game.screenScale);
            goodTimeFont.draw(batch, "to be used at any", buttons[5].getX() + 43f * game.screenScale, buttons[5].getY() - goodTimeFont.getLineHeight() * 2 - 20f * game.screenScale);
            goodTimeFont.draw(batch, "time during game.", buttons[5].getX() + 46f * game.screenScale, buttons[5].getY() - goodTimeFont.getLineHeight() * 3 - 20f * game.screenScale);
        } else {
            goodTimeFont.setScale(.25f * game.screenScale);
            goodTimeFont.draw(batch, "Description", inventoryButton.getX() + 18f * game.screenScale, inventoryButton.getY() + goodTimeFont.getLineHeight() + 30f * game.screenScale);
            goodTimeFont.setScale(.22f * game.screenScale);

            int currentQuantity = game.prefs.getInteger("FastLaserOnly");
            float minusAmount = MinusAmountReturn(currentQuantity);

            goodTimeFont.setColor(.45f, .83f, .45f, 1);
            goodTimeFont.draw(batch, "Inventory:", buttons[0].getX() + 55f * game.screenScale, buttons[0].getY() - 20f * game.screenScale);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, String.format("%d", currentQuantity),  buttons[0].getX() + buttons[0].getOriginX() - minusAmount * game.screenScale, buttons[0].getY() - 70f * game.screenScale);

            currentQuantity = game.prefs.getInteger("DualShotOnly");
            minusAmount = MinusAmountReturn(currentQuantity);

            goodTimeFont.setColor(.45f, .83f, .45f, 1);
            goodTimeFont.draw(batch, "Inventory:", buttons[1].getX() + 55f * game.screenScale, buttons[1].getY() - 20f * game.screenScale);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, String.format("%d", currentQuantity),  buttons[1].getX() + buttons[1].getOriginX() - minusAmount * game.screenScale, buttons[1].getY() - 70f * game.screenScale);

            currentQuantity = game.prefs.getInteger("PiercingOnly");
            minusAmount = MinusAmountReturn(currentQuantity);

            goodTimeFont.setColor(.45f, .83f, .45f, 1);
            goodTimeFont.draw(batch, "Inventory:", buttons[2].getX() + 55f  * game.screenScale, buttons[2].getY() - 20f * game.screenScale);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, String.format("%d", currentQuantity),  buttons[2].getX() + buttons[2].getOriginX() - minusAmount * game.screenScale, buttons[2].getY() - 70f * game.screenScale);

            currentQuantity = game.prefs.getInteger("X2InHand");
            minusAmount = MinusAmountReturn(currentQuantity);

            goodTimeFont.setColor(.45f, .83f, .45f, 1);
            goodTimeFont.draw(batch, "Inventory:", buttons[3].getX() + 55f  * game.screenScale, buttons[3].getY() - 20f * game.screenScale);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, String.format("%d", currentQuantity),  buttons[3].getX() + buttons[3].getOriginX() - minusAmount * game.screenScale, buttons[3].getY() - 70f * game.screenScale);

            currentQuantity = game.prefs.getInteger("HealthInHand");
            minusAmount = MinusAmountReturn(currentQuantity);

            goodTimeFont.setColor(.45f, .83f, .45f, 1);
            goodTimeFont.draw(batch, "Inventory:", buttons[4].getX() + 55f  * game.screenScale, buttons[4].getY() - 20f * game.screenScale);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, String.format("%d", currentQuantity),  buttons[4].getX() + buttons[4].getOriginX() - minusAmount * game.screenScale, buttons[4].getY() - 70f * game.screenScale);

            currentQuantity = game.prefs.getInteger("BombInHand");
            minusAmount = MinusAmountReturn(currentQuantity);

            goodTimeFont.setColor(.45f, .83f, .45f, 1);
            goodTimeFont.draw(batch, "Inventory:", buttons[5].getX() + 55f  * game.screenScale, buttons[5].getY() - 20f * game.screenScale);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, String.format("%d", currentQuantity),  buttons[5].getX() + buttons[5].getOriginX() - minusAmount * game.screenScale, buttons[5].getY() - 70f * game.screenScale);
        }


    }

    public float MinusAmountReturn(int currentQuantity) {
        float minusAmount;
        if(currentQuantity == 1) {
            minusAmount = 5f;
        } else if((currentQuantity >= 2 && currentQuantity <= 9)  || currentQuantity == 0) {
            minusAmount = 13f;
        } else if(currentQuantity >= 10 && currentQuantity <= 19) {
            minusAmount = 20f;
        } else {
            minusAmount = 25f;
        }

        return minusAmount;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
