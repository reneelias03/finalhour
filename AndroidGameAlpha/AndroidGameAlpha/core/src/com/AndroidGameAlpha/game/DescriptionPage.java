package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rene__000 on 12/15/2014.
 */
public class DescriptionPage implements Screen {

    MyGdxGame game;
    BitmapFont goodTimeFont;
    Sprite[] asteroids, powerups;
    SpriteBatch batch;
    Sprite background, whitePixel;
    GameButtons backButton, creditsButton;

    public DescriptionPage(MyGdxGame game) {
        this.game = game;
        this.goodTimeFont = game.goodTimeFont;
        this.batch = game.batch;

        asteroids = new Sprite[4];
        asteroids[0] = new Sprite(game.manager.get("data/AsteroidRedone.png", Texture.class));
        asteroids[0].setSize(50 * game.screenScale, 50 * game.screenScale);
        asteroids[0].setPosition(Gdx.graphics.getWidth() / 2 + 152.5f * game.screenScale, Gdx.graphics.getHeight() / 2 + 200f * game.screenScale);
        asteroids[1] = new Sprite(game.manager.get("data/AsteroidRedone.png", Texture.class));
        asteroids[1].setSize(50 * game.screenScale, 50 * game.screenScale);
        asteroids[1].setPosition(Gdx.graphics.getWidth() / 2 + 152.5f * game.screenScale, Gdx.graphics.getHeight() / 2 + 50f * game.screenScale);
        asteroids[1].setColor(Color.RED);
        asteroids[2] = new Sprite(game.manager.get("data/AsteroidRedone.png", Texture.class));
        asteroids[2].setSize(50 * game.screenScale, 50 * game.screenScale);
        asteroids[2].setPosition(Gdx.graphics.getWidth() / 2 + 152.5f * game.screenScale, Gdx.graphics.getHeight() / 2 - 100f * game.screenScale);
        asteroids[2].setColor(Color.YELLOW);
        asteroids[3] = new Sprite(game.manager.get("data/AsteroidRedone.png", Texture.class));
        asteroids[3].setSize(155 * game.screenScale, 155 * game.screenScale);
        asteroids[3].setPosition(Gdx.graphics.getWidth() / 2 + 50 * game.screenScale, Gdx.graphics.getHeight() / 2 - 335f * game.screenScale);
        for(Sprite asteroid : asteroids){
            asteroid.setOriginCenter();
        }

        powerups = new Sprite[6];
        powerups[0] = new Sprite(game.manager.get("data/X2Powerup.png", Texture.class));
        powerups[1] = new Sprite(game.manager.get("data/ShootSpeedPowerup.png", Texture.class));
        powerups[2] = new Sprite(game.manager.get("data/PierceBulletPowerup.png", Texture.class));
        powerups[3] = new Sprite(game.manager.get("data/LifePowerup.png", Texture.class));
        powerups[4] = new Sprite(game.manager.get("data/ExplosionPowerup.png", Texture.class));
        powerups[5] = new Sprite(game.manager.get("data/DualSided.png", Texture.class));
        int powerupX = (int)( Gdx.graphics.getWidth() / 2f - 300f * game.screenScale);
        int powerupY = (int)( Gdx.graphics.getHeight() / 2 + 200f * game.screenScale);
        for(int i = 0; i < powerups.length; i++)
        {
            powerups[i].setSize(50f * game.screenScale, 50f * game.screenScale);
            if(i == powerups.length / 2) {
                powerupX = (int)( Gdx.graphics.getWidth() / 2f - 750f * game.screenScale);
                powerupY = (int)( Gdx.graphics.getHeight() / 2 + 200f * game.screenScale);
            }
            powerups[i].setPosition(powerupX, powerupY);
            powerupY -= 250f * game.screenScale;
        }
        powerups[1].setY(powerups[1].getY() + 25f * game.screenScale);
        powerups[4].setY(powerups[4].getY() + 25f * game.screenScale);

        backButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        backButton.setSize(backButton.getWidth() * game.screenScale, backButton.getHeight() * game.screenScale);
        backButton.setOriginCenter();
        backButton.setPosition(Gdx.graphics.getWidth() - backButton.getWidth() - backButton.getOriginX() / 2, backButton.getHeight() / 2);

        creditsButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        creditsButton.setSize(creditsButton.getWidth() * game.screenScale, creditsButton.getHeight() * game.screenScale);
        creditsButton.setOriginCenter();
        creditsButton.setPosition(backButton.getX() - backButton.getWidth() - backButton.getOriginX() / 2, backButton.getY());
        creditsButton.setColor(Color.RED);

        background = new Sprite(this.game.manager.get("data/SpaceSquare.png", Texture.class));
        background.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
        background.setPosition(0, 0);

        whitePixel = new Sprite(game.manager.get("data/WhitePixel.png", Texture.class));
        whitePixel.setSize(Gdx.graphics.getWidth(), 80 * game.screenScale);
        whitePixel.setPosition(0, Gdx.graphics.getHeight() - 150 * game.screenScale);
//        whitePixel.setColor(Color.BLUE);
        whitePixel.setAlpha(.35f);

        game.blackSprite.setPosition(0,0);
        game.blackSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.blackSprite.setAlpha(.2f);

        game.handler.showAds(false);
    }


    @Override
    public void render(float delta) {
        backButton.Update();
        creditsButton.Update();

        if(backButton.Tapped() || Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
            game.setScreen(new MenuScreen(game));
            this.dispose();
        }

        if(creditsButton.Tapped()) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            if(game.prefs.getBoolean("16x10")) {
                game.manager.load("data/Credits16x10.png", Texture.class);
            } else if(game.prefs.getBoolean("3x2")) {
                game.manager.load("data/Credits3x2.png", Texture.class);
            } else if(game.prefs.getBoolean("4x3")) {
                game.manager.load("data/Credits4x3.png", Texture.class);
            } else {
                game.manager.load("data/Credits16x9.png", Texture.class);
            }
            game.manager.finishLoading();
//            game.handler.showAds(true);
            game.setScreen(new CreditsScreen(game));
            this.dispose();
        }


        for(Sprite asteroid : asteroids){
            asteroid.rotate(-.5f);
        }
        //Draw
        batch.begin();
        background.draw(batch);
        game.blackSprite.draw(batch);
        whitePixel.setSize(Gdx.graphics.getWidth(), 80 * game.screenScale);
        whitePixel.setPosition(0, Gdx.graphics.getHeight() - 150 * game.screenScale);
        whitePixel.setAlpha(.35f);
        whitePixel.draw(batch);
        whitePixel.setSize(440f * game.screenScale, 10 * game.screenScale);
        whitePixel.setPosition(243f * game.screenScale, Gdx.graphics.getHeight() - 260f * game.screenScale);
        whitePixel.setAlpha(1);
        whitePixel.draw(batch);
        whitePixel.setSize(435f * game.screenScale, 10 * game.screenScale);
        whitePixel.setPosition(Gdx.graphics.getWidth() / 2 + 300f * game.screenScale, Gdx.graphics.getHeight() - 260f * game.screenScale);
        whitePixel.setAlpha(1);
        whitePixel.draw(batch);
        backButton.draw(batch);
        creditsButton.draw(batch);
        for(Sprite asteroid : asteroids){
            asteroid.draw(batch);
        }
        for(Sprite powerup : powerups) {
            powerup.draw(batch);
        }
        whitePixel.setPosition(0, Gdx.graphics.getHeight() - 150 * game.screenScale);
        TextDrawing();
        batch.end();
    }

    private void TextDrawing() {
        goodTimeFont.setScale(.55f * game.screenScale);
        goodTimeFont.setColor(Color.WHITE);
//        goodTimeFont.setColor(Color.BLACK);
        String pageTitle = "Information";
        goodTimeFont.draw(batch, pageTitle, Gdx.graphics.getWidth() / 2 - pageTitle.length() * 32f * game.screenScale, whitePixel.getY() + goodTimeFont.getLineHeight() - 25f * game.screenScale);
        goodTimeFont.draw(game.batch, "Back", backButton.getX() + 29 * game.screenScale, backButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
        goodTimeFont.setScale(.41f * game.screenScale);
        goodTimeFont.draw(game.batch, "Credits", creditsButton.getX() + 14f * game.screenScale, creditsButton.getY() + goodTimeFont.getLineHeight() + 12.5f * game.screenScale);
        goodTimeFont.setScale(.4f * game.screenScale);
        goodTimeFont.draw(batch, "Powerups", Gdx.graphics.getWidth() / 2 - 720f * game.screenScale, whitePixel.getY() - 40f * game.screenScale);
        goodTimeFont.draw(batch, "Asteroids", Gdx.graphics.getWidth() / 2 + 300f * game.screenScale, whitePixel.getY() - 40f * game.screenScale);
        goodTimeFont.setScale(.2f * game.screenScale);
//        goodTimeFont.setColor(Color.RED);
        goodTimeFont.draw(batch, "Health", powerups[3].getX() - 50f * game.screenScale, powerups[3].getY() - 20f * game.screenScale);
        goodTimeFont.draw(batch, "Score x2", powerups[0].getX() - 70f * game.screenScale, powerups[0].getY() - 20f * game.screenScale);
        goodTimeFont.draw(batch, "Bomb", powerups[4].getX() - 33f * game.screenScale, powerups[4].getY() - 20f * game.screenScale);
        goodTimeFont.draw(batch, "Laser Speed Boost", powerups[1].getX() - 170f * game.screenScale, powerups[1].getY() - 20f * game.screenScale);
        goodTimeFont.draw(batch, "DualShot", powerups[5].getX() - 74f * game.screenScale, powerups[5].getY() - 20f * game.screenScale);
        goodTimeFont.draw(batch, "Piercing Laser", powerups[2].getX() - 130f * game.screenScale, powerups[2].getY() - 20f * game.screenScale);

        goodTimeFont.draw(batch, "Asteroid", asteroids[0].getX() + asteroids[0].getWidth() + 20f * game.screenScale, asteroids[0].getY() + goodTimeFont.getLineHeight());
        goodTimeFont.draw(batch, "Explosive Asteroid", asteroids[1].getX() + asteroids[1].getWidth() + 20f * game.screenScale, asteroids[1].getY() + goodTimeFont.getLineHeight());
        goodTimeFont.draw(batch, "Magnetic Asteroid", asteroids[2].getX() + asteroids[2].getWidth() + 20f * game.screenScale, asteroids[2].getY() + goodTimeFont.getLineHeight());
        goodTimeFont.draw(batch, "Huge Asteroid", asteroids[3].getX() + asteroids[3].getWidth() + 20f * game.screenScale, asteroids[3].getY() + goodTimeFont.getLineHeight() * 3.68f);

        goodTimeFont.setScale(.15f * game.screenScale);
        goodTimeFont.setColor(.28f, .43f, .68f, 1);
        goodTimeFont.draw(batch, "Gives one life back.", powerups[3].getX() - 130f * game.screenScale, powerups[3].getY() - 55f * game.screenScale);
        goodTimeFont.draw(batch, "Only spawns when", powerups[3].getX() - 130f * game.screenScale, powerups[3].getY() - 85f * game.screenScale);
        goodTimeFont.draw(batch, "health is not full.", powerups[3].getX() - 120f * game.screenScale, powerups[3].getY() - 115f * game.screenScale);

        goodTimeFont.draw(batch, "Each asteroid is worth", powerups[0].getX() - 160f * game.screenScale, powerups[0].getY() - 55f * game.screenScale);
        goodTimeFont.draw(batch, "twice as many points.", powerups[0].getX() - 145f * game.screenScale, powerups[0].getY() - 85f * game.screenScale);

        goodTimeFont.draw(batch, "A giant bomb is", powerups[4].getX() - 95f * game.screenScale, powerups[4].getY() - 55f * game.screenScale);
        goodTimeFont.draw(batch, "detonated from the", powerups[4].getX() - 130f * game.screenScale, powerups[4].getY() - 85f * game.screenScale);
        goodTimeFont.draw(batch, "center of the Earth.", powerups[4].getX() - 130f * game.screenScale, powerups[4].getY() - 115f * game.screenScale);
        goodTimeFont.draw(batch, "All asteroids get", powerups[4].getX() - 115f * game.screenScale, powerups[4].getY() - 145f * game.screenScale);
        goodTimeFont.draw(batch, "destroyed.", powerups[4].getX() - 60f * game.screenScale, powerups[4].getY() - 175f * game.screenScale);

        goodTimeFont.draw(batch, "Laser shoots nearly", powerups[1].getX() - 140f * game.screenScale, powerups[1].getY() - 55f * game.screenScale);
        goodTimeFont.draw(batch, "twice as fast as usual.", powerups[1].getX() - 155f * game.screenScale, powerups[1].getY() - 85f * game.screenScale);

        goodTimeFont.draw(batch, "A nearby asteroid", powerups[5].getX() - 125f * game.screenScale, powerups[5].getY() - 55f * game.screenScale);
        goodTimeFont.draw(batch, "is pulled into Earth's", powerups[5].getX() - 145f * game.screenScale, powerups[5].getY() - 85f * game.screenScale);
        goodTimeFont.draw(batch, "orbit and used as", powerups[5].getX() - 115f * game.screenScale, powerups[5].getY() - 115f * game.screenScale);
        goodTimeFont.draw(batch, "a second laser", powerups[5].getX() - 100f * game.screenScale, powerups[5].getY() - 145f * game.screenScale);
        goodTimeFont.draw(batch, "focal point.", powerups[5].getX() - 65f * game.screenScale, powerups[5].getY() - 175f * game.screenScale);

        goodTimeFont.draw(batch, "Laser bullets disappear", powerups[2].getX() - 175f * game.screenScale, powerups[2].getY() - 55f * game.screenScale);
        goodTimeFont.draw(batch, "after hitting two asteroids", powerups[2].getX() - 197f * game.screenScale, powerups[2].getY() - 85f * game.screenScale);
        goodTimeFont.draw(batch, "rather than just one.", powerups[2].getX() - 145f * game.screenScale, powerups[2].getY() - 115f * game.screenScale);


        goodTimeFont.draw(batch, "Although they do nothing more than", asteroids[0].getX() + asteroids[0].getWidth() + 20f * game.screenScale, asteroids[0].getY());
        goodTimeFont.draw(batch, "move across space, getting stuck in a ", asteroids[0].getX() + asteroids[0].getWidth() + 20f * game.screenScale, asteroids[0].getY() - goodTimeFont.getLineHeight());
        goodTimeFont.draw(batch, "cluster of these is highly dangerous.", asteroids[0].getX() + asteroids[0].getWidth() + 20f * game.screenScale, asteroids[0].getY() - goodTimeFont.getLineHeight() * 2);

        goodTimeFont.draw(batch, "Release a giant explosion when", asteroids[1].getX() + asteroids[1].getWidth() + 20f * game.screenScale, asteroids[1].getY());
        goodTimeFont.draw(batch, "shot. Anything caught in the blast", asteroids[1].getX() + asteroids[1].getWidth() + 20f * game.screenScale, asteroids[1].getY() - goodTimeFont.getLineHeight());
        goodTimeFont.draw(batch, "radius will take damage, including Earth.", asteroids[1].getX() + asteroids[1].getWidth() + 20f * game.screenScale, asteroids[1].getY() - goodTimeFont.getLineHeight() * 2);

        goodTimeFont.draw(batch, "Highly magnetic properties cause", asteroids[2].getX() + asteroids[2].getWidth() + 20f * game.screenScale, asteroids[2].getY());
        goodTimeFont.draw(batch, "this asteroid to follow the Earth.", asteroids[2].getX() + asteroids[2].getWidth() + 20f * game.screenScale, asteroids[2].getY() - goodTimeFont.getLineHeight());
        goodTimeFont.draw(batch, "Immediate eradication is advised.", asteroids[2].getX() + asteroids[2].getWidth() + 20f * game.screenScale, asteroids[2].getY() - goodTimeFont.getLineHeight() * 2);

        goodTimeFont.draw(batch, "These cosmic behemoths take four", asteroids[3].getX() + asteroids[3].getWidth() + 20f * game.screenScale, asteroids[3].getY() + goodTimeFont.getLineHeight() * 3.5f);
        goodTimeFont.draw(batch, "laser bullets to be taken down.", asteroids[3].getX() + asteroids[3].getWidth() + 20f * game.screenScale, asteroids[3].getY()  + goodTimeFont.getLineHeight() * 2.5f);
        goodTimeFont.draw(batch, "They then split into four asteroids.", asteroids[3].getX() + asteroids[3].getWidth() + 20f * game.screenScale, asteroids[3].getY() + goodTimeFont.getLineHeight() * 1.5f);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

}
