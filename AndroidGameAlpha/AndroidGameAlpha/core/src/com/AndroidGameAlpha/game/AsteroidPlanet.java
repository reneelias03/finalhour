package com.AndroidGameAlpha.game;

import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rene__000 on 9/15/2014.
 */
public class AsteroidPlanet extends Sprite{

    protected Animation _explosionAnimation;
    private Vector2 _destinationLocation;
    protected Vector2 _speed;
    protected boolean _toExplode;
    private boolean _destinationMet;
    protected boolean _exploded;
    public boolean Exploded()
    {
        return _exploded;
    }
    public boolean DestinationMet()
    {
        return _destinationMet;
    }
    private float _attachDistance;
    protected float _attachSpeed;
    protected float _elapsedFrameTime;
    private float _gameScreenScale;
    protected TextureRegion _currentFrame;

    public AsteroidPlanet(Asteroid asteroid, Vector2 destinationLocation, float attachDistance, float attachSpeed, TextureAtlas explosionAtlas)
    {
        super(asteroid.OriginalTexture());
        this.setSize(asteroid._originalSize.x, asteroid._originalSize.y);
        this.setPosition(asteroid.getX(), asteroid.getY());
        this.setOriginCenter();
        _attachSpeed = attachSpeed;
        _speed = new Vector2(destinationLocation.x - this.getX(), destinationLocation.y - this.getY());
        _speed.nor();
        _speed.x = _speed.x * _attachSpeed;
        _speed.y = _speed.y * _attachSpeed;
        _destinationMet = false;
        _attachDistance = attachDistance;
        _explosionAnimation = new Animation(.018f, explosionAtlas.getRegions(), Animation.PlayMode.NORMAL);
        _toExplode = false;
        _elapsedFrameTime = 0;
//        this.setColor(.6f, .39f, .08f, 1);
    }

    public void Update(Vector2 destinationLocation, float deltaTime, boolean powerupActive, float earthRotation)
    {
        Vector2 currentDistance = new Vector2(destinationLocation.x - this.getX(), destinationLocation.y - this.getY());

        if(currentDistance.len() <= _attachDistance)
        {
            _destinationMet = true;
        }

        if(!_destinationMet) {
            _speed.x = destinationLocation.x - this.getX();
            _speed.y = destinationLocation.y - this.getY();
            _speed.nor();
            _speed.x = _speed.x * _attachSpeed;
            _speed.y = _speed.y * _attachSpeed;
            this.setPosition(this.getX() + _speed.x, this.getY() + _speed.y);
        }
        else
        {
            this.setPosition(destinationLocation.x - this.getOriginX(), destinationLocation.y - this.getOriginY());
            this.setRotation(earthRotation);
        }

        if(!powerupActive && !_toExplode)
        {
            _toExplode = true;
            _elapsedFrameTime = 0;
        }
        _elapsedFrameTime += deltaTime;
        if(_toExplode)
        {
            _currentFrame = _explosionAnimation.getKeyFrame(_elapsedFrameTime, false);
            this.setRegion(_currentFrame);
        }
        if((_explosionAnimation.isAnimationFinished(_elapsedFrameTime) || _elapsedFrameTime > _explosionAnimation.getAnimationDuration()) && _toExplode)
        {
            _exploded = true;
        }
    }

}
