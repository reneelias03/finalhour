package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rene__000 on 1/23/2015.
 */
public class CreditsScreen implements Screen {

    MyGdxGame game;
    Sprite creditsSprite;
    SpriteBatch batch;
    Texture creditTexture;
    GameButtons backButton;
    BitmapFont goodTimeFont;

    public CreditsScreen(MyGdxGame game) {
        this.game = game;
        this.batch = game.batch;
        this.goodTimeFont = game.goodTimeFont;

        if(game.prefs.getBoolean("16x9") || game.prefs.getBoolean("5x3")) {
            creditTexture = game.manager.get("data/Credits16x9.png", Texture.class);
        } else if(game.prefs.getBoolean("16x10")) {
            creditTexture = game.manager.get("data/Credits16x10.png", Texture.class);
        } else if(game.prefs.getBoolean("4x3")) {
            creditTexture = game.manager.get("data/Credits4x3.png", Texture.class);
        } else {
            creditTexture = game.manager.get("data/Credits3x2.png", Texture.class);
        }

        creditsSprite = new Sprite(creditTexture);
        creditsSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        creditsSprite.setPosition(0,0);

        backButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        backButton.setSize(backButton.getWidth() * game.screenScale, backButton.getHeight() * game.screenScale);
        backButton.setOriginCenter();
        backButton.setPosition(Gdx.graphics.getWidth() - backButton.getWidth() - backButton.getOriginX() / 2, backButton.getHeight() / 2);
    }

    @Override
    public void render(float delta) {
        backButton.Update();
        if(backButton.Tapped() || Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            if(game.prefs.getBoolean("16x10")) {
                game.manager.unload("data/Credits16x10.png");
            } else if(game.prefs.getBoolean("3x2")) {
                game.manager.unload("data/Credits3x2.png");
            } else if(game.prefs.getBoolean("4x3")) {
                game.manager.unload("data/Credits4x3.png");
            } else {
                game.manager.unload("data/Credits16x9.png");
            }
//            game.manager.finishLoading();
            game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
            game.setScreen(new DescriptionPage(game));
            this.dispose();
        }


        //draw
        batch.begin();
        creditsSprite.draw(batch);
        backButton.draw(batch);
        DrawText();
        batch.end();
    }

    public void DrawText() {
        goodTimeFont.setScale(.55f * game.screenScale);
        goodTimeFont.setColor(Color.WHITE);
        goodTimeFont.draw(game.batch, "Back", backButton.getX() + 29 * game.screenScale, backButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
