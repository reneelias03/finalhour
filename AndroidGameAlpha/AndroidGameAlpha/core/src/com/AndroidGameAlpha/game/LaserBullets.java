package com.AndroidGameAlpha.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;


/**
 * Created by rene__000 on 9/3/2014.
 */
public class LaserBullets extends Sprite {

    private Vector2 _originDistance;
    private Vector2 _speed;
    private float _speedMultiplier;
    private boolean _initialShot;
    public int _asteroidCount;
    public boolean _active;

    public LaserBullets(Texture texture, float originDistance, float speed)
    {
        super(texture);
        this.setColor(Color.RED);
        _originDistance = new Vector2(0, originDistance);
        _speed = new Vector2(0, speed);
        _initialShot = false;
        _asteroidCount = 0;
        _active = false;
    }

    public void Update(Vector2 reticleLocation, float reticleRotation)
    {
        if(_active) {
            if (!_initialShot) {
                StartingPosition(reticleLocation, reticleRotation);
                _speed.setAngle(reticleRotation);
                _initialShot = true;
            } else {
                this.setPosition(this.getX() + _speed.x, this.getY() + _speed.y);
            }
        }
    }

    public void StartingPosition(Vector2 reticleLocation, float reticleRotation)
    {
        _originDistance.setAngle(reticleRotation);
        this.setRotation(reticleRotation - 90);
        this.setPosition(reticleLocation.x + _originDistance.x - this.getOriginX(), reticleLocation.y + _originDistance.y - this.getOriginY());
    }

    public boolean OffScreen(float cameraZoom)
    { if(this.getX() < (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2 || this.getX() + this.getWidth() > Gdx.graphics.getWidth() - ((Gdx.graphics.getWidth() / 2) - (Gdx.graphics.getWidth() / 2) * cameraZoom)||
            this.getY() < 0 + ((Gdx.graphics.getHeight() / 2) - (Gdx.graphics.getHeight() / 2) * cameraZoom) ||
            this.getY() + this.getHeight() > Gdx.graphics.getHeight() - ((Gdx.graphics.getHeight() / 2) - (Gdx.graphics.getHeight() / 2) * cameraZoom))
    {
        return true;
    }
        else {
        return  false;
    }
    }

    public void ResetLaser() {
        this.setColor(Color.RED);
//        _originDistance = new Vector2(0, _originDistance);
//        _speed = new Vector2(0, _speedMultiplier);
        _initialShot = false;
        _asteroidCount = 0;
        _active = false;
    }
}
