package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by rene__000 on 11/13/2014.
 */
public class MagneticAsteroid extends AsteroidPlanet {

    private Vector2 _radius;
    public Vector2 Radius()
    {
        return _radius;
    }
    public boolean _toScore;
    public boolean _doubleScore;
    private int _rotationSpeed;
    protected Sprite _circleOverlay;

    public Sprite get_circleOverlay() {
        return _circleOverlay;
    }

    protected Sprite shineSprite;
    public float _lifeTime;

    public MagneticAsteroid(Asteroid asteroid, Vector2 destinationLocation, float attachDistance, float attachSpeed, TextureAtlas explosionAtlas, Vector2 deadZone, float scale, Texture circleOverlay, Texture cannonBallShine) {
    super(asteroid, destinationLocation, attachDistance, attachSpeed, explosionAtlas);
        this.setColor(Color.YELLOW);
        _radius = new Vector2(0, this.getOriginY());
        _toScore = false;

        Random random = new Random();
        int screenLocation = random.nextInt(4);
        int randomSizeAddition = random.nextInt(15);
        this.setSize((45 + randomSizeAddition) * scale, (45 + randomSizeAddition) * scale);
        this.setOriginCenter();
        if(screenLocation == 0) {
            //Spawn to left of screen
            this.setPosition(((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2) - this.getWidth() * 2,
                    deadZone.y + random.nextInt((int)(Gdx.graphics.getHeight() * 1 - deadZone.y - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2)));
        }
        else if(screenLocation == 1)
        {
            //Spawn to top of screen
            this.setPosition(random.nextInt((int)(Gdx.graphics.getWidth() * 1 - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2)),
                    (Gdx.graphics.getHeight() - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2) + this.getHeight() * 2);
        }
        else if(screenLocation == 2)
        {
            //Spawn to the right of the screen
            this.setPosition(Gdx.graphics.getWidth() - ((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2) + this.getWidth(),
                    deadZone.y + random.nextInt((int)(Gdx.graphics.getHeight()* 1 - deadZone.y - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2)));
        }
        else
        {
            //Spawn to the bottom of the screen
            this.setPosition(deadZone.x + random.nextInt((int)(Gdx.graphics.getWidth() * 1  - deadZone.x * 2 - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2)),
                    ((Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2) - this.getHeight());
        }

        _rotationSpeed = random.nextInt(10) + 1;

        _circleOverlay = new Sprite(circleOverlay);
        _circleOverlay.setSize(this.getWidth() * 2, this.getHeight() * 2);
        _circleOverlay.setColor(Color.YELLOW);
        _circleOverlay.setAlpha(.25f);
        _circleOverlay.setOriginCenter();

        if(cannonBallShine != null) {
            shineSprite = new Sprite(cannonBallShine);
            shineSprite.setSize(this.getWidth(), this.getHeight());
            shineSprite.setOriginCenter();
        }

        _doubleScore = false;
        _lifeTime = 0;
    }

    public void Update(float cameraZoom, LaserBullets[] bullets, Planet Earth, Powerup powerup, boolean vibrate, Sound explosion, boolean playSound, float deltaTime, float volume) {
        _speed.x = (Earth.getX() + Earth.getOriginX()) - (this.getX() + this.getOriginX());
        _speed.y = (Earth.getY() + Earth.getOriginY()) - (this.getY() + this.getOriginY());
        _speed.nor();
        _speed.x = _speed.x * _attachSpeed;
        _speed.y = _speed.y * _attachSpeed;
        this.setPosition(this.getX() + _speed.x, this.getY() + _speed.y);

            for (int i = 0; i < bullets.length; i++) {
                if (bullets[i]._active && this.getBoundingRectangle().overlaps(bullets[i].getBoundingRectangle()) && !_toExplode) {
                    powerup.Generate(new Vector2(this.getX(), this.getY()), Earth.GetHealth());
                    if(powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.PiercingLaser && bullets[i]._asteroidCount < 1)
                    {
                        bullets[i]._asteroidCount++;
                        bullets[i].setColor(Color.YELLOW);
                    }
                    else
                    {
//                        bullets.remove(bullets.get(i));
                        bullets[i]._active = false;
                    }
                    _toExplode = true;
                    _toScore = true;
                    if(playSound) {
                        explosion.play(.2f * volume);
                    }
                    break;
                }

            }

        _circleOverlay.setPosition(this.getX() - _circleOverlay.getOriginX() / 2, this.getY() - _circleOverlay.getOriginY() / 2);
        if(shineSprite != null) {
            shineSprite.setPosition(this.getX(), this.getY());
            shineSprite.setRotation(this.getRotation());
        }

        _lifeTime += Gdx.graphics.getDeltaTime();
        if(_lifeTime > 10f) {
            _toExplode = true;
            if(playSound) {
                explosion.play(.2f * volume);
            }
            _lifeTime = 0;
        }
        if(Earth != null && CheckEarthCollision(Earth) && !Earth.IsHit() && !_toExplode)
        {
            Earth.SetHealth(Earth.GetHealth() - 1);
            if(playSound)
            {
                explosion.play(.2f * volume);
            }
            if(Earth.GetHealth() > 0) {
                Earth.SetIsHit(true);
                Earth.PrepareHitSprite();
            }
            if(vibrate)
            {
                Gdx.input.vibrate(300);
            }
            _toExplode = true;
            this.setSize(this.getWidth() * 1.5f, this.getHeight() * 1.5f);
        }

        if(powerup != null && ((powerup.PowerUpType() == PowerupType.Explosion && powerup.PowerupActive()) || powerup.explosionSimultaneous) && !powerup.ExplsionDying() && !_toExplode)
        {
            Vector2 distanceBetweenCircles = new Vector2( powerup.ExplosionSprite().getX() + powerup.ExplosionSprite().getOriginX() - this.getX() - this.getOriginX(),  powerup.ExplosionSprite().getY() + powerup.ExplosionSprite().getOriginY() - this.getY() - this.getOriginY());
            if(_radius.len() + powerup.ExplosionRadius() >= distanceBetweenCircles.len())
            {
                _toExplode = true;
                _toScore = true;
                if(playSound) {
                    explosion.play(.2f * volume);
                }
                if(vibrate)
                {
                    Gdx.input.vibrate(150);
                }
            }
        }

        if(CheckEarthCollision(Earth))
        {
            _toExplode = true;

        }
        if(_toExplode)
        {
            _elapsedFrameTime += deltaTime;
            _currentFrame = _explosionAnimation.getKeyFrame(_elapsedFrameTime, false);
            this.setRegion(_currentFrame);
        } else {
            this.rotate(_rotationSpeed);
        }
        if((_explosionAnimation.isAnimationFinished(_elapsedFrameTime) || _elapsedFrameTime > _explosionAnimation.getAnimationDuration()) && _toExplode)
        {
            _exploded = true;
        }
    }

    private boolean CheckEarthCollision(Planet Earth)
    {
        Vector2 distanceBetweenCircles = new Vector2( Earth.getX() + Earth.getOriginX() - this.getX() - this.getOriginX(),  Earth.getY() + Earth.getOriginY() - this.getY() - this.getOriginY());
        float len = _radius.len();
        if(_radius.len() + Earth.GetRadius().len() >= distanceBetweenCircles.len())
        {
            return true;
        }
        return false;
    }

    public void Draw(SpriteBatch batch)
    {
        if(!_toExplode) {
            _circleOverlay.draw(batch);
        }
        this.draw(batch);
        if(shineSprite != null) {
            shineSprite.draw(batch);
        }
    }
}
