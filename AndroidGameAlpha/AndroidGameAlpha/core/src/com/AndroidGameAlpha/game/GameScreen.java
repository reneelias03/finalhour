package com.AndroidGameAlpha.game;

import android.gesture.Gesture;
import android.hardware.input.InputManager;
import com.badlogic.gdx.*;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.collision.BoundingBox;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by rene__000 on 8/21/2014.
 */
public class GameScreen implements Screen {

    //region Variables
    Texture background, laserTexture, asteroidTexture, emptyButtonTemplate;
    Sprite backgroundSprite[], laser, whitePixel, catSkin, catSkinLives, ninjaSkin, ninjaSkinMoon, ninjaSkinLives, continentsSprite, continentsLives, grayOverlay, moonBlotsSprite, grayOverlayMoon, coin, pirateSkinEarth, pirateSkinMoon, pirateSkinHealth, soccerSkinEarth, soccerSkinMoon, inventoryContainer, leftSlideIndicators, rightSlideIndicators, sumoSkinEarth, sumoSkinMoon, soccerSkinLife, sumoSkinLife;
    OrthographicCamera camera;
    float elapsedTime, _calibrationX, planetScale, laserTime, elapsedAsteroidTime, asteroidTime, difficultyTimer, difficultyTimerIncrement, screenWidth, screenHeight, scoreAddTime, showScoreChangeTime, waveTime, warningMessageAlpha, warningMessageSpeed, slideIndicatorAlphaSpeed;
    Vector2 distanceBetweenFingers, previousDistanceBetweenFingers, initialSlideLocation, postSlideLocation;
    BitmapFont font, numberFont, goodTimeFont;
    int asteroidMax, score, scoreAdd, seconds, minutes, previousElapsedTime, currentProTipIndex, gameCoinTotal, cointAdditionRate, previousCoinTotal, previousCoinAddition, explodingAsteroidMax, magneticAsteroidMax, hugeAsteroidMax, waveTotal, waveCount, indexToTrack, tutorialClicks, invTapIndextToTrack, activeAsteroidIndex, hugeAsteroidAsteroidsIndex, laserBulletIndex, explodingAsteroidsIndex, hugeAsteroidsIndex;
    int[] powerupTotals;
    Gestures currentTouchState, previousTouchState;
    Planet Earth, Moon;
    MyGdxGame game;
    VirtualJoystick virtualJoystick, shootJoystick;
    GameButtons shootButton, pauseButton, continueButton, restartButton, menuButton, optionsButton, inventoryButton, powerupShopButton, cancelPowerupButton, realCancelPowerupButton, tutorialPlayButton;
    GameButtons[] inventoryPowerups;
    TextureAtlas shootAtlas;
    Animation shootAnimation;
    TextureRegion currentFrame;
    boolean drawLaser, gameOver, pause, showScoreChange, currentBackTouched, previousBackTouched, firstShot, firstMove, inventoryActive, powerupShopTrip, tutorial, invJustActivated;
    boolean[] inventoryBools;
    Asteroid[] asteroidsArray;
    Asteroid[] hugeAsteroidAsteroids;
    LaserBullets[] bulletsArray;
    ExplodingAsteroid[] explodingAsteroidsArray;
    HugeAsteroid[] hugeAsteroidsArray;
//    ArrayList<LaserBullets> bullets;
    //    ArrayList<Asteroid> asteroids;
    ArrayList<MagneticAsteroid> magneticAsteroids;
//    ArrayList<ExplodingAsteroid> explodingAsteroids;
//    ArrayList<HugeAsteroid> hugeAsteroids;
    ArrayList<Float> scoreAlphas;
    ArrayList<Vector2> scoreLocations;
    ArrayList<Integer> scoreValues;
    Health health;
    Powerup powerup;
    AsteroidPlanet asteroidPlanet;
    Vector2 EarthPreviousPosition;
    String[] proTips;
    Random random;
    Rectangle inventorySlideBox;

    //endregion


    public GameScreen(MyGdxGame game, float calibrationX) {

        screenWidth = game.screenWidth;
        screenHeight = game.screenHeight;
//        Gdx.graphics.setDisplayMode((int) screenWidth, (int) screenHeight, false);
        this.game = game;
        this.game.handler.showAds(false);

        Gdx.input.setCatchBackKey(true);
        emptyButtonTemplate = game.manager.get("data/EmptyButtonTemplate.png", Texture.class);
        goodTimeFont = game.goodTimeFont;

        virtualJoystick = new VirtualJoystick(this.game.manager.get("data/JoystickOuterWhite.png", Texture.class), true, new Vector2(30 * game.screenScale, 30 * game.screenScale), this.game.manager.get("data/JoystickHoverWhite.png", Texture.class), 1.65f * game.screenScale, true);
        virtualJoystick.setOriginCenter();
        virtualJoystick.setColor(game.prefs.getFloat("LeftOuterColorR"), game.prefs.getFloat("LeftOuterColorG"), game.prefs.getFloat("LeftOuterColorB"), 1);
        virtualJoystick.FingerHover().setColor(game.prefs.getFloat("LeftInnerColorR"), game.prefs.getFloat("LeftInnerColorG"), game.prefs.getFloat("LeftInnerColorB"), 1);


        if (!game.prefs.getBoolean("AccelControls")) {
            if (!game.prefs.getBoolean("SmallStick")) {
                shootJoystick = new VirtualJoystick(this.game.manager.get("data/JoystickOuterWhite.png", Texture.class), true, new Vector2(Gdx.graphics.getWidth() - ((this.game.manager.get("data/JoystickOuterWhite.png", Texture.class).getWidth() * 1.65f) * game.screenScale) - 30 * game.screenScale, 30 * game.screenScale), this.game.manager.get("data/JoystickHoverWhite.png", Texture.class), 1.5f * game.screenScale, true);
            } else {
                shootJoystick = new VirtualJoystick(this.game.manager.get("data/JoystickOuterWhite.png", Texture.class), true, new Vector2(Gdx.graphics.getWidth() - ((this.game.manager.get("data/JoystickOuterWhite.png", Texture.class).getWidth() * 1.55f) * game.screenScale) - 20 * game.screenScale, ((this.game.manager.get("data/JoystickOuterWhite.png", Texture.class).getHeight() * .75f) * game.screenScale) - 20 * game.screenScale), this.game.manager.get("data/JoystickHoverWhite.png", Texture.class), 1f * game.screenScale, false);
                shootJoystick.SetTwiceSizeBoundingBox(new Rectangle(shootJoystick.getBoundingRectangle().getX() - (shootJoystick.getBoundingRectangle().getWidth() * 2 / 3), shootJoystick.getBoundingRectangle().getY() - (shootJoystick.getBoundingRectangle().getHeight() * 2 / 3), shootJoystick.getBoundingRectangle().getWidth() * 3, shootJoystick.getBoundingRectangle().getHeight() * 3));
            }
            shootJoystick.setOriginCenter();
        } else {
            shootButton = new GameButtons(this.game.manager.get("data/AButton.png", Texture.class), true, new Vector2().setZero());
            shootButton.setSize(325 * game.screenScale, 325 * game.screenScale);
            shootButton.setOriginCenter();
            shootButton.SetOriginalPosition(new Vector2(Gdx.graphics.getWidth() - shootButton.getWidth() - 20 * game.screenScale, 20 * game.screenScale));
            shootButton.SetOriginalWidthHeight(shootButton.getWidth(), shootButton.getHeight());
        }
        shootJoystick.setColor(game.prefs.getFloat("RightOuterColorR"), game.prefs.getFloat("RightOuterColorG"), game.prefs.getFloat("RightOuterColorB"), 1);
        shootJoystick.FingerHover().setColor(game.prefs.getFloat("RightInnerColorR"), game.prefs.getFloat("RightInnerColorG"), game.prefs.getFloat("RightInnerColorB"), 1);
//        shootJoystick.FingerHover().setColor(Color.CYAN);
        //region Buttons
        pauseButton = new GameButtons(this.game.manager.get("data/PauseButton.png", Texture.class), true, new Vector2().setZero());
        pauseButton.setSize(pauseButton.getWidth() * .75f * game.screenScale, pauseButton.getHeight() * .75f * game.screenScale);
        pauseButton.setOriginCenter();
        pauseButton.SetOriginalWidthHeight(pauseButton.getWidth(), pauseButton.getHeight());
        pauseButton.setPosition(Gdx.graphics.getWidth() - pauseButton.getWidth() - 10 * game.screenScale, Gdx.graphics.getHeight() - pauseButton.getHeight() - 10 * game.screenScale);
        pauseButton.SetOriginalPosition(new Vector2(Gdx.graphics.getWidth() - pauseButton.getWidth() - 10 * game.screenScale, Gdx.graphics.getHeight() - pauseButton.getHeight() - 10 * game.screenScale));
        pauseButton.setColor(.28f, .43f, .68f, 1);

        restartButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        restartButton.setSize(restartButton.getWidth() * game.screenScale, restartButton.getHeight() * game.screenScale);
        restartButton.setOriginCenter();
        restartButton.setPosition(Gdx.graphics.getWidth() / 2 - restartButton.getOriginX(), Gdx.graphics.getHeight() / 2 - restartButton.getOriginY() * 2.5f);
        restartButton.setColor(Color.RED);

        continueButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        continueButton.setSize(continueButton.getWidth() * game.screenScale, continueButton.getHeight() * game.screenScale);
        continueButton.setOriginCenter();
        continueButton.setPosition(Gdx.graphics.getWidth() / 2 - continueButton.getOriginX(), restartButton.getY() - restartButton.getHeight() * 1.5f);
        continueButton.setColor(Color.GREEN);

        optionsButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        optionsButton.setSize(optionsButton.getWidth() * game.screenScale, optionsButton.getHeight() * game.screenScale);
        optionsButton.setOriginCenter();
        optionsButton.setPosition(Gdx.graphics.getWidth() / 2 - optionsButton.getOriginX(), restartButton.getY() + optionsButton.getHeight() * 1.5f);
        optionsButton.setColor(Color.PURPLE);

        menuButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        menuButton.setSize(menuButton.getWidth() * game.screenScale, menuButton.getHeight() * game.screenScale);
        menuButton.setOriginCenter();
        menuButton.setPosition(Gdx.graphics.getWidth() / 2 - menuButton.getOriginX(), optionsButton.getY() + menuButton.getHeight() * 1.5f);

        //endregion

        shootAtlas = this.game.manager.get("data/RealEarthLaserAnimationThick-packed/pack.atlas", TextureAtlas.class);
        shootAnimation = new Animation(.018f, shootAtlas.getRegions(), Animation.PlayMode.NORMAL);

        laserTime = 0;
        currentFrame = shootAnimation.getKeyFrame(laserTime, false);


        //fingerHover = new GameButtons(this.game.manager.get("data/FingerHover.png", Texture.class), true, new Vector2().setZero());


        if (game.prefs.getBoolean("PixelSkin")) {
            background = this.game.manager.get("data/PixelSpace.png", Texture.class);
        } else if (game.prefs.getBoolean("NinjaSkin")) {
            background = this.game.manager.get("data/NinjaBackground1920.png", Texture.class);
        } else if (game.prefs.getBoolean("CatSkin")) {
//            background = this.game.manager.get("data/YarnBallExperimentGreenless1920.png", Texture.class);
            background = this.game.manager.get("data/CatBackground.png", Texture.class);
        } else if (game.prefs.getBoolean("PirateSkin")) {
//            background = this.game.manager.get("data/PirateBackgroundProto1920.png", Texture.class);
            background = this.game.manager.get("data/JackyPirateFog.png", Texture.class);
        } else if (game.prefs.getBoolean("SoccerSkin")) {
            background = this.game.manager.get("data/SoccerField1920.png", Texture.class);
        } else if (game.prefs.getBoolean("DefaultSkin")) {
            background = this.game.manager.get("data/SpaceSquare.png", Texture.class);
        } else {
            background = this.game.manager.get("data/SumoBackground.png", Texture.class);
        }

        backgroundSprite = new Sprite[2];
        for (int i = 0; i < backgroundSprite.length; i++) {
            backgroundSprite[i] = new Sprite(background);
            backgroundSprite[i].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
//        backgroundSprite.setPosition(-(backgroundSprite.getWidth() / 2)/2, -(backgroundSprite.getHeight() /2) / 2);
            if (i == 0) {
                if (game.prefs.getBoolean("SoccerSkin") || game.prefs.getBoolean("SumoSkin")) {
                    if (game.prefs.getBoolean("16x9")) {
                        backgroundSprite[i].setPosition(0, -backgroundSprite[i].getHeight() / 4 + 50 * game.screenScale);
                    } else if (game.prefs.getBoolean("16x10")) {
                        backgroundSprite[i].setPosition(0, -backgroundSprite[i].getHeight() / 4 + 120 * game.screenScale);
                    } else if (game.prefs.getBoolean("4x3")) {
                        backgroundSprite[i].setPosition(0, -backgroundSprite[i].getHeight() / 4 + 240 * game.screenScale);
                    } else if (game.prefs.getBoolean("5x3")) {
                        backgroundSprite[i].setPosition(0, -backgroundSprite[i].getHeight() / 4 + 100 * game.screenScale);
                    } else if (game.prefs.getBoolean("3x2")) {
                        backgroundSprite[i].setPosition(0, -backgroundSprite[i].getHeight() / 4 + 160 * game.screenScale);
                    }
                } else {
                    backgroundSprite[i].setPosition(0, 0);
                }
            } else {
                backgroundSprite[i].setPosition(0, backgroundSprite[i - 1].getY() + backgroundSprite[i].getHeight());
            }
        }
        elapsedTime = 0;
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
//        if()
        camera.translate(960 * game.screenScale, 540 * game.screenScale + game.screenScaleHeight);
        distanceBetweenFingers = new Vector2().setZero();
        previousDistanceBetweenFingers = new Vector2().setZero();
        font = new BitmapFont(Gdx.files.internal("data/poop.fnt"), Gdx.files.internal("data/poop.png"), false);
        font.setColor(Color.ORANGE);
        numberFont = new BitmapFont(Gdx.files.internal("data/number.fnt"), Gdx.files.internal("data/number.png"), false);
        numberFont.setColor(Color.BLUE);

        currentTouchState = Gestures.None;
        previousTouchState = currentTouchState;
        planetScale = .45f * game.screenScale;

        if (game.prefs.getBoolean("PixelSkin")) {
            Earth = new Planet(this.game.manager.get("data/EmptyCirclePixel.png", Texture.class), false, 9f, false, true, false, .05f, 4f, calibrationX, this.game.manager.get("data/EmptyCirclePixel.png", Texture.class));
        }
//        else if(game.prefs.getBoolean("DefaultSkin")){
//            Earth = new Planet(Animation.PlayMode.LOOP, .03f, this.game.manager.get("data/pack.atlas", TextureAtlas.class), false, 9f, false, true, false, .05f, 4f, calibrationX, this.game.manager.get("data/HitEarthRed.png", Texture.class));
//        }
//        else if(game.prefs.getBoolean("NinjaSkin")){
//            Earth = new Planet(this.game.manager.get("data/B&WEarth.png", Texture.class), false, 9f, false, true, false, .05f, 4f, calibrationX, this.game.manager.get("data/HitEarthRed.png", Texture.class));
//        }
        else {
//            Earth = new Planet(new TextureRegion(this.game.manager.get("data/pack.atlas", TextureAtlas.class).getRegions().first().getTexture(), 0,0,300,300), false, 9f, false, true, false, .05f, 4f, calibrationX, this.game.manager.get("data/HitEarthRed.png", Texture.class));
            Earth = new Planet(this.game.manager.get("data/EmptyCircle.png", Texture.class), false, 9f, false, true, false, .05f, 4f, calibrationX, this.game.manager.get("data/HitEarthRed.png", Texture.class));
        }
        Earth.setColor(game.prefs.getFloat("EarthCircleR"), game.prefs.getFloat("EarthCircleG"), game.prefs.getFloat("EarthCircleB"), 1f);
        Earth.setSize(Earth.getWidth() * planetScale, Earth.getHeight() * planetScale);
        Earth.setOriginCenter();
        Earth.setPosition(Gdx.graphics.getWidth() / 2 - Earth.getOriginX(), Gdx.graphics.getHeight() / 2 - Earth.getOriginY());
        Earth.SetRadius();
        if (game.prefs.getBoolean("PixelSkin")) {
            continentsSprite = new Sprite(game.manager.get("data/ContinentsPixel.png", Texture.class));
        } else {
            continentsSprite = new Sprite(game.manager.get("data/Continents.png", Texture.class));
        }
        continentsSprite.setSize(Earth.getWidth(), Earth.getHeight());
        continentsSprite.setOriginCenter();
        continentsSprite.setPosition(Earth.getX(), Earth.getY());
//        continentsSprite.setColor(.16f, .65f, .055f, 1f);
        continentsSprite.setColor(game.prefs.getFloat("EarthContinentsR"), game.prefs.getFloat("EarthContinentsG"), game.prefs.getFloat("EarthContinentsB"), 1f);
//        continentsSprite.setColor(Color.RED);

        grayOverlay = new Sprite(game.manager.get("data/EmptyCircleGray.png", Texture.class));
        grayOverlay.setSize(Earth.getWidth(), Earth.getHeight());
        grayOverlay.setOriginCenter();
        grayOverlay.setPosition(Earth.getX(), Earth.getY());
        grayOverlay.setAlpha(.35f);

        if (game.prefs.getBoolean("PixelSkin")) {
            Moon = new Planet(this.game.manager.get("data/MoonPixelNew11.png", Texture.class), false, 1f, false, false, false, Earth.getHeight() / 2 * 1.25f, 4f, calibrationX, this.game.manager.get("data/HitEarthRed.png", Texture.class));
        } else {
//            Moon = new Planet(Animation.PlayMode.LOOP, .03f, this.game.manager.get("data/moonpack.atlas", TextureAtlas.class), false, 1f, false, false, false, Earth.getHeight() / 2 * 1.25f, 4f, calibrationX, this.game.manager.get("data/HitEarthRed.png", Texture.class));
            Moon = new Planet(this.game.manager.get("data/EmptyCircle.png", Texture.class), false, 1f, false, false, false, Earth.getHeight() / 2 * 1.25f, 4f, calibrationX, this.game.manager.get("data/HitEarthRed.png", Texture.class));
        }
        Moon.setSize(80 * planetScale, 80 * planetScale);
        Moon.setOriginCenter();
        Moon.setColor(game.prefs.getFloat("MoonMajorR"), game.prefs.getFloat("MoonMajorG"), game.prefs.getFloat("MoonMajorB"), 1);
        Moon.setPosition(Earth.getX() + Earth.getWidth() / 2 - Moon.getOriginX() + Moon.DistanceBetweenPlanets().y * 1.7f, Earth.getY() + Earth.getOriginY() - Moon.getOriginY());

        moonBlotsSprite = new Sprite(this.game.manager.get("data/MoonBlotsWhite.png", Texture.class));
        moonBlotsSprite.setSize(Moon.getWidth(), Moon.getHeight());
        moonBlotsSprite.setOriginCenter();
        moonBlotsSprite.setPosition(Moon.getX(), Moon.getY());
        moonBlotsSprite.setColor(game.prefs.getFloat("MoonMinorR"), game.prefs.getFloat("MoonMinorG"), game.prefs.getFloat("MoonMinorB"), 1);

        grayOverlayMoon = new Sprite(game.manager.get("data/EmptyCircleGray.png", Texture.class));
        grayOverlayMoon.setSize(Moon.getWidth(), Moon.getHeight());
        grayOverlayMoon.setOriginCenter();
        grayOverlayMoon.setPosition(Moon.getX(), Moon.getY());
        grayOverlayMoon.setAlpha(.35f);

        laser = new Sprite(currentFrame);
        laser.setSize(laser.getWidth() * planetScale, laser.getHeight() * planetScale);
        laser.setOrigin(laser.getWidth() / 2, laser.getHeight() / 3.3f);
        laser.setPosition(Earth.getX() - Earth.getX(), Earth.getY() - Earth.getY());
        drawLaser = false;
//        bullets = new ArrayList<LaserBullets>();
        laserTexture = this.game.manager.get("data/LaserBulletWhite.png", Texture.class);

        bulletsArray = new LaserBullets[20];
        for (int i = 0; i < bulletsArray.length; i++) {
            bulletsArray[i] = new LaserBullets(laserTexture, 50 * game.screenScale, 20 * game.screenScale);
            bulletsArray[i].setSize(7f * game.screenScale, 40 * game.screenScale);
            bulletsArray[i].setOriginCenter();
        }
        laserBulletIndex = 0;

        if (game.prefs.getBoolean("PixelSkin")) {
            asteroidTexture = this.game.manager.get("data/PixelAsteroidRedone14.png", Texture.class);
        } else if (game.prefs.getBoolean("NinjaSkin")) {
            asteroidTexture = this.game.manager.get("data/ShurikenRedone.png", Texture.class);
        } else if (game.prefs.getBoolean("CatSkin")) {
//            asteroidTexture = this.game.manager.get("data/MouseNiceSmall.png", Texture.class);
            asteroidTexture = this.game.manager.get("data/MouseMeanSmall.png", Texture.class);
        } else if (game.prefs.getBoolean("PirateSkin")) {
            asteroidTexture = this.game.manager.get("data/CanonBallAsteroid100.png", Texture.class);
//            asteroidTexture = this.game.manager.get("data/EmptyCircle.png", Texture.class);
//            canonBallShines = new ArrayList<Sprite>();
        } else if (game.prefs.getBoolean("SoccerSkin")) {
            asteroidTexture = this.game.manager.get("data/SoccerballAsteroid100.png", Texture.class);
        } else if (game.prefs.getBoolean("DefaultSkin")) {
            asteroidTexture = this.game.manager.get("data/AsteroidRedone.png", Texture.class);
        } else {
            asteroidTexture = this.game.manager.get("data/SumoAsteroid.png", Texture.class);
        }

//        asteroids = new ArrayList<Asteroid>();
        magneticAsteroids = new ArrayList<MagneticAsteroid>();
//        explodingAsteroids = new ArrayList<ExplodingAsteroid>();
//        hugeAsteroids = new ArrayList<HugeAsteroid>();

        //region LoadingAsteroidArrays
        asteroidsArray = new Asteroid[30];
        for (int i = 0; i < asteroidsArray.length; i++) {
            if (game.prefs.getBoolean("PixelSkin")) {
                asteroidsArray[i] = (new Asteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class)));
            } else {
                asteroidsArray[i] = (new Asteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class)));
            }
        }
        activeAsteroidIndex = 0;

        hugeAsteroidAsteroids = new Asteroid[12];
        for (int i = 0; i < hugeAsteroidAsteroids.length; i++) {
            if (game.prefs.getBoolean("PixelSkin")) {
                hugeAsteroidAsteroids[i] = (new Asteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class)));
            } else {
                hugeAsteroidAsteroids[i] = (new Asteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class)));
            }
        }
        hugeAsteroidAsteroidsIndex = 0;

        explodingAsteroidsArray = new ExplodingAsteroid[4];
        for (int i = 0; i < explodingAsteroidsArray.length; i++) {
            if (game.prefs.getBoolean("PixelSkin")) {
                explodingAsteroidsArray[i] = new ExplodingAsteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class), this.game.manager.get("data/EmptyCircle.png", Texture.class), null);
            } else if (game.prefs.getBoolean("PirateSkin")) {
                explodingAsteroidsArray[i] = new ExplodingAsteroid(this.game.manager.get("data/EmptyCircle.png", Texture.class), new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class), this.game.manager.get("data/EmptyCircle.png", Texture.class), this.game.manager.get("data/CanonBallShine.png", Texture.class));
                explodingAsteroidsArray[i].get_circleOverlay().setAlpha(.5f);
            } else {
                explodingAsteroidsArray[i] = new ExplodingAsteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class), this.game.manager.get("data/EmptyCircle.png", Texture.class), null);
            }
        }
        explodingAsteroidsIndex = 0;

        hugeAsteroidsArray = new HugeAsteroid[2];
        for(int i = 0; i < hugeAsteroidsArray.length; i++) {
            if (game.prefs.getBoolean("PixelSkin")) {
                hugeAsteroidsArray[i] = new HugeAsteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class));
            } else {
                hugeAsteroidsArray[i] = new HugeAsteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class));
            }
        }
        hugeAsteroidsIndex = 0;

        //endregion

        elapsedAsteroidTime = 0;
        asteroidTime = 1f;
        asteroidMax = 30;
        if (game.prefs.getBoolean("Easy")) {
            difficultyTimer = 30;
            difficultyTimerIncrement = difficultyTimer;
            waveTotal = 10;
        } else if (game.prefs.getBoolean("Normal")) {
            difficultyTimer = 20;
            difficultyTimerIncrement = difficultyTimer;
            waveTotal = 11;
        } else if (game.prefs.getBoolean("Hard")) {
            difficultyTimer = 10;
            difficultyTimerIncrement = difficultyTimer;
            waveTotal = 15;
        }

        waveCount = 1;

        _calibrationX = calibrationX;
        score = 0;
        if (game.prefs.getBoolean("PixelSkin")) {
            health = new Health(this.game.manager.get("data/EmptyCirclePixel.png", Texture.class), Earth.GetHealth(), .2f * game.screenScale);
            continentsLives = new Sprite(game.manager.get("data/ContinentsPixel.png", Texture.class));
        } else {
            health = new Health(this.game.manager.get("data/EmptyCircle.png", Texture.class), Earth.GetHealth(), .2f * game.screenScale);
            continentsLives = new Sprite(game.manager.get("data/Continents.png", Texture.class));
        }
        for (Sprite life : health.Lives()) {
            life.setColor(Earth.getColor());
        }

        continentsLives.setSize(health.Lives().get(0).getWidth(), health.Lives().get(0).getHeight());
        continentsLives.setOriginCenter();
        continentsLives.setColor(continentsSprite.getColor());

        if (game.prefs.getBoolean("CatSkin")) {
            catSkin = new Sprite(game.manager.get("data/CatSkinProto.png", Texture.class));
            catSkin.setSize(catSkin.getWidth() * planetScale, catSkin.getHeight() * planetScale);
            catSkin.setOrigin(catSkin.getWidth() / 2, catSkin.getHeight() / 2.4f);

            catSkinLives = new Sprite(game.manager.get("data/CatSkinProto.png", Texture.class));
            catSkinLives.setSize(health.Lives().get(0).getWidth(), health.Lives().get(0).getHeight());
            catSkinLives.setOriginCenter();

        } else if (game.prefs.getBoolean("NinjaSkin")) {
            ninjaSkin = new Sprite(game.manager.get("data/NinjaSkinProto.png", Texture.class));
            ninjaSkin.setSize(ninjaSkin.getWidth() * planetScale, ninjaSkin.getHeight() * planetScale);
            ninjaSkin.setOriginCenter();

            ninjaSkinLives = new Sprite(game.manager.get("data/NinjaSkinProto.png", Texture.class));
            ninjaSkinLives.setSize(health.Lives().get(0).getWidth(), health.Lives().get(0).getHeight());
            ninjaSkinLives.setOriginCenter();

            ninjaSkinMoon = new Sprite(game.manager.get("data/NinjaHatSmall.png", Texture.class));
            ninjaSkinMoon.setSize(ninjaSkinMoon.getWidth() * planetScale * 1.25f, ninjaSkinMoon.getHeight() * planetScale * 1.25f);
            ninjaSkinMoon.setOrigin(ninjaSkinMoon.getWidth() / 2, 0);
        } else if (game.prefs.getBoolean("PirateSkin")) {
            pirateSkinEarth = new Sprite(game.manager.get("data/PirateEarth.png", Texture.class));
            pirateSkinEarth.setSize(pirateSkinEarth.getWidth() * planetScale + 4f * game.screenScale, pirateSkinEarth.getHeight() * planetScale);
            pirateSkinEarth.setOrigin(pirateSkinEarth.getWidth() / 2, pirateSkinEarth.getHeight() / 2.72f);

            pirateSkinHealth = new Sprite(game.manager.get("data/PirateEarth.png", Texture.class));
            pirateSkinHealth.setSize(pirateSkinEarth.getWidth() * .45f, pirateSkinEarth.getHeight() * .45f);
            pirateSkinHealth.setOriginCenter();

            pirateSkinMoon = new Sprite(game.manager.get("data/MoonPirateSkin.png", Texture.class));
            pirateSkinMoon.setSize(Moon.getWidth(), Moon.getHeight());
//            pirateSkinMoon.setOrigin(pirateSkinMoon.getWidth() / 2, 0);
            pirateSkinMoon.setOriginCenter();
        } else if (game.prefs.getBoolean("SoccerSkin")) {
            soccerSkinEarth = new Sprite(game.manager.get("data/EarthSoccer.png", Texture.class));
            soccerSkinEarth.setSize((Earth.getHeight() / soccerSkinEarth.getHeight()) * soccerSkinEarth.getWidth(), Earth.getHeight());
//            soccerSkinEarth.setOriginCenter();
            soccerSkinEarth.setOrigin(soccerSkinEarth.getWidth() / 2 - 1 * game.screenScale, soccerSkinEarth.getHeight() / 2 + 1 * game.screenScale);

            soccerSkinLife = new Sprite(game.manager.get("data/EarthSoccer.png", Texture.class));
            soccerSkinLife.setSize((health.Lives().get(0).getHeight() / soccerSkinLife.getHeight()) * soccerSkinLife.getWidth(), health.Lives().get(0).getHeight());
            soccerSkinLife.setOriginCenter();

            soccerSkinMoon = new Sprite(game.manager.get("data/MoonReferee.png", Texture.class));
            soccerSkinMoon.setSize((Moon.getHeight() / soccerSkinMoon.getHeight()) * soccerSkinMoon.getWidth(), Moon.getHeight());
            soccerSkinMoon.setOriginCenter();
        } else if (game.prefs.getBoolean("SumoSkin")) {
            sumoSkinEarth = new Sprite(game.manager.get("data/SumoEarth.png", Texture.class));
            sumoSkinEarth.setSize(sumoSkinEarth.getWidth() * planetScale, sumoSkinEarth.getHeight() * planetScale);
            sumoSkinEarth.setOrigin(sumoSkinEarth.getWidth() / 2 + 1f * game.screenScale, sumoSkinEarth.getHeight() / 2.37f);

            sumoSkinLife = new Sprite(game.manager.get("data/SumoEarth.png", Texture.class));
            sumoSkinLife.setSize(health.Lives().get(0).getWidth(), (health.Lives().get(0).getWidth() / sumoSkinLife.getWidth()) * sumoSkinLife.getHeight());
            sumoSkinLife.setOriginCenter();

            sumoSkinMoon = new Sprite(game.manager.get("data/SumoMoon.png", Texture.class));
            sumoSkinMoon.setSize(Moon.getWidth(), Moon.getHeight());
            sumoSkinMoon.setOriginCenter();
        }

        game.blackSprite.setSize(Gdx.graphics.getWidth() * game.screenScale, Gdx.graphics.getHeight() * game.screenScale);
        game.blackSprite.setPosition(0, 0);

        powerup = new Powerup(game.manager, 3f, game.screenScale, game.PowerupSound, game.prefs.getBoolean("PixelSkin"));

        coin = new Sprite(game.manager.get("data/CoinLarge.png", Texture.class));
        coin.setSize(coin.getWidth() * .1f * game.screenScale, coin.getHeight() * .1f * game.screenScale);
        coin.setOriginCenter();
        coin.setPosition(Gdx.graphics.getWidth() / 2 - coin.getOriginX(), Gdx.graphics.getHeight() / 2 - coin.getHeight() * 3);

        scoreAdd = 50;
        scoreAddTime = 30f;
        showScoreChange = false;
        showScoreChangeTime = 3f;
        if (game.prefs.getBoolean("Sound")) {
            game.gameSong.play();
            game.gameSong.setVolume(.3f * game.prefs.getFloat("MusicVolumeLevel"));
            game.gameSong.setLooping(true);
        }
        gameOver = false;
        pause = false;
        currentBackTouched = false;
        previousBackTouched = currentBackTouched;
        minutes = 0;
        seconds = 0;
        previousElapsedTime = 0;
        firstShot = false;
        firstMove = false;
        EarthPreviousPosition = new Vector2().setZero();
        whitePixel = new Sprite(game.manager.get("data/WhitePixel.png", Texture.class));
        proTips = new String[]{"Keep your finger down on the movement joystick. You can avoid jittery movements this way.", "Don't be afraid to keep shooting! You don't run out of ammo or overheat.",
                "The shoot joystick works best when your keep your finger around its circumference, not its center.", "Surviving is as much about dodging as it is about shooting. Be wary of your surroundings.",
                "Avoid sticking too close to the edge of the screen. Asteroids can come from anywhere.", "Does the Earth move too slow or too fast? You can change the movement speed in Options.",
                "When the asteroids get thick, concentrate more on clearing out an escape route rather than scoring points.", "The piercing laser shows its true worth when the asteroid pack becomes overwhelming.",
                "Slide up or tap the inv tab to activate the inventory. Slide down or tap the tab again to deactivate it.", "Use the laser powerup spawn modifiers in your inventory to gain some control over the game.",
                "Detonating a bomb gives you a short window of time to safely access your inventory.", "Chain asteroid kills using an Explosive Asteroid's explosion for double points.",
                "Don't like the game pausing with the inventory? Switch it off in the options."};


        random = new Random();

        scoreAlphas = new ArrayList<Float>();
        scoreLocations = new ArrayList<Vector2>();
        scoreValues = new ArrayList<Integer>();

        gameCoinTotal = 0;
        cointAdditionRate = 0;
        previousCoinTotal = game.prefs.getInteger("Coins");
        previousCoinAddition = previousCoinTotal;

        explodingAsteroidMax = 1;
        hugeAsteroidMax = 1;
        magneticAsteroidMax = 1;
        waveTime = 0;

        warningMessageAlpha = 1;
        warningMessageSpeed = .05f;

        if (game.prefs.getBoolean("PC")) {
            Pixmap pm = new Pixmap(Gdx.files.internal("data/Crosshair.png"));
            Gdx.input.setCursorImage(pm, 16, 16);
            pm.dispose();
        }

        inventoryActive = false;
        inventoryBools = new boolean[6];
        for (int i = 0; i < inventoryBools.length; i++) {
            inventoryBools[i] = false;
        }
        inventoryContainer = new Sprite(game.manager.get("data/GameInventory.png", Texture.class));
        inventoryContainer.setSize(Gdx.graphics.getWidth() * .55f, (Gdx.graphics.getWidth() * .55f / inventoryContainer.getWidth()) * inventoryContainer.getHeight());
//        inventoryContainer.setRotation(90);
        inventoryContainer.setOriginCenter();
        inventoryContainer.setPosition(Gdx.graphics.getWidth() / 2 - inventoryContainer.getOriginX(), -inventoryContainer.getHeight() * .78f);
        inventoryContainer.setColor(.28f, .43f, .68f, .9f);

        inventoryButton = new GameButtons(menuButton.getTexture(), true, new Vector2().setZero());
        inventoryButton.setSize(240f * game.screenScale, (inventoryContainer.getHeight() * .22f) * 2f);
        inventoryButton.setOriginCenter();
        inventoryButton.setPosition(Gdx.graphics.getWidth() / 2 - inventoryButton.getOriginX(), 0);
//        inventoryButton.setColor(.28f, .43f, .68f, 1);

        inventoryPowerups = new GameButtons[6];
        float inventoryPowerupX = inventoryContainer.getX() + 60f * game.screenScale;
        for (int i = 0; i < inventoryBools.length; i++) {
            if (i == 0) {
                inventoryPowerups[i] = new GameButtons(game.manager.get("data/ShootSpeedPowerup.png", Texture.class), true, new Vector2().setZero());
            } else if (i == 1) {
                inventoryPowerups[i] = new GameButtons(game.manager.get("data/DualSided.png", Texture.class), true, new Vector2().setZero());
            } else if (i == 2) {
                inventoryPowerups[i] = new GameButtons(game.manager.get("data/PierceBulletPowerup.png", Texture.class), true, new Vector2().setZero());
            } else if (i == 3) {
                inventoryPowerups[i] = new GameButtons(game.manager.get("data/X2Powerup.png", Texture.class), true, new Vector2().setZero());
            } else if (i == 4) {
                inventoryPowerups[i] = new GameButtons(game.manager.get("data/LifePowerup.png", Texture.class), true, new Vector2().setZero());
            } else if (i == 5) {
                inventoryPowerups[i] = new GameButtons(game.manager.get("data/ExplosionPowerup.png", Texture.class), true, new Vector2().setZero());
            }
            inventoryPowerups[i].setPosition(inventoryPowerupX, 20f * game.screenScale);
//            if(i == 2) {
//                inventoryPowerupY -= 90f * game.screenScale;
//            } else {
            inventoryPowerupX += 170f * game.screenScale;
//            }
            inventoryPowerups[i].setSize(inventoryContainer.getHeight() * .45f, inventoryContainer.getHeight() * .45f);
            inventoryPowerups[i].setOriginCenter();

            powerupTotals = new int[]{game.prefs.getInteger("FastLaserOnly"), game.prefs.getInteger("DualShotOnly"), game.prefs.getInteger("PiercingOnly"), game.prefs.getInteger("X2InHand"), game.prefs.getInteger("HealthInHand"), game.prefs.getInteger("BombInHand")};

            powerupShopButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
            powerupShopButton.setSize(powerupShopButton.getWidth() * game.screenScale, powerupShopButton.getHeight() * game.screenScale);
            powerupShopButton.setOriginCenter();
            powerupShopButton.setPosition(Gdx.graphics.getWidth() / 2 - powerupShopButton.getOriginX(), powerupShopButton.getHeight());
            powerupShopButton.setColor(Color.PURPLE);

        }

        invJustActivated = false;
        invTapIndextToTrack = 0;

        initialSlideLocation = new Vector2().setZero();
        postSlideLocation = new Vector2().setZero();

        inventorySlideBox = new Rectangle(inventoryContainer.getX(), 0, inventoryContainer.getWidth(), Gdx.graphics.getHeight() * .75f);
        indexToTrack = 0;
        waitingForRelease = false;

        cancelPowerupButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        cancelPowerupButton.setSize(inventoryPowerups[0].getWidth(), (inventoryPowerups[0].getWidth() / cancelPowerupButton.getWidth()) * cancelPowerupButton.getHeight());
        cancelPowerupButton.setOriginCenter();
        cancelPowerupButton.setPosition(pauseButton.getX() + (pauseButton.getWidth() - cancelPowerupButton.getWidth()) / 2, pauseButton.getY() - inventoryPowerups[0].getHeight() * 1.75f);
        if (game.prefs.getBoolean("NinjaSkin")) {
            cancelPowerupButton.setColor(Color.BLACK);
        }

        realCancelPowerupButton = new GameButtons(this.game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        realCancelPowerupButton.setSize(cancelPowerupButton.getWidth(), cancelPowerupButton.getHeight() + inventoryPowerups[0].getHeight());
        realCancelPowerupButton.setOriginCenter();
        realCancelPowerupButton.setPosition(cancelPowerupButton.getX(), cancelPowerupButton.getY());

        leftSlideIndicators = new Sprite(game.manager.get("data/SlideIndicators.png", Texture.class));
        leftSlideIndicators.setSize((inventoryButton.getHeight() * .4f / leftSlideIndicators.getHeight()) * leftSlideIndicators.getWidth(), inventoryButton.getHeight() * .4f);
        leftSlideIndicators.setOriginCenter();
        leftSlideIndicators.setPosition(inventoryContainer.getX() + 25f * game.screenScale, inventoryContainer.getY() + (inventoryContainer.getHeight() - inventoryButton.getHeight() * .7f) + (inventoryButton.getHeight() - leftSlideIndicators.getHeight()) / 2);
        leftSlideIndicators.setColor(.28f, .43f, .68f, .1f);

        rightSlideIndicators = new Sprite(game.manager.get("data/SlideIndicators.png", Texture.class));
        rightSlideIndicators.setSize((inventoryButton.getHeight() * .4f / leftSlideIndicators.getHeight()) * leftSlideIndicators.getWidth(), inventoryButton.getHeight() * .4f);
        rightSlideIndicators.setOriginCenter();
        rightSlideIndicators.setPosition(inventoryContainer.getX() + (inventoryContainer.getWidth() / 2) + 150f * game.screenScale, inventoryContainer.getY() + (inventoryContainer.getHeight() - inventoryButton.getHeight() * .7f) + (inventoryButton.getHeight() - leftSlideIndicators.getHeight()) / 2);
        rightSlideIndicators.setColor(.28f, .43f, .68f, .1f);

        slideIndicatorAlphaSpeed = .01f;

        powerupShopTrip = false;

        tutorial = game.prefs.getBoolean("TutorialPlay");
//        tutorial = true;

        tutorialPlayButton = new GameButtons(game.manager.get("data/EmptyButtonTemplate.png", Texture.class), true, new Vector2().setZero());
        tutorialPlayButton.setSize(restartButton.getWidth(), restartButton.getHeight());
        tutorialPlayButton.setOriginCenter();
        tutorialPlayButton.setPosition(Gdx.graphics.getWidth() / 2 - tutorialPlayButton.getOriginX(), Gdx.graphics.getHeight() * .75f);

        tutorialClicks = 0;
    }

    @Override
    public void render(float delta) {
        //update

        currentTouchState = GestureManager.GetCurrentTouchState();
        currentBackTouched = Gdx.input.isKeyPressed(Input.Keys.BACK);

        if (tutorial) {
            tutorialPlayButton.Update();
            if (tutorialPlayButton.Tapped()) {
                tutorialClicks++;
            }
            if (tutorialClicks >= 3) {
                game.prefs.putBoolean("TutorialPlay", false);
                game.prefs.flush();
                tutorial = false;
                if (game.prefs.getBoolean("Sound")) {
                    game.gameSong.play();
                    game.gameSong.setVolume(.3f * game.prefs.getFloat("MusicVolumeLevel"));
                    game.gameSong.setLooping(true);
                }
            }
            if (tutorialClicks < 2) {
                tutorialPlayButton.setColor(Color.ORANGE);
            } else {
                tutorialPlayButton.setColor(Color.GREEN);
            }
            if (tutorialClicks == 0) {
                shootJoystick.setAlpha(.25f);
            } else if (tutorialClicks == 1) {
                shootJoystick.setAlpha(1f);
                virtualJoystick.setAlpha(.25f);
            } else {
                shootJoystick.setAlpha(.25f);
            }
        }
        if (inventoryActive && game.prefs.getBoolean("PauseOnInventory")) {

            pauseButton.Update();

            if (pauseButton.Tapped() || Gdx.input.isKeyPressed(Input.Keys.ESCAPE) || Gdx.input.isKeyPressed(Input.Keys.HOME) || (!currentBackTouched && previousBackTouched)) {
                pause = true;
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                }
                game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
            }

            inventoryButton.Update();

            if (inventoryButton.Tapped()) {
                inventoryActive = !inventoryActive;
                if (!inventoryActive) {
                    inventoryContainer.setPosition(Gdx.graphics.getWidth() / 2 - inventoryContainer.getOriginX(), -inventoryContainer.getHeight() * .78f);
                    inventoryButton.setPosition(Gdx.graphics.getWidth() / 2 - inventoryButton.getOriginX(), 0);
                } else {
                    inventoryContainer.setPosition(Gdx.graphics.getWidth() / 2 - inventoryContainer.getOriginX(), 0);
                    inventoryButton.setPosition(Gdx.graphics.getWidth() / 2 - inventoryButton.getOriginX(), inventoryContainer.getHeight() - inventoryContainer.getHeight() * .22f);
                }
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
            }

            InventorySlideTracker();


            if (inventoryActive && !invJustActivated) {
                for (int i = 0; i < inventoryPowerups.length; i++) {
                    inventoryPowerups[i].Update();
                    if (inventoryPowerups[i].Tapped()) {
                        if (i < 3) {
                            if (powerupTotals[i] > 0 && !inventoryBools[i]) {
                                inventoryBools[i] = true;
                                powerup.onlyLaserNumber = i;
                                for (int j = 0; j < 3; j++) {
                                    if (i != j) {
                                        inventoryBools[j] = false;
                                    }
                                }
                                if (i == 0) {
                                    game.prefs.putInteger("FastLaserOnly", game.prefs.getInteger("FastLaserOnly") - 1);
                                } else if (i == 1) {
                                    game.prefs.putInteger("DualShotOnly", game.prefs.getInteger("DualShotOnly") - 1);
                                } else if (i == 2) {
                                    game.prefs.putInteger("PiercingOnly", game.prefs.getInteger("PiercingOnly") - 1);
                                }
                                if (game.prefs.getBoolean("Sound")) {
                                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                                }
                                powerupTotals[i]--;
                                game.prefs.flush();
                            }
                        } else if (i == 4 && powerupTotals[i] > 0 && !inventoryBools[i] && Earth.GetHealth() < 3) {
                            if (game.prefs.getBoolean("Sound")) {
                                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                            }
                            inventoryBools[i] = true;
                            game.prefs.putInteger("HealthInHand", game.prefs.getInteger("HealthInHand") - 1);
                            game.prefs.flush();
//                            if (powerup.PowerupOnScreen()) {
//                                powerup.SetPowerupOnScreen(false);
//                            }
                            Earth.SetHealth(Earth.GetHealth() + 1);
                            powerupTotals[i]--;
                        } else if (powerupTotals[i] > 0 && !inventoryBools[i] && i != 4) {
                            if (i == 3 && !powerup.PowerupActive()) {
                                if (game.prefs.getBoolean("Sound")) {
                                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                                }
                                inventoryBools[i] = true;
                                game.prefs.putInteger("X2InHand", game.prefs.getInteger("X2InHand") - 1);
                                game.prefs.flush();
                                if (powerup.PowerupOnScreen()) {
                                    powerup.SetPowerupOnScreen(false);
                                }
                                powerupTotals[i]--;
                                powerup.SetPowerupActive(true, PowerupType.x2Multiplier);
                            } else if (i == 5 && (!powerup.PowerupActive() || (powerup.PowerupActive() && powerup.PowerUpType() != PowerupType.Explosion))) {
                                if (game.prefs.getBoolean("Sound")) {
                                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                                }
                                inventoryBools[i] = true;
                                game.prefs.putInteger("BombInHand", game.prefs.getInteger("BombInHand") - 1);
                                game.prefs.flush();
                                powerup.SetExplosionSprite(Earth, game.prefs.getBoolean("Sound"), game.prefs.getFloat("SfxVolumeLevel"), true);
                            }
                            if (i != 3) {
                                powerupTotals[i]--;
                            }
                        }
                    }
                    if (inventoryBools[i] || powerupTotals[i] == 0 || (i == 4 && Earth.GetHealth() == 3)) {
                        inventoryPowerups[i].setAlpha(.15f);
                    } else {
                        inventoryPowerups[i].setAlpha(1f);
                    }
                }
            }

            if (!Gdx.input.isTouched(invTapIndextToTrack)) {
                invJustActivated = false;
                for (GameButtons button : inventoryPowerups) {
                    button.Update();
                    button.setTapped(false);
                }
            }

            if (inventoryBools[0] || inventoryBools[1] || inventoryBools[2]) {
                realCancelPowerupButton.Update();
                if (realCancelPowerupButton.Tapped()) {
                    for (int i = 0; i < 3; i++) {
                        inventoryBools[i] = false;
                    }
                    powerup.onlyLaserNumber = -1;
                }
            }
        }

        //region GameRunning
        if (!gameOver && !pause && !tutorial && !(inventoryActive && game.prefs.getBoolean("PauseOnInventory"))) {

            if (game.prefs.getBoolean("Sound")) {
                game.gameSong.setVolume(.3f * game.prefs.getFloat("MusicVolumeLevel"));
            }

//            if(invJustActivated) {
//                if (game.prefs.getBoolean("Sound")) {
//                    game.gameSong.play();
//                    game.gameSong.setVolume(.3f * game.prefs.getFloat("MusicVolumeLevel"));
//                    game.gameSong.setLooping(true);
//                }
//            }

            //region Controls

//            pauseButton.getBoundingRectangle().set(pauseButton.getBoundingRectangle().getX() - pauseButton.getBoundingRectangle().getX() * 1.5f, pauseButton.getBoundingRectangle().getY() - pauseButton.getBoundingRectangle().getY() * 1.5f,
//                    pauseButton.getBoundingRectangle().getWidth() * 10, pauseButton.getBoundingRectangle().getHeight() * 10);
//            pauseButton.setBounds(Gdx.graphics.getWidth() - pauseButton.getWidth() * 2, Gdx.graphics.getHeight() - pauseButton.getHeight() * 2,
//                    pauseButton.getBoundingRectangle().getWidth() * 3, pauseButton.getBoundingRectangle().getHeight() * 3);
//            whitePixel.setPosition(pauseButton.getBoundingRectangle().getX(), pauseButton.getBoundingRectangle().getY());
//            whitePixel.setSize(pauseButton.getBoundingRectangle().getWidth(), pauseButton.getBoundingRectangle().getHeight());
//            whitePixel.setAlpha(1f);
            if (pauseButton.Tapped() || Gdx.input.isKeyPressed(Input.Keys.ESCAPE) || Gdx.input.isKeyPressed(Input.Keys.HOME) || (!currentBackTouched && previousBackTouched)) {
                pause = true;
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                }
                game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
            }


//            if (Gdx.input.isKeyPressed(Input.Keys.Z) && camera.zoom < 1.95f) {
//                camera.zoom += .05;
//                if (camera.zoom > 1.95f) {
//                    camera.zoom = 1.95f;
//                }
//            }
//
//            if (Gdx.input.isKeyPressed(Input.Keys.X) && camera.zoom > .05f) {
//                camera.zoom -= .05;
//                if (camera.zoom < .5f) {
//                    camera.zoom = .5f;
//                }
//            }

            if (Gdx.input.isKeyPressed(Input.Keys.E)) {
                Earth.setRotation(Earth.getRotation() - 5);
            }

            if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
                Earth.setRotation(Earth.getRotation() + 5);
            }

            if (Gdx.input.isKeyPressed(Input.Keys.W)) {
                Earth.setY(Earth.getY() + 10);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.A)) {
                Earth.setX(Earth.getX() - 10);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.S)) {
                Earth.setY(Earth.getY() - 10);
            }
            if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                Earth.setX(Earth.getX() + 10);
            }

            //endregion


            //region SystemWork
            if (currentTouchState == Gestures.TwoFingers || currentTouchState == Gestures.ThreeFingers) {
                distanceBetweenFingers = GestureManager.DistanceBetweenTwoFingers(1, 0);
            }

            if (distanceBetweenFingers.angle() > 360) {
                distanceBetweenFingers.setAngle(distanceBetweenFingers.angle() - 360);
                previousDistanceBetweenFingers.setAngle(previousDistanceBetweenFingers.angle() - 360);
            } else if (distanceBetweenFingers.angle() < 0) {
                distanceBetweenFingers.setAngle(distanceBetweenFingers.angle() + 360);
                previousDistanceBetweenFingers.setAngle(previousDistanceBetweenFingers.angle() + 360);
            }

            //region PinchToZoom

//            if (currentTouchState == Gestures.ThreeFingers && camera.zoom >= .5f && camera.zoom <= 1.95f) {
//                camera.zoom = GestureManager.PinchCameraZoom(distanceBetweenFingers, previousDistanceBetweenFingers, camera.zoom, .05f);
//                if (camera.zoom > 1.95f) {
//                    camera.zoom = 1.95f;
//                } else if (camera.zoom < .5f) {
//                    camera.zoom = .5f;
//                }
//            }
//endregion

            if (!game.prefs.getBoolean("PC")) {
                if (game.prefs.getBoolean("AccelControls")) {
                    virtualJoystick.Update(camera.zoom, -1, false);
                } else {
                    virtualJoystick.Update(camera.zoom, shootJoystick.PreviousFingerTouching(), shootJoystick.FingerDown());
                }
                virtualJoystick.KeepOriginalPosition(camera.zoom);
                if (game.prefs.getBoolean("AccelControls")) {
                    shootButton.Update(camera.zoom);
                    shootButton.KeepOriginalPosition(camera.zoom);
                } else {
                    shootJoystick.Update(camera.zoom, virtualJoystick.PreviousFingerTouching(), virtualJoystick.FingerDown());
                    shootJoystick.KeepOriginalPosition(camera.zoom);
                }

                if (virtualJoystick.FingerDown()) {

                    if (!firstMove) {
                        EarthPreviousPosition = new Vector2(Earth.getX(), Earth.getY());
                    }
                    Earth.setPosition(Earth.getX() + virtualJoystick.DragSpeed().x / game.prefs.getFloat("MovementSpeed"), Earth.getY() + virtualJoystick.DragSpeed().y / game.prefs.getFloat("MovementSpeed"));
                    if (!firstMove) {
                        if (EarthPreviousPosition.x != Earth.getX() && EarthPreviousPosition.y != Earth.getY()) {
                            firstMove = true;
                        }
                    }
                }
            } else {
                if (Gdx.input.isTouched()) {
                    Vector2 fingerPosition = GestureManager.DragLocation(Earth.getWidth(), Earth.getHeight(), 1);
                    Earth.EarthRotationJoystick(new Vector2(fingerPosition.x - Earth.getX(), fingerPosition.y - Earth.getY()).angle(), true);
                }
            }

            Earth.Update(elapsedTime, camera.zoom, null, currentTouchState, previousTouchState, distanceBetweenFingers, previousDistanceBetweenFingers, game.prefs.getBoolean("AccelControls"));
            Moon.Update(elapsedTime, camera.zoom, Earth, currentTouchState, previousTouchState, distanceBetweenFingers, previousDistanceBetweenFingers, game.prefs.getBoolean("AccelControls"));
            continentsSprite.setPosition(Earth.getX(), Earth.getY());
            continentsSprite.setRotation(Earth.getRotation());
            grayOverlay.setPosition(Earth.getX(), Earth.getY());
            grayOverlay.setRotation(Earth.getRotation());
            grayOverlayMoon.setPosition(Moon.getX(), Moon.getY());
            grayOverlayMoon.setRotation(Moon.getRotation());
            moonBlotsSprite.setPosition(Moon.getX(), Moon.getY());
            moonBlotsSprite.setRotation(Moon.getRotation());
            camera.update();
            health.Update(Earth.GetHealth(), camera.zoom);
            pauseButton.Update(camera.zoom);

            //region Inventory Logic
            inventoryButton.Update();


            if (inventoryButton.Tapped()) {
                inventoryActive = !inventoryActive;
                int[] invFingersDown = GestureManager.CurrentFingersDown();
                for (int i = 0; i < invFingersDown.length; i++) {
                    Vector2 fingerLocation = GestureManager.InputLocation(invFingersDown[i]);
                    if (inventoryButton.getBoundingRectangle().overlaps(new Rectangle(fingerLocation.x, fingerLocation.y, 1, 1))) {
                        invTapIndextToTrack = invFingersDown[i];
                        break;
                    }
                }

                if (!inventoryActive) {
                    inventoryContainer.setPosition(Gdx.graphics.getWidth() / 2 - inventoryContainer.getOriginX(), -inventoryContainer.getHeight() * .78f);
                    inventoryButton.setPosition(Gdx.graphics.getWidth() / 2 - inventoryButton.getOriginX(), 0);
                } else {
                    inventoryContainer.setPosition(Gdx.graphics.getWidth() / 2 - inventoryContainer.getOriginX(), 0);
                    inventoryButton.setPosition(Gdx.graphics.getWidth() / 2 - inventoryButton.getOriginX(), inventoryContainer.getHeight() - inventoryContainer.getHeight() * .22f);
                }
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                invJustActivated = true;
            }


            InventorySlideTracker();

            if (inventoryActive && !invJustActivated && !game.prefs.getBoolean("PauseOnInventory")) {
                for (int i = 0; i < inventoryPowerups.length; i++) {
                    inventoryPowerups[i].Update();
                    if (inventoryPowerups[i].Tapped()) {
                        if (i < 3) {
                            if (powerupTotals[i] > 0 && !inventoryBools[i]) {
                                inventoryBools[i] = true;
                                powerup.onlyLaserNumber = i;
                                for (int j = 0; j < 3; j++) {
                                    if (i != j) {
                                        inventoryBools[j] = false;
                                    }
                                }
                                if (i == 0) {
                                    game.prefs.putInteger("FastLaserOnly", game.prefs.getInteger("FastLaserOnly") - 1);
                                } else if (i == 1) {
                                    game.prefs.putInteger("DualShotOnly", game.prefs.getInteger("DualShotOnly") - 1);
                                } else if (i == 2) {
                                    game.prefs.putInteger("PiercingOnly", game.prefs.getInteger("PiercingOnly") - 1);
                                }
                                if (game.prefs.getBoolean("Sound")) {
                                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                                }
                                powerupTotals[i]--;
                                game.prefs.flush();
                            }
                        } else if (i == 4 && powerupTotals[i] > 0 && !inventoryBools[i] && Earth.GetHealth() < 3) {
                            if (game.prefs.getBoolean("Sound")) {
                                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                            }
                            inventoryBools[i] = true;
                            game.prefs.putInteger("HealthInHand", game.prefs.getInteger("HealthInHand") - 1);
                            game.prefs.flush();
//                            if (powerup.PowerupOnScreen()) {
//                                powerup.SetPowerupOnScreen(false);
//                            }
                            Earth.SetHealth(Earth.GetHealth() + 1);
                            powerupTotals[i]--;
                        } else if (powerupTotals[i] > 0 && !inventoryBools[i] && i != 4) {
                            if (i == 3 && !powerup.PowerupActive()) {
                                if (game.prefs.getBoolean("Sound")) {
                                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                                }
                                inventoryBools[i] = true;
                                game.prefs.putInteger("X2InHand", game.prefs.getInteger("X2InHand") - 1);
                                game.prefs.flush();
                                if (powerup.PowerupOnScreen()) {
                                    powerup.SetPowerupOnScreen(false);
                                }
                                powerupTotals[i]--;
                                powerup.SetPowerupActive(true, PowerupType.x2Multiplier);
                            } else if (i == 5 && (!powerup.PowerupActive() || (powerup.PowerupActive() && powerup.PowerUpType() != PowerupType.Explosion))) {
                                if (game.prefs.getBoolean("Sound")) {
                                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                                }
                                inventoryBools[i] = true;
                                game.prefs.putInteger("BombInHand", game.prefs.getInteger("BombInHand") - 1);
                                game.prefs.flush();
                                powerup.SetExplosionSprite(Earth, game.prefs.getBoolean("Sound"), game.prefs.getFloat("SfxVolumeLevel"), true);
                            }
                            if (i != 3) {
                                powerupTotals[i]--;
                            }
                        }
                    }
                    if (inventoryBools[i] || powerupTotals[i] == 0 || (i == 4 && Earth.GetHealth() == 3)) {
                        inventoryPowerups[i].setAlpha(.15f);
                    } else {
                        inventoryPowerups[i].setAlpha(1f);
                    }
                }
            }

            if (inventoryBools[0] || inventoryBools[1] || inventoryBools[2]) {
                realCancelPowerupButton.Update();
                if (realCancelPowerupButton.Tapped()) {
                    for (int i = 0; i < 3; i++) {
                        inventoryBools[i] = false;
                    }
                    powerup.onlyLaserNumber = -1;
                }
            }

            //endregion

            if (asteroidPlanet != null) {
                powerup.Update(Gdx.graphics.getDeltaTime(), Earth, game.prefs.getBoolean("Sound"), game.prefs.getFloat("SfxVolumeLevel"), Gdx.graphics.getDeltaTime(), asteroidPlanet.DestinationMet(), game.prefs.getBoolean("Vibrate"));
            } else {
                powerup.Update(Gdx.graphics.getDeltaTime(), Earth, game.prefs.getBoolean("Sound"), game.prefs.getFloat("SfxVolumeLevel"), Gdx.graphics.getDeltaTime(), false, game.prefs.getBoolean("Vibrate"));
            }

            if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.DualShot && !powerup.AsteroidPlanetGenerated()) {
//                Random random = new Random();
//                int randomAsteroid;
                int closestAsteroidIndex = 0;
                float closestAsteroidDistance = 0;
                for (int i = 0; i < asteroidsArray.length; i++) {
                    if (i == 0 && asteroidsArray[i]._active) {
                        closestAsteroidIndex = i;
                        closestAsteroidDistance = new Vector2(Earth.getX() + Earth.getOriginX() - asteroidsArray[i].getX() + asteroidsArray[i].getOriginX(), Earth.getY() + Earth.getOriginY() - asteroidsArray[i].getY() + asteroidsArray[i].getOriginY()).len();
                    } else if (asteroidsArray[i]._active && ((new Vector2(Earth.getX() + Earth.getOriginX() - asteroidsArray[i].getX() + asteroidsArray[i].getOriginX(), Earth.getY() + Earth.getOriginY() - asteroidsArray[i].getY() + asteroidsArray[i].getOriginY()).len() < closestAsteroidDistance && !asteroidsArray[i].Exploding()) || asteroidsArray[closestAsteroidIndex].Exploding())) {
                        closestAsteroidIndex = i;
                        closestAsteroidDistance = new Vector2(Earth.getX() + Earth.getOriginX() - asteroidsArray[i].getX() + asteroidsArray[i].getOriginX(), Earth.getY() + Earth.getOriginY() - asteroidsArray[i].getY() + asteroidsArray[i].getOriginY()).len();
                    } else if (asteroidsArray[i]._active && closestAsteroidDistance == 0) {
                        closestAsteroidIndex = i;
                        closestAsteroidDistance = new Vector2(Earth.getX() + Earth.getOriginX() - asteroidsArray[i].getX() + asteroidsArray[i].getOriginX(), Earth.getY() + Earth.getOriginY() - asteroidsArray[i].getY() + asteroidsArray[i].getOriginY()).len();
                    }
                }
//                do
//                {
//                    randodmAsteroid = random.nextInt(asteroids.size());
//                }while(asteroids.get(randomAsteroid).Exploding());
                if (game.prefs.getBoolean("PixelSkin")) {
                    asteroidPlanet = new AsteroidPlanet(asteroidsArray[closestAsteroidIndex], Earth.OppositeMoonLocation(new Vector2(Moon.getX(), Moon.getY()), Moon.getOriginX(), Moon.getOriginY()), 10 * game.screenScale, 20 * game.screenScale, game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class));
                } else {
                    asteroidPlanet = new AsteroidPlanet(asteroidsArray[closestAsteroidIndex], Earth.OppositeMoonLocation(new Vector2(Moon.getX(), Moon.getY()), Moon.getOriginX(), Moon.getOriginY()), 10 * game.screenScale, 20 * game.screenScale, game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class));
                }
                powerup.SetAsteroidPlanetGenerated(true);
                asteroidsArray[closestAsteroidIndex]._active = false;
            }

            if (powerup.PowerUpType() == PowerupType.DualShot && powerup.AsteroidPlanetGenerated() && !asteroidPlanet.Exploded()) {
                asteroidPlanet.Update(Earth.OppositeMoonLocation(new Vector2(Moon.getX(), Moon.getY()), Moon.getOriginX(), Moon.getOriginY()), Gdx.graphics.getDeltaTime(), powerup.PowerupActive(), Earth.getRotation());
            }

            for (Sprite background : backgroundSprite) {
                if (!game.prefs.getBoolean("NinjaSkin") && !game.prefs.getBoolean("PirateSkin") && !game.prefs.getBoolean("SoccerSkin") && !game.prefs.getBoolean("SumoSkin")) {
                    background.setPosition(background.getX(), background.getY() - 2 * game.screenScale);
                    if (background.getY() + background.getHeight() <= (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * camera.zoom) / 2) {
//                        background.setPosition(background.getX(), Gdx.graphics.getHeight() - ((Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * camera.zoom) / 2));
                        background.setPosition(background.getX(), background.getHeight());
                    }
                }
            }

            int activeAsteroids = 0;
            int activeExplodingAsteroids = 0;
            int activeHugeAsteroids = 0;
            for (int i = 0; i < asteroidsArray.length; i++) {
                if (i < hugeAsteroidAsteroids.length) {
                    if (hugeAsteroidAsteroids[i]._active) {
                        activeAsteroids++;
                    }
                }
                if(i < explodingAsteroidsArray.length && explodingAsteroidsArray[i]._active) {
                    activeExplodingAsteroids++;
                }
                if(i < hugeAsteroidsArray.length && hugeAsteroidsArray[i]._active) {
                    activeHugeAsteroids++;
                }
                if (asteroidsArray[i]._active) {
                    activeAsteroids++;
                }
            }


            if (activeAsteroids + magneticAsteroids.size() + activeHugeAsteroids + activeExplodingAsteroids < asteroidMax && elapsedAsteroidTime >= asteroidTime) {
                int asteroidType;
                if (game.prefs.getBoolean("Hard")) {
                    if (minutes == 1 && seconds == 30 && hugeAsteroidMax != 2) {
                        hugeAsteroidMax = 2;
                        explodingAsteroidMax = 2;
                    }
                    if (minutes == 2 && seconds == 30 && explodingAsteroidMax != 3) {
                        explodingAsteroidMax = 3;
                        magneticAsteroidMax = 2;
                        waveTotal++;
                    }
                    if (minutes == 3 && seconds == 30 && explodingAsteroidMax != 4) {
                        explodingAsteroidMax = 4;
                        magneticAsteroidMax = 3;
                        waveTotal++;
                        waveTime = 0;
                    }
                } else if (game.prefs.getBoolean("Normal")) {
                    if (minutes == 2 && hugeAsteroidMax != 2) {
                        hugeAsteroidMax = 2;
                        explodingAsteroidMax = 2;
                    }
                    if (minutes == 3 && explodingAsteroidMax != 3) {
                        explodingAsteroidMax = 3;
                        magneticAsteroidMax = 2;
//                        waveTotal++;
//                        waveTime = 0;
                    }
//                    if (minutes == 4 && explodingAsteroidMax != 4) {
//                        explodingAsteroidMax = 4;
//                        magneticAsteroidMax = 3;
//                        waveTotal++;
//                        waveTime = 0;
//                    }
                } else {
                    if (minutes == 2 && seconds == 30 && hugeAsteroidMax != 2) {
                        hugeAsteroidMax = 2;
                        explodingAsteroidMax = 2;
                    }
                    if (minutes == 3 && seconds == 30 && magneticAsteroidMax != 2) {
                        magneticAsteroidMax = 2;
                    }
                }

                if (activeAsteroids > 0) {
                    asteroidType = random.nextInt(10);
                } else {
                    asteroidType = 3;
                }
//                asteroidType = 2;
                //
                if (asteroidType == 0 && magneticAsteroids.size() < magneticAsteroidMax && ((seconds > 30 && minutes >= 1) || minutes >= 2)) {
                    if (game.prefs.getBoolean("PixelSkin")) {
                        magneticAsteroids.add(new MagneticAsteroid(asteroidsArray[0], new Vector2(Earth.getX(), Earth.getY()), 10 * game.screenScale, 5 * game.screenScale, game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class), new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.screenScale, this.game.manager.get("data/EmptyCircle.png", Texture.class), null));
                    } else if (game.prefs.getBoolean("PirateSkin")) {
                        magneticAsteroids.add(new MagneticAsteroid(asteroidsArray[0], new Vector2(Earth.getX(), Earth.getY()), 10 * game.screenScale, 5 * game.screenScale, game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class), new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.screenScale, this.game.manager.get("data/EmptyCircle.png", Texture.class), this.game.manager.get("data/CanonBallShine.png", Texture.class)));
                        magneticAsteroids.get(magneticAsteroids.size() - 1).setTexture(this.game.manager.get("data/EmptyCircle.png", Texture.class));
                        magneticAsteroids.get(magneticAsteroids.size() - 1).get_circleOverlay().setAlpha(.5f);
                    } else {
                        magneticAsteroids.add(new MagneticAsteroid(asteroidsArray[0], new Vector2(Earth.getX(), Earth.getY()), 10 * game.screenScale, 5 * game.screenScale, game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class), new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.screenScale, this.game.manager.get("data/EmptyCircle.png", Texture.class), null));
                        if (game.prefs.getBoolean("NinjaSkin")) {
                            magneticAsteroids.get(magneticAsteroids.size() - 1).get_circleOverlay().setAlpha(.5f);
                        }
                    }

                } else if (asteroidType == 1 && activeExplodingAsteroids < explodingAsteroidMax && minutes >= 1) {
                    while(explodingAsteroidsArray[explodingAsteroidsIndex]._active) {
                        explodingAsteroidsIndex++;
                        if(explodingAsteroidsIndex == explodingAsteroidsArray.length) {
                            explodingAsteroidsIndex = 0;
                        }
                    }

                    explodingAsteroidsArray[explodingAsteroidsIndex].Reset(new Vector2(Earth.getX(), Earth.getY()));
                    explodingAsteroidsArray[explodingAsteroidsIndex]._active = true;

//                    if (game.prefs.getBoolean("PixelSkin")) {
//                        explodingAsteroids.add(new ExplodingAsteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class), this.game.manager.get("data/EmptyCircle.png", Texture.class), null));
//                    } else if (game.prefs.getBoolean("PirateSkin")) {
//                        explodingAsteroids.add(new ExplodingAsteroid(this.game.manager.get("data/EmptyCircle.png", Texture.class), new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class), this.game.manager.get("data/EmptyCircle.png", Texture.class), this.game.manager.get("data/CanonBallShine.png", Texture.class)));
//                        explodingAsteroids.get(explodingAsteroids.size() - 1).get_circleOverlay().setAlpha(.5f);
//                    } else {
//                        explodingAsteroids.add(new ExplodingAsteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class), this.game.manager.get("data/EmptyCircle.png", Texture.class), null));
//                    }
                } else if (asteroidType == 2 && activeHugeAsteroids < hugeAsteroidMax && (seconds > 30 || minutes >= 1)) {
                    while(hugeAsteroidsArray[hugeAsteroidsIndex]._active) {
                        hugeAsteroidsIndex++;
                        if(hugeAsteroidsIndex == hugeAsteroidsArray.length) {
                            hugeAsteroidsIndex = 0;
                        }
                    }

                    hugeAsteroidsArray[hugeAsteroidsIndex].Reset(new Vector2(Earth.getX(), Earth.getY()));
                    hugeAsteroidsArray[hugeAsteroidsIndex]._active = true;

//                    if (game.prefs.getBoolean("PixelSkin")) {
//                        hugeAsteroids.add(new HugeAsteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class)));
//                    } else {
//                        hugeAsteroids.add(new HugeAsteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class)));
//                    }
                } else {
                    while (asteroidsArray[activeAsteroidIndex]._active) {
                        activeAsteroidIndex++;
                        if (activeAsteroidIndex == asteroidsArray.length) {
                            activeAsteroidIndex = 0;
                        }
                    }
                    asteroidsArray[activeAsteroidIndex].ResetAsteroid(asteroidTexture, new Vector2(Earth.getX(), Earth.getY()));
                    asteroidsArray[activeAsteroidIndex]._active = true;
//                    if (game.prefs.getBoolean("PixelSkin")) {
//                        asteroids.add(new Asteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class)));
//                    } else {
//                        asteroids.add(new Asteroid(asteroidTexture, new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY()), planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class)));
//                    }
                }
                elapsedAsteroidTime = 0;
            }

            if ((int) elapsedTime % difficultyTimer == 0 && (int) elapsedTime != 0) {
                if (game.prefs.getBoolean("Easy") && asteroidTime > .25f) {
                    difficultyTimer += difficultyTimerIncrement;
                    asteroidTime *= .85f;
                    waveCount++;
                    waveTime = 0;
                } else if (game.prefs.getBoolean("Normal") && asteroidTime > .20f) {
                    difficultyTimer += difficultyTimerIncrement;
                    asteroidTime *= .85f;
                    waveCount++;
                    waveTime = 0;
                } else if (game.prefs.getBoolean("Hard") && asteroidTime > .15f) {
                    difficultyTimer += difficultyTimerIncrement;
                    asteroidTime *= .85f;
                    waveCount++;
                    waveTime = 0;
                }
                if (game.prefs.getBoolean("Hard") && asteroidTime < .15f) {
                    asteroidTime = .15f;
                }
                if (game.prefs.getBoolean("Normal") && asteroidTime < .2f) {
                    asteroidTime = .2f;
                }
                if (game.prefs.getBoolean("Easy") && asteroidTime < .25f) {
                    asteroidTime = .25f;
                }

            }

            if ((int) elapsedTime % (int) scoreAddTime == 0 && (int) elapsedTime != 0) {
                scoreAdd *= 2;
                showScoreChange = true;
                showScoreChangeTime = scoreAddTime + 3;
                scoreAddTime += scoreAddTime;
            }

            if ((int) elapsedTime % (int) showScoreChangeTime == 0) {
                showScoreChange = false;
            }

            if (game.prefs.getBoolean("AccelControls")) {
                if ((shootButton.Tapped() || Gdx.input.isKeyPressed(Input.Keys.SPACE)) && !drawLaser) {
                    laserTime = 0;
                    drawLaser = true;
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(50);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.LaserSound.play(.075f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            } else {
                if ((game.prefs.getBoolean("PC") && Gdx.input.isTouched() && !drawLaser) || (shootJoystick.GetDrawHover() && (shootAnimation.isAnimationFinished(laserTime) || laserTime == 0)) || Gdx.input.isKeyPressed(Input.Keys.SPACE) && !drawLaser) {
                    laserTime = 0;
                    drawLaser = true;
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(50);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.LaserSound.play(.075f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            }
            if (powerup.PowerupActive() && powerup.GetSwitchShoot() && powerup.PowerUpType() == PowerupType.ShootFaster) {
                shootAnimation = new Animation(.028f, game.manager.get("data/SpeedPowerup-packed/pack.atlas", TextureAtlas.class).getRegions(), Animation.PlayMode.NORMAL);
            }
//            else if(powerup.PowerupActive() && powerup.GetSwitchShoot() && powerup.PowerUpType() == PowerupType.DualShot)
//            {
//                shootAnimation = new Animation(.036f, game.manager.get("data/DualShotEarthAnimation-packed/pack.atlas", TextureAtlas.class).getRegions(), Animation.PlayMode.NORMAL);
//            }
            else if (!powerup.PowerupActive() && powerup.GetSwitchShoot()) {
                shootAnimation = new Animation(.018f, game.manager.get("data/RealEarthLaserAnimationThick-packed/pack.atlas", TextureAtlas.class).getRegions(), Animation.PlayMode.NORMAL);
                powerup.SetSwitchShoot(false);
                laserTime = 0;
            }

            if (drawLaser) {
                currentFrame = shootAnimation.getKeyFrame(laserTime, false);
                laserTime += Gdx.graphics.getDeltaTime();
                laser.setPosition(Earth.getX() - 8 * game.screenScale, Earth.getY() - 9 * game.screenScale);
                laser.setRotation(Earth.getRotation() - 90);
            }

            if (shootAnimation.isAnimationFinished(laserTime) && drawLaser) {
                drawLaser = false;
                if (!firstShot) {
                    firstShot = true;
                }
//                bullets.add(new LaserBullets(laserTexture, 50 * game.screenScale, 20 * game.screenScale));
//                bullets.get(bullets.size() - 1).setSize(7f * game.screenScale, 40 * game.screenScale);
//                bullets.get(bullets.size() - 1).setOriginCenter();
                while (bulletsArray[laserBulletIndex]._active) {
                    laserBulletIndex++;
                    if (laserBulletIndex == bulletsArray.length) {
                        laserBulletIndex = 0;
                    }
                }
                bulletsArray[laserBulletIndex].ResetLaser();
                bulletsArray[laserBulletIndex]._active = true;
                if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.DualShot && asteroidPlanet.DestinationMet()) {
//                    bullets.add(new LaserBullets(laserTexture, 50 * game.screenScale, 20 * game.screenScale));
//                    bullets.get(bullets.size() - 1).setSize(7f * game.screenScale, 40 * game.screenScale);
//                    bullets.get(bullets.size() - 1).setOriginCenter();
                    while (bulletsArray[laserBulletIndex]._active) {
                        laserBulletIndex++;
                        if (laserBulletIndex == bulletsArray.length) {
                            laserBulletIndex = 0;
                        }
                    }
                    bulletsArray[laserBulletIndex].ResetLaser();
                    bulletsArray[laserBulletIndex]._active = true;
                }
            }

            for (int i = 0, j= 0; i < bulletsArray.length; i++) {
                if (asteroidPlanet != null && asteroidPlanet.DestinationMet() && powerup.PowerUpType() == PowerupType.DualShot && powerup.PowerupActive()) {
                    if (j % 2 == 0) {
                        if(bulletsArray[i]._active) {
                            bulletsArray[i].Update(new Vector2(asteroidPlanet.getX() + asteroidPlanet.getOriginX(), asteroidPlanet.getY() + asteroidPlanet.getOriginY()), Earth.getRotation() - 180);
                            j++;
                        }
                    } else {
                        if(bulletsArray[i]._active) {
                            bulletsArray[i].Update(new Vector2(Moon.getX() + Moon.getOriginX(), Moon.getY() + Moon.getOriginY()), Earth.getRotation());
                            j++;
                        }
                    }
                } else {
                    if(bulletsArray[i]._active) {
                        bulletsArray[i].Update(new Vector2(Moon.getX() + Moon.getOriginX(), Moon.getY() + Moon.getOriginY()), Earth.getRotation());
                    }
                }
                if (bulletsArray[i].OffScreen(camera.zoom)) {
                    bulletsArray[i]._active = false;
                }
            }

//            if (bullets.size() > 0) {
//                for (int i = 0; i < bullets.size(); i++) {
//                    if (asteroidPlanet != null && asteroidPlanet.DestinationMet() && powerup.PowerUpType() == PowerupType.DualShot && powerup.PowerupActive()) {
//                        if (i % 2 == 0) {
//                            bullets.get(i).Update(new Vector2(asteroidPlanet.getX() + asteroidPlanet.getOriginX(), asteroidPlanet.getY() + asteroidPlanet.getOriginY()), Earth.getRotation() - 180);
//                        } else {
//                            bullets.get(i).Update(new Vector2(Moon.getX() + Moon.getOriginX(), Moon.getY() + Moon.getOriginY()), Earth.getRotation());
//                        }
//                    } else {
//                        bullets.get(i).Update(new Vector2(Moon.getX() + Moon.getOriginX(), Moon.getY() + Moon.getOriginY()), Earth.getRotation());
//
//                    }
//                    if (bullets.get(i).OffScreen(camera.zoom)) {
//                        bullets.remove(i);
//                    }
//                }
//            }

            //region Asteroids ArrayList
//            if (asteroids.size() > 0) {
//                for (int i = 0; i < asteroids.size(); i++) {
//                    asteroids.get(i).Update(camera.zoom, bullets, Earth, powerup, game.prefs.getBoolean("Vibrate"), game.SmallExplosionSound, game.prefs.getBoolean("Sound"), Gdx.graphics.getDeltaTime(), game.prefs.getFloat("SfxVolumeLevel"));
////                    if(game.prefs.getBoolean("PirateSkin")) {
////                        canonBallShines.get(i).setPosition(asteroids.get(i).getX(), asteroids.get(i).getY());
////                        canonBallShines.get(i).setRotation(asteroids.get(i).getRotation());
////                    }
//                    if (asteroids.get(i)._toScore) {
//                        if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.x2Multiplier) {
//                            if (asteroids.get(i)._doubleScore) {
//                                score += scoreAdd * 4;
//                                scoreValues.add(scoreAdd * 4);
//                            } else {
//                                score += scoreAdd * 2;
//                                scoreValues.add(scoreAdd * 2);
//                            }
//                        } else {
//                            if (asteroids.get(i)._doubleScore) {
//                                score += scoreAdd * 2;
//                                scoreValues.add(scoreAdd * 2);
//                            } else {
//                                score += scoreAdd;
//                                scoreValues.add(scoreAdd);
//                            }
//                        }
//                        scoreLocations.add(new Vector2(asteroids.get(i).getX(), asteroids.get(i).getY()));
//                        scoreAlphas.add(new Float(1.0));
//                        asteroids.get(i)._toScore = false;
//                    }
//                    if (asteroids.get(i).OutOfBounds()) {
//                        asteroids.remove(i);
//                    }
//                }
//            }
            //endregion


            for (int i = 0; i < asteroidsArray.length; i++) {
                asteroidsArray[i].Update(camera.zoom, bulletsArray, Earth, powerup, game.prefs.getBoolean("Vibrate"), game.SmallExplosionSound, game.prefs.getBoolean("Sound"), Gdx.graphics.getDeltaTime(), game.prefs.getFloat("SfxVolumeLevel"));
//                    if(game.prefs.getBoolean("PirateSkin")) {
//                        canonBallShines.get(i).setPosition(asteroids.get(i).getX(), asteroids.get(i).getY());
//                        canonBallShines.get(i).setRotation(asteroids.get(i).getRotation());
//                    }
                if (asteroidsArray[i]._toScore) {
                    if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.x2Multiplier) {
                        if (asteroidsArray[i]._doubleScore) {
                            score += scoreAdd * 4;
                            scoreValues.add(scoreAdd * 4);
                        } else {
                            score += scoreAdd * 2;
                            scoreValues.add(scoreAdd * 2);
                        }
                    } else {
                        if (asteroidsArray[i]._doubleScore) {
                            score += scoreAdd * 2;
                            scoreValues.add(scoreAdd * 2);
                        } else {
                            score += scoreAdd;
                            scoreValues.add(scoreAdd);
                        }
                    }
                    scoreLocations.add(new Vector2(asteroidsArray[i].getX(), asteroidsArray[i].getY()));
                    scoreAlphas.add(new Float(1.0));
                    asteroidsArray[i]._toScore = false;
                }
            }

            for (int i = 0; i < hugeAsteroidAsteroids.length; i++) {
                hugeAsteroidAsteroids[i].Update(camera.zoom, bulletsArray, Earth, powerup, game.prefs.getBoolean("Vibrate"), game.SmallExplosionSound, game.prefs.getBoolean("Sound"), Gdx.graphics.getDeltaTime(), game.prefs.getFloat("SfxVolumeLevel"));
//                    if(game.prefs.getBoolean("PirateSkin")) {
//                        canonBallShines.get(i).setPosition(asteroids.get(i).getX(), asteroids.get(i).getY());
//                        canonBallShines.get(i).setRotation(asteroids.get(i).getRotation());
//                    }
                if (hugeAsteroidAsteroids[i]._toScore) {
                    if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.x2Multiplier) {
                        if (hugeAsteroidAsteroids[i]._doubleScore) {
                            score += scoreAdd * 4;
                            scoreValues.add(scoreAdd * 4);
                        } else {
                            score += scoreAdd * 2;
                            scoreValues.add(scoreAdd * 2);
                        }
                    } else {
                        if (hugeAsteroidAsteroids[i]._doubleScore) {
                            score += scoreAdd * 2;
                            scoreValues.add(scoreAdd * 2);
                        } else {
                            score += scoreAdd;
                            scoreValues.add(scoreAdd);
                        }
                    }
                    scoreLocations.add(new Vector2(hugeAsteroidAsteroids[i].getX(), hugeAsteroidAsteroids[i].getY()));
                    scoreAlphas.add(new Float(1.0));
                    hugeAsteroidAsteroids[i]._toScore = false;
                }
            }

            if (magneticAsteroids.size() > 0) {
                for (int i = 0; i < magneticAsteroids.size(); i++) {
                    magneticAsteroids.get(i).Update(camera.zoom, bulletsArray, Earth, powerup, game.prefs.getBoolean("Vibrate"), game.SmallExplosionSound, game.prefs.getBoolean("Sound"), Gdx.graphics.getDeltaTime(), game.prefs.getFloat("SfxVolumeLevel"));
                    if (magneticAsteroids.get(i)._toScore) {
                        if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.x2Multiplier) {
                            if (magneticAsteroids.get(i)._doubleScore) {
                                score += scoreAdd * 4;
                                scoreValues.add(scoreAdd * 4);
                            } else {
                                score += scoreAdd * 2;
                                scoreValues.add(scoreAdd * 2);
                            }
                        } else {
                            if (magneticAsteroids.get(i)._doubleScore) {
                                score += scoreAdd * 2;
                                scoreValues.add(scoreAdd * 2);
                            } else {
                                score += scoreAdd;
                                scoreValues.add(scoreAdd);
                            }
                        }
                        scoreLocations.add(new Vector2(magneticAsteroids.get(i).getX(), magneticAsteroids.get(i).getY()));
                        scoreAlphas.add(new Float(1.0));
                        magneticAsteroids.get(i)._toScore = false;
                    }
                    if (magneticAsteroids.get(i).Exploded()) {
                        magneticAsteroids.remove(i);
                    }
                }
            }

            //region ExplodingAsteroidsLogic

            for (int i = 0; i < explodingAsteroidsArray.length; i++) {
                explodingAsteroidsArray[i].Update(camera.zoom, bulletsArray, Earth, powerup, game.prefs.getBoolean("Vibrate"), game.SmallExplosionSound, game.prefs.getBoolean("Sound"), Gdx.graphics.getDeltaTime(), game.prefs.getFloat("SfxVolumeLevel"));
                if (explodingAsteroidsArray[i]._active && explodingAsteroidsArray[i].Exploding()) {
                    for (int j = 0; j < asteroidsArray.length; j++) {
                        if (asteroidsArray[j]._active && !asteroidsArray[j]._exploding && explodingAsteroidsArray[i].CheckCollision(new Vector2(asteroidsArray[j].getX(), asteroidsArray[j].getY()), asteroidsArray[j].Radius(), new Vector2(asteroidsArray[j].getOriginX(), asteroidsArray[j].getOriginY()))) {
                            asteroidsArray[j]._toScore = true;
//                                asteroidsArray[j].setOutOfBounds(true);
                            asteroidsArray[j]._exploding = true;
                            asteroidsArray[j]._doubleScore = true;
                        }
                        if (j < hugeAsteroidAsteroids.length && !hugeAsteroidAsteroids[j]._exploding && hugeAsteroidAsteroids[j]._active && explodingAsteroidsArray[i].CheckCollision(new Vector2(hugeAsteroidAsteroids[j].getX(), hugeAsteroidAsteroids[j].getY()), hugeAsteroidAsteroids[j].Radius(), new Vector2(hugeAsteroidAsteroids[j].getOriginX(), hugeAsteroidAsteroids[j].getOriginY()))) {
                            hugeAsteroidAsteroids[j]._toScore = true;
//                                asteroidsArray[j].setOutOfBounds(true);
                            hugeAsteroidAsteroids[j]._exploding = true;
                            hugeAsteroidAsteroids[j]._doubleScore = true;
                        }
                        if (j < magneticAsteroids.size() && explodingAsteroidsArray[i].CheckCollision(new Vector2(magneticAsteroids.get(j).getX(), magneticAsteroids.get(j).getY()), magneticAsteroids.get(j).Radius(), new Vector2(magneticAsteroids.get(j).getOriginX(), magneticAsteroids.get(j).getOriginY())) && !magneticAsteroids.get(j)._toExplode) {
                            magneticAsteroids.get(j)._toScore = true;
                            magneticAsteroids.get(j)._toExplode = true;
                            magneticAsteroids.get(j)._doubleScore = true;
                        }
                        if (i != j && j < explodingAsteroidsArray.length && explodingAsteroidsArray[j]._active && (!powerup.PowerupActive() || !powerup.explosionSimultaneous || (powerup.PowerupActive() && powerup.PowerUpType() != PowerupType.Explosion)) && explodingAsteroidsArray[i].CheckCollision(new Vector2(explodingAsteroidsArray[j].getX(), explodingAsteroidsArray[j].getY()), explodingAsteroidsArray[j].Radius(), new Vector2(explodingAsteroidsArray[j].getOriginX(), explodingAsteroidsArray[j].getOriginY())) && !explodingAsteroidsArray[j]._exploding) {
                            explodingAsteroidsArray[j]._toScore = true;
                            explodingAsteroidsArray[j]._exploding = true;
                            explodingAsteroidsArray[j].SetEarthCollision(true);
                            explodingAsteroidsArray[j]._doubleScore = true;
                        }
                        if (j < hugeAsteroidsArray.length && hugeAsteroidsArray[j]._active && explodingAsteroidsArray[i].CheckCollision(new Vector2(hugeAsteroidsArray[j].getX(), hugeAsteroidsArray[j].getY()), hugeAsteroidsArray[j].Radius(), new Vector2(hugeAsteroidsArray[j].getOriginX(), hugeAsteroidsArray[j].getOriginY())) && !hugeAsteroidsArray[j]._exploding) {
                            hugeAsteroidsArray[j]._toScore = true;
                            hugeAsteroidsArray[j]._exploding = true;
                            hugeAsteroidsArray[j]._doubleScore = true;
                        }
                    }
                }
                if (explodingAsteroidsArray[i]._toScore) {
                    if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.x2Multiplier) {
                        if (explodingAsteroidsArray[i]._doubleScore) {
                            score += scoreAdd * 4;
                            scoreValues.add(scoreAdd * 4);
                        } else {
                            score += scoreAdd * 2;
                            scoreValues.add(scoreAdd * 2);
                        }
                    } else {
                        if (explodingAsteroidsArray[i]._doubleScore) {
                            score += scoreAdd * 2;
                            scoreValues.add(scoreAdd * 2);
                        } else {
                            score += scoreAdd;
                            scoreValues.add(scoreAdd);
                        }
                    }
                    scoreLocations.add(new Vector2(explodingAsteroidsArray[i].getX() + explodingAsteroidsArray[i].getOriginX(), explodingAsteroidsArray[i].getY() + explodingAsteroidsArray[i].getOriginY()));
                    scoreAlphas.add(new Float(1.0));
                    explodingAsteroidsArray[i]._toScore = false;
                }
//                if (explodingAsteroids.get(i).OutOfBounds()) {
//                    explodingAsteroids.remove(i);
//                }
            }

            //endregion

            //region oldExplodingLogic

//            if (explodingAsteroids.size() > 0) {
//                for (int i = 0; i < explodingAsteroids.size(); i++) {
//                    explodingAsteroids.get(i).Update(camera.zoom, bulletsArray, Earth, powerup, game.prefs.getBoolean("Vibrate"), game.SmallExplosionSound, game.prefs.getBoolean("Sound"), Gdx.graphics.getDeltaTime(), game.prefs.getFloat("SfxVolumeLevel"));
//                    if (explodingAsteroids.get(i).Exploding()) {
//                        for (int j = 0; j < asteroidsArray.length; j++) {
//                            if (asteroidsArray[j]._active && !asteroidsArray[j]._exploding && explodingAsteroids.get(i).CheckCollision(new Vector2(asteroidsArray[j].getX(), asteroidsArray[j].getY()), asteroidsArray[j].Radius(), new Vector2(asteroidsArray[j].getOriginX(), asteroidsArray[j].getOriginY()))) {
//                                asteroidsArray[j]._toScore = true;
////                                asteroidsArray[j].setOutOfBounds(true);
//                                asteroidsArray[j]._exploding = true;
//                                asteroidsArray[j]._doubleScore = true;
//                            }
//                            if (j < hugeAsteroidAsteroids.length && !hugeAsteroidAsteroids[j]._exploding && hugeAsteroidAsteroids[j]._active && explodingAsteroids.get(i).CheckCollision(new Vector2(hugeAsteroidAsteroids[j].getX(), hugeAsteroidAsteroids[j].getY()), hugeAsteroidAsteroids[j].Radius(), new Vector2(hugeAsteroidAsteroids[j].getOriginX(), hugeAsteroidAsteroids[j].getOriginY()))) {
//                                hugeAsteroidAsteroids[j]._toScore = true;
////                                asteroidsArray[j].setOutOfBounds(true);
//                                hugeAsteroidAsteroids[j]._exploding = true;
//                                hugeAsteroidAsteroids[j]._doubleScore = true;
//                            }
//                            if (j < magneticAsteroids.size() && explodingAsteroids.get(i).CheckCollision(new Vector2(magneticAsteroids.get(j).getX(), magneticAsteroids.get(j).getY()), magneticAsteroids.get(j).Radius(), new Vector2(magneticAsteroids.get(j).getOriginX(), magneticAsteroids.get(j).getOriginY())) && !magneticAsteroids.get(j)._toExplode) {
//                                magneticAsteroids.get(j)._toScore = true;
//                                magneticAsteroids.get(j)._toExplode = true;
//                                magneticAsteroids.get(j)._doubleScore = true;
//                            }
//                            if (i != j && j < explodingAsteroids.size() && (!powerup.PowerupActive() || !powerup.explosionSimultaneous || (powerup.PowerupActive() && powerup.PowerUpType() != PowerupType.Explosion)) && explodingAsteroids.get(i).CheckCollision(new Vector2(explodingAsteroids.get(j).getX(), explodingAsteroids.get(j).getY()), explodingAsteroids.get(j).Radius(), new Vector2(explodingAsteroids.get(j).getOriginX(), explodingAsteroids.get(j).getOriginY())) && !explodingAsteroids.get(j)._exploding) {
//                                explodingAsteroids.get(j)._toScore = true;
//                                explodingAsteroids.get(j)._exploding = true;
//                                explodingAsteroids.get(j).SetEarthCollision(true);
//                                explodingAsteroids.get(j)._doubleScore = true;
//                            }
//                            if (j < hugeAsteroids.size() && explodingAsteroids.get(i).CheckCollision(new Vector2(hugeAsteroids.get(j).getX(), hugeAsteroids.get(j).getY()), hugeAsteroids.get(j).Radius(), new Vector2(hugeAsteroids.get(j).getOriginX(), hugeAsteroids.get(j).getOriginY())) && !hugeAsteroids.get(j)._exploding) {
//                                hugeAsteroids.get(j)._toScore = true;
//                                hugeAsteroids.get(j)._exploding = true;
//                                hugeAsteroids.get(j)._doubleScore = true;
//                            }
//                        }
//                    }
//                    if (explodingAsteroids.get(i)._toScore) {
//                        if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.x2Multiplier) {
//                            if (explodingAsteroids.get(i)._doubleScore) {
//                                score += scoreAdd * 4;
//                                scoreValues.add(scoreAdd * 4);
//                            } else {
//                                score += scoreAdd * 2;
//                                scoreValues.add(scoreAdd * 2);
//                            }
//                        } else {
//                            if (explodingAsteroids.get(i)._doubleScore) {
//                                score += scoreAdd * 2;
//                                scoreValues.add(scoreAdd * 2);
//                            } else {
//                                score += scoreAdd;
//                                scoreValues.add(scoreAdd);
//                            }
//                        }
//                        scoreLocations.add(new Vector2(explodingAsteroids.get(i).getX() + explodingAsteroids.get(i).getOriginX(), explodingAsteroids.get(i).getY() + explodingAsteroids.get(i).getOriginY()));
//                        scoreAlphas.add(new Float(1.0));
//                        explodingAsteroids.get(i)._toScore = false;
//                    }
//                    if (explodingAsteroids.get(i).OutOfBounds()) {
//                        explodingAsteroids.remove(i);
//                    }
//                }
//            }

            //endregion


            //region OldHugeAsteroidLogic
//            if (hugeAsteroids.size() > 0) {
//                for (int i = 0; i < hugeAsteroids.size(); i++) {
//                    hugeAsteroids.get(i).Update(camera.zoom, bulletsArray, Earth, powerup, game.prefs.getBoolean("Vibrate"), game.SmallExplosionSound, game.prefs.getBoolean("Sound"), Gdx.graphics.getDeltaTime(), game.prefs.getFloat("SfxVolumeLevel"));
//                    if (hugeAsteroids.get(i)._toScore) {
//                        if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.x2Multiplier) {
//                            if (hugeAsteroids.get(i)._doubleScore) {
//                                score += scoreAdd * 4;
//                                scoreValues.add(scoreAdd * 4);
//                            } else {
//                                score += scoreAdd * 2;
//                                scoreValues.add(scoreAdd * 2);
//                            }
//                        } else {
//                            if (hugeAsteroids.get(i)._doubleScore) {
//                                score += scoreAdd * 2;
//                                scoreValues.add(scoreAdd * 2);
//                            } else {
//                                score += scoreAdd;
//                                scoreValues.add(scoreAdd);
//                            }
//                        }
//                        scoreLocations.add(new Vector2(hugeAsteroids.get(i).getX(), hugeAsteroids.get(i).getY()));
//                        scoreAlphas.add(new Float(1.0));
//                        hugeAsteroids.get(i)._toScore = false;
//
//                        for (int j = 0; j < 4; j++) {
//                            Vector2 EarthLocation = new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY());
//                            if (j != 0) {
//                                EarthLocation.rotate(30 * j);
//                            }
////                            if (game.prefs.getBoolean("PixelSkin")) {
//                            while (hugeAsteroidAsteroids[hugeAsteroidAsteroidsIndex]._active) {
//                                hugeAsteroidAsteroidsIndex++;
//                                if (hugeAsteroidAsteroidsIndex == hugeAsteroidAsteroids.length) {
//                                    hugeAsteroidAsteroidsIndex = 0;
//                                }
//                            }
//                            hugeAsteroidAsteroids[hugeAsteroidAsteroidsIndex].ResetAsteroid(asteroidTexture, EarthLocation);
//                            hugeAsteroidAsteroids[hugeAsteroidAsteroidsIndex]._active = true;
////                                asteroids.add(new Asteroid(asteroidTexture, EarthLocation, planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class)));
////                            }
////                            else {
////                                asteroids.add(new Asteroid(asteroidTexture, EarthLocation, planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class)));
////
////                            }
//                            hugeAsteroidAsteroids[hugeAsteroidAsteroidsIndex].setPosition(hugeAsteroids.get(i).getX() + hugeAsteroids.get(i).getOriginX(), hugeAsteroids.get(i).getY() + hugeAsteroids.get(i).getOriginY());
//                        }
//                    }
//                    if (hugeAsteroids.get(i).OutOfBounds()) {
//                        hugeAsteroids.remove(i);
//                    }
//                }
//            }

            //endregion

            for (int i = 0; i < hugeAsteroidsArray.length; i++) {
                hugeAsteroidsArray[i].Update(camera.zoom, bulletsArray, Earth, powerup, game.prefs.getBoolean("Vibrate"), game.SmallExplosionSound, game.prefs.getBoolean("Sound"), Gdx.graphics.getDeltaTime(), game.prefs.getFloat("SfxVolumeLevel"));
                if (hugeAsteroidsArray[i]._toScore) {
                    if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.x2Multiplier) {
                        if (hugeAsteroidsArray[i]._doubleScore) {
                            score += scoreAdd * 4;
                            scoreValues.add(scoreAdd * 4);
                        } else {
                            score += scoreAdd * 2;
                            scoreValues.add(scoreAdd * 2);
                        }
                    } else {
                        if (hugeAsteroidsArray[i]._doubleScore) {
                            score += scoreAdd * 2;
                            scoreValues.add(scoreAdd * 2);
                        } else {
                            score += scoreAdd;
                            scoreValues.add(scoreAdd);
                        }
                    }
                    scoreLocations.add(new Vector2(hugeAsteroidsArray[i].getX(), hugeAsteroidsArray[i].getY()));
                    scoreAlphas.add(new Float(1.0));
                    hugeAsteroidsArray[i]._toScore = false;

                    for (int j = 0; j < 4; j++) {
                        Vector2 EarthLocation = new Vector2(Earth.getX() + Earth.getOriginX(), Earth.getY() + Earth.getOriginY());
                        if (j != 0) {
                            EarthLocation.rotate(30 * j);
                        }
//                            if (game.prefs.getBoolean("PixelSkin")) {
                        while (hugeAsteroidAsteroids[hugeAsteroidAsteroidsIndex]._active) {
                            hugeAsteroidAsteroidsIndex++;
                            if (hugeAsteroidAsteroidsIndex == hugeAsteroidAsteroids.length) {
                                hugeAsteroidAsteroidsIndex = 0;
                            }
                        }
                        hugeAsteroidAsteroids[hugeAsteroidAsteroidsIndex].ResetAsteroid(asteroidTexture, EarthLocation);
                        hugeAsteroidAsteroids[hugeAsteroidAsteroidsIndex]._active = true;
//                                asteroids.add(new Asteroid(asteroidTexture, EarthLocation, planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class)));
//                            }
//                            else {
//                                asteroids.add(new Asteroid(asteroidTexture, EarthLocation, planetScale, 5 * game.screenScale, camera.zoom, new Vector2(virtualJoystick.getX() + virtualJoystick.getWidth(), virtualJoystick.getY() + virtualJoystick.getHeight()), game.manager.get("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class)));
//
//                            }
                        hugeAsteroidAsteroids[hugeAsteroidAsteroidsIndex].setPosition(hugeAsteroidsArray[i].getX() + hugeAsteroidsArray[i].getOriginX(), hugeAsteroidsArray[i].getY() + hugeAsteroidsArray[i].getOriginY());
                    }
                }
//                if (hugeAsteroids.get(i).OutOfBounds()) {
//                    hugeAsteroids.remove(i);
//                }
            }

//            if(Earth.IsHit() && game.prefs.getBoolean("CatSkin") && game.prefs.getBoolean("Sound"))
//            {
//                game.catMeow.play(.4f * game.prefs.getFloat("SfxVolumeLevel"));
//            }

            if (Earth.GetHealth() <= 0) {
                // game.BigExplosionSound.play(10f);
                gameOver = true;
                gameCoinTotal = score / 500;
                currentProTipIndex = random.nextInt(proTips.length);
                game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
            }

            previousTouchState = currentTouchState;
            previousDistanceBetweenFingers.x = distanceBetweenFingers.x;
            previousDistanceBetweenFingers.y = distanceBetweenFingers.y;


            elapsedTime += Gdx.graphics.getDeltaTime();
            elapsedAsteroidTime += Gdx.graphics.getDeltaTime();
            waveTime += Gdx.graphics.getDeltaTime();
            AchievementsCheck();

        }
        //endregion

        //region Pause
        else if (pause) {
            if (game.gameSong.isPlaying()) {
                game.gameSong.pause();
            }
            continueButton.Update();
            restartButton.Update();
            menuButton.Update();
            optionsButton.Update();
            menuButton.setPosition(Gdx.graphics.getWidth() / 2 - menuButton.getOriginX(), optionsButton.getY() + restartButton.getHeight() * 1.5f);
            restartButton.setPosition(Gdx.graphics.getWidth() / 2 - restartButton.getOriginX(), Gdx.graphics.getHeight() / 2 - restartButton.getOriginY() * 2.5f);
            //|| Gdx.input.isKeyPressed(Input.Keys.ESCAPE) || Gdx.input.isKeyPressed(Input.Keys.MENU) || pauseButton.Tapped()
            if (continueButton.Tapped() || (!currentBackTouched && previousBackTouched)) {
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    game.gameSong.play();
                }
                pause = false;
                game.handler.showAds(false);
            } else if (restartButton.Tapped()) {
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    game.gameSong.stop();
                }
                game.setScreen(new GameScreen(game, _calibrationX));
                this.dispose();
            } else if (menuButton.Tapped()) {
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    game.gameSong.stop();
                }
                if (game.prefs.getBoolean("PixelSkin")) {
                    game.manager.unload("data/ExplosionSpritesMosaic-packed/pack.atlas");
                    game.manager.unload("data/CosmicExplosion15.png");
                } else {
                    game.manager.unload("data/ExplosionSprites-packed/pack.atlas");
                }
                game.manager.unload("data/RealEarthLaserAnimationThick-packed/pack.atlas");
                game.manager.unload("data/SpeedPowerup-packed/pack.atlas");
                game.manager.unload("data/GameInventory.png");
                game.setScreen(new MenuScreen(game));
                this.dispose();
            } else if (optionsButton.Tapped()) {
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    game.gameSong.stop();
                }
                game.manager.load("data/VolumeSliderBack.png", Texture.class);
                game.manager.load("data/VolumeSliderFill.png", Texture.class);
                game.manager.load("data/VolumeRockerControl.png", Texture.class);
                game.manager.finishLoading();
                game.setScreen(new OptionsScreen(game, this));
            }

        }
        //endregion


        //region GameOver
        else {
            if (!inventoryActive) {
                if (game.gameSong.isPlaying()) {
                    game.gameSong.pause();
                }

                menuButton.setPosition(menuButton.getOriginX() / 2, Gdx.graphics.getHeight() / 2 - menuButton.getOriginY());
                restartButton.setPosition(Gdx.graphics.getWidth() - restartButton.getWidth() * 1.25f, Gdx.graphics.getHeight() / 2 - restartButton.getOriginY());
                camera.update();
                restartButton.Update();
                menuButton.Update();
                powerupShopButton.Update();

                if (game.prefs.getInteger("Coins") != previousCoinTotal + gameCoinTotal && previousCoinAddition == previousCoinTotal) {
                    game.prefs.putInteger("Coins", previousCoinTotal + gameCoinTotal);
                    game.prefs.flush();
                }

                if (!powerupShopTrip) {
                    if (previousCoinAddition < previousCoinTotal + gameCoinTotal) {
                        cointAdditionRate++;
                        if (cointAdditionRate % 2 == 0) {
                            previousCoinAddition++;
                        }
                    }
                } else {
                    previousCoinAddition = game.prefs.getInteger("Coins");
                }

                SetHighScore();


                if (Gdx.input.isKeyPressed(Input.Keys.MENU) || Gdx.input.isKeyPressed(Input.Keys.ESCAPE) || menuButton.Tapped() || (!currentBackTouched && previousBackTouched)) {
//                camera.zoom = 1f;
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                        game.gameSong.stop();
                    }

                    if (game.prefs.getBoolean("Easy")) {
                        if (game.actionResolver.getSignedInGPGS()) {
                            game.actionResolver.submitScoreGPGS(game.prefs.getString("EasyLeaderboard"), score);
                        }
                    }
                    if (game.prefs.getBoolean("Normal")) {
                        if (game.actionResolver.getSignedInGPGS()) {
                            game.actionResolver.submitScoreGPGS(game.prefs.getString("NormalLeaderboard"), score);
                        }
                    }
                    if (game.prefs.getBoolean("Hard")) {
                        if (game.actionResolver.getSignedInGPGS()) {
                            game.actionResolver.submitScoreGPGS(game.prefs.getString("HardLeaderboard"), score);
                        }
                    }
                    if (game.prefs.getBoolean("PixelSkin")) {
                        game.manager.unload("data/ExplosionSpritesMosaic-packed/pack.atlas");
                        game.manager.unload("data/CosmicExplosion15.png");
                    } else {
                        game.manager.unload("data/ExplosionSprites-packed/pack.atlas");
                    }
                    game.manager.unload("data/RealEarthLaserAnimationThick-packed/pack.atlas");
                    game.manager.unload("data/SpeedPowerup-packed/pack.atlas");
                    game.manager.unload("data/GameInventory.png");
                    game.setScreen(new MenuScreen(game));
                    this.dispose();
                }
                if (restartButton.Tapped()) {
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                        game.gameSong.stop();
                    }
                    if (game.prefs.getBoolean("Easy")) {
                        if (game.actionResolver.getSignedInGPGS()) {
                            game.actionResolver.submitScoreGPGS(game.prefs.getString("EasyLeaderboard"), score);
                        }
                    }
                    if (game.prefs.getBoolean("Normal")) {
                        if (game.actionResolver.getSignedInGPGS()) {
                            game.actionResolver.submitScoreGPGS(game.prefs.getString("NormalLeaderboard"), score);
                        }
                    }
                    if (game.prefs.getBoolean("Hard")) {
                        if (game.actionResolver.getSignedInGPGS()) {
                            game.actionResolver.submitScoreGPGS(game.prefs.getString("HardLeaderboard"), score);
                        }
                    }
                    game.setScreen(new GameScreen(game, _calibrationX));
                    this.dispose();
                }
                if (powerupShopButton.Tapped()) {
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                    powerupShopTrip = true;
                    game.setScreen(new PowerupShop(game, this));
                }
            }
        }
        previousBackTouched = currentBackTouched;
        //endregion
        //draw

        //region GameRunningDraw
        game.batch.getProjectionMatrix().set(camera.combined);
        game.batch.begin();
        for (Sprite background : backgroundSprite) {
            background.draw(game.batch);
        }
        for (Asteroid asteroid : asteroidsArray) {
            if (asteroid._active) {
                asteroid.draw(game.batch);
            }
        }
        for (Asteroid asteroid : hugeAsteroidAsteroids) {
            if (asteroid._active) {
                asteroid.draw(game.batch);
            }
        }

        if (magneticAsteroids.size() > 0) {
            for (MagneticAsteroid magneticAsteroid : magneticAsteroids) {
                magneticAsteroid.Draw(game.batch);
            }
        }

        for(ExplodingAsteroid explodingAsteroid : explodingAsteroidsArray) {
            if(explodingAsteroid._active) {
                explodingAsteroid.Draw(game.batch);
            }
        }
//        if (explodingAsteroids.size() > 0) {
//            for (ExplodingAsteroid explodingAsteroid : explodingAsteroids) {
//                explodingAsteroid.Draw(game.batch);
//            }
//        }

        for(HugeAsteroid hugeAsteroid : hugeAsteroidsArray) {
            if(hugeAsteroid._active) {
                hugeAsteroid.draw(game.batch);
            }
        }

//        if (hugeAsteroids.size() > 0) {
//            for (HugeAsteroid hugeAsteroid : hugeAsteroids) {
//                hugeAsteroid.draw(game.batch);
//            }
//        }

        if (drawLaser) {

            laser.setRegion(currentFrame);
            laser.draw(game.batch);
        }

        if (powerup.PowerUpType() == PowerupType.DualShot && asteroidPlanet != null && !asteroidPlanet.Exploded()) {
            asteroidPlanet.draw(game.batch);
        }


        Earth.draw(game.batch);
        continentsSprite.draw(game.batch);
        if (!game.prefs.getBoolean("PixelSkin")) {
            grayOverlay.draw(game.batch);
        }
        Moon.draw(game.batch);
        if (!game.prefs.getBoolean("PixelSkin")) {
            moonBlotsSprite.draw(game.batch);
            grayOverlayMoon.draw(game.batch);
        }
        if (game.prefs.getBoolean("CatSkin")) {
            catSkin.setRotation(Earth.getRotation());
            catSkin.setPosition(Earth.getX() - 22f * game.screenScale, Earth.getY());
            catSkin.draw(game.batch);
        } else if (game.prefs.getBoolean("NinjaSkin")) {
            ninjaSkin.setRotation(Earth.getRotation());
            ninjaSkin.setPosition(Earth.getX(), Earth.getY());
            ninjaSkin.draw(game.batch);

            ninjaSkinMoon.setRotation(Moon.getRotation() + 90);
            ninjaSkinMoon.setPosition(Moon.getX() - ((ninjaSkinMoon.getWidth() - Moon.getWidth()) / 2), Moon.getY() + Moon.getHeight() * 5f / 10f);
            ninjaSkinMoon.draw(game.batch);
        } else if (game.prefs.getBoolean("PirateSkin")) {
            pirateSkinEarth.setRotation(Earth.getRotation());
            pirateSkinEarth.setPosition(Earth.getX() - 33f * game.screenScale, Earth.getY() - 1f * game.screenScale);
            pirateSkinEarth.draw(game.batch);

            pirateSkinMoon.setRotation(Moon.getRotation() + 90);
            pirateSkinMoon.setPosition(Moon.getX(), Moon.getY());
            pirateSkinMoon.draw(game.batch);
        } else if (game.prefs.getBoolean("SoccerSkin")) {
            soccerSkinEarth.setRotation(Earth.getRotation());
            soccerSkinEarth.setPosition(Earth.getX() - 25f * game.screenScale, Earth.getY() - 1f * game.screenScale);
            soccerSkinEarth.draw(game.batch);

            soccerSkinMoon.setRotation(Moon.getRotation() + 90);
            soccerSkinMoon.setPosition(Moon.getX() - 7f * game.screenScale, Moon.getY());
            soccerSkinMoon.draw(game.batch);
        } else if (game.prefs.getBoolean("SumoSkin")) {
            sumoSkinEarth.setRotation(Earth.getRotation());
            sumoSkinEarth.setPosition(Earth.getX() - 2f * game.screenScale, Earth.getY());
            sumoSkinEarth.draw(game.batch);

            sumoSkinMoon.setRotation(Moon.getRotation() + 90);
            sumoSkinMoon.setPosition(Moon.getX(), Moon.getY());
            sumoSkinMoon.draw(game.batch);
        }

        if (Earth.IsHit()) {
            Earth.PrepareHitSprite();
            Earth.HitSprite().draw(game.batch);
            Moon.PrepareHitSprite();
            Moon.HitSprite().draw(game.batch);
        }

//        if (bullets.size() > 0) {
//            for (LaserBullets bullet : bullets) {
//                bullet.draw(game.batch);
//            }
//        }

        for(LaserBullets bullet : bulletsArray) {
            if(bullet._active) {
                bullet.draw(game.batch);
            }
        }

        if ((powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.Explosion) || powerup.explosionSimultaneous) {
            powerup.ExplosionSprite().draw(game.batch);
        }

        if (powerup.PowerupOnScreen()) {
            if (powerup.PowerUpType() == PowerupType.Explosion && (game.prefs.getBoolean("NinjaSkin"))) {
                powerup.CurrentPowerup().setColor(Color.DARK_GRAY);
            } else {
                powerup.CurrentPowerup().setColor(Color.WHITE);
            }
            powerup.CurrentPowerup().draw(game.batch);
        }
        if (!game.prefs.getBoolean("PC")) {
            virtualJoystick.draw(game.batch);
            if (virtualJoystick.GetDrawHover()) {
                virtualJoystick._fingerHover.draw(game.batch);
            }
        }

        if (game.prefs.getBoolean("AccelControls")) {
            shootButton.draw(game.batch);
        } else {
            if (!game.prefs.getBoolean("PC")) {
                shootJoystick.draw(game.batch);
                if (shootJoystick.FingerDown()) {
                    shootJoystick._fingerHover.draw(game.batch);
                    if (!gameOver && shootJoystick.DistanceBetweenCenterAndFinger().len() > shootJoystick.getHeight() / 10f) {
                        Earth.EarthRotationJoystick(shootJoystick.DistanceBetweenCenterAndFingerAngle(), false);
                    }
                }
            }
        }

        for (Sprite life : health.Lives()) {
            life.draw(game.batch);
            continentsLives.setPosition(life.getX(), life.getY());
            continentsLives.draw(game.batch);
            if (game.prefs.getBoolean("NinjaSkin")) {
                ninjaSkinLives.setPosition(life.getX(), life.getY());
                ninjaSkinLives.draw(game.batch);
            }
            if (game.prefs.getBoolean("CatSkin")) {
                catSkinLives.setPosition(life.getX(), life.getY() + 10 * game.screenScale);
                catSkinLives.draw(game.batch);
            }
            if (game.prefs.getBoolean("PirateSkin")) {
                pirateSkinHealth.setPosition(life.getX() - 15f * game.screenScale, life.getY() - 1f * game.screenScale);
                pirateSkinHealth.draw(game.batch);
            }
            if (game.prefs.getBoolean("SoccerSkin")) {
                soccerSkinLife.setPosition(life.getX() - 11f * game.screenScale, life.getY());
                soccerSkinLife.draw(game.batch);
            }
            if (game.prefs.getBoolean("SumoSkin")) {
                sumoSkinLife.setPosition(life.getX(), life.getY());
                sumoSkinLife.draw(game.batch);
            }
        }

        if (!gameOver) {
            goodTimeFont.setScale(.22f * game.screenScale);
            if (game.prefs.getBoolean("NinjaSkin") || game.prefs.getBoolean("PirateSkin")) {
                goodTimeFont.setColor(Color.BLACK);
            } else {
                goodTimeFont.setColor(Color.WHITE);
            }
            if (showScoreChange) {
//                goodTimeFont.setColor(Color.WHITE);
                goodTimeFont.draw(game.batch, String.format("Hits now worth %d points!", scoreAdd), Gdx.graphics.getWidth() / 2 - 310 * game.screenScale, Gdx.graphics.getHeight() - 105 * game.screenScale);
            }
            if (powerup.PowerupActive()) {
                powerup.DrawText(goodTimeFont, game.batch, game.screenScale, game.prefs.getBoolean("PirateSkin"), inventoryActive);
            }
        }
//        inventoryButton.draw(game.batch);
        goodTimeFont.setScale(.15f * game.screenScale);
        if (game.prefs.getBoolean("NinjaSkin") || game.prefs.getBoolean("PirateSkin")) {
            goodTimeFont.setColor(Color.BLACK);
        } else {
            goodTimeFont.setColor(Color.WHITE);
        }

        if (leftSlideIndicators.getColor().a >= .9f || leftSlideIndicators.getColor().a <= 0f) {
            slideIndicatorAlphaSpeed *= -1f;
        }

        if (leftSlideIndicators.getColor().a != 0f) {
            leftSlideIndicators.setAlpha(leftSlideIndicators.getColor().a + slideIndicatorAlphaSpeed);
            rightSlideIndicators.setAlpha(rightSlideIndicators.getColor().a + slideIndicatorAlphaSpeed);
        } else if ((int) elapsedTime % 20 == 0) {
            leftSlideIndicators.setAlpha(.1f);
            rightSlideIndicators.setAlpha(.1f);
            slideIndicatorAlphaSpeed = Math.abs(slideIndicatorAlphaSpeed);
        }
        if (game.prefs.getBoolean("PirateSkin")) {
            leftSlideIndicators.setColor(1, 1, 1, leftSlideIndicators.getColor().a);
            rightSlideIndicators.setColor(1, 1, 1, leftSlideIndicators.getColor().a);
        }

        if (!inventoryActive && leftSlideIndicators.getRotation() != 0) {
            leftSlideIndicators.setRotation(0);
            rightSlideIndicators.setRotation(0);
        } else if (inventoryActive && leftSlideIndicators.getRotation() != 180) {
            leftSlideIndicators.setRotation(180);
            rightSlideIndicators.setRotation(180);
        }
        leftSlideIndicators.setPosition(inventoryContainer.getX() + 25f * game.screenScale, inventoryContainer.getY() + (inventoryContainer.getHeight() - inventoryButton.getHeight() * .7f) + (inventoryButton.getHeight() - leftSlideIndicators.getHeight()) / 2);
        rightSlideIndicators.setPosition(inventoryContainer.getX() + (inventoryContainer.getWidth() / 2) + 150f * game.screenScale, inventoryContainer.getY() + (inventoryContainer.getHeight() - inventoryButton.getHeight() * .7f) + (inventoryButton.getHeight() - leftSlideIndicators.getHeight()) / 2);
        leftSlideIndicators.draw(game.batch);
        rightSlideIndicators.draw(game.batch);
        inventoryContainer.draw(game.batch);
        goodTimeFont.draw(game.batch, "Inv", inventoryButton.getX() + 95f * game.screenScale, inventoryButton.getY() + goodTimeFont.getLineHeight() - 2.5f * game.screenScale);

        for (int i = 0; i < inventoryPowerups.length; i++) {
            if (inventoryActive) {
                float alpha = inventoryPowerups[i].getColor().a;
                if (alpha != .99607843f) {
                    goodTimeFont.setColor(1, 1, 1, .25f);
                } else {
                    goodTimeFont.setColor(1, 1, 1, 1f);
                }
                inventoryPowerups[i].draw(game.batch);
                goodTimeFont.setScale(.15f * game.screenScale);
                goodTimeFont.draw(game.batch, String.format("x%d", powerupTotals[i]), inventoryPowerups[i].getX(), inventoryPowerups[i].getY() + inventoryPowerups[i].getHeight() + goodTimeFont.getLineHeight() - 5f * game.screenScale);
            }
            if (inventoryBools[i] && i < 3) {
                Vector2 priorLocation = new Vector2(inventoryPowerups[i].getX(), inventoryPowerups[i].getY());
                float priorAlpha = inventoryPowerups[i].getColor().a;
                inventoryPowerups[i].setPosition(pauseButton.getX() + (pauseButton.getWidth() - inventoryPowerups[i].getWidth()) / 2, pauseButton.getY() - pauseButton.getHeight() - 10f * game.screenScale);
                inventoryPowerups[i].setAlpha(1f);
                inventoryPowerups[i].draw(game.batch);
                if (game.prefs.getBoolean("NinjaSkin")) {
                    goodTimeFont.setColor(Color.BLACK);
                } else {
                    goodTimeFont.setColor(Color.WHITE);
                }
                goodTimeFont.draw(game.batch, "ONLY", cancelPowerupButton.getX() + 7.5f * game.screenScale, inventoryPowerups[i].getY() + +inventoryPowerups[i].getHeight() + goodTimeFont.getLineHeight() - 5f * game.screenScale);
                inventoryPowerups[i].setPosition(priorLocation.x, priorLocation.y);
                inventoryPowerups[i].setAlpha(priorAlpha);
                cancelPowerupButton.draw(game.batch);
//                goodTimeFont.setScale(.15f * game.screenScale);

//                goodTimeFont.setScale(.1f * game.screenScale);
//                goodTimeFont.draw(game.batch, "Disable", cancelPowerupButton.getX() + 5f * game.screenScale, cancelPowerupButton.getY() + goodTimeFont.getLineHeight() + 5f * game.screenScale);
                goodTimeFont.setScale(.2f * game.screenScale);
                goodTimeFont.draw(game.batch, "Off", cancelPowerupButton.getX() + 8.5f * game.screenScale, cancelPowerupButton.getY() + goodTimeFont.getLineHeight() - 7.5f * game.screenScale);
            }
        }
        goodTimeFont.setScale(.15f * game.screenScale);
        goodTimeFont.setColor(Color.WHITE);
        pauseButton.draw(game.batch);
        DrawTime();


//        whitePixel.draw(game.batch);
//endregion

        //region PauseDraw
        if (pause) {
            pauseButton.draw(game.batch);
            game.blackSprite.setSize(Gdx.graphics.getWidth() * camera.zoom, Gdx.graphics.getHeight() * camera.zoom);
            game.blackSprite.setPosition((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * camera.zoom) / 2, (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * camera.zoom) / 2);
            game.blackSprite.setAlpha(.5f);
            game.blackSprite.draw(game.batch);
            continueButton.draw(game.batch);
            restartButton.draw(game.batch);
            menuButton.draw(game.batch);
            optionsButton.draw(game.batch);
        }
//endregion

        //region GameOverDraw
        if (gameOver) {
            game.blackSprite.setSize(Gdx.graphics.getWidth() * camera.zoom, Gdx.graphics.getHeight() * camera.zoom);
            game.blackSprite.setPosition((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * camera.zoom) / 2, (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * camera.zoom) / 2);
            game.blackSprite.setAlpha(.5f);
            game.blackSprite.draw(game.batch);

            DrawEndScore();

            restartButton.draw(game.batch);
            menuButton.draw(game.batch);
            coin.draw(game.batch);
            powerupShopButton.draw(game.batch);
            DrawTime();
        }
        //endregion

        //Cannot set two vectors equal to each other in libGDX. its a reference pointer
        goodTimeFont.setScale(.22f * game.screenScale);
        if (tutorial) {
            tutorialPlayButton.draw(game.batch);
        }
        TextDrawing();
        for (int i = 0; i < scoreLocations.size(); i++) {
            if (game.prefs.getBoolean("NinjaSkin")) {
                goodTimeFont.setColor(0, 0, 0, scoreAlphas.get(i));
            } else if (game.prefs.getBoolean("PirateSkin")) {
                goodTimeFont.setColor(1, 1, 0, scoreAlphas.get(i));
            } else {
                goodTimeFont.setColor(1, .85f, 0, scoreAlphas.get(i));
            }
            goodTimeFont.draw(game.batch, String.format("+%d", scoreValues.get(i)), scoreLocations.get(i).x, scoreLocations.get(i).y);
            scoreAlphas.set(i, scoreAlphas.get(i) - .03f);
            if (scoreAlphas.get(i) <= 0) {
                scoreAlphas.remove(i);
                scoreLocations.remove(i);
                scoreValues.remove(i);
            }
        }
        if (!gameOver) {
            if (!game.prefs.getBoolean("PirateSkin")) {
                goodTimeFont.setColor(1, .8f, 0, 1);
            } else {
                goodTimeFont.setColor(Color.YELLOW);
            }
            String scoreString = "Score: " + score;
            goodTimeFont.draw(game.batch, scoreString, Gdx.graphics.getWidth() / 2 - scoreString.length() * 12.1f * game.screenScale, Gdx.graphics.getHeight() - 20 * game.screenScale);
        } else {
            goodTimeFont.setScale(.15f * game.screenScale);
            goodTimeFont.setColor(1, .8f, 0, 1);
            goodTimeFont.setColor(Color.RED);
            String proTip = proTips[currentProTipIndex];
            goodTimeFont.draw(game.batch, "Pro Tip:", Gdx.graphics.getWidth() / 2 - 57 * game.screenScale, Gdx.graphics.getHeight() - 150 * game.screenScale);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, proTip, Gdx.graphics.getWidth() / 2 - proTip.length() * 8.17f * game.screenScale, Gdx.graphics.getHeight() - 180 * game.screenScale);
        }
        game.batch.end();
    }

    public boolean waitingForRelease;

    public void InventorySlideTracker() {

        if (!inventoryActive) {
            if (!waitingForRelease) {
                if (Gdx.input.justTouched()) {
                    int[] fingersDown = GestureManager.CurrentFingersDown();
                    for (int i = 0, j = 0; i < fingersDown.length; i++) {
                        while (fingersDown[i] != j) {
                            j++;
                        }
                        if (GestureManager.IsCurrentlyBeingTouchedIndex(inventorySlideBox, 1f, fingersDown[i])) {
                            initialSlideLocation = GestureManager.InputLocation(fingersDown[i]);
                            indexToTrack = fingersDown[i];
                            waitingForRelease = true;
                            break;
                        }
                    }
                }
            } else {
                if (Gdx.input.isTouched(indexToTrack)) {
                    postSlideLocation = GestureManager.InputLocation(indexToTrack);
                } else if (!Gdx.input.isTouched(indexToTrack)) {
                    if (postSlideLocation.y > initialSlideLocation.y + 100 * game.screenScale) {
                        inventoryActive = true;
                        inventoryContainer.setPosition(Gdx.graphics.getWidth() / 2 - inventoryContainer.getOriginX(), 0);
                        inventoryButton.setPosition(Gdx.graphics.getWidth() / 2 - inventoryButton.getOriginX(), inventoryContainer.getHeight() - inventoryContainer.getHeight() * .22f);
                    }
                    waitingForRelease = false;
                }
            }
        } else {
            if (!waitingForRelease) {
                if (Gdx.input.justTouched()) {
                    int[] fingersDown = GestureManager.CurrentFingersDown();
                    for (int i = 0, j = 0; i < fingersDown.length; i++) {
                        while (fingersDown[i] != j) {
                            j++;
                        }
                        Vector2 touchDownPosition = GestureManager.InputLocation(fingersDown[i]);
                        if (touchDownPosition.x > inventoryContainer.getX() && touchDownPosition.x < inventoryContainer.getX() + inventoryContainer.getWidth() && touchDownPosition.y <= Gdx.graphics.getHeight() * .75f) {
                            initialSlideLocation = touchDownPosition;
                            indexToTrack = fingersDown[i];
                            waitingForRelease = true;
                            break;
                        }
                    }
                }
            } else {
                if (Gdx.input.isTouched(indexToTrack)) {
                    postSlideLocation = GestureManager.InputLocation(indexToTrack);
                } else if (!Gdx.input.isTouched(indexToTrack)) {
                    if (postSlideLocation.y < initialSlideLocation.y - 100 * game.screenScale) {
                        inventoryActive = false;
                        inventoryContainer.setPosition(Gdx.graphics.getWidth() / 2 - inventoryContainer.getOriginX(), -inventoryContainer.getHeight() * .78f);
                        inventoryButton.setPosition(Gdx.graphics.getWidth() / 2 - inventoryButton.getOriginX(), 0);
//                        if (game.prefs.getBoolean("Sound")) {
//                            game.gameSong.play();
//                            game.gameSong.setVolume(.3f * game.prefs.getFloat("MusicVolumeLevel"));
//                            game.gameSong.setLooping(true);
//                        }
                    }
                    waitingForRelease = false;
                }
            }
        }
    }

    public void DrawEndScore() {
        goodTimeFont.setScale(.22f * game.screenScale);
        goodTimeFont.setColor(1, .85f, 0, 1);
        goodTimeFont.draw(game.batch, String.format("Game Over"), Gdx.graphics.getWidth() / 2 - 125 * game.screenScale, Gdx.graphics.getHeight() / 2 + goodTimeFont.getLineHeight() + 60 * game.screenScale);
        goodTimeFont.setColor(Color.WHITE);
        String highScoreText = "Final Score: " + score;
        goodTimeFont.draw(game.batch, highScoreText, Gdx.graphics.getWidth() / 2 - highScoreText.length() * 11.25f * game.screenScale, Gdx.graphics.getHeight() / 2 + goodTimeFont.getLineHeight() + 10 * game.screenScale);
        if (game.prefs.getBoolean("Easy")) {
            highScoreText = "Previous High Score: " + game.prefs.getInteger("EasyHighScore");
            goodTimeFont.draw(game.batch, highScoreText, Gdx.graphics.getWidth() / 2 - highScoreText.length() * 12f * game.screenScale, Gdx.graphics.getHeight() / 2 + goodTimeFont.getLineHeight() - 30 * game.screenScale);
            if (game.prefs.getInteger("EasyHighScore") < score) {
                highScoreText = "New High Score!!!";
                goodTimeFont.draw(game.batch, highScoreText, Gdx.graphics.getWidth() / 2 - highScoreText.length() * 11.25f * game.screenScale, Gdx.graphics.getHeight() / 2 + goodTimeFont.getLineHeight() - 70 * game.screenScale);
            }
        } else if (game.prefs.getBoolean("Normal")) {
            highScoreText = "Previous High Score: " + game.prefs.getInteger("NormalHighScore");
            goodTimeFont.draw(game.batch, highScoreText, Gdx.graphics.getWidth() / 2 - highScoreText.length() * 12f * game.screenScale, Gdx.graphics.getHeight() / 2 + goodTimeFont.getLineHeight() - 30 * game.screenScale);

            if (game.prefs.getInteger("NormalHighScore") < score) {
                highScoreText = "New High Score!!!";
                goodTimeFont.draw(game.batch, highScoreText, Gdx.graphics.getWidth() / 2 - highScoreText.length() * 11.25f * game.screenScale, Gdx.graphics.getHeight() / 2 + goodTimeFont.getLineHeight() - 70 * game.screenScale);
            }
        } else if (game.prefs.getBoolean("Hard")) {
            highScoreText = "Previous High Score: " + game.prefs.getInteger("HardHighScore");
            goodTimeFont.draw(game.batch, highScoreText, Gdx.graphics.getWidth() / 2 - highScoreText.length() * 12f * game.screenScale, Gdx.graphics.getHeight() / 2 + goodTimeFont.getLineHeight() - 30 * game.screenScale);

            if (game.prefs.getInteger("HardHighScore") < score) {
                highScoreText = "New High Score!!!";
                goodTimeFont.draw(game.batch, highScoreText, Gdx.graphics.getWidth() / 2 - highScoreText.length() * 11.25f * game.screenScale, Gdx.graphics.getHeight() / 2 + goodTimeFont.getLineHeight() - 70 * game.screenScale);
            }
        }
    }

    public void DrawTime() {
        goodTimeFont.setScale(.22f * game.screenScale);
        if (previousElapsedTime != (int) elapsedTime) {
            seconds++;
            previousElapsedTime = (int) elapsedTime;
        }
        if (seconds >= 60) {
            minutes++;
            seconds -= 60;
        }
        if (game.prefs.getBoolean("NinjaSkin")) {
            goodTimeFont.setColor(Color.BLACK);
        } else {
            goodTimeFont.setColor(Color.WHITE);
        }
        if (!gameOver) {
            if (!inventoryActive) {
                if (seconds < 10) {
                    goodTimeFont.draw(game.batch, String.format("Time:  %d:0%d", minutes, seconds), Gdx.graphics.getWidth() / 2 - 105f * game.screenScale, inventoryContainer.getHeight() * .22f + 40 * game.screenScale);
                } else {
                    goodTimeFont.draw(game.batch, String.format("Time:  %d:%d", minutes, seconds), Gdx.graphics.getWidth() / 2 - 105f * game.screenScale, inventoryContainer.getHeight() * .22f + 40 * game.screenScale);
                }
            } else {
                if (seconds < 10) {
                    goodTimeFont.draw(game.batch, String.format("Time:  %d:0%d", minutes, seconds), Gdx.graphics.getWidth() / 2 - 105f * game.screenScale, inventoryContainer.getHeight() + 40 * game.screenScale);
                } else {
                    goodTimeFont.draw(game.batch, String.format("Time:  %d:%d", minutes, seconds), Gdx.graphics.getWidth() / 2 - 105f * game.screenScale, inventoryContainer.getHeight() + 40 * game.screenScale);
                }
            }
        } else {
            if (seconds < 10) {
                goodTimeFont.draw(game.batch, String.format("Time:  %d:0%d", minutes, seconds), Gdx.graphics.getWidth() / 2 - 105f * game.screenScale, inventoryContainer.getHeight() + 85f * game.screenScale);
            } else {
                goodTimeFont.draw(game.batch, String.format("Time:  %d:%d", minutes, seconds), Gdx.graphics.getWidth() / 2 - 105f * game.screenScale, inventoryContainer.getHeight() + 85f * game.screenScale);
            }
        }
    }

    public void SetHighScore() {
        if (game.prefs.getBoolean("Easy")) {
            if (game.prefs.contains("EasyHighScore")) {
                if (score > game.prefs.getInteger("EasyHighScore")) {
                    game.prefs.putInteger("EasyHighScore", score);
                    game.prefs.flush();
                }
            } else {
                game.prefs.putInteger("EasyHighScore", score);
                game.prefs.flush();
            }
        } else if (game.prefs.getBoolean("Normal")) {
            if (game.prefs.contains("NormalHighScore")) {
                if (score > game.prefs.getInteger("NormalHighScore")) {
                    game.prefs.putInteger("NormalHighScore", score);
                    game.prefs.flush();
                }
            } else {
                game.prefs.putInteger("NormalHighScore", score);
                game.prefs.flush();
            }
        } else if (game.prefs.getBoolean("Hard")) {
            if (game.prefs.contains("HardHighScore")) {
                if (score > game.prefs.getInteger("HardHighScore")) {
                    game.prefs.putInteger("HardHighScore", score);
                    game.prefs.flush();
                }
            } else {
                game.prefs.putInteger("HardHighScore", score);
                game.prefs.flush();
            }
        }
    }

    public void AchievementsCheck() {
        //Easy Achievements
        if (game.prefs.getBoolean("Easy") && score >= 100000 && !game.prefs.getBoolean("OYWUnlocked")) {
            game.actionResolver.unlockAchievementGPGS(game.prefs.getString("OnYourWay"));
            game.prefs.putBoolean("OYWUnlocked", true);
            game.prefs.flush();
        }
        if (game.prefs.getBoolean("Easy") && score >= 200000 && !game.prefs.getBoolean("SUTTBLUnlocked")) {
            game.actionResolver.unlockAchievementGPGS(game.prefs.getString("StepUpToTheBigLeagues"));
            game.prefs.putBoolean("SUTTBLUnlocked", true);
            game.prefs.flush();
        }
        if (game.prefs.getBoolean("Easy") && minutes >= 3 && !firstShot && !game.prefs.getBoolean("EWUnlocked")) {
            game.actionResolver.unlockAchievementGPGS(game.prefs.getString("EasyWeave"));
            game.prefs.putBoolean("EWUnlocked", true);
            game.prefs.flush();
        }
        //NormalAchievements
        if (game.prefs.getBoolean("Normal") && score >= 100000 && !game.prefs.getBoolean("TMBHYUnlocked")) {
            game.actionResolver.unlockAchievementGPGS(game.prefs.getString("ThereMayBeHopeYet"));
            game.prefs.putBoolean("TMBHYUnlocked", true);
            game.prefs.flush();
        }
        if (game.prefs.getBoolean("Normal") && score >= 200000 && !game.prefs.getBoolean("AAHUnlocked")) {
            game.actionResolver.unlockAchievementGPGS(game.prefs.getString("AnAbleHero"));
            game.prefs.putBoolean("AAHUnlocked", true);
            game.prefs.flush();
        }
        if (game.prefs.getBoolean("Normal") && minutes >= 3 && !firstShot && !game.prefs.getBoolean("NWUnlocked")) {
            game.actionResolver.unlockAchievementGPGS(game.prefs.getString("NormalWeave"));
            game.prefs.putBoolean("NWUnlocked", true);
            game.prefs.flush();
        }
        //Hard Achievements
        if (game.prefs.getBoolean("Hard") && score >= 100000 && !game.prefs.getBoolean("MUnlocked")) {
            game.actionResolver.unlockAchievementGPGS(game.prefs.getString("Masterful"));
            game.prefs.putBoolean("MUnlocked", true);
            game.prefs.flush();
        }
        if (game.prefs.getBoolean("Hard") && score >= 200000 && !game.prefs.getBoolean("AYGUnlocked")) {
            game.actionResolver.unlockAchievementGPGS(game.prefs.getString("AreYouAGod"));
            game.prefs.putBoolean("AYGUnlocked", true);
            game.prefs.flush();
        }
        if (game.prefs.getBoolean("Hard") && minutes >= 2 && !firstShot && !game.prefs.getBoolean("HWUnlocked")) {
            game.actionResolver.unlockAchievementGPGS(game.prefs.getString("HardWeave"));
            game.prefs.putBoolean("HWUnlocked", true);
            game.prefs.flush();
        }
        if (game.prefs.getBoolean("Hard") && seconds >= 30 && !firstMove && !game.prefs.getBoolean("SPUnlocked")) {
            game.actionResolver.unlockAchievementGPGS(game.prefs.getString("StationaryPlanet"));
            game.prefs.putBoolean("SPUnlocked", true);
            game.prefs.flush();
        }
    }

    private void TextDrawing() {
        if (pause || gameOver) {
            goodTimeFont.setScale(game.screenScale * .55f);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, "Menu", menuButton.getX() + 25 * game.screenScale, menuButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
            goodTimeFont.setScale(game.screenScale * .36f);
            goodTimeFont.draw(game.batch, "Restart", restartButton.getX() + 15 * game.screenScale, restartButton.getY() + goodTimeFont.getLineHeight() + 18 * game.screenScale);
            if (pause) {
                goodTimeFont.setScale(game.screenScale * .39f);
                goodTimeFont.draw(game.batch, "Options", optionsButton.getX() + 12.5f * game.screenScale, optionsButton.getY() + goodTimeFont.getLineHeight() + 17 * game.screenScale);
                goodTimeFont.setScale(game.screenScale * .34f);
                goodTimeFont.draw(game.batch, "Continue", continueButton.getX() + 16f * game.screenScale, continueButton.getY() + goodTimeFont.getLineHeight() + 18 * game.screenScale);
            }
            if (gameOver) {
                goodTimeFont.setScale(game.screenScale * .55f);
                goodTimeFont.draw(game.batch, "Shop", powerupShopButton.getX() + 30f * game.screenScale, powerupShopButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
                goodTimeFont.setScale(game.screenScale * .3f);
                String gameCointTotalS = "+ " + gameCoinTotal;
                goodTimeFont.draw(game.batch, gameCointTotalS, Gdx.graphics.getWidth() / 2 - gameCointTotalS.length() * 10f, coin.getY() - 10f * game.screenScale);
                String coinTotal = "Coin Total: " + previousCoinAddition;
                goodTimeFont.setScale(game.screenScale * .22f);
                goodTimeFont.draw(game.batch, String.format(coinTotal), Gdx.graphics.getWidth() / 2 - coinTotal.length() * 11.25f * game.screenScale, coin.getY() - 60f * game.screenScale);
            }
        }

        if (tutorial) {
            goodTimeFont.setScale(.2f * game.screenScale);
            goodTimeFont.setColor(Color.WHITE);
            if (tutorialClicks == 0) {
                goodTimeFont.draw(game.batch, "Use the left joystick to move the Earth around.", virtualJoystick.getX() + virtualJoystick.getWidth() + 70f * game.screenScale, virtualJoystick.getY() + virtualJoystick.getHeight() * .75f + goodTimeFont.getLineHeight());
                goodTimeFont.draw(game.batch, "Keeping your finger on the joystick will help", virtualJoystick.getX() + virtualJoystick.getWidth() + 70f * game.screenScale, virtualJoystick.getY() + virtualJoystick.getHeight() * .65f + goodTimeFont.getLineHeight());
                goodTimeFont.draw(game.batch, "avoid jumpy movement.", virtualJoystick.getX() + virtualJoystick.getWidth() + 70f * game.screenScale, virtualJoystick.getY() + virtualJoystick.getHeight() * .55f + goodTimeFont.getLineHeight());
                goodTimeFont.draw(game.batch, "Sensitivity can be changed in the options.", virtualJoystick.getX() + virtualJoystick.getWidth() + 70f * game.screenScale, virtualJoystick.getY() + virtualJoystick.getHeight() * .45f + goodTimeFont.getLineHeight());
            } else if (tutorialClicks == 1) {
                goodTimeFont.draw(game.batch, "Use the right joystick to shoot lasers.", shootJoystick.getX() - virtualJoystick.getWidth() * 3f + 75f * game.screenScale, virtualJoystick.getY() + virtualJoystick.getHeight() * .75f + goodTimeFont.getLineHeight());
                goodTimeFont.draw(game.batch, "The moon will aim the lasers according to", shootJoystick.getX() - virtualJoystick.getWidth() * 3f + 5f * game.screenScale, virtualJoystick.getY() + virtualJoystick.getHeight() * .65f + goodTimeFont.getLineHeight());
                goodTimeFont.draw(game.batch, "where you have your finger on the joystick.", shootJoystick.getX() - virtualJoystick.getWidth() * 3f - 38f * game.screenScale, virtualJoystick.getY() + virtualJoystick.getHeight() * .55f + goodTimeFont.getLineHeight());
                goodTimeFont.draw(game.batch, "You don't run out of lasers: KEEP SHOOTING!", shootJoystick.getX() - virtualJoystick.getWidth() * 3f - 12f * game.screenScale, virtualJoystick.getY() + virtualJoystick.getHeight() * .45f + goodTimeFont.getLineHeight());
            } else if (tutorialClicks == 2) {
                goodTimeFont.draw(game.batch, "Slide up or click on the 'inv' tab to access.", shootJoystick.getX() - virtualJoystick.getWidth() * 3.3f, virtualJoystick.getY() + virtualJoystick.getHeight() * .75f + goodTimeFont.getLineHeight());
                goodTimeFont.draw(game.batch, "the inventory. Slide down or click on the 'inv'", shootJoystick.getX() - virtualJoystick.getWidth() * 3.4f, virtualJoystick.getY() + virtualJoystick.getHeight() * .65f + goodTimeFont.getLineHeight());
                goodTimeFont.draw(game.batch, "tab again to close the inventory.", shootJoystick.getX() - virtualJoystick.getWidth() * 3.04f, virtualJoystick.getY() + virtualJoystick.getHeight() * .55f + goodTimeFont.getLineHeight());
                goodTimeFont.draw(game.batch, "Inventory items can be purchased in the powerup shop.", shootJoystick.getX() - virtualJoystick.getWidth() * 3.65f, virtualJoystick.getY() + virtualJoystick.getHeight() * .45f + goodTimeFont.getLineHeight());
            }
            goodTimeFont.setScale(game.screenScale * .6f);
            if (tutorialClicks < 2) {
                goodTimeFont.draw(game.batch, String.format("Next"), tutorialPlayButton.getX() + 22f * game.screenScale, tutorialPlayButton.getY() + goodTimeFont.getLineHeight() - 7 * game.screenScale);
            } else {
                goodTimeFont.draw(game.batch, String.format("Play"), tutorialPlayButton.getX() + 17f * game.screenScale, tutorialPlayButton.getY() + goodTimeFont.getLineHeight() - 7 * game.screenScale);
            }
        }

        goodTimeFont.setScale(game.screenScale * .22f);
        goodTimeFont.setColor(1, 0, 0, warningMessageAlpha);
        if (!pause && !gameOver) {
            if (warningMessageAlpha >= 1 || warningMessageAlpha <= 0) {
                warningMessageSpeed *= -1;
            }
            warningMessageAlpha += warningMessageSpeed;
        }
        float warningMessageY;
        if (!inventoryActive) {
            warningMessageY = inventoryContainer.getHeight() * .22f + 110 * game.screenScale;
        } else {
            warningMessageY = inventoryContainer.getHeight() + 110 * game.screenScale;
        }
        if (!gameOver) {
            if (seconds > 29 && seconds < 36 && minutes < 1) {
                if (game.prefs.getBoolean("DefaultSkin") || game.prefs.getBoolean("PixelSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: HUGE ASTEROIDS APPROACHING!", Gdx.graphics.getWidth() / 2 - 430f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("CatSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: HUGE MICE APPROACHING!", Gdx.graphics.getWidth() / 2 - 370f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("NinjaSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: HUGE SHURIKENS APPROACHING!", Gdx.graphics.getWidth() / 2 - 430f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("PirateSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: HUGE CANNONBALLS APPROACHING!", Gdx.graphics.getWidth() / 2 - 460f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("SoccerSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: HUGE SOCCER BALLS APPROACHING!", Gdx.graphics.getWidth() / 2 - 480f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("SumoSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: HUGE SUMO WRESTLERS APPROACHING!", Gdx.graphics.getWidth() / 2 - 532.5f * game.screenScale, warningMessageY);
                }
            }
            if ((seconds == 0 || seconds < 6) && minutes == 1) {
                if (game.prefs.getBoolean("DefaultSkin") || game.prefs.getBoolean("PixelSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: EXPLOSIVE ASTEROIDS APPROACHING!", Gdx.graphics.getWidth() / 2 - 480f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("CatSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: EXPLOSIVE MICE APPROACHING!", Gdx.graphics.getWidth() / 2 - 420f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("NinjaSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: EXPLOSIVE SHURIKENS APPROACHING!", Gdx.graphics.getWidth() / 2 - 480f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("PirateSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: EXPLOSIVE CANNONBALLS APPROACHING!", Gdx.graphics.getWidth() / 2 - 510f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("SoccerSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: EXPLOSIVE SOCCER BALLS APPROACHING!", Gdx.graphics.getWidth() / 2 - 520f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("SumoSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: EXPLOSIVE SUMO WRESTLERS APPROACHING!", Gdx.graphics.getWidth() / 2 - 600f * game.screenScale, warningMessageY);
                }
            }
            if ((seconds > 29 && seconds < 36) && minutes == 1) {
                if (game.prefs.getBoolean("DefaultSkin") || game.prefs.getBoolean("PixelSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: MAGNETIC ASTEROIDS APPROACHING!", Gdx.graphics.getWidth() / 2 - 470f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("CatSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: MAGNETIC MICE APPROACHING!", Gdx.graphics.getWidth() / 2 - 410f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("NinjaSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: MAGNETIC SHURIKENS APPROACHING!", Gdx.graphics.getWidth() / 2 - 470f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("PirateSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: MAGNETIC CANNONBALLS APPROACHING!", Gdx.graphics.getWidth() / 2 - 500f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("SoccerSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: MAGNETIC SOCCER BALLS APPROACHING!", Gdx.graphics.getWidth() / 2 - 510f * game.screenScale, warningMessageY);
                } else if (game.prefs.getBoolean("SumoSkin")) {
                    goodTimeFont.draw(game.batch, "WARNING: MAGNETIC SUMO WRESTLERS APPROACHING!", Gdx.graphics.getWidth() / 2 - 590f * game.screenScale, warningMessageY);
                }
            }
        }
        if (!gameOver) {
            if (waveTime < 4) {
                goodTimeFont.setColor(1, 0, 0, 1);
            } else {
                goodTimeFont.setColor(1, 0, 0, .5f);
            }
            goodTimeFont.draw(game.batch, String.format("Wave %d", waveCount), 20 * game.screenScale, health.Lives().get(0).getY() - 20 * game.screenScale);
            if (!game.prefs.getBoolean("NinjaSkin") && !game.prefs.getBoolean("SoccerSkin")) {
                if (waveTime < 4) {
                    goodTimeFont.setColor(1, 1, 1, 1);
                } else {
                    goodTimeFont.setColor(1, 1, 1, .5f);
                }
            } else {
                if (waveTime < 4) {
                    goodTimeFont.setColor(0, 0, 0, 1);
                } else {
                    goodTimeFont.setColor(0, 0, 0, .5f);
                }
            }
            goodTimeFont.setScale(.15f * game.screenScale);
            goodTimeFont.draw(game.batch, String.format("Of %d", waveTotal), 20 * game.screenScale, health.Lives().get(0).getY() - 50 * game.screenScale);
            goodTimeFont.setScale(.22f * game.screenScale);
//            goodTimeFont.draw(game.batch, String.format("Lasers: %d", bullets.size()), 20 * game.screenScale, Gdx.graphics.getHeight() / 2 + 50 * game.screenScale);
        }


        if (inventoryActive && !pause && !gameOver && game.prefs.getBoolean("PauseOnInventory")) {
            float previousScale = goodTimeFont.getScaleX();
            Color previousColor = goodTimeFont.getColor();
            game.blackSprite.setSize(Gdx.graphics.getWidth(), 125f * game.screenScale);
            game.blackSprite.setPosition(0, Gdx.graphics.getHeight() / 2);
            game.blackSprite.setAlpha(.5f);
            game.blackSprite.draw(game.batch);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.setScale(.53f * game.screenScale);
            goodTimeFont.draw(game.batch, "Inventory Active", Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 2 + goodTimeFont.getLineHeight());
            goodTimeFont.setColor(previousColor);
            goodTimeFont.setScale(previousScale);
        }

//        goodTimeFont.draw(game.batch, String.format("MagneticAsteroidMax: %d", magneticAsteroidMax), 0, 40);
//        goodTimeFont.draw(game.batch, String.format("ExplodingAsteroidMax: %d", explodingAsteroidMax), 0, 90);
//        goodTimeFont.draw(game.batch, String.format("HugeAsteroidMax: %d", hugeAsteroidMax), 0, 140);
//        goodTimeFont.draw(game.batch, String.format("WaveTime: %d", waveTime), 0, 40);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
