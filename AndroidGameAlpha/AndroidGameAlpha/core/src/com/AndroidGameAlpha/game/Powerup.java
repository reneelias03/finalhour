package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 * Created by rene__000 on 9/10/2014.
 */
public class Powerup {

    private Random _random;
    private Sprite _currentPowerup;

    public Sprite CurrentPowerup() {
        return _currentPowerup;
    }

    private Sprite _explosionSprite;

    public Sprite ExplosionSprite() {
        return _explosionSprite;
    }

    private Vector2 _explosionSpriteOriginalPosition;
    private int _powerupTypeInt;

    public int PowerupTypeInt() {
        return _powerupTypeInt;
    }

    private float _dropSpeed;
    private float _powerupTimePassed;
    private float _powerupTime;
    private float _timeSinceLastPowerup;
    private float _scale;
    private Vector2 _originalExplosionSize;
    private float _explosionRadius;

    public float ExplosionRadius() {
        return _explosionRadius;
    }

    private float _explosionAlpha;
    private boolean _explosionDying;

    public boolean ExplsionDying() {
        return _explosionDying;
    }

    private boolean _powerupOnScreen;

    public boolean PowerupOnScreen() {
        return _powerupOnScreen;
    }
    public void SetPowerupOnScreen(boolean powerupOnScreen) {
        _powerupOnScreen = powerupOnScreen;
    }

    private boolean _powerupActive;
    public void SetPowerupActive(boolean active, PowerupType powerupType){_powerupActive = active; _powerupType = powerupType;}
    private boolean _switchShootAnimation;

    public boolean GetSwitchShoot() {
        return _switchShootAnimation;
    }

    private boolean _asteroidPlanetGenerated;

    public boolean AsteroidPlanetGenerated() {
        return _asteroidPlanetGenerated;
    }

    public void SetAsteroidPlanetGenerated(boolean generated) {
        _asteroidPlanetGenerated = generated;
    }

    public void SetSwitchShoot(boolean switchShootAnimation) {
        _switchShootAnimation = switchShootAnimation;
    }

    private com.badlogic.gdx.assets.AssetManager _manager;
    private PowerupType _powerupType;

    public PowerupType PowerUpType() {
        return _powerupType;
    }

    public boolean PowerupActive() {
        return _powerupActive;
    }

    public boolean explosionSimultaneous;

    private Sound _powerupSound;
    private Sound _explosionPowerupSound;

    public int onlyLaserNumber;

    public Powerup(com.badlogic.gdx.assets.AssetManager manager, float dropSpeed, float scale, Sound powerupSound, boolean pixelSkin) {
        _manager = manager;
        _scale = scale;
        _powerupTypeInt = 0;
        _powerupTime = 15f;
        _powerupTimePassed = 0;
        _powerupOnScreen = false;
        _powerupActive = false;
        _dropSpeed = dropSpeed * _scale;
        _random = new Random();
        _powerupSound = powerupSound;
        _powerupType = PowerupType.x2Multiplier;
        _switchShootAnimation = false;
        _timeSinceLastPowerup = 0;
        _asteroidPlanetGenerated = false;
        if(pixelSkin) {
            _explosionSprite = new Sprite(manager.get("data/CosmicExplosion15.png", Texture.class));
        } else {
            _explosionSprite = new Sprite(manager.get("data/CosmicExplosionCutLessBlack.png", Texture.class));
        }
        _explosionSprite.setSize(20 * scale, 20 * scale);
        _originalExplosionSize = new Vector2(_explosionSprite.getWidth(), _explosionSprite.getHeight());
        _explosionSprite.setOriginCenter();
        _explosionRadius = _explosionSprite.getHeight() - _explosionSprite.getOriginX() - .5f * _scale;
        _explosionAlpha = .75f;
        _explosionSprite.setAlpha(_explosionAlpha);
        _explosionDying = false;
        _explosionPowerupSound = Gdx.audio.newSound(Gdx.files.internal("data/ExplosionPowerupSound.mp3"));
        onlyLaserNumber = -1;
        explosionSimultaneous = false;
    }

    public void Update(float gameTime, Planet Earth, boolean playSound, float volume, float deltaTime, boolean attached, boolean vibrate) {
        if (_powerupOnScreen) {
            _currentPowerup.setPosition(_currentPowerup.getX(), _currentPowerup.getY() - _dropSpeed);
            if (_currentPowerup.getY() + _currentPowerup.getHeight() < 0) {
                _powerupOnScreen = false;
                _timeSinceLastPowerup = 0;
            } else if (_currentPowerup.getBoundingRectangle().overlaps(Earth.getBoundingRectangle())) {
                if (playSound) {
                    _powerupSound.play(.5f * volume);
                }
                if (_powerupType == PowerupType.HealthAdd) {
                    Earth.SetHealth(Earth.GetHealth() + 1);
                } else {
                    _powerupActive = true;
                }
                if (_powerupType == PowerupType.ShootFaster) {
                    _switchShootAnimation = true;
                }
                if (_powerupType == PowerupType.Explosion) {
                    SetExplosionSprite(Earth, playSound, volume, false);
                }
                _powerupOnScreen = false;
            }
        }
        if(explosionSimultaneous) {
            _explosionSprite.setSize(_explosionSprite.getWidth() + 20 * _scale, _explosionSprite.getHeight() + 20 * _scale);
            _explosionSprite.setOriginCenter();
            _explosionSprite.setPosition(_explosionSpriteOriginalPosition.x - _explosionSprite.getOriginX(), _explosionSpriteOriginalPosition.y - _explosionSprite.getOriginY());
            _explosionRadius = _explosionSprite.getHeight() - _explosionSprite.getOriginX() - (.5f * _scale) * (_explosionSprite.getHeight() / _originalExplosionSize.y);
            if (ExplosionSpriteOffScreen()) {
                _explosionAlpha = _explosionAlpha - .02f;
                _explosionSprite.setAlpha(_explosionAlpha);
                _explosionDying = true;
            }
            if (_explosionAlpha <= 0) {
                explosionSimultaneous = false;
            }
        }
        if (_powerupActive) {
            if (_powerupType != PowerupType.DualShot && _powerupType != PowerupType.Explosion) {
                _powerupTime -= gameTime;
            } else if (_powerupType == PowerupType.DualShot && attached) {
                if (!_switchShootAnimation) {
                    _switchShootAnimation = true;
                }
                _powerupTime -= gameTime;
            }
            if (_powerupTime <= 0 && _powerupType != PowerupType.Explosion) {
//                _powerupTimePassed = 0;
                _powerupTime = 15f;
                _powerupActive = false;
                _timeSinceLastPowerup = 0;
                if (_powerupType == PowerupType.ShootFaster || _powerupType == PowerupType.DualShot) {
                    _switchShootAnimation = true;
                }
            }
            if (_powerupType == PowerupType.Explosion)//SPECIFY WHEN THIS SHOULD BECOME INACTIVE
            {
                _explosionSprite.setSize(_explosionSprite.getWidth() + 20 * _scale, _explosionSprite.getHeight() + 20 * _scale);
                _explosionSprite.setOriginCenter();
                _explosionSprite.setPosition(_explosionSpriteOriginalPosition.x - _explosionSprite.getOriginX(), _explosionSpriteOriginalPosition.y - _explosionSprite.getOriginY());
                _explosionRadius = _explosionSprite.getHeight() - _explosionSprite.getOriginX() - (.5f * _scale) * (_explosionSprite.getHeight() / _originalExplosionSize.y);
                if (ExplosionSpriteOffScreen()) {
                    _explosionAlpha = _explosionAlpha - .02f;
                    _explosionSprite.setAlpha(_explosionAlpha);
                    _explosionDying = true;
                }
                if (_explosionAlpha <= 0) {
                    _powerupTimePassed = 0;
                    _powerupActive = false;
                    _timeSinceLastPowerup = 0;
                }
            }

        }
        _timeSinceLastPowerup += deltaTime;
    }

    public void Generate(Vector2 generationLocation, int planetHealth) {
        int create;
        if (_timeSinceLastPowerup > 20) {
            create = _random.nextInt(5);
        } else {
            create = _random.nextInt(10);
        }
        if (create == 0 && !_powerupOnScreen && !_powerupActive) {
            do {
                _powerupTypeInt = _random.nextInt(18);
            } while(explosionSimultaneous && (_powerupTypeInt == 0 || _powerupTypeInt == 1));
            if(onlyLaserNumber != -1) {
                if((planetHealth < 3 && _powerupTypeInt > 4 && _powerupTypeInt < 14) || (planetHealth == 3 && _powerupTypeInt > 1 && _powerupTypeInt < 14)) {
                    if (onlyLaserNumber == 0) {
                        _powerupTypeInt = 5;
                    } else if (onlyLaserNumber == 1) {
                        _powerupTypeInt = 9;
                    } else if (onlyLaserNumber == 2) {
                        _powerupTypeInt = 11;
                    }
                }
            }
//            _powerupTypeInt = 0;
            if (planetHealth < 3) {
//                if (_powerupTypeInt == 0 || _powerupTypeInt == 1) {
//                    _powerupType = PowerupType.DivineIntervention;
//                }
                if (_powerupTypeInt == 0 || _powerupTypeInt == 1) {
                    _powerupType = PowerupType.Explosion;
                } else if ((_powerupTypeInt == 2 || _powerupTypeInt == 3 || _powerupTypeInt == 4)) {
                    _powerupType = PowerupType.HealthAdd;
                } else if (_powerupTypeInt == 5 || _powerupTypeInt == 6 || _powerupTypeInt == 7) {
                    _powerupType = PowerupType.ShootFaster;
                } else if (_powerupTypeInt == 8 || _powerupTypeInt == 9 || _powerupTypeInt == 10) {
                    _powerupType = PowerupType.DualShot;
                } else if (_powerupTypeInt == 11 || _powerupTypeInt == 12 || _powerupTypeInt == 13) {
                    _powerupType = PowerupType.PiercingLaser;
                } else {
                    _powerupType = PowerupType.x2Multiplier;
                }
            } else {
//                if (_powerupTypeInt == 0 || _powerupTypeInt == 1) {
//                    _powerupType = PowerupType.DivineIntervention;
//                }
                if (_powerupTypeInt == 0 || _powerupTypeInt == 1) {
                    _powerupType = PowerupType.Explosion;
                } else if (_powerupTypeInt == 2 || _powerupTypeInt == 3 || _powerupTypeInt == 4 || _powerupTypeInt == 5) {
                    _powerupType = PowerupType.ShootFaster;
                } else if (_powerupTypeInt == 6 || _powerupTypeInt == 7 || _powerupTypeInt == 8 || _powerupTypeInt == 9) {
                    _powerupType = PowerupType.DualShot;
                } else if (_powerupTypeInt == 10 || _powerupTypeInt == 11 || _powerupTypeInt == 12 || _powerupTypeInt == 13) {
                    _powerupType = PowerupType.PiercingLaser;
                } else {
                    _powerupType = PowerupType.x2Multiplier;
                }
            }
            if (_powerupType == PowerupType.x2Multiplier) {
                _currentPowerup = new Sprite(_manager.get("data/X2Powerup.png", Texture.class));
            } else if (_powerupType == PowerupType.DivineIntervention) {
                _currentPowerup = new Sprite(_manager.get("data/TouchingPowerup.png", Texture.class));
            } else if (_powerupType == PowerupType.HealthAdd) {
                _currentPowerup = new Sprite(_manager.get("data/LifePowerup.png", Texture.class));
            } else if (_powerupType == PowerupType.ShootFaster) {
                _currentPowerup = new Sprite(_manager.get("data/ShootSpeedPowerup.png", Texture.class));
            } else if (_powerupType == PowerupType.DualShot) {
                _currentPowerup = new Sprite(_manager.get("data/DualSided.png", Texture.class));
                _asteroidPlanetGenerated = false;
            } else if (_powerupType == PowerupType.Explosion) {
                _currentPowerup = new Sprite(_manager.get("data/ExplosionPowerup.png", Texture.class));
            } else if (_powerupType == PowerupType.PiercingLaser) {
                _currentPowerup = new Sprite(_manager.get("data/PierceBulletPowerup.png", Texture.class));
            }
            _currentPowerup.setSize(60 * _scale, 60 * _scale);
            _currentPowerup.setPosition(generationLocation.x, generationLocation.y);
            _powerupOnScreen = true;
        }
    }

    public void DrawText(BitmapFont font, SpriteBatch batch, float scale, boolean pirateSkin, boolean invActive) {
        if (_powerupType == PowerupType.x2Multiplier) {
            font.draw(batch, String.format("Score x2 Activated"), Gdx.graphics.getWidth() / 2 - 230 * scale, Gdx.graphics.getHeight() - 60 * scale);
        }
        else if (_powerupType == PowerupType.DivineIntervention) {
            font.draw(batch, String.format("Divine Intervention Activated. Tap The Asteroids!"), Gdx.graphics.getWidth() / 2 - 450 * scale, Gdx.graphics.getHeight() - 60 * scale);
        }
        else if (_powerupType == PowerupType.ShootFaster) {
            font.draw(batch, String.format("Laser Speed Boost Activated"), Gdx.graphics.getWidth() / 2 - 330 * scale, Gdx.graphics.getHeight() - 60 * scale);
        }
        else if (_powerupType == PowerupType.DualShot) {
            font.draw(batch, String.format("DualShot Activated"), Gdx.graphics.getWidth() / 2 - 225 * scale, Gdx.graphics.getHeight() - 60 * scale);
        }
        else if (_powerupType == PowerupType.PiercingLaser) {
            font.draw(batch, String.format("Piercing Laser Activated"), Gdx.graphics.getWidth() / 2 - 295 * scale, Gdx.graphics.getHeight() - 60 * scale);
        }
        if(_powerupType != PowerupType.Explosion && _powerupType != PowerupType.HealthAdd) {
            if(pirateSkin) {
                font.setColor(Color.YELLOW);
            } else {
                font.setColor(Color.ORANGE);
            }
            if(!invActive) {
                if ((int) _powerupTime >= 10) {
                    font.draw(batch, String.format("Powerup: %d", (int) _powerupTime), Gdx.graphics.getWidth() / 2 - 137.5f * scale, 120 * scale);
                } else {
                    font.draw(batch, String.format("Powerup: 0%d", (int) _powerupTime), Gdx.graphics.getWidth() / 2 - 137.5f * scale, 120 * scale);
                }
            } else {
                if ((int) _powerupTime >= 10) {
                    font.draw(batch, String.format("Powerup: %d", (int) _powerupTime), Gdx.graphics.getWidth() / 2 - 137.5f * scale, 280 * scale);
                } else {
                    font.draw(batch, String.format("Powerup: 0%d", (int) _powerupTime), Gdx.graphics.getWidth() / 2 - 137.5f * scale, 280 * scale);
                }
            }
        }
    }

    public void SetExplosionSprite(Planet Earth, boolean playSound, float volume, boolean explosionSimultaneous) {
        this.explosionSimultaneous = explosionSimultaneous;
        _explosionSprite.setSize(20 * _scale, 20 * _scale);
        _explosionSprite.setOriginCenter();
        _explosionSprite.setPosition(Earth.getX() + Earth.getOriginX() - _explosionSprite.getOriginX(), Earth.getY() + Earth.getOriginY() - _explosionSprite.getOriginY());
        _explosionSpriteOriginalPosition = new Vector2(Earth.getX() + Earth.getOriginX() - _explosionSprite.getOriginX(), Earth.getY() + Earth.getOriginY() - _explosionSprite.getOriginY());
        _explosionAlpha = .75f;
        _explosionSprite.setAlpha(_explosionAlpha);
        _explosionDying = false;
        _explosionRadius = _explosionSprite.getHeight() - _explosionSprite.getOriginX() - .5f * _scale;
        if (playSound) {
            _explosionPowerupSound.play(.175f * volume);
        }
    }

    public boolean ExplosionSpriteOffScreen() {
        if (_explosionSprite.getX() < 0 && _explosionSprite.getY() < 0 && _explosionSprite.getY() + _explosionSprite.getHeight() > Gdx.graphics.getHeight() && _explosionSprite.getX() + _explosionSprite.getWidth() > Gdx.graphics.getWidth()) {
            return true;
        } else {
            return false;
        }
    }
}
