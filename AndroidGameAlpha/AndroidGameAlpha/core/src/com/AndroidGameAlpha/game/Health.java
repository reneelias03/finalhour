package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/**
 * Created by rene__000 on 9/5/2014.
 */
public class Health {

    private ArrayList<Sprite> _lives;
    public ArrayList<Sprite> Lives()
    {
        return _lives;
    }
    private ArrayList<Vector2> _originalLifeLocations;
    private int _lifeCapacity;
    private int _currentHealth;
    private float _originalWidth;
    private float _originalHeight;
    public int GetLifeCapacity()
    {
        return _lifeCapacity;
    }
    public void SetLifeCapacity(int lifeCapacity)
    {
        _lifeCapacity = lifeCapacity;
    }
    private TextureRegion _textureRegion;
    private Texture _texture;
    private float _scale;

    public Health(Texture texture, int lifeCapacity, float scale)
    {
        _scale = scale;
        _lives = new ArrayList<Sprite>(lifeCapacity);
        _originalLifeLocations = new ArrayList<Vector2>(lifeCapacity);
        float xLocation = 60 * scale;
        for(int i = 0; i < lifeCapacity; i++)
        {
            Sprite life = new Sprite(texture);
            life.setSize(life.getWidth() * scale, life.getHeight() * scale);
            life.setOriginCenter();
            life.setX(xLocation);
            life.setY(Gdx.graphics.getHeight() - life.getHeight() - 60 * scale);
            _lives.add(life);
            _originalLifeLocations.add(new Vector2(life.getX(), life.getY()));
            xLocation += life.getWidth() + 60 * scale;
        }
        _originalWidth = _lives.get(0).getWidth();
        _originalHeight = _lives.get(0).getHeight();
        _lifeCapacity = lifeCapacity;
        _currentHealth = lifeCapacity;
        _texture = texture;
    }

    public Health(TextureRegion texture, int lifeCapacity, float scale)
    {
        _scale = scale;
        _lives = new ArrayList<Sprite>(lifeCapacity);
        _originalLifeLocations = new ArrayList<Vector2>(lifeCapacity);
        float xLocation = 60 * scale;
        for(int i = 0; i < lifeCapacity; i++)
        {
            Sprite life = new Sprite(texture);
            life.setSize(life.getWidth() * scale, life.getHeight() * scale);
            life.setOriginCenter();
            life.setX(xLocation);
            life.setY(Gdx.graphics.getHeight() - life.getHeight() - 60 * scale);
            _lives.add(life);
            _originalLifeLocations.add(new Vector2(life.getX(), life.getY()));
            xLocation += life.getWidth() + 60 * scale;
        }
        _originalWidth = _lives.get(0).getWidth();
        _originalHeight = _lives.get(0).getHeight();
        _lifeCapacity = lifeCapacity;
        _currentHealth = lifeCapacity;
        _textureRegion = texture;
    }

    public void Update(int EartHealth, float cameraZoom)
    {
        if(EartHealth < _currentHealth)
        {
            _lives.remove(_lives.size() - 1);
            _originalLifeLocations.remove(_originalLifeLocations.size() - 1);
            _currentHealth--;
        }
        if(EartHealth > _currentHealth)
        {
            Sprite life;
            if(_textureRegion == null) {
                life = new Sprite(_texture);
            } else
            {
                life = new Sprite(_textureRegion);
            }
            life.setSize(_lives.get(_lives.size() -1).getWidth(), _lives.get(_lives.size() -1).getHeight());
            life.setOriginCenter();
            life.setX(_lives.get(_lives.size() -1).getX() + life.getWidth() + 60 * _scale);
            life.setY(Gdx.graphics.getHeight() - life.getHeight() - 60 * _scale);
            life.setColor(_lives.get(0).getColor());
            _lives.add(life);
            _originalLifeLocations.add(new Vector2(life.getX(), life.getY()));
            _currentHealth++;
        }
        for(int i = 0; i < _lives.size(); i++)
        {
            KeepScale(cameraZoom, _lives.get(i));
            KeepOriginalPosition(cameraZoom, _originalLifeLocations.get(i), _lives.get(i));
        }
}

    private void KeepScale(float cameraZoom, Sprite life)
    {
        life.setSize(_originalWidth * cameraZoom, _originalHeight * cameraZoom);
    }

    public void KeepOriginalPosition(float cameraZoom, Vector2 originalPosition, Sprite life)
    {
        life.setPosition(originalPosition.x * cameraZoom + (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2, originalPosition.y * cameraZoom + (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2);
    }
}
