package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

/**
 * Created by rene__000 on 11/26/2014.
 */
public class HugeAsteroid extends Asteroid {

    private int life;
    //    private LaserBullets[]

    public  HugeAsteroid(Texture texture, Vector2 PlanetLocation, float scale, float speed, float cameraZoom, Vector2 deadZone, TextureAtlas explosionAtlas)
    {
        super(texture, PlanetLocation, scale, speed, 1, deadZone, explosionAtlas);
        this.setSize(310 * scale, 310 * scale);
        this.setOriginCenter();
        _radius = new Vector2(0, this.getOriginY());
        life = 4;
        _hugeAsteroid = true;
        Random random = new Random();
        int screenLocation = random.nextInt(4);
        if(screenLocation == 0) {
            //Spawn to left of screen
            this.setPosition(((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2) - this.getWidth() * 2,
                    deadZone.y + random.nextInt((int)(Gdx.graphics.getHeight() * 1 - deadZone.y - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2)));
        }
        else if(screenLocation == 1)
        {
            //Spawn to top of screen
            this.setPosition(random.nextInt((int)(Gdx.graphics.getWidth() * 1 - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2)),
                    (Gdx.graphics.getHeight() - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2) + this.getHeight() * 2);
        }
        else if(screenLocation == 2)
        {
            //Spawn to the right of the screen
            this.setPosition(Gdx.graphics.getWidth() - ((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2) + this.getWidth(),
                    deadZone.y + random.nextInt((int)(Gdx.graphics.getHeight()* 1 - deadZone.y - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2)));
        }
        else
        {
            //Spawn to the bottom of the screen
            this.setPosition(deadZone.x + random.nextInt((int)(Gdx.graphics.getWidth() * 1  - deadZone.x * 2 - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2)),
                    ((Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2) - this.getHeight());
    }
    _speed = new Vector2(PlanetLocation.x - this.getX(), PlanetLocation.y - this.getY());
    _speed.nor();
        _speed.x *= speed;
        _speed.y *= speed;
        rotationSpeed = random.nextInt(5) + 1;
        _active = false;
    }

    @Override
    public void Update(float cameraZoom, LaserBullets[] bullets, Planet Earth, Powerup powerup, boolean vibrate, Sound explosion, boolean playSound, float deltaTime, float volume)
    {
        super.Update(1, bullets, Earth, powerup, vibrate, explosion, playSound, deltaTime, volume);

        if(_active) {
            for (int i = 0; i < bullets.length; i++) {
                if (bullets[i]._active && this.getBoundingRectangle().overlaps(bullets[i].getBoundingRectangle()) && !_exploding) {
                    Vector2 endCirclePosition = new Vector2((float) bullets[i].getX() + (float) bullets[i].getWidth() - ((float) bullets[i].getHeight() / 2f), (float) bullets[i].getY() + (float) bullets[i].getHeight() / 2f);
                    float endCircleRadius = (float) bullets[i].getHeight() / 2f;
                    Vector2 endCircleToAsteroid = new Vector2(this.getX() + this.getOriginX() - endCirclePosition.x, this.getY() + this.getOriginY() - endCirclePosition.y);
                    if (endCircleToAsteroid.len() <= _radius.len() + endCircleRadius) {
                        if (powerup.PowerupActive() && powerup.PowerUpType() == PowerupType.PiercingLaser && bullets[i]._asteroidCount < 1) {
                            bullets[i]._asteroidCount++;
                            bullets[i].setColor(Color.YELLOW);
                        } else {
//                            bullets.remove(bullets.get(i));
                            bullets[i]._active = false;
                        }
                        life--;
                        if (life == 0) {
                            _exploding = true;
                            _toScore = true;
                            powerup.Generate(new Vector2(this.getX(), this.getY()), Earth.GetHealth());
                            if (playSound) {
                                explosion.play(.2f * volume);
                            }
                        }
                    }
                    break;
                }

            }
        }
    }
    
    public void Reset(Vector2 PlanetLocation) {
        ResetAsteroid(_originalTexture, PlanetLocation);

        this.setSize(310 * _scale, 310 * _scale);
        this.setOriginCenter();
        _radius = new Vector2(0, this.getOriginY());
        life = 4;
        _hugeAsteroid = true;
        Random random = new Random();
        int screenLocation = random.nextInt(4);
        if(screenLocation == 0) {
            //Spawn to left of screen
            this.setPosition(((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2) - this.getWidth() * 2,
                    _deadZone.y + random.nextInt((int)(Gdx.graphics.getHeight() * 1 - _deadZone.y - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2)));
        }
        else if(screenLocation == 1)
        {
            //Spawn to top of screen
            this.setPosition(random.nextInt((int)(Gdx.graphics.getWidth() * 1 - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2)),
                    (Gdx.graphics.getHeight() - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2) + this.getHeight() * 2);
        }
        else if(screenLocation == 2)
        {
            //Spawn to the right of the screen
            this.setPosition(Gdx.graphics.getWidth() - ((Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2) + this.getWidth(),
                    _deadZone.y + random.nextInt((int)(Gdx.graphics.getHeight()* 1 - _deadZone.y - (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2)));
        }
        else
        {
            //Spawn to the bottom of the screen
            this.setPosition(_deadZone.x + random.nextInt((int)(Gdx.graphics.getWidth() * 1  - _deadZone.x * 2 - (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * 1) / 2)),
                    ((Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * 1) / 2) - this.getHeight());
        }
        _speed = new Vector2(PlanetLocation.x - this.getX(), PlanetLocation.y - this.getY());
        _speed.nor();
        _speed.x *= _speedMultiplier;
        _speed.y *= _speedMultiplier;
        rotationSpeed = random.nextInt(5) + 1;
        _active = false;
    }

}
