package com.AndroidGameAlpha.game;

import android.gesture.Gesture;
import android.graphics.Rect;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rene__000 on 8/29/2014.
 */
public class VirtualJoystick extends GameButtons {
    public GameButtons _fingerHover;
    public GameButtons FingerHover() {
    return _fingerHover;
    }
    private Vector2 _distanceFromCenter;
    private Vector2 _distanceBetweenCenterAndFinger;
    public Vector2 DistanceBetweenCenterAndFinger() {
        return _distanceBetweenCenterAndFinger;
    }
    private Vector2 _hoverPosition;
    public Vector2 HoverPosition()
    {
        return _hoverPosition;
    }
    private float _distanceBetweenCenterAndFingerAngle;
    public float DistanceBetweenCenterAndFingerAngle() {
        return _distanceBetweenCenterAndFingerAngle;
    }
    private int currentFingerTouching;
    private int _previousFingerTouching;
    public int PreviousFingerTouching()
    {
        return _previousFingerTouching;
    }
    private boolean _drawHover;
    public boolean GetDrawHover() {
        return _drawHover;
    }
    private boolean _fingerDown;
    public boolean FingerDown()
    {
        return _fingerDown;
    }
    private boolean _toUpdatePosition;
    private boolean _notUpdatePrevious;
    private boolean _shrinkHover;
    private Rectangle _twiceSizeBoundingBox;
    public void SetTwiceSizeBoundingBox(Rectangle twiceSizeBoundingBox)
    {
        _twiceSizeBoundingBox = new Rectangle(twiceSizeBoundingBox.getX(), twiceSizeBoundingBox.getY(), twiceSizeBoundingBox.getWidth(), twiceSizeBoundingBox.getHeight());
    }

    public VirtualJoystick(Texture greaterCircleTexture, boolean isStatic, Vector2 position, Texture fingerPositionTexture, float _scale, boolean shrinkHover) {
        super(greaterCircleTexture, isStatic, position);
        this.setSize(this.getWidth() * _scale, this.getHeight() * _scale);
        _originalWidth = this.getWidth();
        _originalHeight = this.getHeight();
        _fingerHover = new GameButtons(fingerPositionTexture, true, new Vector2().setZero());
        if(shrinkHover) {
            _fingerHover.setSize(_fingerHover.getWidth() * _scale, _fingerHover.getHeight() * _scale);
        }else{
            _fingerHover.setSize(_fingerHover.getWidth() * 1.55f * _scale, _fingerHover.getHeight() * 1.55f * _scale);
        }
        _fingerHover.setOriginCenter();
        _fingerHover._originalWidth = _fingerHover.getWidth();
        _fingerHover._originalHeight = _fingerHover.getHeight();
        _distanceFromCenter = new Vector2(0, this.getHeight() - this.getHeight() / 2);
        _drawHover = false;
        _distanceBetweenCenterAndFinger = new Vector2().setZero();
        _distanceBetweenCenterAndFingerAngle = _distanceBetweenCenterAndFinger.angle();
        _fingerDown = false;
        _previousFingerTouching = 0;
        _hoverPosition = new Vector2().setZero();
        _toUpdatePosition = false;
        _notUpdatePrevious = false;
        _shrinkHover = shrinkHover;
        currentFingerTouching = 0;
    }

    public void Update(float cameraZoom, int otherJoyStickFinger, boolean secondJoyStickTouched) {
        super.Update(cameraZoom);
        GetFingerCurrentlyTouching(cameraZoom);
        if(currentFingerTouching == otherJoyStickFinger && _toUpdatePosition && secondJoyStickTouched && !_fingerDown)
        {
            _toUpdatePosition = false;
        }
        if(!_shrinkHover)
        {
            if (GestureManager.IsCurrentlyBeingTouchedIndex(_twiceSizeBoundingBox, cameraZoom, currentFingerTouching) && GestureManager.GetCurrentTouchState() != Gestures.None && _toUpdatePosition) {
                _distanceFromCenter.y = this.getHeight() - this.getHeight() / 2;
                _distanceBetweenCenterAndFinger = new Vector2(Gdx.input.getX(currentFingerTouching) * cameraZoom + (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2 - (this.getX() + this.getWidth() / 2),
                        (-Gdx.input.getY(currentFingerTouching) + Gdx.graphics.getHeight()) * cameraZoom + (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2 - (this.getY() + this.getHeight() / 2));
                _distanceBetweenCenterAndFingerAngle = _distanceBetweenCenterAndFinger.angle();
                        if (!_drawHover) {
                            _drawHover = true;
                        }
                        if (!_fingerDown) {
                            _fingerDown = true;
                        }
                        _fingerHover.Update(cameraZoom);
                        _hoverPosition = GestureManager.DragLocationIndex(_fingerHover.getWidth(), _fingerHover.getHeight(), cameraZoom, currentFingerTouching);
                        _fingerHover.setPosition(_hoverPosition.x, _hoverPosition.y);
            }
            }
            else {
                if (GestureManager.IsCurrentlyBeingTouchedIndex(this.getBoundingRectangle(), cameraZoom, currentFingerTouching) && GestureManager.GetCurrentTouchState() != Gestures.None && _toUpdatePosition) {
                    _distanceFromCenter.y = this.getHeight() - this.getHeight() / 2;
                    _distanceBetweenCenterAndFinger = new Vector2(Gdx.input.getX(currentFingerTouching) * cameraZoom + (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2 - (this.getX() + this.getWidth() / 2),
                            (-Gdx.input.getY(currentFingerTouching) + Gdx.graphics.getHeight()) * cameraZoom + (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2 - (this.getY() + this.getHeight() / 2));
                    _distanceBetweenCenterAndFingerAngle = _distanceBetweenCenterAndFinger.angle();
                        if (_distanceBetweenCenterAndFinger.len() <= _distanceFromCenter.len() && GestureManager.GetCurrentTouchState() != Gestures.None) {
                            if (!_drawHover) {
                                _drawHover = true;
                            }
                            if (!_fingerDown) {
                                _fingerDown = true;
                            }
                            _fingerHover.Update(cameraZoom);
                            _hoverPosition = GestureManager.DragLocationIndex(_fingerHover.getWidth(), _fingerHover.getHeight(), cameraZoom, currentFingerTouching);
                            _fingerHover.setPosition(_hoverPosition.x, _hoverPosition.y);
                        }

                }
            }
        if(_toUpdatePosition) {
            _distanceFromCenter.y = this.getHeight() - this.getHeight() / 2;
            _distanceBetweenCenterAndFinger = new Vector2(Gdx.input.getX(_previousFingerTouching) * cameraZoom + (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2 - (this.getX() + this.getWidth() / 2),
                    (-Gdx.input.getY(_previousFingerTouching) + Gdx.graphics.getHeight()) * cameraZoom + (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2 - (this.getY() + this.getHeight() / 2));
            _distanceBetweenCenterAndFingerAngle = _distanceBetweenCenterAndFinger.angle();
        }
        if(!_notUpdatePrevious)
        {
            _previousFingerTouching = currentFingerTouching;
        }
        if (_fingerDown && _distanceBetweenCenterAndFinger.len() > _distanceFromCenter.len() && _toUpdatePosition) {
            _hoverPosition = new Vector2(Gdx.input.getX(_previousFingerTouching) * cameraZoom + (Gdx.graphics.getWidth() - Gdx.graphics.getWidth() * cameraZoom) / 2 - (this.getX() + this.getWidth() / 2),
                    (-Gdx.input.getY(_previousFingerTouching) + Gdx.graphics.getHeight()) * cameraZoom + (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() * cameraZoom) / 2 - (this.getY() + this.getHeight() / 2));
            _hoverPosition.nor();
            _hoverPosition.set((this.getX() + this.getOriginX() - _fingerHover.getOriginX()) +  _hoverPosition.x * _distanceFromCenter.len(), ( this.getY() + this.getOriginY() - _fingerHover.getOriginY()) + _hoverPosition.y * _distanceFromCenter.len());
            _fingerHover.setPosition(_hoverPosition.x, _hoverPosition.y);
        }
        if (!Gdx.input.isTouched(_previousFingerTouching)) {
            _fingerDown = false;
        }
        if (_drawHover && !_fingerDown && _toUpdatePosition) {
            _drawHover = false;
            _distanceBetweenCenterAndFinger = new Vector2().setZero();
            _toUpdatePosition = false;
        }

        if(!_fingerDown) {
            this.setAlpha(.5f);
        } else if(_fingerDown && this.getColor().a != 1) {
            this.setAlpha(1f);
        }
    }

    public void GetFingerCurrentlyTouching(float cameraZoom) {
        int[] currentFingersDown = GestureManager.CurrentFingersDown();
        for (int i = 0; i < currentFingersDown.length; i++) {
            if(!_shrinkHover)
            {
                if (GestureManager.IsCurrentlyBeingTouchedIndex(_twiceSizeBoundingBox, cameraZoom, currentFingersDown[i])) {
                    currentFingerTouching = currentFingersDown[i];
                    _toUpdatePosition = true;
                    _notUpdatePrevious = false;
                    break;
                }
            }
            else if (_shrinkHover){
                if (GestureManager.IsCurrentlyBeingTouchedIndex(this.getBoundingRectangle(), cameraZoom, currentFingersDown[i])) {
                    currentFingerTouching = currentFingersDown[i];
                    _toUpdatePosition = true;
                    _notUpdatePrevious = false;
                    break;
                }
            }
            else if(i == currentFingersDown.length - 1)
            {
                _notUpdatePrevious = true;
            }
        }
    }

    public Vector2 DragSpeed()
    {
        Vector2 _dragSpeed = new Vector2().setZero();

        _dragSpeed.x = (_hoverPosition.x + _fingerHover.getOriginX()) - (this.getOriginX() + this.getX());
        _dragSpeed.y = (_hoverPosition.y + _fingerHover.getOriginY()) - (this.getOriginY() + this.getY());
        return  _dragSpeed;
    }
}
