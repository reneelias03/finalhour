package com.AndroidGameAlpha.game;

/**
 * Created by rene__000 on 9/10/2014.
 */
public enum PowerupType {
    x2Multiplier,
    DivineIntervention,
    HealthAdd,
    ShootFaster,
    DualShot,
    Explosion,
    PiercingLaser;
}
