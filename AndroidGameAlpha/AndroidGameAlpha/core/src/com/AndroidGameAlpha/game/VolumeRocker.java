package com.AndroidGameAlpha.game;

import android.widget.Button;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rene__000 on 9/11/2014.
 */
public class VolumeRocker
{
    private Sprite _borderSprite;
    private Sprite _fillSprite;
    private GameButtons _dial;
    private float _fillSpriteOriginalWidth;
    private float _fillSpriteOriginalX;
    private float _currentValue;
    public float CurrentValue()
    {
        return _currentValue;
    }
    public void SetValue(float value)
    {
        _currentValue = value;
        _fillSprite.setSize(_fillSpriteOriginalWidth * _currentValue, _fillSprite.getHeight());
        _dial.setPosition(_fillSprite.getX() + _fillSprite.getWidth() - _dial.getOriginX(), _dial.getY());
    }
    private float _scaleValue;
    private int _fingerCurrentlyToucing;
    private boolean _fingerStillDown;
    private boolean _touched;
    public boolean Active()
    {
        return _fingerStillDown;
    }
    public float GetRockerHeight()
    {
        return _borderSprite.getHeight();
    }

    public Vector2 GetRockerPosition()
    {
        return new Vector2(_borderSprite.getX(), _borderSprite.getY());
    }
    public float ScaleValue()
    {
        return _scaleValue;
    }


    public VolumeRocker(Texture borderTexture, Texture fillTexture, Texture controlTexture, Vector2 position, Vector2 size, float startingValue)
    {
        _borderSprite = new Sprite(borderTexture);
        _scaleValue =  size.x / _borderSprite.getWidth();
        _borderSprite.setSize(size.x, size.y);
        _borderSprite.setPosition(position.x, position.y);
        _borderSprite.setOriginCenter();
        _fillSprite = new Sprite(fillTexture);
        _fillSprite.setSize(_borderSprite.getWidth(), _borderSprite.getHeight());
        _fillSpriteOriginalWidth = _fillSprite.getWidth();
        _fillSprite.setSize(_borderSprite.getWidth() * startingValue, _borderSprite.getHeight());
        _fillSprite.setOriginCenter();
        _fillSprite.setPosition(_borderSprite.getX(), _borderSprite.getY());
        _fillSpriteOriginalX = _fillSprite.getX();
        _fillSprite.setAlpha(.8f);
        _dial = new GameButtons(controlTexture, true, new Vector2().setZero());
        _dial.setSize(size.y, size.y);
        _dial.setOriginCenter();
        _dial.setPosition(_fillSprite.getX() + _fillSprite.getWidth() - _dial.getOriginX(), _borderSprite.getY());
        _currentValue = _fillSprite.getWidth() / _fillSpriteOriginalWidth;
        _fingerCurrentlyToucing = 0;
        _fingerStillDown = false;
        _touched = false;
    }

    public void Update(boolean otherRockerActive, boolean thirdRockerActive)
    {
        _dial.Update();
        if(GetFingerCurrentlyTouching(_dial.getBoundingRectangle()))
        {
            _touched = true;
            _fingerStillDown = true;
        }
        else {
            _touched = false;
        }
        if(!Gdx.input.isTouched(_fingerCurrentlyToucing) && !_touched)
        {
            _fingerStillDown = false;
        }
        if(otherRockerActive || thirdRockerActive)
        {
            _fingerStillDown = false;
            _touched = false;
        }
        if(_fingerStillDown)
        {
            if(_dial.getX() + _dial.getOriginX() >= _fillSprite.getX() && _dial.getX() + _dial.getOriginX() <= _fillSprite.getX() + _fillSpriteOriginalWidth) {
                Vector2 newDialPosition = GestureManager.DragLocation(_dial.getWidth(), _dial.getHeight(), 1);
                _dial.setX(newDialPosition.x);
                _fillSprite.setSize(_dial.getX() + _dial.getOriginX() - _fillSprite.getX(), _fillSprite.getHeight());
                _fillSprite.setX(_fillSpriteOriginalX);
                _currentValue = _fillSprite.getWidth() / _fillSpriteOriginalWidth;
            }
        }
        if(_dial.getX() + _dial.getOriginX() > _fillSprite.getX() + _fillSpriteOriginalWidth)
        {
            _dial.setX(_fillSprite.getX() + _fillSpriteOriginalWidth - _dial.getOriginX());
            _fillSprite.setSize(_dial.getX() + _dial.getOriginX() - _fillSprite.getX(), _fillSprite.getHeight());
            _fillSprite.setX(_fillSpriteOriginalX);
            _currentValue = _fillSprite.getWidth() / _fillSpriteOriginalWidth;
        }
        else if(_dial.getX() + _dial.getOriginX() < _fillSprite.getX())
        {
            _dial.setX(_fillSprite.getX() - _dial.getOriginX());
            _fillSprite.setSize(_dial.getX() + _dial.getOriginX() - _fillSprite.getX(), _fillSprite.getHeight());
            _fillSprite.setX(_fillSpriteOriginalX);
            _currentValue = _fillSprite.getWidth() / _fillSpriteOriginalWidth;
        }
    }

    public boolean GetFingerCurrentlyTouching(Rectangle boundingBox) {
        int[] currentFingersDown = GestureManager.CurrentFingersDown();
        boolean foundFinger = false;
        for (int i = 0; i < currentFingersDown.length; i++) {
            if (GestureManager.IsCurrentlyBeingTouchedIndex(boundingBox, 1, currentFingersDown[i])) {
                _fingerCurrentlyToucing = currentFingersDown[i];
                foundFinger = true;
                break;
            }
        }
        return foundFinger;
    }

    public void Draw(SpriteBatch batch)
    {
        _fillSprite.draw(batch);
        _borderSprite.draw(batch);
        _dial.draw(batch);
    }
}
