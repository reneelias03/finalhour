package com.AndroidGameAlpha.game;

import android.view.ContextThemeWrapper;
import android.widget.Button;
import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.common.GooglePlayServicesClient;
import org.w3c.dom.Text;

import java.security.Key;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import java.lang.String.*;
import java.util.Enumeration;

/**
 * Created by rene__000 on 8/21/2014.
 */
public class MenuScreen implements Screen {
    MyGdxGame game;
    GameButtons playButton, highScoresButton, difficultyButton, optionsButton, achievementsButton, customizerButton, skinButton, PCButton, infoButton, noAdsButton, restoreAdsButton;
    Sprite backgroundSprite[], explosionSprite;
    BitmapFont portFont, goodTimeFont;
    float calibrationX, speedGrowth, elapsedAsteroidTime, asteroidMax, timeElapsedButton;
    Texture emptyButtonTemplate;
    ArrayList<Asteroid> asteroids;
    Random random;
    boolean purchaseSucces, noAdButtonTapped, initialQuery, backPressed, previousBackPressed, mhelperSetup;
    int backPresses;

//    BitmapFont font;

    public MenuScreen(MyGdxGame game) {
        this.portFont = game.portFont;
        this.goodTimeFont = game.goodTimeFont;
        this.game = game;

        emptyButtonTemplate = game.manager.get("data/EmptyButtonTemplate.png", Texture.class);
        Gdx.input.setCatchBackKey(false);

        playButton = new GameButtons(emptyButtonTemplate, false, new Vector2().setZero());
        playButton.setSize(playButton.getWidth() * game.screenScale, playButton.getHeight() * game.screenScale);
        playButton.setOriginCenter();
        playButton.setColor(.28f, .43f, .68f, 1);
        playButton.setPosition(Gdx.graphics.getWidth() * 2 / 5 - playButton.getOriginX(), Gdx.graphics.getHeight() / 2 - playButton.getOriginY());

//        calibrateButton = new GameButtons(this.game.manager.get("data/Calibrate.png", Texture.class), false, new Vector2().setZero());
//        calibrateButton.setSize(calibrateButton.getWidth() * game.screenScale, calibrateButton.getHeight() * game.screenScale);
//        calibrateButton.setOriginCenter();
//        calibrateButton.setPosition(playButton.getX(), playButton.getY() - playButton.getHeight() * 1.5f);
//        calibrationX = 0;

        customizerButton = new GameButtons(emptyButtonTemplate, false, new Vector2().setZero());
        customizerButton.setSize(customizerButton.getWidth() * game.screenScale, customizerButton.getHeight() * game.screenScale);
        customizerButton.setOriginCenter();
//        customizerButton.setColor(.28f, .43f, .68f, 1);
//        customizerButton.setColor(.25f, .43f, .25f, 1);
//        customizerButton.setColor(Color.ORANGE);
        customizerButton.setPosition(playButton.getX(), playButton.getY() - playButton.getHeight() * 1.5f);

        highScoresButton = new GameButtons(emptyButtonTemplate, false, new Vector2().setZero());
        highScoresButton.setSize(highScoresButton.getWidth() * game.screenScale, highScoresButton.getHeight() * game.screenScale);
        highScoresButton.setOriginCenter();
//        highScoresButton.setColor(.28f, .43f, .68f, 1);
        highScoresButton.setColor(.25f, .43f, .25f, 1);
        highScoresButton.setPosition(customizerButton.getX(), customizerButton.getY() - playButton.getHeight() * 1.5f);

        difficultyButton = new GameButtons(emptyButtonTemplate, false, new Vector2().setZero());
        difficultyButton.setSize(difficultyButton.getWidth() * game.screenScale, difficultyButton.getHeight() * game.screenScale);
        difficultyButton.setOriginCenter();
        if (game.prefs.getBoolean("Easy")) {
            difficultyButton.setColor(Color.GREEN);
        } else if (game.prefs.getBoolean("Normal")) {
            difficultyButton.setColor(Color.ORANGE);
        } else {
            difficultyButton.setColor(Color.RED);
        }
        difficultyButton.setPosition(Gdx.graphics.getWidth() * 3 / 5 - playButton.getOriginX(), playButton.getY());

        optionsButton = new GameButtons(emptyButtonTemplate, false, new Vector2().setZero());
        optionsButton.setSize(optionsButton.getWidth() * game.screenScale, optionsButton.getHeight() * game.screenScale);
        optionsButton.setOriginCenter();
//        optionsButton.setColor(.28f, .43f, .68f, 1);
//        optionsButton.setColor(.25f, .43f, .25f, 1);
        optionsButton.setPosition(difficultyButton.getX(), difficultyButton.getY() - playButton.getHeight() * 1.5f);

        achievementsButton = new GameButtons(emptyButtonTemplate, false, new Vector2().setZero());
        achievementsButton.setSize(achievementsButton.getWidth() * game.screenScale, achievementsButton.getHeight() * game.screenScale);
        achievementsButton.setOriginCenter();
//        achievementsButton.setColor(.28f, .43f, .68f, 1);
        achievementsButton.setColor(.25f, .43f, .25f, 1);
        achievementsButton.setPosition(optionsButton.getX(), optionsButton.getY() - playButton.getHeight() * 1.5f);

        infoButton = new GameButtons(game.manager.get("data/SquareButton.png", Texture.class), false, new Vector2().setZero());
        infoButton.setSize(achievementsButton.getHeight(),achievementsButton.getHeight());
        infoButton.setOriginCenter();
        infoButton.setPosition(achievementsButton.getX() + achievementsButton.getWidth() * 2f, achievementsButton.getY());

        noAdsButton = new GameButtons(game.manager.get("data/SquareButton.png", Texture.class), false, new Vector2().setZero());
        noAdsButton.setSize(achievementsButton.getHeight(), achievementsButton.getHeight());
        noAdsButton.setOriginCenter();
        noAdsButton.setPosition(achievementsButton.getWidth() * .3f, achievementsButton.getY());

        restoreAdsButton = new GameButtons(game.manager.get("data/SquareButton.png", Texture.class), false, new Vector2().setZero());
        restoreAdsButton.setSize(achievementsButton.getHeight(), achievementsButton.getHeight());
        restoreAdsButton.setOriginCenter();
        restoreAdsButton.setPosition(noAdsButton.getX(), noAdsButton.getY() + noAdsButton.getHeight() * 1.5f);

        skinButton = new GameButtons(emptyButtonTemplate, false, new Vector2().setZero());
        skinButton.setSize(skinButton.getWidth() * game.screenScale, skinButton.getHeight() * game.screenScale);
        skinButton.setOriginCenter();
//        skinButton.setColor(.25f, .43f, .25f, 1);
        skinButton.setPosition(achievementsButton.getX() + achievementsButton.getWidth() * 1.5f, difficultyButton.getY());

        PCButton = new GameButtons(emptyButtonTemplate, false, new Vector2().setZero());
        PCButton.setSize(PCButton.getWidth() * game.screenScale, PCButton.getHeight() * game.screenScale);
        PCButton.setOriginCenter();
//        skinButton.setColor(.25f, .43f, .25f, 1);
        PCButton.setPosition(highScoresButton.getX() - highScoresButton.getWidth() * 1.5f, difficultyButton.getY());

        backgroundSprite = new Sprite[2];
        for (int i = 0; i < backgroundSprite.length; i++) {
            backgroundSprite[i] = new Sprite(this.game.manager.get("data/SpaceSquare.png", Texture.class));
            backgroundSprite[i].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
            if (i == 0) {
                backgroundSprite[i].setPosition(0, 0);
            } else {
                backgroundSprite[i].setPosition(0, backgroundSprite[i - 1].getY() - backgroundSprite[i].getHeight());
            }
        }

        asteroids = new ArrayList<Asteroid>();
        random = new Random();

        asteroidMax = 8;
        elapsedAsteroidTime = 0;

        game.blackSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        game.blackSprite.setPosition(0, 0);
        game.blackSprite.setAlpha(.1f);
//        font = new BitmapFont(Gdx.files.internal("poop.fnt"), Gdx.files.internal("poop.png"), false);
//        font.setColor(Color.ORANGE);

//        explosionSprite = new Sprite(this.game.manager.get("data/CosmicExplosionCut.png", Texture.class));
//        explosionSprite.setOriginCenter();
//        explosionSprite.setPosition(game.screenWidth / 2 - explosionSprite.getOriginX(), game.screenHeight / 2 - explosionSprite.getOriginY());
//        explosionSprite.setAlpha(.85f);
        if (game.prefs.getBoolean("Sound") && !game.menuSong.isPlaying()) {
            game.menuSong.play();
            game.menuSong.setVolume(.55f * game.prefs.getFloat("MusicVolumeLevel"));
            game.menuSong.setLooping(true);
        }
//        game.prefs.putBoolean("PremiumVersion", false);
//        game.prefs.flush();
        game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
        purchaseSucces = false;



        noAdButtonTapped = false;
        timeElapsedButton = 0;
        initialQuery = false;
        mhelperSetup = false;
        backPresses = 0;
    }

    @Override
    public void render(float delta) {

        //region ButtonUpdating
        playButton.Update();
        highScoresButton.Update();
        difficultyButton.Update();
        optionsButton.Update();
        achievementsButton.Update();
        customizerButton.Update();
//        PCButton.Update();
        infoButton.Update();
//        skinButton.Update();

        backPressed = Gdx.input.isKeyPressed(Input.Keys.BACK);

        if(!backPressed && previousBackPressed) {
            Gdx.input.setCatchBackKey(false);
        }
        if(game.actionResolver.mhelperSetupFinished() && !mhelperSetup) {
            game.actionResolver.queryInventory();
            initialQuery = true;
            mhelperSetup = true;
        }

        if(initialQuery) {
//            if(timeElapsedButton == 0f) {
//                game.actionResolver.queryInventory();
//            }
            timeElapsedButton += Gdx.graphics.getDeltaTime();
            if(timeElapsedButton > 1.5f) {
                if(game.actionResolver.returnPurchaseSuccess()) {
                    game.prefs.putBoolean("PremiumVersion", true);
                    game.prefs.flush();
                } else {
                    game.prefs.putBoolean("PremiumVersion", false);
                    game.prefs.flush();
                }

                timeElapsedButton = 0;
                initialQuery = false;
            }
            game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
        }
        if(!initialQuery) {
            if (!game.prefs.getBoolean("PremiumVersion")) {
                noAdsButton.Update();
                if (noAdsButton.Tapped()) {
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    game.actionResolver.purchaseCoinItem(3);
                    noAdButtonTapped = true;
                }
                if (noAdButtonTapped) {
                    timeElapsedButton += Gdx.graphics.getDeltaTime();
                }
                if (timeElapsedButton > 1.5f) {
                    noAdButtonTapped = false;
                    timeElapsedButton = 0;
                    game.actionResolver.queryInventory();
                }
                purchaseSucces = game.actionResolver.returnPurchaseSuccess();
                if (purchaseSucces) {
                    game.prefs.putBoolean("PremiumVersion", true);
                    game.prefs.putInteger("Coins", game.prefs.getInteger("Coins") + 2000);
                    game.prefs.flush();
                    game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
            if(game.prefs.getBoolean("Sound")) {
                game.cashRegisterSound.play(.45f * game.prefs.getFloat("SfxVolumeLevel"));
            }
                }
            }
//            else {
//                restoreAdsButton.Update();
//                if (restoreAdsButton.Tapped()) {
//                    game.actionResolver.restoreAds();
//                    if (game.prefs.getBoolean("Vibrate")) {
//                        Gdx.input.vibrate(150);
//                    }
//                }
//                if (game.actionResolver.adsRestored()) {
//                    game.prefs.putBoolean("PremiumVersion", false);
//                    game.prefs.flush();
//                    game.handler.showAds(!game.prefs.getBoolean("PremiumVersion"));
//                }
//            }
        }

        if(Gdx.input.isKeyPressed(Input.Keys.P))
        {
            game.setScreen(new ShopScreen(game));
            this.dispose();
        }

        if(Gdx.input.isKeyPressed(Input.Keys.C))
        {
            game.setScreen(new CustomizePortal(game));
            this.dispose();
        }

        if (asteroids.size() < asteroidMax && elapsedAsteroidTime >= 1) {
            asteroids.add(new Asteroid(game.manager.get("data/AsteroidRedone.png", Texture.class), new Vector2(random.nextInt(Gdx.graphics.getWidth()), random.nextInt(Gdx.graphics.getHeight())), .45f * game.screenScale, 5 * game.screenScale, 1, new Vector2().setZero(), new TextureAtlas()));
            Vector2 asteroidSpeed = asteroids.get(asteroids.size() - 1).Speed();
            asteroids.get(asteroids.size() - 1)._active = true;
            int multiplier = random.nextInt(2) + 1;
            asteroidSpeed.nor();
            asteroidSpeed.x *= multiplier * game.screenScale;
            asteroidSpeed.y *= multiplier * game.screenScale;
            asteroids.get(asteroids.size() - 1).SetSpeed(asteroidSpeed);
            asteroids.get(asteroids.size() - 1).RotationSpeed(random.nextInt(2) + 1);
            if (multiplier == 1) {
                asteroids.get(asteroids.size() - 1).over = true;
            } else {
                asteroids.get(asteroids.size() - 1).over = false;
            }
            elapsedAsteroidTime = 0;
        }

        if (asteroids.size() > 0) {
            for (int i = 0; i < asteroids.size(); i++) {
                asteroids.get(i).Update(1, null, null, null, game.prefs.getBoolean("Vibrate"), game.SmallExplosionSound, game.prefs.getBoolean("Sound"), Gdx.graphics.getDeltaTime(), game.prefs.getFloat("SfxVolumeLevel"));
                if (asteroids.get(i).OutOfBounds()) {
                    asteroids.remove(i);
                }
            }
        }

        if (playButton.Tapped()) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            game.menuSong.stop();
            if(game.prefs.getBoolean("PC")) {
                game.manager.load("data/Crosshair.png", Texture.class);
            }
            if(game.prefs.getBoolean("PixelSkin")) {
                game.manager.load("data/ExplosionSpritesMosaic-packed/pack.atlas", TextureAtlas.class);
                game.manager.load("data/CosmicExplosion15.png", Texture.class);
            } else {
                game.manager.load("data/ExplosionSprites-packed/pack.atlas", TextureAtlas.class);
                game.manager.load("data/CosmicExplosionCutLessBlack.png", Texture.class);
            }
            game.manager.load("data/RealEarthLaserAnimationThick-packed/pack.atlas", TextureAtlas.class);
            game.manager.load("data/SpeedPowerup-packed/pack.atlas", TextureAtlas.class);
            game.manager.load("data/GameInventory.png", Texture.class);
            game.manager.load("data/SlideIndicators.png", Texture.class);
            game.manager.finishLoading();
            game.setScreen(new GameScreen(game, calibrationX));
            this.dispose();
        }

        if (highScoresButton.Tapped()) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            game.setScreen(new HighScoresScreen(game));
            this.dispose();

        }

        if (achievementsButton.Tapped()) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            game.actionResolver.getAchievementsGPGS();
            this.dispose();
        }

//        if(calibrateButton.Tapped())
//        {
//            calibrationX = Gdx.input.getAccelerometerX();
//            if(game.prefs.getBoolean("Vibrate")) {
//                Gdx.input.vibrate(150);
//            }
//            if(game.prefs.getBoolean("Sound"))
//            {
//                game.MenuButtonSound.play();
//            }
//        }


        if (difficultyButton.Tapped()) {
            if (game.prefs.getBoolean("Normal")) {
                difficultyButton.setColor(Color.RED);
                game.prefs.putBoolean("Normal", false);
                game.prefs.putBoolean("Hard", true);
                game.prefs.flush();
            } else if (game.prefs.getBoolean("Hard")) {
                difficultyButton.setColor(Color.GREEN);
                game.prefs.putBoolean("Hard", false);
                game.prefs.putBoolean("Easy", true);
                game.prefs.flush();
            } else if (game.prefs.getBoolean("Easy")) {
                difficultyButton.setColor(Color.ORANGE);
                game.prefs.putBoolean("Easy", false);
                game.prefs.putBoolean("Normal", true);
                game.prefs.flush();
            }
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
        }

        if (customizerButton.Tapped()) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            if(!game.prefs.getBoolean("PixelSkin")) {
                game.manager.load("data/PixelSpace.png", Texture.class);
                game.manager.load("data/EmptyCirclePixel.png", Texture.class);
                game.manager.load("data/ContinentsPixel.png", Texture.class);
                game.manager.load("data/MoonPixelNew11.png", Texture.class);
                game.manager.load("data/PixelAsteroidRedone14.png", Texture.class);
            }

            if(!game.prefs.getBoolean("NinjaSkin")) {
                game.manager.load("data/NinjaBackground1920.png", Texture.class);
                game.manager.load("data/NinjaSkinProto.png", Texture.class);
                game.manager.load("data/ShurikenRedone.png", Texture.class);
                game.manager.load("data/NinjaHatSmall.png", Texture.class);
            }
            game.manager.finishLoading();
            game.setScreen(new CustomizePortal(game));
            this.dispose();
        }

        if (optionsButton.Tapped()) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            game.manager.load("data/VolumeSliderBack.png", Texture.class);
            game.manager.load("data/VolumeSliderFill.png", Texture.class);
            game.manager.load("data/VolumeRockerControl.png", Texture.class);
            game.manager.finishLoading();
            game.setScreen(new OptionsScreen(game, null));
            this.dispose();
        }

        if (infoButton.Tapped()) {
            if (game.prefs.getBoolean("Vibrate")) {
                Gdx.input.vibrate(150);
            }
            if (game.prefs.getBoolean("Sound")) {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            game.setScreen(new DescriptionPage(game));
            this.dispose();
        }

//        if(PCButton.Tapped()) {
//            game.prefs.putBoolean("PC", !game.prefs.getBoolean("PC"));
//            game.prefs.flush();
//        }

        previousBackPressed = backPressed;
//        if (skinButton.Tapped()) {
//            if (game.prefs.getBoolean("Vibrate")) {
//                Gdx.input.vibrate(150);
//            }
//            if (game.prefs.getBoolean("Sound")) {
//                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
//            }
//            if (game.prefs.getBoolean("DefaultSkin")) {
//                game.prefs.putBoolean("DefaultSkin", false);
//                game.prefs.putBoolean("PixelSkin", true);
//                game.prefs.putBoolean("NinjaSkin", false);
//                game.prefs.putBoolean("CatSkin", false);
//                game.prefs.putBoolean("PirateSkin", false);
//                game.prefs.putBoolean("SoccerSkin", false);
//            } else if (game.prefs.getBoolean("PixelSkin")) {
//                game.prefs.putBoolean("DefaultSkin", false);
//                game.prefs.putBoolean("PixelSkin", false);
//                game.prefs.putBoolean("NinjaSkin", true);
//                game.prefs.putBoolean("CatSkin", false);
//                game.prefs.putBoolean("PirateSkin", false);
//                game.prefs.putBoolean("SoccerSkin", false);
//            } else if (game.prefs.getBoolean("NinjaSkin")) {
//                game.prefs.putBoolean("DefaultSkin", false);
//                game.prefs.putBoolean("PixelSkin", false);
//                game.prefs.putBoolean("NinjaSkin", false);
//                game.prefs.putBoolean("CatSkin", true);
//                game.prefs.putBoolean("PirateSkin", false);
//                game.prefs.putBoolean("SoccerSkin", false);
//            } else if(game.prefs.getBoolean("CatSkin")) {
//                game.prefs.putBoolean("DefaultSkin", false);
//                game.prefs.putBoolean("PixelSkin", false);
//                game.prefs.putBoolean("NinjaSkin", false);
//                game.prefs.putBoolean("CatSkin", false);
//                game.prefs.putBoolean("PirateSkin", true);
//                game.prefs.putBoolean("SoccerSkin", false);
//            } else if(game.prefs.getBoolean("PirateSkin")) {
//                game.prefs.putBoolean("DefaultSkin", false);
//                game.prefs.putBoolean("PixelSkin", false);
//                game.prefs.putBoolean("NinjaSkin", false);
//                game.prefs.putBoolean("CatSkin", false);
//                game.prefs.putBoolean("PirateSkin", false);
//                game.prefs.putBoolean("SoccerSkin", true);
//            } else {
//                game.prefs.putBoolean("DefaultSkin", true);
//                game.prefs.putBoolean("PixelSkin", false);
//                game.prefs.putBoolean("NinjaSkin", false);
//                game.prefs.putBoolean("CatSkin", false);
//                game.prefs.putBoolean("PirateSkin", false);
//                game.prefs.putBoolean("SoccerSkin", false);
//            }
//
//            game.prefs.flush();
//        }


        elapsedAsteroidTime += Gdx.graphics.getDeltaTime();

        //endregion

        //region BackgroundScrolling
//        for (Sprite background : backgroundSprite) {
//            background.setPosition(background.getX(), background.getY() - 1 * game.screenScale);
//            if (background.getY() + background.getHeight() < 0) {
//                background.setPosition(background.getX(), Gdx.graphics.getHeight());
//            }
//        }
        //endregion

//        explosionSprite.setSize(explosionSprite.getWidth() + 20, explosionSprite.getHeight()+20);
//        explosionSprite.setOriginCenter();
//        explosionSprite.setPosition(game.screenWidth / 2 - explosionSprite.getOriginX(), game.screenHeight / 2 - explosionSprite.getOriginY());
        game.batch.begin();
        for (Sprite background : backgroundSprite) {
            background.draw(game.batch);
        }
        for (Asteroid asteroid : asteroids) {
            if (!asteroid.over) {
                asteroid.draw(game.batch);
            }
        }
        portFont.setColor(Color.WHITE);
        portFont.setScale(game.screenScale);
        portFont.draw(game.batch, String.format("FINAL HOUR"), Gdx.graphics.getWidth() / 2 - 550 * game.screenScale, Gdx.graphics.getHeight() / 2 + portFont.getLineHeight() * 2.5f);
        for (Asteroid asteroid : asteroids) {
            if (asteroid.over) {
                asteroid.draw(game.batch);
            }
        }
//        game.blackSprite.draw(game.batch);
//        explosionSprite.draw(game.batch);
        optionsButton.draw(game.batch);
        playButton.draw(game.batch);
        highScoresButton.draw(game.batch);
        difficultyButton.draw(game.batch);
        achievementsButton.draw(game.batch);
        customizerButton.draw(game.batch);
//        PCButton.draw(game.batch);
        infoButton.draw(game.batch);
//        skinButton.draw(game.batch);
        if(!game.prefs.getBoolean("PremiumVersion")) {
            noAdsButton.draw(game.batch);
        }
//        else {
//            restoreAdsButton.draw(game.batch);
//        }
        TextDrawing();
        game.batch.end();
    }

    private void TextDrawing() {
        goodTimeFont.setScale(game.screenScale * .6f);
        goodTimeFont.setColor(Color.WHITE);
        goodTimeFont.draw(game.batch, String.format("PLAY"), playButton.getX() + 17 * game.screenScale, playButton.getY() + goodTimeFont.getLineHeight() - 7 * game.screenScale);
        goodTimeFont.draw(game.batch, "?", infoButton.getX() + 30f * game.screenScale, infoButton.getY() + goodTimeFont.getLineHeight() - 10f * game.screenScale);

        if (game.prefs.getBoolean("Easy")) {
            goodTimeFont.draw(game.batch, "Easy", difficultyButton.getX() + 14 * game.screenScale, difficultyButton.getY() + goodTimeFont.getLineHeight() - 7 * game.screenScale);
        } else if (game.prefs.getBoolean("Hard")) {
            goodTimeFont.draw(game.batch, "Hard", difficultyButton.getX() + 12 * game.screenScale, difficultyButton.getY() + goodTimeFont.getLineHeight() - 7 * game.screenScale);
        } else {
            goodTimeFont.setScale(game.screenScale * .39f);
            goodTimeFont.draw(game.batch, "Normal", difficultyButton.getX() + 10 * game.screenScale, difficultyButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);
        }


        if(!game.prefs.getBoolean("PremiumVersion")) {

            goodTimeFont.setScale(game.screenScale * .37f);
            goodTimeFont.setColor(Color.WHITE);
            goodTimeFont.draw(game.batch, "Ad", noAdsButton.getX() + 11f * game.screenScale, infoButton.getY() + goodTimeFont.getLineHeight() + 15f * game.screenScale);

            goodTimeFont.setScale(game.screenScale * .6f);
            goodTimeFont.setColor(1, 0, 0, .5f);
            goodTimeFont.draw(game.batch, "X", noAdsButton.getX() + 20f * game.screenScale, infoButton.getY() + goodTimeFont.getLineHeight() - 12.5f * game.screenScale);
            goodTimeFont.setColor(Color.WHITE);
        }
//        else {
//            goodTimeFont.setScale(game.screenScale * .37f);
//            goodTimeFont.setColor(Color.WHITE);
//            goodTimeFont.draw(game.batch, "Ad", restoreAdsButton.getX() + 11f * game.screenScale, restoreAdsButton.getY() + goodTimeFont.getLineHeight() + 15f * game.screenScale);
//        }

        goodTimeFont.setScale(game.screenScale * .3f);
        goodTimeFont.draw(game.batch, "Customize", customizerButton.getX() + 12 * game.screenScale, customizerButton.getY() + goodTimeFont.getLineHeight() + 28 * game.screenScale);
        goodTimeFont.setScale(game.screenScale * .39f);
        goodTimeFont.draw(game.batch, "Options", optionsButton.getX() + 12.5f * game.screenScale, optionsButton.getY() + goodTimeFont.getLineHeight() + 17 * game.screenScale);
        goodTimeFont.setScale(game.screenScale * .218f);
        goodTimeFont.draw(game.batch, "Achievements", achievementsButton.getX() + 14f * game.screenScale, achievementsButton.getY() + goodTimeFont.getLineHeight() + 37 * game.screenScale);
        goodTimeFont.setScale(game.screenScale * .26f);
        goodTimeFont.draw(game.batch, "High Scores", highScoresButton.getX() + 13.5f * game.screenScale, highScoresButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
//        if (game.prefs.getBoolean("DefaultSkin")) {
//            goodTimeFont.draw(game.batch, "Def Skin", skinButton.getX() + 13.5f * game.screenScale, skinButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
//        } else if (game.prefs.getBoolean("PixelSkin")) {
//            goodTimeFont.draw(game.batch, "Pixel Skin", skinButton.getX() + 13.5f * game.screenScale, skinButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
//        } else if (game.prefs.getBoolean("NinjaSkin")) {
//            goodTimeFont.draw(game.batch, "Ninja Skin", skinButton.getX() + 13.5f * game.screenScale, skinButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
//        } else if (game.prefs.getBoolean("CatSkin")) {
//            goodTimeFont.draw(game.batch, "Cat Skin", skinButton.getX() + 13.5f * game.screenScale, skinButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
//        } else if (game.prefs.getBoolean("PirateSkin")) {
//            goodTimeFont.draw(game.batch, "Pir Skin", skinButton.getX() + 13.5f * game.screenScale, skinButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
//        } else {
//            goodTimeFont.draw(game.batch, "Socc Skin", skinButton.getX() + 13.5f * game.screenScale, skinButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
//        }


//        if(game.prefs.getBoolean("PC")) {
//            goodTimeFont.draw(game.batch, "PC", PCButton.getX() + 13.5f * game.screenScale, PCButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
//        } else {
//            goodTimeFont.draw(game.batch, "NOT PC", PCButton.getX() + 13.5f * game.screenScale, PCButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
//        }

        //region portFontButtons
//        portFont.setColor(Color.WHITE);
//        portFont.setScale(game.screenScale);
//        portFont.draw(game.batch, String.format("FINAL HOUR"), Gdx.graphics.getWidth() / 2 - 550 * game.screenScale, Gdx.graphics.getHeight() / 2 + portFont.getLineHeight() * 2.5f);
//        portFont.setScale(game.screenScale * .6f);
//        portFont.draw(game.batch, String.format("PLAY"), playButton.getX() + 30 * game.screenScale, playButton.getY() + portFont.getLineHeight() + 18 * game.screenScale);
//        if(game.prefs.getBoolean("Easy")) {
//            portFont.draw(game.batch, "Easy", difficultyButton.getX() + 21 * game.screenScale, difficultyButton.getY() + portFont.getLineHeight() + 18 * game.screenScale);
//        }else if(game.prefs.getBoolean("Hard"))
//        {
//            portFont.draw(game.batch, "Hard", difficultyButton.getX() + 21 * game.screenScale, difficultyButton.getY() + portFont.getLineHeight() + 18 * game.screenScale);
//        }else{
//            portFont.setScale(game.screenScale * .44f);
//            portFont.draw(game.batch, "Normal", difficultyButton.getX() + 7 * game.screenScale, difficultyButton.getY() + portFont.getLineHeight() + 30 * game.screenScale);
//        }
//        portFont.setScale(game.screenScale * .314f);
//        portFont.draw(game.batch, "Customize", customizerButton.getX() + 7 * game.screenScale, customizerButton.getY() + portFont.getLineHeight() + 38 * game.screenScale);
//        portFont.setScale(game.screenScale * .42f);
//        portFont.draw(game.batch, "Options", optionsButton.getX() + 7 * game.screenScale, optionsButton.getY() + portFont.getLineHeight() + 30 * game.screenScale);
//        portFont.setScale(game.screenScale * .23f);
//        portFont.draw(game.batch, "Achievements", achievementsButton.getX() + 7 * game.screenScale, achievementsButton.getY() + portFont.getLineHeight() + 45 * game.screenScale);
//        portFont.setScale(game.screenScale * .2665f);
//        portFont.draw(game.batch, "High Scores", highScoresButton.getX() + 7 * game.screenScale, highScoresButton.getY() + portFont.getLineHeight() + 42 * game.screenScale);
        //endregion

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
