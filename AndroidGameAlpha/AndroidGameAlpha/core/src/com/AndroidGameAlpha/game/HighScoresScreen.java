package com.AndroidGameAlpha.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rene__000 on 9/5/2014.
 */
public class HighScoresScreen implements Screen {

    MyGdxGame game;
    BitmapFont font, goodTimeFont;
    GameButtons menuButton, hardButton, easyButton, normalButton;
    Sprite backgroundSprite[], whitePixel;
    Texture emptyButtonTemplate;

    public HighScoresScreen(MyGdxGame game)
    {
        this.game = game;
        font = new BitmapFont(Gdx.files.internal("data/poop.fnt"), Gdx.files.internal("data/poop.png"), false);
        font.setColor(Color.WHITE);
        this.goodTimeFont = game.goodTimeFont;
        goodTimeFont.setColor(Color.WHITE);
        emptyButtonTemplate = game.manager.get("data/EmptyButtonTemplate.png", Texture.class);
        Gdx.input.setCatchBackKey(true);

        menuButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        menuButton.setSize(menuButton.getWidth() * game.screenScale, menuButton.getHeight() * game.screenScale);
        menuButton.setOriginCenter();
        menuButton.setPosition( menuButton.getOriginX(), menuButton.getHeight() * .5f);

        easyButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        easyButton.setSize(easyButton.getWidth() * game.screenScale, easyButton.getHeight() * game.screenScale);
        easyButton.setOriginCenter();
        easyButton.setPosition( Gdx.graphics.getWidth() / 4 - easyButton.getOriginX(), Gdx.graphics.getHeight() / 2 + easyButton.getOriginY());
//        easyButton.setColor(1, .8f, 0, 1);

        normalButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        normalButton.setSize(normalButton.getWidth() * game.screenScale, normalButton.getHeight() * game.screenScale);
        normalButton.setOriginCenter();
        normalButton.setPosition( Gdx.graphics.getWidth() / 2 - normalButton.getOriginX(), Gdx.graphics.getHeight() / 2 + normalButton.getOriginY());
//        normalButton.setColor(1, .8f, 0, 1);

        hardButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        hardButton.setSize(hardButton.getWidth() * game.screenScale, hardButton.getHeight() * game.screenScale);
        hardButton.setOriginCenter();
        hardButton.setPosition( Gdx.graphics.getWidth() * 3 / 4 - hardButton.getOriginX(), Gdx.graphics.getHeight() / 2 + hardButton.getOriginY());
//        hardButton.setColor(1, .8f, 0, 1);

        backgroundSprite = new Sprite[2];
        for (int i = 0; i < backgroundSprite.length; i++) {
            backgroundSprite[i] = new Sprite(this.game.manager.get("data/SpaceSquare.png", Texture.class));
            backgroundSprite[i].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
            if (i == 0) {
                backgroundSprite[i].setPosition(0, 0);
            } else {
                backgroundSprite[i].setPosition(0, backgroundSprite[i - 1].getY() - backgroundSprite[i].getHeight());
            }
        }
        whitePixel = new Sprite(game.manager.get("data/WhitePixel.png", Texture.class));
        whitePixel.setSize(Gdx.graphics.getWidth(), 80 * game.screenScale);
        whitePixel.setPosition(0, Gdx.graphics.getHeight() - 222.5f * game.screenScale);
        whitePixel.setAlpha(.35f);
    }

    @Override
    public void render(float delta) {

        menuButton.Update();
        easyButton.Update();
        normalButton.Update();
        hardButton.Update();

        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE) || menuButton.Tapped() || game.BackPressed()) {
            if(game.prefs.getBoolean("Vibrate"))
            {
                Gdx.input.vibrate(150);
            }
            if(game.prefs.getBoolean("Sound"))
            {
                game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
            }
            game.setScreen(new MenuScreen(game));
            this.dispose();
        }
        if(easyButton.Tapped()) {
            if(game.prefs.getBoolean("Vibrate"))
            {
                Gdx.input.vibrate(150);
            }
            if (game.actionResolver.getSignedInGPGS()) {
                game.actionResolver.getLeaderboardGPGS(game.prefs.getString("EasyLeaderboard"));
            } else {
                game.actionResolver.loginGPGS();
            }
        }

        if(normalButton.Tapped()) {
            if(game.prefs.getBoolean("Vibrate"))
            {
                Gdx.input.vibrate(150);
            }
            if (game.actionResolver.getSignedInGPGS()) {
                game.actionResolver.getLeaderboardGPGS(game.prefs.getString("NormalLeaderboard"));
            } else {
                game.actionResolver.loginGPGS();
            }
        }

        if(hardButton.Tapped()) {
            if(game.prefs.getBoolean("Vibrate"))
            {
                Gdx.input.vibrate(150);
            }
            if (game.actionResolver.getSignedInGPGS()) {
                game.actionResolver.getLeaderboardGPGS(game.prefs.getString("HardLeaderboard"));
            } else {
                game.actionResolver.loginGPGS();
            }
        }


        game.batch.begin();
        for (Sprite background : backgroundSprite) {
            background.draw(game.batch);
        }

        menuButton.draw(game.batch);
        easyButton.draw(game.batch);
        normalButton.draw(game.batch);
        hardButton.draw(game.batch);
        whitePixel.draw(game.batch);
        TextDrawing();
        game.batch.end();
    }

    private void TextDrawing() {
        goodTimeFont.setScale(game.screenScale * .6f);
        goodTimeFont.setColor(1, .8f, 0, 1);
        goodTimeFont.draw(game.batch, "Easy", easyButton.getX() + 14 * game.screenScale, easyButton.getY() + goodTimeFont.getLineHeight() - 7 * game.screenScale);
        goodTimeFont.draw(game.batch, "Hard", hardButton.getX() + 12 * game.screenScale, hardButton.getY() + goodTimeFont.getLineHeight()  - 7 * game.screenScale);

        goodTimeFont.setScale(game.screenScale * .39f);
        goodTimeFont.draw(game.batch, "Normal", normalButton.getX() + 10 * game.screenScale, normalButton.getY() + goodTimeFont.getLineHeight() + 16 * game.screenScale);

        goodTimeFont.setScale(game.screenScale * .55f);
        goodTimeFont.draw(game.batch, String.format("Google Play Leaderboards:"), Gdx.graphics.getWidth() / 2 - 825 * game.screenScale, Gdx.graphics.getHeight() - 150 * game.screenScale);
        goodTimeFont.setColor(Color.WHITE);
        goodTimeFont.draw(game.batch, "Menu", menuButton.getX() + 25 * game.screenScale, menuButton.getY() + goodTimeFont.getLineHeight()  - 5 * game.screenScale);

        goodTimeFont.setColor(1, .8f, 0, 1);
        goodTimeFont.setScale(game.screenScale * .35f);
        goodTimeFont.draw(game.batch, String.format("Local High Scores:"), Gdx.graphics.getWidth() / 2 - 300 * game.screenScale, menuButton.getY() + goodTimeFont.getLineHeight() + 220 * game.screenScale);
        goodTimeFont.setColor(Color.WHITE);
        goodTimeFont.draw(game.batch, String.format("Easy High Score: %d", game.prefs.getInteger("EasyHighScore")), Gdx.graphics.getWidth() / 2 - 300 * game.screenScale, menuButton.getY() + goodTimeFont.getLineHeight() + 140 * game.screenScale);
        goodTimeFont.draw(game.batch, String.format("Normal High Score: %d", game.prefs.getInteger("NormalHighScore")), Gdx.graphics.getWidth() / 2 - 300 * game.screenScale, menuButton.getY() + goodTimeFont.getLineHeight() + 60 * game.screenScale);
        goodTimeFont.draw(game.batch, String.format("Hard High Score: %d", game.prefs.getInteger("HardHighScore")), Gdx.graphics.getWidth() / 2 - 300 * game.screenScale, menuButton.getY() + goodTimeFont.getLineHeight() - 20 * game.screenScale);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
