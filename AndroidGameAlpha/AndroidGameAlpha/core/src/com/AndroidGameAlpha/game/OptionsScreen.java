package com.AndroidGameAlpha.game;

import android.inputmethodservice.Keyboard;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rene__000 on 9/11/2014.
 */
public class OptionsScreen implements Screen {
    MyGdxGame game;
    GameButtons vibrateButton, soundButton, controlTypeButton, backButton, stickSizeButton, pauseInvButton;
    Sprite backgroundSprite[];
    VolumeRocker soundEffectsRocker, musicRocker, movementSpeedRocker;
    BitmapFont font, goodTimeFont;
    float movementRockerScale;
    boolean rockerActive, previousRockerActive, currentBackTouched, previousBackTouched;
    Texture emptyButtonTemplate;
    GameScreen gameScreen;

    public OptionsScreen(MyGdxGame game, GameScreen gameScreen) {
        this.game = game;
        goodTimeFont = game.goodTimeFont;
        goodTimeFont.setColor(Color.WHITE);
        Gdx.input.setCatchBackKey(true);
//        controlTypeButton = new GameButtons(this.game.manager.get("data/ControlTypeBButton.png", Texture.class), false, new Vector2().setZero());
//        controlTypeButton.setSize(controlTypeButton.getWidth() * game.screenScale, controlTypeButton.getHeight() * game.screenScale);
//        controlTypeButton.setOriginCenter();
//        controlTypeButton.setPosition(Gdx.graphics.getWidth() / 2 - controlTypeButton.getOriginX(),(Gdx.graphics.getHeight() * 6/ 10) - controlTypeButton.getOriginY());
//        if(!game.prefs.getBoolean("AccelControls"))
//        {
//            controlTypeButton.setTexture(this.game.manager.get("data/ControlTypeAButton.png", Texture.class));
//        }
        float movementSpeed = game.prefs.getFloat("MovementSpeed");
        emptyButtonTemplate = game.manager.get("data/EmptyButtonTemplate.png", Texture.class);

        movementRockerScale = (5 - movementSpeed + 10) / 10f;

        vibrateButton = new GameButtons(emptyButtonTemplate, false, new Vector2().setZero());
        vibrateButton.setSize(vibrateButton.getWidth() * game.screenScale, vibrateButton.getHeight() * game.screenScale);
        vibrateButton.setOriginCenter();
        vibrateButton.setPosition(Gdx.graphics.getWidth() * .33f - vibrateButton.getOriginX(), (Gdx.graphics.getHeight() * 3 / 4) - vibrateButton.getHeight());
        if (game.prefs.getBoolean("Vibrate")) {
            vibrateButton.setColor(.25f, .43f, .25f, 1);
        } else {
            vibrateButton.setColor(Color.RED);
        }

        soundButton = new GameButtons(emptyButtonTemplate, false, new Vector2().setZero());
        soundButton.setSize(soundButton.getWidth() * game.screenScale, soundButton.getHeight() * game.screenScale);
        soundButton.setOriginCenter();
        soundButton.setPosition(vibrateButton.getX(), vibrateButton.getY() - vibrateButton.getHeight() * 1.5f);
        if (game.prefs.getBoolean("Sound")) {
            soundButton.setColor(.25f, .43f, .25f, 1);
        } else {
            soundButton.setColor(Color.RED);
        }


        backButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        backButton.setSize(backButton.getWidth() * game.screenScale, backButton.getHeight() * game.screenScale);
        backButton.setOriginCenter();
        backButton.setPosition(Gdx.graphics.getWidth() - backButton.getWidth() - backButton.getOriginX() / 2, backButton.getHeight() / 2);

        stickSizeButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        stickSizeButton.setSize(stickSizeButton.getWidth() * game.screenScale, stickSizeButton.getHeight() * game.screenScale);
        stickSizeButton.setOriginCenter();
        stickSizeButton.setPosition(Gdx.graphics.getWidth() * .66f - stickSizeButton.getOriginX(), vibrateButton.getY());
        if (game.prefs.getBoolean("SmallStick")) {
            stickSizeButton.setColor(.25f, .43f, .25f, 1);
        } else {
            stickSizeButton.setColor(Color.RED);
        }

        pauseInvButton = new GameButtons(emptyButtonTemplate, true, new Vector2().setZero());
        pauseInvButton.setSize(pauseInvButton.getWidth() * game.screenScale, pauseInvButton.getHeight() * game.screenScale);
        pauseInvButton.setOriginCenter();
        pauseInvButton.setPosition(stickSizeButton.getX(), soundButton.getY());
        if (game.prefs.getBoolean("PauseOnInventory")) {
            pauseInvButton.setColor(.25f, .43f, .25f, 1);
        } else {
            pauseInvButton.setColor(Color.RED);
        }

        soundEffectsRocker = new VolumeRocker(game.manager.get("data/VolumeSliderBack.png", Texture.class), game.manager.get("data/VolumeSliderFill.png", Texture.class), game.manager.get("data/VolumeRockerControl.png", Texture.class),
                new Vector2(Gdx.graphics.getWidth() / 2 - 450 * game.screenScale / 2, soundButton.getY() - 75 * game.screenScale * 1.5f), new Vector2(450 * game.screenScale, 75 * game.screenScale), game.prefs.getFloat("SfxVolumeLevel"));

        musicRocker = new VolumeRocker(game.manager.get("data/VolumeSliderBack.png", Texture.class), game.manager.get("data/VolumeSliderFill.png", Texture.class), game.manager.get("data/VolumeRockerControl.png", Texture.class),
                new Vector2(Gdx.graphics.getWidth() / 2 - 450 * game.screenScale / 2, soundButton.getY() - 75 * game.screenScale * 2 * 1.5f), new Vector2(450 * game.screenScale, 75 * game.screenScale), game.prefs.getFloat("MusicVolumeLevel"));

        movementSpeedRocker = new VolumeRocker(game.manager.get("data/VolumeSliderBack.png", Texture.class), game.manager.get("data/VolumeSliderFill.png", Texture.class), game.manager.get("data/VolumeRockerControl.png", Texture.class),
                new Vector2(Gdx.graphics.getWidth() / 2 - 450 * game.screenScale / 2, soundButton.getY() - 75 * game.screenScale * 3 * 1.5f), new Vector2(450 * game.screenScale, 75 * game.screenScale), movementRockerScale);


        backgroundSprite = new Sprite[2];
        for (int i = 0; i < backgroundSprite.length; i++) {
            backgroundSprite[i] = new Sprite(this.game.manager.get("data/SpaceSquare.png", Texture.class));
            if (gameScreen != null) {
                if (game.prefs.getBoolean("PixelSkin")) {
                    backgroundSprite[i] = new Sprite(this.game.manager.get("data/PixelSpace.png", Texture.class));
                } else if (game.prefs.getBoolean("NinjaSkin")) {
                    backgroundSprite[i] = new Sprite(this.game.manager.get("data/NinjaBackground1920.png", Texture.class));
                } else if (game.prefs.getBoolean("CatSkin")) {
                    backgroundSprite[i] = new Sprite(this.game.manager.get("data/CatBackground.png", Texture.class));
//                    game.blackSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
//                    game.blackSprite.setAlpha(.2f);
//                    game.blackSprite.setPosition(0, 0);
                } else if (game.prefs.getBoolean("PirateSkin")) {
                    backgroundSprite[i] = new Sprite(this.game.manager.get("data/JackyPirateFog.png", Texture.class));
                } else if(game.prefs.getBoolean("SoccerSkin")) {
                    backgroundSprite[i] = new Sprite(this.game.manager.get("data/SoccerField1920.png", Texture.class));
                } else if(game.prefs.getBoolean("SumoSkin")) {
                    backgroundSprite[i] = new Sprite(this.game.manager.get("data/SumoBackground.png", Texture.class));
                }
                if (game.prefs.getBoolean("PirateSkin") || game.prefs.getBoolean("NinjaSkin")) {
                    game.blackSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
                    game.blackSprite.setAlpha(.2f);
                    game.blackSprite.setPosition(0, 0);
                }
            }
            backgroundSprite[i].setSize(Gdx.graphics.getWidth(), Gdx.graphics.getWidth());
            if (game.prefs.getBoolean("SoccerSkin") || game.prefs.getBoolean("SumoSkin")) {
                if (game.prefs.getBoolean("16x9")) {
                    backgroundSprite[i].setPosition(0, -backgroundSprite[i].getHeight() / 4 + 50 * game.screenScale);
                } else if (game.prefs.getBoolean("16x10")) {
                    backgroundSprite[i].setPosition(0, -backgroundSprite[i].getHeight() / 4 + 120 * game.screenScale);
                } else if (game.prefs.getBoolean("4x3")) {
                    backgroundSprite[i].setPosition(0, -backgroundSprite[i].getHeight() / 4 + 240 * game.screenScale);
                } else if (game.prefs.getBoolean("5x3")) {
                    backgroundSprite[i].setPosition(0, -backgroundSprite[i].getHeight() / 4 + 100 * game.screenScale);
                } else if (game.prefs.getBoolean("3x2")) {
                    backgroundSprite[i].setPosition(0, -backgroundSprite[i].getHeight() / 4 + 160 * game.screenScale);
                }
            } else if (i == 0) {
                backgroundSprite[i].setPosition(0, 0);
            }
        }

        font = new BitmapFont(Gdx.files.internal("data/poop.fnt"), Gdx.files.internal("data/poop.png"), false);
        font.setColor(Color.WHITE);

        rockerActive = false;
        currentBackTouched = false;
        previousBackTouched = currentBackTouched;

        this.gameScreen = gameScreen;
    }

    @Override
    public void render(float delta) {

        //region ButtonUpdating
        currentBackTouched = Gdx.input.isKeyPressed(Input.Keys.BACK);
        vibrateButton.Update();
        soundButton.Update();
        pauseInvButton.Update();
//        controlTypeButton.Update();
        backButton.Update();
        if (gameScreen == null) {
            stickSizeButton.Update();
        }
        soundEffectsRocker.Update(musicRocker.Active(), movementSpeedRocker.Active());
        musicRocker.Update(soundEffectsRocker.Active(), movementSpeedRocker.Active());
        movementSpeedRocker.Update(soundEffectsRocker.Active(), musicRocker.Active());

        game.menuSong.setVolume(.55f * musicRocker.CurrentValue());

        if (soundEffectsRocker.Active() || musicRocker.Active() || movementSpeedRocker.Active()) {
            rockerActive = true;
        } else {
            rockerActive = false;
        }

        if (!rockerActive && !previousRockerActive) {
            if (vibrateButton.Tapped()) {
                game.prefs.putBoolean("Vibrate", !game.prefs.getBoolean("Vibrate"));
                game.prefs.flush();
                if (game.prefs.getBoolean("Vibrate")) {
                    vibrateButton.setColor(.25f, .43f, .25f, 1);
                    Gdx.input.vibrate(150);
                } else {
                    vibrateButton.setColor(Color.RED);
                }
            }

            if (soundButton.Tapped()) {
                game.prefs.putBoolean("Sound", !game.prefs.getBoolean("Sound"));
                game.prefs.flush();
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    soundButton.setColor(.25f, .43f, .25f, 1);
                } else {
                    game.menuSong.stop();
                    soundButton.setColor(Color.RED);
                }
            }

            if (pauseInvButton.Tapped()) {
                game.prefs.putBoolean("PauseOnInventory", !game.prefs.getBoolean("PauseOnInventory"));
                game.prefs.flush();
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                if (game.prefs.getBoolean("PauseOnInventory")) {
                    pauseInvButton.setColor(.25f, .43f, .25f, 1);
                } else {
                    pauseInvButton.setColor(Color.RED);
                }
            }

            if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE) || backButton.Tapped() || (!currentBackTouched && previousBackTouched)) {
                if (game.prefs.getBoolean("Vibrate")) {
                    Gdx.input.vibrate(150);
                }
                game.prefs.putFloat("SfxVolumeLevel", soundEffectsRocker.CurrentValue());
                game.prefs.putFloat("MusicVolumeLevel", musicRocker.CurrentValue());
                game.prefs.putFloat("MovementSpeed", -(movementSpeedRocker.CurrentValue() * 10) + 10 + 5);
                game.prefs.flush();
                if (game.prefs.getBoolean("Sound")) {
                    game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                }
                game.manager.unload("data/VolumeSliderBack.png");
                game.manager.unload("data/VolumeSliderFill.png");
                game.manager.unload("data/VolumeRockerControl.png");
                if (gameScreen == null) {
                    game.setScreen(new MenuScreen(game));
                } else {
                    game.setScreen(gameScreen);
                }
                this.dispose();
            }
            if (gameScreen == null) {
                if (stickSizeButton.Tapped()) {
                    game.prefs.putBoolean("SmallStick", !game.prefs.getBoolean("SmallStick"));
                    game.prefs.flush();
                    if (game.prefs.getBoolean("SmallStick")) {
                        stickSizeButton.setColor(.25f, .43f, .25f, 1);
                    } else {
                        stickSizeButton.setColor(Color.RED);
                    }
                    if (game.prefs.getBoolean("Vibrate")) {
                        Gdx.input.vibrate(150);
                    }
                    if (game.prefs.getBoolean("Sound")) {
                        game.MenuButtonSound.play(.7f * game.prefs.getFloat("SfxVolumeLevel"));
                    }
                }
            }
        }

        previousRockerActive = rockerActive;
        previousBackTouched = currentBackTouched;
        //endregion

        game.batch.begin();
        for (Sprite background : backgroundSprite) {
            background.draw(game.batch);
        }
        if (gameScreen != null && (game.prefs.getBoolean("NinjaSkin") || game.prefs.getBoolean("PirateSkin"))) {

            game.blackSprite.draw(game.batch);
        }
        backButton.draw(game.batch);
        vibrateButton.draw(game.batch);
        soundButton.draw(game.batch);
        pauseInvButton.draw(game.batch);
//        controlTypeButton.draw(game.batch);
        soundEffectsRocker.Draw(game.batch);
        musicRocker.Draw(game.batch);
        movementSpeedRocker.Draw(game.batch);
        if (gameScreen == null) {
            stickSizeButton.draw(game.batch);
        }
        TextDrawing();
        game.batch.end();
    }

    private void TextDrawing() {
        if (game.prefs.getBoolean("NinjaSkin")) {
            goodTimeFont.setColor(Color.WHITE);
        } else {
            goodTimeFont.setColor(Color.WHITE);
        }
        goodTimeFont.setScale(game.screenScale * .26f);
        if (game.prefs.getBoolean("Vibrate")) {
            goodTimeFont.draw(game.batch, "Vibrate: On", vibrateButton.getX() + 16.5f * game.screenScale, vibrateButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
        } else {
            goodTimeFont.draw(game.batch, "Vibrate: Off", vibrateButton.getX() + 11f * game.screenScale, vibrateButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
        }
        goodTimeFont.setScale(game.screenScale * .22f);
        if (game.prefs.getBoolean("PauseOnInventory")) {
            goodTimeFont.draw(game.batch, "Inv Pause: On", pauseInvButton.getX() + 22.5f * game.screenScale, pauseInvButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
        } else {
            goodTimeFont.draw(game.batch, "Inv Pause: Off", pauseInvButton.getX() + 13f * game.screenScale, pauseInvButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
        }
        goodTimeFont.setScale(game.screenScale * .26f);
        if (gameScreen == null) {
            if (game.prefs.getBoolean("SmallStick")) {
                goodTimeFont.draw(game.batch, "Small", stickSizeButton.getX() + stickSizeButton.getOriginX() * .55f, stickSizeButton.getY() + goodTimeFont.getLineHeight() + stickSizeButton.getOriginY() * .9f);
                goodTimeFont.draw(game.batch, "ShootStick", stickSizeButton.getX() + stickSizeButton.getOriginX() * .13f, stickSizeButton.getY() + goodTimeFont.getLineHeight() + stickSizeButton.getOriginY() * .1f);
            } else {
                goodTimeFont.draw(game.batch, "Large", stickSizeButton.getX() + stickSizeButton.getOriginX() * .55f, stickSizeButton.getY() + goodTimeFont.getLineHeight() + stickSizeButton.getOriginY() * .9f);
                goodTimeFont.draw(game.batch, "ShootStick", stickSizeButton.getX() + stickSizeButton.getOriginX() * .13f, stickSizeButton.getY() + goodTimeFont.getLineHeight() + stickSizeButton.getOriginY() * .1f);
            }
        }
        goodTimeFont.setScale(game.screenScale * .28f);
        if (game.prefs.getBoolean("Sound")) {
            goodTimeFont.draw(game.batch, "Sound: On", soundButton.getX() + 26f * game.screenScale, soundButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
        } else {
            goodTimeFont.draw(game.batch, "Sound: Off", soundButton.getX() + 17f * game.screenScale, soundButton.getY() + goodTimeFont.getLineHeight() + 31 * game.screenScale);
        }
        goodTimeFont.setScale(game.screenScale * .32f);
        goodTimeFont.draw(game.batch, "Sfx Vol Level:", soundEffectsRocker.GetRockerPosition().x - 516 * game.screenScale, soundEffectsRocker.GetRockerPosition().y + goodTimeFont.getLineHeight());
        goodTimeFont.draw(game.batch, "Music Vol Level:", musicRocker.GetRockerPosition().x - 582 * game.screenScale, musicRocker.GetRockerPosition().y + goodTimeFont.getLineHeight());
        goodTimeFont.draw(game.batch, "Movement Speed:", movementSpeedRocker.GetRockerPosition().x - 610 * game.screenScale, movementSpeedRocker.GetRockerPosition().y + goodTimeFont.getLineHeight());
        goodTimeFont.setScale(game.screenScale * .55f);
        goodTimeFont.draw(game.batch, "Back", backButton.getX() + 29 * game.screenScale, backButton.getY() + goodTimeFont.getLineHeight() - 5 * game.screenScale);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
