package com.AndroidGameAlpha.game.android;

import android.app.*;
import android.content.*;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.*;

import java.lang.String.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.ResourceBundle;

import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.widget.*;
import com.AndroidGameAlpha.game.ActionResolver;
import com.AndroidGameAlpha.game.IActivityRequestHandler;
import com.android.vending.billing.IInAppBillingService;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.AndroidGameAlpha.game.MyGdxGame;
import com.google.android.gms.ads.*;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;

import android.widget.FrameLayout.*;
import android.view.*;
import com.jirbo.adcolony.*;

public class AndroidLauncher extends AndroidApplication implements ActionResolver, IActivityRequestHandler, AdColonyAdListener, AdColonyAdAvailabilityListener, GameHelper.GameHelperListener{
    private GameHelper gameHelper;

    public IabHelper mHelper;

    ArrayList<String> skuList;
    Bundle querySkus = new Bundle();

    private static final String AD_UNIT_ID = "ca-app-pub-2849949798715754/7017312424";
//    private static final String intersertialAdID = "ca-app-pub-2849949798715754/8494045627";
    private static final String adColonyInterstitialZone = "vz1b17b6eccfd54c82ba";
    private static final String adColonyAppID = "app623094caab5b4493a1";

    protected AdView adView;
//    protected InterstitialAd interstitialAd;
    protected AdColonyVideoAd videoAd;
    public View gameView;

    boolean purchaseSuccess;
    boolean adClosed;
    boolean adsRestoredBool;

    private final int SHOW_ADS = 1;
    private final int HIDE_ADS = 0;

    protected Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case SHOW_ADS:
                {
                    adView.setVisibility(View.VISIBLE);
                    break;
                }
                case HIDE_ADS:
                {
                    adView.setVisibility(View.GONE);
                    break;
                }
            }
        }
    };

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useImmersiveMode = true;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
//        initialize(new MyGdxGame(this), config);
        View gameView = initializeForView(new MyGdxGame(this, this), config);
        adView = new AdView(this);

        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(AD_UNIT_ID);
        adView.loadAd(new AdRequest.Builder().build());
        adClosed = false;
//        adView.setVisibility();
//        interstitialAd = new InterstitialAd(this);
//        interstitialAd.setAdUnitId(intersertialAdID);
//        interstitialAd.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
////                Toast.makeText(getApplicationContext(), "Finished Loading AD", Toast.LENGTH_SHORT).show();
//                showOrLoadInterstital();
//            }
//
//            @Override
//            public void onAdClosed() {
////                Toast.makeText(getApplicationContext(), "Closed Interstitial", Toast.LENGTH_SHORT).show();
////                adClosed = true;
//            }
//        });

        RelativeLayout layout = new RelativeLayout(this);
        layout.addView(gameView);

        RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        adParams.addRule(RelativeLayout.CENTER_IN_PARENT);

        layout.addView(adView, adParams);

//        showAds(true);


        AdColony.configure(this, "version:1.0,store:google", adColonyAppID, adColonyInterstitialZone);
        AdColony.addAdAvailabilityListener(this);
        videoAd = new AdColonyVideoAd( adColonyInterstitialZone ).withListener(this);

        //FIX THIS
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoA1pIMcBmsQAmWhC1jJ6l0OvZnPiu8rrWmxsb4u4v8vs4kYCZwlBF7fb9p884pICrmegzXAl0jepZ9oZmR/vAP8kHN07aTgQAHFysg1DBjEfvzBy9I6mE6VmtyS85GVfvOutjNKJxzAKROf2wQDrBZ4hvoWLw/w5fgajBCPB7jbBY803x0TZbRKbMmw7r9DaXgeJyRcErXS1DzQqfvbrDl4hp8utYI6LMRt37aZsIICT4wlpJd9NnjVgU5c2SueQ8vYVTkyD8ra0ghww7YaWRHcfBexABwZC2KQCc5uroPcj4Sn5OM7apgfGou9hxByiE5n9cbB+8XV2zm3JElFvDwIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if(!result.isSuccess()) {
                    try {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Failed to connect billing", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (Exception e) {
                    }
                }

            }
        });

//        if(!AdColony.isTablet()) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }

        purchaseSuccess = false;

        setContentView(layout);
        if (gameHelper == null) {
            gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
            gameHelper.enableDebugLog(true);
        }

        skuList = new ArrayList<String>();
        skuList.add("one_dollar_coins");
        skuList.add("two_dollar_coins");
        skuList.add("three_dollar_coins");
        skuList.add("premium_version");

        adsRestoredBool = false;

        gameHelper.setup(this);
	}

    @Override
    public boolean mhelperSetupFinished() {
        return mHelper.mSetupDone;
    }

    @Override
    public boolean adsRestored() {
        boolean tempBool = adsRestoredBool;
        adsRestoredBool = false;
        return tempBool;
    }

    @Override
    public void restoreAds() {
        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    mHelper.queryInventoryAsync(mGotInventoryListenerRestoreAds);
                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    public boolean adWatchedSuccessfully() {
        boolean tempWatched = adClosed;
        adClosed = false;
        return tempWatched;
    }

    @Override
    public void queryInventory() {
        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                }
            });
        } catch (Exception e) {
        }
    }


    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if(result.isFailure()) {
                Log.d("inventory", "query failed");
            } else {
                for(int i = 0; i < skuList.size(); i++) {
                    if(inventory.hasPurchase(skuList.get(i))) {
                        if(i < 3) {
                            mHelper.consumeAsync(inventory.getPurchase(skuList.get(i)), mConsumeFinishedListener);
                        } else {
                            purchaseSuccess = true;
                        }
                    }
                }
            }
        }
    };

    IabHelper.QueryInventoryFinishedListener mGotInventoryListenerRestoreAds = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if(result.isFailure()) {
                Log.d("inventory", "query failed");
            } else {
                mHelper.consumeAsync(inventory.getPurchase(skuList.get(3)), mConsumeFinishedListenerRestoreAds);
            }
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase)
        {
            if (result.isFailure()) {
//                purchaseSuccess = false;
                return;
            }
            else if (skuList.contains(purchase.getSku())) {
                // consume the gas and update the UI
                try {
                    runOnUiThread(new Runnable() {
                        public void run() {
//                            mHelper.queryInventoryAsync(mGotInventoryListener);
//                            Toast.makeText(getApplicationContext(), "Purchase successful", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                }

//                purchaseSuccess = true;
                if(purchase.getSku().equals(skuList.get(0)) || purchase.getSku().equals(skuList.get(1)) || purchase.getSku().equals(skuList.get(2))) {
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                    try {
                        runOnUiThread(new Runnable() {
                            public void run() {
//                            mHelper.queryInventoryAsync(mGotInventoryListener);
//                                Toast.makeText(getApplicationContext(), "Sent Consume Request", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (Exception e) {
                    }
                }
                return;
            }
//                else if (purchase.getSku().equals(SKU_PREMIUM)) {
//                    // give user access to premium content and update the UI
//                }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListenerRestoreAds =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(final Purchase purchase, IabResult result) {
                    if (result.isSuccess()) {
                        // provision the in-app purchase to the user
                        // (for example, credit 50 gold coins to player's character)
                        try {
                            runOnUiThread(new Runnable() {
                                public void run() {
//                                    Toast.makeText(getApplicationContext(), "Succeeded Consuming", Toast.LENGTH_SHORT).show();
                                    adsRestoredBool = true;
                                }
                            });
                        } catch (Exception e) {
                        }
                    }
                    else {
                        // handle error
                        try {
                            runOnUiThread(new Runnable() {
                                public void run() {
//                                    Toast.makeText(getApplicationContext(), "Failed Consuming", Toast.LENGTH_SHORT).show();
                                    adsRestoredBool = false;
                                }
                            });
                        } catch (Exception e) {
                        }
                    }
                }
            };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(final Purchase purchase, IabResult result) {
                    if (result.isSuccess()) {
                        // provision the in-app purchase to the user
                        // (for example, credit 50 gold coins to player's character)
                        try {
                            runOnUiThread(new Runnable() {
                                public void run() {
//                                    Toast.makeText(getApplicationContext(), "Succeeded Consuming", Toast.LENGTH_SHORT).show();
                                    purchaseSuccess = true;
                                }
                            });
                        } catch (Exception e) {
                        }
                    }
                    else {
                        // handle error
                        try {
                            runOnUiThread(new Runnable() {
                                public void run() {
//                                    Toast.makeText(getApplicationContext(), "Failed Consuming", Toast.LENGTH_SHORT).show();
                                    purchaseSuccess = false;
                                }
                            });
                        } catch (Exception e) {
                        }
                    }
                }
            };


    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
//        Log.d(TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }

    @Override
    public boolean returnPurchaseSuccess()
    {
        boolean tempSuccess = purchaseSuccess;
        purchaseSuccess = false;
        return tempSuccess;
    }

    @Override
    public void purchaseCoinItem(int index) {
//        try {
//            Bundle buyIntentBundle = mHelper.mService.getBuyIntent(3, getPackageName(), skuList.get(index), "inapp", "akw3dk30gpvx,er031jdp-2mvzzpe32104jf");
//            PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
//            try {
//                startIntentSenderForResult(pendingIntent.getIntentSender(), 1001, new Intent(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0));
//            } catch (IntentSender.SendIntentException e) {
//                e.printStackTrace();
//                Toast.makeText(getApplicationContext(), "Fail Intent", Toast.LENGTH_SHORT).show();
//            }
//        } catch (RemoteException e) {
//            e.printStackTrace();
//            Toast.makeText(getApplicationContext(), "Fail Initial", Toast.LENGTH_SHORT).show();
//        }
        mHelper.launchPurchaseFlow(this, skuList.get(index), 1001, mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJC4tYgbqw");

    }

    @Override
    public void showOrLoadInterstital() {
        try {
            runOnUiThread(new Runnable() {
                public void run() {
//                    if (interstitialAd.isLoaded()) {
//                        interstitialAd.show();
//                        Toast.makeText(getApplicationContext(), "Showing Interstitial", Toast.LENGTH_SHORT).show();
////                         }
//                    else {
//                        AdRequest interstitialRequest = new AdRequest.Builder().build();
////                        interstitialAd.loadAd(interstitialRequest);
////                        Toast.makeText(getApplicationContext(), "Loading Ad", Toast.LENGTH_SHORT).show();
//                        }
                    }
                });
            } catch (Exception e) {
            }
        }

    @Override
    public void showAds(boolean show) {
        handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);
    }

    @Override
    public void onStart(){
        super.onStart();
        gameHelper.onStart(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        gameHelper.onStop();
    }

    public void onPause()
    {
        super.onPause();
        AdColony.pause();
    }

    public void onResume()
    {
        super.onResume();
        AdColony.resume( this );
    }

    @Override
    public void onActivityResult(int request, int response, Intent data) {
        if(!mHelper.handleActivityResult(request, response, data)) {
            Log.d("Handle", "cleared the launch flow");
            super.onActivityResult(request, response, data);
            gameHelper.onActivityResult(request, response, data);
        }

    }

    @Override
    public boolean getSignedInGPGS() {
        return gameHelper.isSignedIn();
    }

    @Override
    public void loginGPGS() {
        try {
            runOnUiThread(new Runnable(){
                public void run() {
                    gameHelper.beginUserInitiatedSignIn();
                }
            });
        } catch (final Exception ex) {
        }
    }
//    "CgkImf-Mg5YTEAIQAg"
    @Override
    public void submitScoreGPGS(String leaderboard, int score) {

        Games.Leaderboards.submitScore(gameHelper.getApiClient(), leaderboard, score);
    }

    @Override
    public void unlockAchievementGPGS(String achievementId) {
        Games.Achievements.unlock(gameHelper.getApiClient(), achievementId);
    }

    @Override
    public void getLeaderboardGPGS(String leaderboard) {
        if (gameHelper.isSignedIn()) {
            startActivityForResult(Games.Leaderboards.getLeaderboardIntent(gameHelper.getApiClient(), leaderboard), 100);
        }
        else if (!gameHelper.isConnecting()) {
            loginGPGS();
        }
    }

    @Override
    public void getAchievementsGPGS() {
        if (gameHelper.isSignedIn()) {
            startActivityForResult(Games.Achievements.getAchievementsIntent(gameHelper.getApiClient()), 101);
        }
        else if (!gameHelper.isConnecting()) {
            loginGPGS();
        }
    }

    @Override
    public void adColonyShowReady() {
//            adColonyShowInterstitial();
//         videoAd = new AdColonyVideoAd( adColonyInterstitialZone ).withListener(this);
            try {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (videoAd.isReady()) {
//                            Toast.makeText(getApplicationContext(), "Video Ready", Toast.LENGTH_SHORT).show();
//                            videoAd.show();
//                            adColonyShowInterstitial();
                        }
                        else {
//                            Toast.makeText(getApplicationContext(), "Not Ready", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } catch (Exception e) {
            }
    }

    @Override
    public void adColonyShowInterstitial() {
        videoAd = new AdColonyVideoAd( adColonyInterstitialZone ).withListener(this);
//        videoAd.show();
    }

    @Override
    public boolean adColonyShowReadyInterstitial() {
        if(videoAd.isReady()) {
            videoAd.show();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onSignInFailed() {
    }

    @Override
    public void onSignInSucceeded() {
    }

    @Override
    public void onAdColonyAdAvailabilityChange(boolean b, String s) {
//        videoAd = new AdColonyVideoAd( adColonyInterstitialZone ).withListener(this);
        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    if (videoAd.isReady()) {
//                        Toast.makeText(getApplicationContext(), "Video Ready", Toast.LENGTH_SHORT).show();
//                        videoAd.show();
//                            adColonyShowInterstitial();
                    }
                    else {
//                        Toast.makeText(getApplicationContext(), "Not Ready", Toast.LENGTH_SHORT).show();
                    }
                    if(videoAd.noFill()) {
//                        Toast.makeText(getApplicationContext(), "No Fill", Toast.LENGTH_SHORT).show();
                    }
                    if(videoAd.notShown()) {
//                        Toast.makeText(getApplicationContext(), "Not Shown", Toast.LENGTH_SHORT).show();
                    }
                    if(videoAd.canceled()) {
//                        Toast.makeText(getApplicationContext(), "Canceled", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } catch (Exception e) {
        }
    }



    @Override
    public void onAdColonyAdAttemptFinished(AdColonyAd adColonyAd) {
        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    if(videoAd.notShown()) {
//                        Toast.makeText(getApplicationContext(), "Not Shown", Toast.LENGTH_SHORT).show();
                    } else if(videoAd.shown()) {
//                        Toast.makeText(getApplicationContext(), "Shown", Toast.LENGTH_SHORT).show();
                        adClosed = true;
                    }
//                        adColonyShowInterstitial();

                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    public void onAdColonyAdStarted(AdColonyAd adColonyAd) {
        try {
            runOnUiThread(new Runnable() {
                public void run() {
//                    Toast.makeText(getApplicationContext(), "Started", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (mService != null) {
//            unbindService(mServiceConnection);
//        }

        if(mHelper != null) {
            mHelper.dispose();
        }
        mHelper = null;
    }
}
