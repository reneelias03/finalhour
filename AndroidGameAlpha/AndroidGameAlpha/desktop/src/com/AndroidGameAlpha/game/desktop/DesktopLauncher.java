package com.AndroidGameAlpha.game.desktop;

import com.AndroidGameAlpha.game.ActionResolver;
import com.AndroidGameAlpha.game.IActivityRequestHandler;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.AndroidGameAlpha.game.MyGdxGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new MyGdxGame(new ActionResolver() {
            @Override
            public boolean getSignedInGPGS() {
                return false;
            }

            @Override
            public void loginGPGS() {

            }

            @Override
            public void submitScoreGPGS(String leaderboard, int score) {

            }

            @Override
            public void unlockAchievementGPGS(String achievementId) {

            }

            @Override
            public void getLeaderboardGPGS(String leaderboard) {

            }

            @Override
            public void getAchievementsGPGS() {

            }

            @Override
            public void showOrLoadInterstital() {

            }

            @Override
            public void adColonyShowInterstitial() {

            }

            @Override
            public boolean adColonyShowReadyInterstitial() {
                return false;
            }

            @Override
            public void adColonyShowReady() {

            }

            @Override
            public void purchaseCoinItem(int index) {

            }

            @Override
            public boolean returnPurchaseSuccess() {
                return false;
            }

            @Override
            public void queryInventory() {

            }

            @Override
            public boolean adWatchedSuccessfully() {
                return false;
            }

            @Override
            public void restoreAds() {

            }

            @Override
            public boolean adsRestored() {
                return false;
            }

            @Override
            public boolean mhelperSetupFinished() {
                return false;
            }
        }, new IActivityRequestHandler() {


            @Override
            public void showAds(boolean show) {

            }
        }), config);
	}
}
